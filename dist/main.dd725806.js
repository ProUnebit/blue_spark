// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"..\\node_modules\\babel-runtime\\helpers\\classCallCheck.js":[function(require,module,exports) {
"use strict";

exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};
},{}],"..\\node_modules\\core-js\\library\\modules\\_global.js":[function(require,module,exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef

},{}],"..\\node_modules\\core-js\\library\\modules\\_core.js":[function(require,module,exports) {
var core = module.exports = { version: '2.5.7' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef

},{}],"..\\node_modules\\core-js\\library\\modules\\_a-function.js":[function(require,module,exports) {
module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};

},{}],"..\\node_modules\\core-js\\library\\modules\\_ctx.js":[function(require,module,exports) {
// optional / simple context binding
var aFunction = require('./_a-function');
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};

},{"./_a-function":"..\\node_modules\\core-js\\library\\modules\\_a-function.js"}],"..\\node_modules\\core-js\\library\\modules\\_is-object.js":[function(require,module,exports) {
module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};

},{}],"..\\node_modules\\core-js\\library\\modules\\_an-object.js":[function(require,module,exports) {
var isObject = require('./_is-object');
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};

},{"./_is-object":"..\\node_modules\\core-js\\library\\modules\\_is-object.js"}],"..\\node_modules\\core-js\\library\\modules\\_fails.js":[function(require,module,exports) {
module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};

},{}],"..\\node_modules\\core-js\\library\\modules\\_descriptors.js":[function(require,module,exports) {
// Thank's IE8 for his funny defineProperty
module.exports = !require('./_fails')(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});

},{"./_fails":"..\\node_modules\\core-js\\library\\modules\\_fails.js"}],"..\\node_modules\\core-js\\library\\modules\\_dom-create.js":[function(require,module,exports) {
var isObject = require('./_is-object');
var document = require('./_global').document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};

},{"./_is-object":"..\\node_modules\\core-js\\library\\modules\\_is-object.js","./_global":"..\\node_modules\\core-js\\library\\modules\\_global.js"}],"..\\node_modules\\core-js\\library\\modules\\_ie8-dom-define.js":[function(require,module,exports) {
module.exports = !require('./_descriptors') && !require('./_fails')(function () {
  return Object.defineProperty(require('./_dom-create')('div'), 'a', { get: function () { return 7; } }).a != 7;
});

},{"./_descriptors":"..\\node_modules\\core-js\\library\\modules\\_descriptors.js","./_fails":"..\\node_modules\\core-js\\library\\modules\\_fails.js","./_dom-create":"..\\node_modules\\core-js\\library\\modules\\_dom-create.js"}],"..\\node_modules\\core-js\\library\\modules\\_to-primitive.js":[function(require,module,exports) {
// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = require('./_is-object');
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};

},{"./_is-object":"..\\node_modules\\core-js\\library\\modules\\_is-object.js"}],"..\\node_modules\\core-js\\library\\modules\\_object-dp.js":[function(require,module,exports) {
var anObject = require('./_an-object');
var IE8_DOM_DEFINE = require('./_ie8-dom-define');
var toPrimitive = require('./_to-primitive');
var dP = Object.defineProperty;

exports.f = require('./_descriptors') ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};

},{"./_an-object":"..\\node_modules\\core-js\\library\\modules\\_an-object.js","./_ie8-dom-define":"..\\node_modules\\core-js\\library\\modules\\_ie8-dom-define.js","./_to-primitive":"..\\node_modules\\core-js\\library\\modules\\_to-primitive.js","./_descriptors":"..\\node_modules\\core-js\\library\\modules\\_descriptors.js"}],"..\\node_modules\\core-js\\library\\modules\\_property-desc.js":[function(require,module,exports) {
module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};

},{}],"..\\node_modules\\core-js\\library\\modules\\_hide.js":[function(require,module,exports) {
var dP = require('./_object-dp');
var createDesc = require('./_property-desc');
module.exports = require('./_descriptors') ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};

},{"./_object-dp":"..\\node_modules\\core-js\\library\\modules\\_object-dp.js","./_property-desc":"..\\node_modules\\core-js\\library\\modules\\_property-desc.js","./_descriptors":"..\\node_modules\\core-js\\library\\modules\\_descriptors.js"}],"..\\node_modules\\core-js\\library\\modules\\_has.js":[function(require,module,exports) {
var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};

},{}],"..\\node_modules\\core-js\\library\\modules\\_export.js":[function(require,module,exports) {

var global = require('./_global');
var core = require('./_core');
var ctx = require('./_ctx');
var hide = require('./_hide');
var has = require('./_has');
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;

},{"./_global":"..\\node_modules\\core-js\\library\\modules\\_global.js","./_core":"..\\node_modules\\core-js\\library\\modules\\_core.js","./_ctx":"..\\node_modules\\core-js\\library\\modules\\_ctx.js","./_hide":"..\\node_modules\\core-js\\library\\modules\\_hide.js","./_has":"..\\node_modules\\core-js\\library\\modules\\_has.js"}],"..\\node_modules\\core-js\\library\\modules\\es6.object.define-property.js":[function(require,module,exports) {
var $export = require('./_export');
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !require('./_descriptors'), 'Object', { defineProperty: require('./_object-dp').f });

},{"./_export":"..\\node_modules\\core-js\\library\\modules\\_export.js","./_descriptors":"..\\node_modules\\core-js\\library\\modules\\_descriptors.js","./_object-dp":"..\\node_modules\\core-js\\library\\modules\\_object-dp.js"}],"..\\node_modules\\core-js\\library\\fn\\object\\define-property.js":[function(require,module,exports) {
require('../../modules/es6.object.define-property');
var $Object = require('../../modules/_core').Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};

},{"../../modules/es6.object.define-property":"..\\node_modules\\core-js\\library\\modules\\es6.object.define-property.js","../../modules/_core":"..\\node_modules\\core-js\\library\\modules\\_core.js"}],"..\\node_modules\\babel-runtime\\core-js\\object\\define-property.js":[function(require,module,exports) {
module.exports = { "default": require("core-js/library/fn/object/define-property"), __esModule: true };
},{"core-js/library/fn/object/define-property":"..\\node_modules\\core-js\\library\\fn\\object\\define-property.js"}],"..\\node_modules\\babel-runtime\\helpers\\createClass.js":[function(require,module,exports) {
"use strict";

exports.__esModule = true;

var _defineProperty = require("../core-js/object/define-property");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();
},{"../core-js/object/define-property":"..\\node_modules\\babel-runtime\\core-js\\object\\define-property.js"}],"..\\node_modules\\form-serialize\\index.js":[function(require,module,exports) {
// get successful control from form and assemble into object
// http://www.w3.org/TR/html401/interact/forms.html#h-17.13.2

// types which indicate a submit action and are not successful controls
// these will be ignored
var k_r_submitter = /^(?:submit|button|image|reset|file)$/i;

// node names which could be successful controls
var k_r_success_contrls = /^(?:input|select|textarea|keygen)/i;

// Matches bracket notation.
var brackets = /(\[[^\[\]]*\])/g;

// serializes form fields
// @param form MUST be an HTMLForm element
// @param options is an optional argument to configure the serialization. Default output
// with no options specified is a url encoded string
//    - hash: [true | false] Configure the output type. If true, the output will
//    be a js object.
//    - serializer: [function] Optional serializer function to override the default one.
//    The function takes 3 arguments (result, key, value) and should return new result
//    hash and url encoded str serializers are provided with this module
//    - disabled: [true | false]. If true serialize disabled fields.
//    - empty: [true | false]. If true serialize empty fields
function serialize(form, options) {
    if (typeof options != 'object') {
        options = { hash: !!options };
    }
    else if (options.hash === undefined) {
        options.hash = true;
    }

    var result = (options.hash) ? {} : '';
    var serializer = options.serializer || ((options.hash) ? hash_serializer : str_serialize);

    var elements = form && form.elements ? form.elements : [];

    //Object store each radio and set if it's empty or not
    var radio_store = Object.create(null);

    for (var i=0 ; i<elements.length ; ++i) {
        var element = elements[i];

        // ingore disabled fields
        if ((!options.disabled && element.disabled) || !element.name) {
            continue;
        }
        // ignore anyhting that is not considered a success field
        if (!k_r_success_contrls.test(element.nodeName) ||
            k_r_submitter.test(element.type)) {
            continue;
        }

        var key = element.name;
        var val = element.value;

        // we can't just use element.value for checkboxes cause some browsers lie to us
        // they say "on" for value when the box isn't checked
        if ((element.type === 'checkbox' || element.type === 'radio') && !element.checked) {
            val = undefined;
        }

        // If we want empty elements
        if (options.empty) {
            // for checkbox
            if (element.type === 'checkbox' && !element.checked) {
                val = '';
            }

            // for radio
            if (element.type === 'radio') {
                if (!radio_store[element.name] && !element.checked) {
                    radio_store[element.name] = false;
                }
                else if (element.checked) {
                    radio_store[element.name] = true;
                }
            }

            // if options empty is true, continue only if its radio
            if (val == undefined && element.type == 'radio') {
                continue;
            }
        }
        else {
            // value-less fields are ignored unless options.empty is true
            if (!val) {
                continue;
            }
        }

        // multi select boxes
        if (element.type === 'select-multiple') {
            val = [];

            var selectOptions = element.options;
            var isSelectedOptions = false;
            for (var j=0 ; j<selectOptions.length ; ++j) {
                var option = selectOptions[j];
                var allowedEmpty = options.empty && !option.value;
                var hasValue = (option.value || allowedEmpty);
                if (option.selected && hasValue) {
                    isSelectedOptions = true;

                    // If using a hash serializer be sure to add the
                    // correct notation for an array in the multi-select
                    // context. Here the name attribute on the select element
                    // might be missing the trailing bracket pair. Both names
                    // "foo" and "foo[]" should be arrays.
                    if (options.hash && key.slice(key.length - 2) !== '[]') {
                        result = serializer(result, key + '[]', option.value);
                    }
                    else {
                        result = serializer(result, key, option.value);
                    }
                }
            }

            // Serialize if no selected options and options.empty is true
            if (!isSelectedOptions && options.empty) {
                result = serializer(result, key, '');
            }

            continue;
        }

        result = serializer(result, key, val);
    }

    // Check for all empty radio buttons and serialize them with key=""
    if (options.empty) {
        for (var key in radio_store) {
            if (!radio_store[key]) {
                result = serializer(result, key, '');
            }
        }
    }

    return result;
}

function parse_keys(string) {
    var keys = [];
    var prefix = /^([^\[\]]*)/;
    var children = new RegExp(brackets);
    var match = prefix.exec(string);

    if (match[1]) {
        keys.push(match[1]);
    }

    while ((match = children.exec(string)) !== null) {
        keys.push(match[1]);
    }

    return keys;
}

function hash_assign(result, keys, value) {
    if (keys.length === 0) {
        result = value;
        return result;
    }

    var key = keys.shift();
    var between = key.match(/^\[(.+?)\]$/);

    if (key === '[]') {
        result = result || [];

        if (Array.isArray(result)) {
            result.push(hash_assign(null, keys, value));
        }
        else {
            // This might be the result of bad name attributes like "[][foo]",
            // in this case the original `result` object will already be
            // assigned to an object literal. Rather than coerce the object to
            // an array, or cause an exception the attribute "_values" is
            // assigned as an array.
            result._values = result._values || [];
            result._values.push(hash_assign(null, keys, value));
        }

        return result;
    }

    // Key is an attribute name and can be assigned directly.
    if (!between) {
        result[key] = hash_assign(result[key], keys, value);
    }
    else {
        var string = between[1];
        // +var converts the variable into a number
        // better than parseInt because it doesn't truncate away trailing
        // letters and actually fails if whole thing is not a number
        var index = +string;

        // If the characters between the brackets is not a number it is an
        // attribute name and can be assigned directly.
        if (isNaN(index)) {
            result = result || {};
            result[string] = hash_assign(result[string], keys, value);
        }
        else {
            result = result || [];
            result[index] = hash_assign(result[index], keys, value);
        }
    }

    return result;
}

// Object/hash encoding serializer.
function hash_serializer(result, key, value) {
    var matches = key.match(brackets);

    // Has brackets? Use the recursive assignment function to walk the keys,
    // construct any missing objects in the result tree and make the assignment
    // at the end of the chain.
    if (matches) {
        var keys = parse_keys(key);
        hash_assign(result, keys, value);
    }
    else {
        // Non bracket notation can make assignments directly.
        var existing = result[key];

        // If the value has been assigned already (for instance when a radio and
        // a checkbox have the same name attribute) convert the previous value
        // into an array before pushing into it.
        //
        // NOTE: If this requirement were removed all hash creation and
        // assignment could go through `hash_assign`.
        if (existing) {
            if (!Array.isArray(existing)) {
                result[key] = [ existing ];
            }

            result[key].push(value);
        }
        else {
            result[key] = value;
        }
    }

    return result;
}

// urlform encoding serializer
function str_serialize(result, key, value) {
    // encode newlines as \r\n cause the html spec says so
    value = value.replace(/(\r)?\n/g, '\r\n');
    value = encodeURIComponent(value);

    // spaces should be '+' rather than '%20'.
    value = value.replace(/%20/g, '+');
    return result + (result ? '&' : '') + encodeURIComponent(key) + '=' + value;
}

module.exports = serialize;

},{}],"..\\node_modules\\imask\\dist\\imask.js":[function(require,module,exports) {
var define;
var global = arguments[3];
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global.IMask = factory());
}(this, (function () { 'use strict';

  // 7.2.1 RequireObjectCoercible(argument)
  var _defined = function (it) {
    if (it == undefined) throw TypeError("Can't call method on  " + it);
    return it;
  };

  // 7.1.13 ToObject(argument)

  var _toObject = function (it) {
    return Object(_defined(it));
  };

  var hasOwnProperty = {}.hasOwnProperty;
  var _has = function (it, key) {
    return hasOwnProperty.call(it, key);
  };

  var toString = {}.toString;

  var _cof = function (it) {
    return toString.call(it).slice(8, -1);
  };

  // fallback for non-array-like ES3 and non-enumerable old V8 strings

  // eslint-disable-next-line no-prototype-builtins
  var _iobject = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
    return _cof(it) == 'String' ? it.split('') : Object(it);
  };

  // to indexed object, toObject with fallback for non-array-like ES3 strings


  var _toIobject = function (it) {
    return _iobject(_defined(it));
  };

  // 7.1.4 ToInteger
  var ceil = Math.ceil;
  var floor = Math.floor;
  var _toInteger = function (it) {
    return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
  };

  // 7.1.15 ToLength

  var min = Math.min;
  var _toLength = function (it) {
    return it > 0 ? min(_toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
  };

  var max = Math.max;
  var min$1 = Math.min;
  var _toAbsoluteIndex = function (index, length) {
    index = _toInteger(index);
    return index < 0 ? max(index + length, 0) : min$1(index, length);
  };

  // false -> Array#indexOf
  // true  -> Array#includes



  var _arrayIncludes = function (IS_INCLUDES) {
    return function ($this, el, fromIndex) {
      var O = _toIobject($this);
      var length = _toLength(O.length);
      var index = _toAbsoluteIndex(fromIndex, length);
      var value;
      // Array#includes uses SameValueZero equality algorithm
      // eslint-disable-next-line no-self-compare
      if (IS_INCLUDES && el != el) while (length > index) {
        value = O[index++];
        // eslint-disable-next-line no-self-compare
        if (value != value) return true;
      // Array#indexOf ignores holes, Array#includes - not
      } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
        if (O[index] === el) return IS_INCLUDES || index || 0;
      } return !IS_INCLUDES && -1;
    };
  };

  function createCommonjsModule(fn, module) {
  	return module = { exports: {} }, fn(module, module.exports), module.exports;
  }

  var _global = createCommonjsModule(function (module) {
  // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
  var global = module.exports = typeof window != 'undefined' && window.Math == Math
    ? window : typeof self != 'undefined' && self.Math == Math ? self
    // eslint-disable-next-line no-new-func
    : Function('return this')();
  if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef
  });

  var SHARED = '__core-js_shared__';
  var store = _global[SHARED] || (_global[SHARED] = {});
  var _shared = function (key) {
    return store[key] || (store[key] = {});
  };

  var id = 0;
  var px = Math.random();
  var _uid = function (key) {
    return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
  };

  var shared = _shared('keys');

  var _sharedKey = function (key) {
    return shared[key] || (shared[key] = _uid(key));
  };

  var arrayIndexOf = _arrayIncludes(false);
  var IE_PROTO = _sharedKey('IE_PROTO');

  var _objectKeysInternal = function (object, names) {
    var O = _toIobject(object);
    var i = 0;
    var result = [];
    var key;
    for (key in O) if (key != IE_PROTO) _has(O, key) && result.push(key);
    // Don't enum bug & hidden keys
    while (names.length > i) if (_has(O, key = names[i++])) {
      ~arrayIndexOf(result, key) || result.push(key);
    }
    return result;
  };

  // IE 8- don't enum bug keys
  var _enumBugKeys = (
    'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
  ).split(',');

  // 19.1.2.14 / 15.2.3.14 Object.keys(O)



  var _objectKeys = Object.keys || function keys(O) {
    return _objectKeysInternal(O, _enumBugKeys);
  };

  var _core = createCommonjsModule(function (module) {
  var core = module.exports = { version: '2.5.5' };
  if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef
  });
  var _core_1 = _core.version;

  var _isObject = function (it) {
    return typeof it === 'object' ? it !== null : typeof it === 'function';
  };

  var _anObject = function (it) {
    if (!_isObject(it)) throw TypeError(it + ' is not an object!');
    return it;
  };

  var _fails = function (exec) {
    try {
      return !!exec();
    } catch (e) {
      return true;
    }
  };

  // Thank's IE8 for his funny defineProperty
  var _descriptors = !_fails(function () {
    return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
  });

  var document$1 = _global.document;
  // typeof document.createElement is 'object' in old IE
  var is = _isObject(document$1) && _isObject(document$1.createElement);
  var _domCreate = function (it) {
    return is ? document$1.createElement(it) : {};
  };

  var _ie8DomDefine = !_descriptors && !_fails(function () {
    return Object.defineProperty(_domCreate('div'), 'a', { get: function () { return 7; } }).a != 7;
  });

  // 7.1.1 ToPrimitive(input [, PreferredType])

  // instead of the ES6 spec version, we didn't implement @@toPrimitive case
  // and the second argument - flag - preferred type is a string
  var _toPrimitive = function (it, S) {
    if (!_isObject(it)) return it;
    var fn, val;
    if (S && typeof (fn = it.toString) == 'function' && !_isObject(val = fn.call(it))) return val;
    if (typeof (fn = it.valueOf) == 'function' && !_isObject(val = fn.call(it))) return val;
    if (!S && typeof (fn = it.toString) == 'function' && !_isObject(val = fn.call(it))) return val;
    throw TypeError("Can't convert object to primitive value");
  };

  var dP = Object.defineProperty;

  var f = _descriptors ? Object.defineProperty : function defineProperty(O, P, Attributes) {
    _anObject(O);
    P = _toPrimitive(P, true);
    _anObject(Attributes);
    if (_ie8DomDefine) try {
      return dP(O, P, Attributes);
    } catch (e) { /* empty */ }
    if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
    if ('value' in Attributes) O[P] = Attributes.value;
    return O;
  };

  var _objectDp = {
  	f: f
  };

  var _propertyDesc = function (bitmap, value) {
    return {
      enumerable: !(bitmap & 1),
      configurable: !(bitmap & 2),
      writable: !(bitmap & 4),
      value: value
    };
  };

  var _hide = _descriptors ? function (object, key, value) {
    return _objectDp.f(object, key, _propertyDesc(1, value));
  } : function (object, key, value) {
    object[key] = value;
    return object;
  };

  var _redefine = createCommonjsModule(function (module) {
  var SRC = _uid('src');
  var TO_STRING = 'toString';
  var $toString = Function[TO_STRING];
  var TPL = ('' + $toString).split(TO_STRING);

  _core.inspectSource = function (it) {
    return $toString.call(it);
  };

  (module.exports = function (O, key, val, safe) {
    var isFunction = typeof val == 'function';
    if (isFunction) _has(val, 'name') || _hide(val, 'name', key);
    if (O[key] === val) return;
    if (isFunction) _has(val, SRC) || _hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
    if (O === _global) {
      O[key] = val;
    } else if (!safe) {
      delete O[key];
      _hide(O, key, val);
    } else if (O[key]) {
      O[key] = val;
    } else {
      _hide(O, key, val);
    }
  // add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
  })(Function.prototype, TO_STRING, function toString() {
    return typeof this == 'function' && this[SRC] || $toString.call(this);
  });
  });

  var _aFunction = function (it) {
    if (typeof it != 'function') throw TypeError(it + ' is not a function!');
    return it;
  };

  // optional / simple context binding

  var _ctx = function (fn, that, length) {
    _aFunction(fn);
    if (that === undefined) return fn;
    switch (length) {
      case 1: return function (a) {
        return fn.call(that, a);
      };
      case 2: return function (a, b) {
        return fn.call(that, a, b);
      };
      case 3: return function (a, b, c) {
        return fn.call(that, a, b, c);
      };
    }
    return function (/* ...args */) {
      return fn.apply(that, arguments);
    };
  };

  var PROTOTYPE = 'prototype';

  var $export = function (type, name, source) {
    var IS_FORCED = type & $export.F;
    var IS_GLOBAL = type & $export.G;
    var IS_STATIC = type & $export.S;
    var IS_PROTO = type & $export.P;
    var IS_BIND = type & $export.B;
    var target = IS_GLOBAL ? _global : IS_STATIC ? _global[name] || (_global[name] = {}) : (_global[name] || {})[PROTOTYPE];
    var exports = IS_GLOBAL ? _core : _core[name] || (_core[name] = {});
    var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
    var key, own, out, exp;
    if (IS_GLOBAL) source = name;
    for (key in source) {
      // contains in native
      own = !IS_FORCED && target && target[key] !== undefined;
      // export native or passed
      out = (own ? target : source)[key];
      // bind timers to global for call from export context
      exp = IS_BIND && own ? _ctx(out, _global) : IS_PROTO && typeof out == 'function' ? _ctx(Function.call, out) : out;
      // extend global
      if (target) _redefine(target, key, out, type & $export.U);
      // export
      if (exports[key] != out) _hide(exports, key, exp);
      if (IS_PROTO && expProto[key] != out) expProto[key] = out;
    }
  };
  _global.core = _core;
  // type bitmap
  $export.F = 1;   // forced
  $export.G = 2;   // global
  $export.S = 4;   // static
  $export.P = 8;   // proto
  $export.B = 16;  // bind
  $export.W = 32;  // wrap
  $export.U = 64;  // safe
  $export.R = 128; // real proto method for `library`
  var _export = $export;

  // most Object methods by ES6 should accept primitives



  var _objectSap = function (KEY, exec) {
    var fn = (_core.Object || {})[KEY] || Object[KEY];
    var exp = {};
    exp[KEY] = exec(fn);
    _export(_export.S + _export.F * _fails(function () { fn(1); }), 'Object', exp);
  };

  // 19.1.2.14 Object.keys(O)



  _objectSap('keys', function () {
    return function keys(it) {
      return _objectKeys(_toObject(it));
    };
  });

  var keys = _core.Object.keys;

  var _stringRepeat = function repeat(count) {
    var str = String(_defined(this));
    var res = '';
    var n = _toInteger(count);
    if (n < 0 || n == Infinity) throw RangeError("Count can't be negative");
    for (;n > 0; (n >>>= 1) && (str += str)) if (n & 1) res += str;
    return res;
  };

  _export(_export.P, 'String', {
    // 21.1.3.13 String.prototype.repeat(count)
    repeat: _stringRepeat
  });

  var repeat = _core.String.repeat;

  // https://github.com/tc39/proposal-string-pad-start-end




  var _stringPad = function (that, maxLength, fillString, left) {
    var S = String(_defined(that));
    var stringLength = S.length;
    var fillStr = fillString === undefined ? ' ' : String(fillString);
    var intMaxLength = _toLength(maxLength);
    if (intMaxLength <= stringLength || fillStr == '') return S;
    var fillLen = intMaxLength - stringLength;
    var stringFiller = _stringRepeat.call(fillStr, Math.ceil(fillLen / fillStr.length));
    if (stringFiller.length > fillLen) stringFiller = stringFiller.slice(0, fillLen);
    return left ? stringFiller + S : S + stringFiller;
  };

  var navigator = _global.navigator;

  var _userAgent = navigator && navigator.userAgent || '';

  // https://github.com/tc39/proposal-string-pad-start-end




  // https://github.com/zloirock/core-js/issues/280
  _export(_export.P + _export.F * /Version\/10\.\d+(\.\d+)? Safari\//.test(_userAgent), 'String', {
    padStart: function padStart(maxLength /* , fillString = ' ' */) {
      return _stringPad(this, maxLength, arguments.length > 1 ? arguments[1] : undefined, true);
    }
  });

  var padStart = _core.String.padStart;

  // https://github.com/tc39/proposal-string-pad-start-end




  // https://github.com/zloirock/core-js/issues/280
  _export(_export.P + _export.F * /Version\/10\.\d+(\.\d+)? Safari\//.test(_userAgent), 'String', {
    padEnd: function padEnd(maxLength /* , fillString = ' ' */) {
      return _stringPad(this, maxLength, arguments.length > 1 ? arguments[1] : undefined, false);
    }
  });

  var padEnd = _core.String.padEnd;

  function _typeof(obj) {
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function (obj) {
        return typeof obj;
      };
    } else {
      _typeof = function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _objectWithoutPropertiesLoose(source, excluded) {
    if (source == null) return {};
    var target = {};
    var sourceKeys = Object.keys(source);
    var key, i;

    for (i = 0; i < sourceKeys.length; i++) {
      key = sourceKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      target[key] = source[key];
    }

    return target;
  }

  function _objectWithoutProperties(source, excluded) {
    if (source == null) return {};

    var target = _objectWithoutPropertiesLoose(source, excluded);

    var key, i;

    if (Object.getOwnPropertySymbols) {
      var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

      for (i = 0; i < sourceSymbolKeys.length; i++) {
        key = sourceSymbolKeys[i];
        if (excluded.indexOf(key) >= 0) continue;
        if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
        target[key] = source[key];
      }
    }

    return target;
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  function _superPropBase(object, property) {
    while (!Object.prototype.hasOwnProperty.call(object, property)) {
      object = _getPrototypeOf(object);
      if (object === null) break;
    }

    return object;
  }

  function _get(target, property, receiver) {
    if (typeof Reflect !== "undefined" && Reflect.get) {
      _get = Reflect.get;
    } else {
      _get = function _get(target, property, receiver) {
        var base = _superPropBase(target, property);

        if (!base) return;
        var desc = Object.getOwnPropertyDescriptor(base, property);

        if (desc.get) {
          return desc.get.call(receiver);
        }

        return desc.value;
      };
    }

    return _get(target, property, receiver || target);
  }

  function set(target, property, value, receiver) {
    if (typeof Reflect !== "undefined" && Reflect.set) {
      set = Reflect.set;
    } else {
      set = function set(target, property, value, receiver) {
        var base = _superPropBase(target, property);

        var desc;

        if (base) {
          desc = Object.getOwnPropertyDescriptor(base, property);

          if (desc.set) {
            desc.set.call(receiver, value);
            return true;
          } else if (!desc.writable) {
            return false;
          }
        }

        desc = Object.getOwnPropertyDescriptor(receiver, property);

        if (desc) {
          if (!desc.writable) {
            return false;
          }

          desc.value = value;
          Object.defineProperty(receiver, property, desc);
        } else {
          _defineProperty(receiver, property, value);
        }

        return true;
      };
    }

    return set(target, property, value, receiver);
  }

  function _set(target, property, value, receiver, isStrict) {
    var s = set(target, property, value, receiver || target);

    if (!s && isStrict) {
      throw new Error('failed to set property');
    }

    return value;
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance");
  }

  /** Checks if value is string */
  function isString(str) {
    return typeof str === 'string' || str instanceof String;
  }
  /**
    Direction
    @prop {string} NONE
    @prop {string} LEFT
    @prop {string} FORCE_LEFT
    @prop {string} RIGHT
    @prop {string} FORCE_RIGHT
  */

  var DIRECTION = {
    NONE: 'NONE',
    LEFT: 'LEFT',
    FORCE_LEFT: 'FORCE_LEFT',
    RIGHT: 'RIGHT',
    FORCE_RIGHT: 'FORCE_RIGHT'
    /**
      Direction
      @enum {string}
    */

  };

  /** Returns next char index in direction */
  function indexInDirection(pos, direction) {
    if (direction === DIRECTION.LEFT) --pos;
    return pos;
  }
  /** Returns next char position in direction */

  function posInDirection(pos, direction) {
    switch (direction) {
      case DIRECTION.LEFT:
        return --pos;

      case DIRECTION.RIGHT:
      case DIRECTION.FORCE_RIGHT:
        return ++pos;

      default:
        return pos;
    }
  }
  /** */

  function forceDirection(direction) {
    switch (direction) {
      case DIRECTION.LEFT:
        return DIRECTION.FORCE_LEFT;

      case DIRECTION.RIGHT:
        return DIRECTION.FORCE_RIGHT;

      default:
        return direction;
    }
  }
  /** Escapes regular expression control chars */

  function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|[\]/\\])/g, '\\$1');
  } // cloned from https://github.com/epoberezkin/fast-deep-equal with small changes

  function objectIncludes(b, a) {
    if (a === b) return true;
    var arrA = Array.isArray(a),
        arrB = Array.isArray(b),
        i;

    if (arrA && arrB) {
      if (a.length != b.length) return false;

      for (i = 0; i < a.length; i++) {
        if (!objectIncludes(a[i], b[i])) return false;
      }

      return true;
    }

    if (arrA != arrB) return false;

    if (a && b && _typeof(a) === 'object' && _typeof(b) === 'object') {
      var dateA = a instanceof Date,
          dateB = b instanceof Date;
      if (dateA && dateB) return a.getTime() == b.getTime();
      if (dateA != dateB) return false;
      var regexpA = a instanceof RegExp,
          regexpB = b instanceof RegExp;
      if (regexpA && regexpB) return a.toString() == b.toString();
      if (regexpA != regexpB) return false;
      var keys = Object.keys(a); // if (keys.length !== Object.keys(b).length) return false;

      for (i = 0; i < keys.length; i++) {
        if (!Object.prototype.hasOwnProperty.call(b, keys[i])) return false;
      }

      for (i = 0; i < keys.length; i++) {
        if (!objectIncludes(b[keys[i]], a[keys[i]])) return false;
      }

      return true;
    }

    return false;
  }
  /* eslint-disable no-undef */

  var g = typeof window !== 'undefined' && window || typeof global !== 'undefined' && global.global === global && global || typeof self !== 'undefined' && self.self === self && self || {};
  /* eslint-enable no-undef */

  /** Selection range */

  /** Provides details of changing input */

  var ActionDetails =
  /*#__PURE__*/
  function () {
    /** Current input value */

    /** Current cursor position */

    /** Old input value */

    /** Old selection */
    function ActionDetails(value, cursorPos, oldValue, oldSelection) {
      _classCallCheck(this, ActionDetails);

      this.value = value;
      this.cursorPos = cursorPos;
      this.oldValue = oldValue;
      this.oldSelection = oldSelection; // double check if left part was changed (autofilling, other non-standard input triggers)

      while (this.value.slice(0, this.startChangePos) !== this.oldValue.slice(0, this.startChangePos)) {
        --this.oldSelection.start;
      }
    }
    /**
      Start changing position
      @readonly
    */


    _createClass(ActionDetails, [{
      key: "startChangePos",
      get: function get() {
        return Math.min(this.cursorPos, this.oldSelection.start);
      }
      /**
        Inserted symbols count
        @readonly
      */

    }, {
      key: "insertedCount",
      get: function get() {
        return this.cursorPos - this.startChangePos;
      }
      /**
        Inserted symbols
        @readonly
      */

    }, {
      key: "inserted",
      get: function get() {
        return this.value.substr(this.startChangePos, this.insertedCount);
      }
      /**
        Removed symbols count
        @readonly
      */

    }, {
      key: "removedCount",
      get: function get() {
        // Math.max for opposite operation
        return Math.max(this.oldSelection.end - this.startChangePos || // for Delete
        this.oldValue.length - this.value.length, 0);
      }
      /**
        Removed symbols
        @readonly
      */

    }, {
      key: "removed",
      get: function get() {
        return this.oldValue.substr(this.startChangePos, this.removedCount);
      }
      /**
        Unchanged head symbols
        @readonly
      */

    }, {
      key: "head",
      get: function get() {
        return this.value.substring(0, this.startChangePos);
      }
      /**
        Unchanged tail symbols
        @readonly
      */

    }, {
      key: "tail",
      get: function get() {
        return this.value.substring(this.startChangePos + this.insertedCount);
      }
      /**
        Remove direction
        @readonly
      */

    }, {
      key: "removeDirection",
      get: function get() {
        if (!this.removedCount || this.insertedCount) return DIRECTION.NONE; // align right if delete at right or if range removed (event with backspace)

        return this.oldSelection.end === this.cursorPos || this.oldSelection.start === this.cursorPos ? DIRECTION.RIGHT : DIRECTION.LEFT;
      }
    }]);

    return ActionDetails;
  }();

  /**
    Provides details of changing model value
    @param {Object} [details]
    @param {string} [details.inserted] - Inserted symbols
    @param {boolean} [details.skip] - Can skip chars
    @param {number} [details.removeCount] - Removed symbols count
    @param {number} [details.tailShift] - Additional offset if any changes occurred before tail
  */
  var ChangeDetails =
  /*#__PURE__*/
  function () {
    /** Inserted symbols */

    /** Can skip chars */

    /** Additional offset if any changes occurred before tail */

    /** Raw inserted is used by dynamic mask */
    function ChangeDetails(details) {
      _classCallCheck(this, ChangeDetails);

      _extends(this, {
        inserted: '',
        rawInserted: '',
        skip: false,
        tailShift: 0
      }, details);
    }
    /**
      Aggregate changes
      @returns {ChangeDetails} `this`
    */


    _createClass(ChangeDetails, [{
      key: "aggregate",
      value: function aggregate(details) {
        this.rawInserted += details.rawInserted;
        this.skip = this.skip || details.skip;
        this.inserted += details.inserted;
        this.tailShift += details.tailShift;
        return this;
      }
      /** Total offset considering all changes */

    }, {
      key: "offset",
      get: function get() {
        return this.tailShift + this.inserted.length;
      }
    }]);

    return ChangeDetails;
  }();

  /** Provides common masking stuff */
  var Masked =
  /*#__PURE__*/
  function () {
    // $Shape<MaskedOptions>; TODO after fix https://github.com/facebook/flow/issues/4773

    /** @type {Mask} */

    /** */
    // $FlowFixMe TODO no ideas

    /** Transforms value before mask processing */

    /** Validates if value is acceptable */

    /** Does additional processing in the end of editing */

    /** */
    function Masked(opts) {
      _classCallCheck(this, Masked);

      this._value = '';

      this._update(opts);

      this.isInitialized = true;
    }
    /** Sets and applies new options */


    _createClass(Masked, [{
      key: "updateOptions",
      value: function updateOptions(opts) {
        this.withValueRefresh(this._update.bind(this, opts));
      }
      /**
        Sets new options
        @protected
      */

    }, {
      key: "_update",
      value: function _update(opts) {
        _extends(this, opts);
      }
      /** Mask state */

    }, {
      key: "reset",

      /** Resets value */
      value: function reset() {
        this._value = '';
      }
      /** */

    }, {
      key: "resolve",

      /** Resolve new value */
      value: function resolve(value) {
        this.reset();
        this.append(value, {
          input: true
        }, {
          value: ''
        });
        this.doCommit();
        return this.value;
      }
      /** */

    }, {
      key: "nearestInputPos",

      /** Finds nearest input position in direction */
      value: function nearestInputPos(cursorPos, direction) {
        return cursorPos;
      }
      /** Extracts value in range considering flags */

    }, {
      key: "extractInput",
      value: function extractInput() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        return this.value.slice(fromPos, toPos);
      }
      /** Extracts tail in range */

    }, {
      key: "extractTail",
      value: function extractTail() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        return {
          value: this.extractInput(fromPos, toPos)
        };
      }
      /** Stores state before tail */

    }, {
      key: "_storeBeforeTailState",
      value: function _storeBeforeTailState() {
        this._beforeTailState = this.state;
      }
      /** Restores state before tail */

    }, {
      key: "_restoreBeforeTailState",
      value: function _restoreBeforeTailState() {
        this.state = this._beforeTailState;
      }
      /** Resets state before tail */

    }, {
      key: "_resetBeforeTailState",
      value: function _resetBeforeTailState() {
        this._beforeTailState = null;
      }
      /** Appends tail */

    }, {
      key: "appendTail",
      value: function appendTail(tail) {
        return this.append(tail ? tail.value : '', {
          tail: true
        });
      }
      /** Appends char */

    }, {
      key: "_appendCharRaw",
      value: function _appendCharRaw(ch) {
        this._value += ch;
        return new ChangeDetails({
          inserted: ch,
          rawInserted: ch
        });
      }
      /** Appends char */

    }, {
      key: "_appendChar",
      value: function _appendChar(ch) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var checkTail = arguments.length > 2 ? arguments[2] : undefined;
        ch = this.doPrepare(ch, flags);
        if (!ch) return new ChangeDetails();
        var consistentState = this.state;

        var details = this._appendCharRaw(ch, flags);

        if (details.inserted) {
          var appended = this.doValidate(flags) !== false;

          if (appended && checkTail != null) {
            // validation ok, check tail
            this._storeBeforeTailState();

            var tailDetails = this.appendTail(checkTail);
            appended = tailDetails.rawInserted === checkTail.value; // if ok, rollback state after tail

            if (appended && tailDetails.inserted) this._restoreBeforeTailState();
          } // revert all if something went wrong


          if (!appended) {
            details.rawInserted = details.inserted = '';
            this.state = consistentState;
          }
        }

        return details;
      }
      /** Appends symbols considering flags */

    }, {
      key: "append",
      value: function append(str, flags, tail) {
        var oldValueLength = this.value.length;
        var details = new ChangeDetails();

        for (var ci = 0; ci < str.length; ++ci) {
          details.aggregate(this._appendChar(str[ci], flags, tail));
        } // append tail but aggregate only tailShift


        if (tail != null) {
          this._storeBeforeTailState();

          details.tailShift += this.appendTail(tail).tailShift; // TODO it's a good idea to clear state after appending ends
          // but it causes bugs when one append calls another (when dynamic dispatch set rawInputValue)
          // this._resetBeforeTailState();
        }

        return details;
      }
      /** */

    }, {
      key: "remove",
      value: function remove() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        this._value = this.value.slice(0, fromPos) + this.value.slice(toPos);
        return new ChangeDetails();
      }
      /** Calls function and reapplies current value */

    }, {
      key: "withValueRefresh",
      value: function withValueRefresh(fn) {
        if (this._refreshing || !this.isInitialized) return fn();
        this._refreshing = true;
        var unmasked = this.unmaskedValue;
        var value = this.value;
        var ret = fn(); // try to update with raw value first to keep fixed chars

        if (this.resolve(value) !== value) {
          // or fallback to unmasked
          this.unmaskedValue = unmasked;
        }

        delete this._refreshing;
        return ret;
      }
      /**
        Prepares string before mask processing
        @protected
      */

    }, {
      key: "doPrepare",
      value: function doPrepare(str) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return this.prepare ? this.prepare(str, this, flags) : str;
      }
      /**
        Validates if value is acceptable
        @protected
      */

    }, {
      key: "doValidate",
      value: function doValidate(flags) {
        return (!this.validate || this.validate(this.value, this, flags)) && (!this.parent || this.parent.doValidate(flags));
      }
      /**
        Does additional processing in the end of editing
        @protected
      */

    }, {
      key: "doCommit",
      value: function doCommit() {
        if (this.commit) this.commit(this.value, this);
      }
      /** */

    }, {
      key: "splice",
      value: function splice(start, deleteCount, inserted, removeDirection) {
        var tailPos = start + deleteCount;
        var tail = this.extractTail(tailPos);
        var startChangePos = this.nearestInputPos(start, removeDirection);
        var changeDetails = new ChangeDetails({
          tailShift: startChangePos - start // adjust tailShift if start was aligned

        }).aggregate(this.remove(startChangePos)).aggregate(this.append(inserted, {
          input: true
        }, tail));
        return changeDetails;
      }
    }, {
      key: "state",
      get: function get() {
        return {
          _value: this.value
        };
      },
      set: function set(state) {
        this._value = state._value;
      }
    }, {
      key: "value",
      get: function get() {
        return this._value;
      },
      set: function set(value) {
        this.resolve(value);
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.value;
      },
      set: function set(value) {
        this.reset();
        this.append(value, {}, {
          value: ''
        });
        this.doCommit();
      }
      /** */

    }, {
      key: "typedValue",
      get: function get() {
        return this.unmaskedValue;
      },
      set: function set(value) {
        this.unmaskedValue = value;
      }
      /** Value that includes raw user input */

    }, {
      key: "rawInputValue",
      get: function get() {
        return this.extractInput(0, this.value.length, {
          raw: true
        });
      },
      set: function set(value) {
        this.reset();
        this.append(value, {
          raw: true
        }, {
          value: ''
        });
        this.doCommit();
      }
      /** */

    }, {
      key: "isComplete",
      get: function get() {
        return true;
      }
    }]);

    return Masked;
  }();

  /** Get Masked class by mask type */
  function maskedClass(mask) {
    if (mask == null) {
      throw new Error('mask property should be defined');
    }

    if (mask instanceof RegExp) return g.IMask.MaskedRegExp;
    if (isString(mask)) return g.IMask.MaskedPattern;
    if (mask instanceof Date || mask === Date) return g.IMask.MaskedDate;
    if (mask instanceof Number || typeof mask === 'number' || mask === Number) return g.IMask.MaskedNumber;
    if (Array.isArray(mask) || mask === Array) return g.IMask.MaskedDynamic; // $FlowFixMe

    if (mask.prototype instanceof g.IMask.Masked) return mask; // $FlowFixMe

    if (mask instanceof Function) return g.IMask.MaskedFunction;
    console.warn('Mask not found for mask', mask); // eslint-disable-line no-console

    return g.IMask.Masked;
  }
  /** Creates new {@link Masked} depending on mask type */

  function createMask(opts) {
    opts = _objectSpread({}, opts);
    var mask = opts.mask;
    if (mask instanceof g.IMask.Masked) return mask;
    var MaskedClass = maskedClass(mask);
    return new MaskedClass(opts);
  }

  var DEFAULT_INPUT_DEFINITIONS = {
    '0': /\d/,
    'a': /[\u0041-\u005A\u0061-\u007A\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05D0-\u05EA\u05F0-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u08A0\u08A2-\u08AC\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097F\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C33\u0C35-\u0C39\u0C3D\u0C58\u0C59\u0C60\u0C61\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D60\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F4\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u1700-\u170C\u170E-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1877\u1880-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191C\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19C1-\u19C7\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4B\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1CE9-\u1CEC\u1CEE-\u1CF1\u1CF5\u1CF6\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312D\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FCC\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA697\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA78E\uA790-\uA793\uA7A0-\uA7AA\uA7F8-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA80-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uABC0-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC]/,
    // http://stackoverflow.com/a/22075070
    '*': /./
  };
  /** */

  var PatternInputDefinition =
  /*#__PURE__*/
  function () {
    /** */

    /** */

    /** */

    /** */

    /** */

    /** */
    function PatternInputDefinition(opts) {
      _classCallCheck(this, PatternInputDefinition);

      var mask = opts.mask,
          blockOpts = _objectWithoutProperties(opts, ["mask"]);

      this.masked = createMask({
        mask: mask
      });

      _extends(this, blockOpts);
    }

    _createClass(PatternInputDefinition, [{
      key: "reset",
      value: function reset() {
        this._isFilled = false;
        this.masked.reset();
      }
    }, {
      key: "remove",
      value: function remove() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;

        if (fromPos === 0 && toPos >= 1) {
          this._isFilled = false;
          return this.masked.remove(fromPos, toPos);
        }

        return new ChangeDetails();
      }
    }, {
      key: "_appendChar",
      value: function _appendChar(str) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        if (this._isFilled) return new ChangeDetails();
        var state = this.masked.state; // simulate input

        var details = this.masked._appendChar(str, flags);

        if (details.inserted && this.doValidate(flags) === false) {
          details.inserted = details.rawInserted = '';
          this.masked.state = state;
        }

        if (!details.inserted && !this.isOptional && !this.lazy && !flags.input) {
          details.inserted = this.placeholderChar;
        }

        details.skip = !details.inserted && !this.isOptional;
        this._isFilled = Boolean(details.inserted);
        return details;
      }
    }, {
      key: "_appendPlaceholder",
      value: function _appendPlaceholder() {
        var details = new ChangeDetails();
        if (this._isFilled || this.isOptional) return details;
        this._isFilled = true;
        details.inserted = this.placeholderChar;
        return details;
      }
    }, {
      key: "extractTail",
      value: function extractTail() {
        var _this$masked;

        return (_this$masked = this.masked).extractTail.apply(_this$masked, arguments);
      }
    }, {
      key: "appendTail",
      value: function appendTail() {
        var _this$masked2;

        return (_this$masked2 = this.masked).appendTail.apply(_this$masked2, arguments);
      }
    }, {
      key: "extractInput",
      value: function extractInput() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        var flags = arguments.length > 2 ? arguments[2] : undefined;
        return this.masked.extractInput(fromPos, toPos, flags);
      }
    }, {
      key: "nearestInputPos",
      value: function nearestInputPos(cursorPos) {
        var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : DIRECTION.NONE;
        var minPos = 0;
        var maxPos = this.value.length;
        var boundPos = Math.min(Math.max(cursorPos, minPos), maxPos);

        switch (direction) {
          case DIRECTION.LEFT:
          case DIRECTION.FORCE_LEFT:
            return this.isComplete ? boundPos : minPos;

          case DIRECTION.RIGHT:
          case DIRECTION.FORCE_RIGHT:
            return this.isComplete ? boundPos : maxPos;

          case DIRECTION.NONE:
          default:
            return boundPos;
        }
      }
    }, {
      key: "doValidate",
      value: function doValidate() {
        var _this$masked3, _this$parent;

        return (_this$masked3 = this.masked).doValidate.apply(_this$masked3, arguments) && (!this.parent || (_this$parent = this.parent).doValidate.apply(_this$parent, arguments));
      }
    }, {
      key: "doCommit",
      value: function doCommit() {
        this.masked.doCommit();
      }
    }, {
      key: "value",
      get: function get() {
        return this.masked.value || (this._isFilled && !this.isOptional ? this.placeholderChar : '');
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.masked.unmaskedValue;
      }
    }, {
      key: "isComplete",
      get: function get() {
        return Boolean(this.masked.value) || this.isOptional;
      }
    }, {
      key: "state",
      get: function get() {
        return {
          masked: this.masked.state,
          _isFilled: this._isFilled
        };
      },
      set: function set(state) {
        this.masked.state = state.masked;
        this._isFilled = state._isFilled;
      }
    }]);

    return PatternInputDefinition;
  }();

  var PatternFixedDefinition =
  /*#__PURE__*/
  function () {
    /** */

    /** */

    /** */

    /** */
    function PatternFixedDefinition(opts) {
      _classCallCheck(this, PatternFixedDefinition);

      _extends(this, opts);

      this._value = '';
    }

    _createClass(PatternFixedDefinition, [{
      key: "reset",
      value: function reset() {
        this._isRawInput = false;
        this._value = '';
      }
    }, {
      key: "remove",
      value: function remove() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this._value.length;
        this._value = this._value.slice(0, fromPos) + this._value.slice(toPos);
        if (!this._value) this._isRawInput = false;
        return new ChangeDetails();
      }
    }, {
      key: "nearestInputPos",
      value: function nearestInputPos(cursorPos) {
        var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : DIRECTION.NONE;
        var minPos = 0;
        var maxPos = this._value.length;

        switch (direction) {
          case DIRECTION.LEFT:
          case DIRECTION.FORCE_LEFT:
            return minPos;

          case DIRECTION.NONE:
          case DIRECTION.RIGHT:
          case DIRECTION.FORCE_RIGHT:
          default:
            return maxPos;
        }
      }
    }, {
      key: "extractInput",
      value: function extractInput() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this._value.length;
        var flags = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        return flags.raw && this._isRawInput && this._value.slice(fromPos, toPos) || '';
      }
    }, {
      key: "_appendChar",
      value: function _appendChar(str, flags) {
        var details = new ChangeDetails();
        if (this._value) return details;
        var appended = this.char === str[0];
        var isResolved = appended && (this.isUnmasking || flags.input || flags.raw) && !flags.tail;
        if (isResolved) details.rawInserted = this.char;
        this._value = details.inserted = this.char;
        this._isRawInput = isResolved && (flags.raw || flags.input);
        return details;
      }
    }, {
      key: "_appendPlaceholder",
      value: function _appendPlaceholder() {
        var details = new ChangeDetails();
        if (this._value) return details;
        this._value = details.inserted = this.char;
        return details;
      }
    }, {
      key: "extractTail",
      value: function extractTail() {
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        return {
          value: ''
        };
      }
    }, {
      key: "appendTail",
      value: function appendTail(tail) {
        return this._appendChar(tail ? tail.value : '', {
          tail: true
        });
      }
    }, {
      key: "doCommit",
      value: function doCommit() {}
    }, {
      key: "value",
      get: function get() {
        return this._value;
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.isUnmasking ? this.value : '';
      }
    }, {
      key: "isComplete",
      get: function get() {
        return true;
      }
    }, {
      key: "state",
      get: function get() {
        return {
          _value: this._value,
          _isRawInput: this._isRawInput
        };
      },
      set: function set(state) {
        _extends(this, state);
      }
    }]);

    return PatternFixedDefinition;
  }();

  var ChunksTailDetails =
  /*#__PURE__*/
  function () {
    function ChunksTailDetails(chunks) {
      _classCallCheck(this, ChunksTailDetails);

      this.chunks = chunks;
    }

    _createClass(ChunksTailDetails, [{
      key: "value",
      get: function get() {
        return this.chunks.map(function (c) {
          return c.value;
        }).join('');
      }
    }]);

    return ChunksTailDetails;
  }();

  /**
    Pattern mask
    @param {Object} opts
    @param {Object} opts.blocks
    @param {Object} opts.definitions
    @param {string} opts.placeholderChar
    @param {boolean} opts.lazy
  */
  var MaskedPattern =
  /*#__PURE__*/
  function (_Masked) {
    _inherits(MaskedPattern, _Masked);

    /** */

    /** */

    /** Single char for empty input */

    /** Show placeholder only when needed */
    function MaskedPattern() {
      var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      _classCallCheck(this, MaskedPattern);

      // TODO type $Shape<MaskedPatternOptions>={} does not work
      opts.definitions = _extends({}, DEFAULT_INPUT_DEFINITIONS, opts.definitions);
      return _possibleConstructorReturn(this, _getPrototypeOf(MaskedPattern).call(this, _objectSpread({}, MaskedPattern.DEFAULTS, opts)));
    }
    /**
      @override
      @param {Object} opts
    */


    _createClass(MaskedPattern, [{
      key: "_update",
      value: function _update() {
        var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        opts.definitions = _extends({}, this.definitions, opts.definitions);

        _get(_getPrototypeOf(MaskedPattern.prototype), "_update", this).call(this, opts);

        this._rebuildMask();
      }
      /** */

    }, {
      key: "_rebuildMask",
      value: function _rebuildMask() {
        var _this = this;

        var defs = this.definitions;
        this._blocks = [];
        this._stops = [];
        this._maskedBlocks = {};
        var pattern = this.mask;
        if (!pattern || !defs) return;
        var unmaskingBlock = false;
        var optionalBlock = false;

        for (var i = 0; i < pattern.length; ++i) {
          if (this.blocks) {
            var _ret = function () {
              var p = pattern.slice(i);
              var bNames = Object.keys(_this.blocks).filter(function (bName) {
                return p.indexOf(bName) === 0;
              }); // order by key length

              bNames.sort(function (a, b) {
                return b.length - a.length;
              }); // use block name with max length

              var bName = bNames[0];

              if (bName) {
                var maskedBlock = createMask(_objectSpread({
                  parent: _this,
                  lazy: _this.lazy,
                  placeholderChar: _this.placeholderChar
                }, _this.blocks[bName]));

                if (maskedBlock) {
                  _this._blocks.push(maskedBlock); // store block index


                  if (!_this._maskedBlocks[bName]) _this._maskedBlocks[bName] = [];

                  _this._maskedBlocks[bName].push(_this._blocks.length - 1);
                }

                i += bName.length - 1;
                return "continue";
              }
            }();

            if (_ret === "continue") continue;
          }

          var char = pattern[i];

          var _isInput = char in defs;

          if (char === MaskedPattern.STOP_CHAR) {
            this._stops.push(this._blocks.length);

            continue;
          }

          if (char === '{' || char === '}') {
            unmaskingBlock = !unmaskingBlock;
            continue;
          }

          if (char === '[' || char === ']') {
            optionalBlock = !optionalBlock;
            continue;
          }

          if (char === MaskedPattern.ESCAPE_CHAR) {
            ++i;
            char = pattern[i];
            if (!char) break;
            _isInput = false;
          }

          var def = void 0;

          if (_isInput) {
            def = new PatternInputDefinition({
              parent: this,
              lazy: this.lazy,
              placeholderChar: this.placeholderChar,
              mask: defs[char],
              isOptional: optionalBlock
            });
          } else {
            def = new PatternFixedDefinition({
              char: char,
              isUnmasking: unmaskingBlock
            });
          }

          this._blocks.push(def);
        }
      }
      /**
        @override
      */

    }, {
      key: "_storeBeforeTailState",

      /**
        @override
      */
      value: function _storeBeforeTailState() {
        this._blocks.forEach(function (b) {
          // $FlowFixMe _storeBeforeTailState is not exist in PatternBlock
          if (typeof b._storeBeforeTailState === 'function') {
            b._storeBeforeTailState();
          }
        });

        _get(_getPrototypeOf(MaskedPattern.prototype), "_storeBeforeTailState", this).call(this);
      }
      /**
        @override
      */

    }, {
      key: "_restoreBeforeTailState",
      value: function _restoreBeforeTailState() {
        this._blocks.forEach(function (b) {
          // $FlowFixMe _restoreBeforeTailState is not exist in PatternBlock
          if (typeof b._restoreBeforeTailState === 'function') {
            b._restoreBeforeTailState();
          }
        });

        _get(_getPrototypeOf(MaskedPattern.prototype), "_restoreBeforeTailState", this).call(this);
      }
      /**
        @override
      */

    }, {
      key: "_resetBeforeTailState",
      value: function _resetBeforeTailState() {
        this._blocks.forEach(function (b) {
          // $FlowFixMe _resetBeforeTailState is not exist in PatternBlock
          if (typeof b._resetBeforeTailState === 'function') {
            b._resetBeforeTailState();
          }
        });

        _get(_getPrototypeOf(MaskedPattern.prototype), "_resetBeforeTailState", this).call(this);
      }
      /**
        @override
      */

    }, {
      key: "reset",
      value: function reset() {
        _get(_getPrototypeOf(MaskedPattern.prototype), "reset", this).call(this);

        this._blocks.forEach(function (b) {
          return b.reset();
        });
      }
      /**
        @override
      */

    }, {
      key: "doCommit",

      /**
        @override
      */
      value: function doCommit() {
        this._blocks.forEach(function (b) {
          return b.doCommit();
        });

        _get(_getPrototypeOf(MaskedPattern.prototype), "doCommit", this).call(this);
      }
      /**
        @override
      */

    }, {
      key: "appendTail",

      /**
        @override
      */
      value: function appendTail(tail) {
        var details = new ChangeDetails();

        if (tail) {
          details.aggregate(tail instanceof ChunksTailDetails ? this._appendTailChunks(tail.chunks) : _get(_getPrototypeOf(MaskedPattern.prototype), "appendTail", this).call(this, tail));
        }

        return details.aggregate(this._appendPlaceholder());
      }
      /**
        @override
      */

    }, {
      key: "_appendCharRaw",
      value: function _appendCharRaw(ch) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        var blockData = this._mapPosToBlock(this.value.length);

        var details = new ChangeDetails();
        if (!blockData) return details;

        for (var bi = blockData.index;; ++bi) {
          var _block = this._blocks[bi];
          if (!_block) break;

          var blockDetails = _block._appendChar(ch, flags);

          var skip = blockDetails.skip;
          details.aggregate(blockDetails);
          if (skip || blockDetails.rawInserted) break; // go next char
        }

        return details;
      }
      /** Appends chunks splitted by stop chars */

    }, {
      key: "_appendTailChunks",
      value: function _appendTailChunks(chunks) {
        var details = new ChangeDetails();

        for (var ci = 0; ci < chunks.length && !details.skip; ++ci) {
          var chunk = chunks[ci];

          var lastBlock = this._mapPosToBlock(this.value.length);

          var chunkBlock = chunk instanceof ChunksTailDetails && chunk.index != null && (!lastBlock || lastBlock.index <= chunk.index) && this._blocks[chunk.index];

          if (chunkBlock) {
            // $FlowFixMe we already check index above
            details.aggregate(this._appendPlaceholder(chunk.index));
            var tailDetails = chunkBlock.appendTail(chunk);
            tailDetails.skip = false; // always ignore skip, it will be set on last

            details.aggregate(tailDetails);
            this._value += tailDetails.inserted; // get not inserted chars

            var remainChars = chunk.value.slice(tailDetails.rawInserted.length);
            if (remainChars) details.aggregate(this.append(remainChars, {
              tail: true
            }));
          } else {
            var _ref = chunk,
                stop = _ref.stop,
                value = _ref.value;
            if (stop != null && this._stops.indexOf(stop) >= 0) details.aggregate(this._appendPlaceholder(stop));
            details.aggregate(this.append(value, {
              tail: true
            }));
          }
        }
        return details;
      }
      /**
        @override
      */

    }, {
      key: "extractTail",
      value: function extractTail() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        return new ChunksTailDetails(this._extractTailChunks(fromPos, toPos));
      }
      /**
        @override
      */

    }, {
      key: "extractInput",
      value: function extractInput() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        var flags = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        if (fromPos === toPos) return '';
        var input = '';

        this._forEachBlocksInRange(fromPos, toPos, function (b, _, fromPos, toPos) {
          input += b.extractInput(fromPos, toPos, flags);
        });

        return input;
      }
      /** Extracts chunks from input splitted by stop chars */

    }, {
      key: "_extractTailChunks",
      value: function _extractTailChunks() {
        var _this2 = this;

        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        if (fromPos === toPos) return [];
        var chunks = [];
        var lastChunk;

        this._forEachBlocksInRange(fromPos, toPos, function (b, bi, fromPos, toPos) {
          var blockChunk = b.extractTail(fromPos, toPos);
          var nearestStop;

          for (var si = 0; si < _this2._stops.length; ++si) {
            var stop = _this2._stops[si];
            if (stop <= bi) nearestStop = stop;else break;
          }

          if (blockChunk instanceof ChunksTailDetails) {
            // TODO append to lastChunk with same index
            if (nearestStop == null) {
              // try append floating chunks to existed lastChunk
              var headFloatChunksCount = blockChunk.chunks.length;

              for (var ci = 0; ci < blockChunk.chunks.length; ++ci) {
                if (blockChunk.chunks[ci].stop != null) {
                  headFloatChunksCount = ci;
                  break;
                }
              }

              var headFloatChunks = blockChunk.chunks.splice(0, headFloatChunksCount);
              headFloatChunks.filter(function (chunk) {
                return chunk.value;
              }).forEach(function (chunk) {
                if (lastChunk) lastChunk.value += chunk.value; // will flat nested chunks
                else lastChunk = {
                    value: chunk.value
                  };
              });
            } // if block chunk has stops


            if (blockChunk.chunks.length) {
              if (lastChunk) chunks.push(lastChunk);
              blockChunk.index = nearestStop;
              chunks.push(blockChunk); // we cant append to ChunksTailDetails, so just reset lastChunk to force adding new

              lastChunk = null;
            }
          } else {
            if (nearestStop != null) {
              // on middle chunks consider stop flag and do not consider value
              // add block even if it is empty
              if (lastChunk) chunks.push(lastChunk);
              blockChunk.stop = nearestStop;
            } else if (lastChunk) {
              lastChunk.value += blockChunk.value;
              return;
            }

            lastChunk = blockChunk;
          }
        });

        if (lastChunk && lastChunk.value) chunks.push(lastChunk);
        return chunks;
      }
      /** Appends placeholder depending on laziness */

    }, {
      key: "_appendPlaceholder",
      value: function _appendPlaceholder(toBlockIndex) {
        var _this3 = this;

        var details = new ChangeDetails();
        if (this.lazy && toBlockIndex == null) return details;

        var startBlockData = this._mapPosToBlock(this.value.length);

        if (!startBlockData) return details;
        var startBlockIndex = startBlockData.index;
        var endBlockIndex = toBlockIndex != null ? toBlockIndex : this._blocks.length;

        this._blocks.slice(startBlockIndex, endBlockIndex).forEach(function (b) {
          if (typeof b._appendPlaceholder === 'function') {
            // $FlowFixMe `_blocks` may not be present
            var args = b._blocks != null ? [b._blocks.length] : [];

            var bDetails = b._appendPlaceholder.apply(b, args);

            _this3._value += bDetails.inserted;
            details.aggregate(bDetails);
          }
        });

        return details;
      }
      /** Finds block in pos */

    }, {
      key: "_mapPosToBlock",
      value: function _mapPosToBlock(pos) {
        var accVal = '';

        for (var bi = 0; bi < this._blocks.length; ++bi) {
          var _block2 = this._blocks[bi];
          var blockStartPos = accVal.length;
          accVal += _block2.value;

          if (pos <= accVal.length) {
            return {
              index: bi,
              offset: pos - blockStartPos
            };
          }
        }
      }
      /** */

    }, {
      key: "_blockStartPos",
      value: function _blockStartPos(blockIndex) {
        return this._blocks.slice(0, blockIndex).reduce(function (pos, b) {
          return pos += b.value.length;
        }, 0);
      }
      /** */

    }, {
      key: "_forEachBlocksInRange",
      value: function _forEachBlocksInRange(fromPos) {
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        var fn = arguments.length > 2 ? arguments[2] : undefined;

        var fromBlock = this._mapPosToBlock(fromPos);

        if (fromBlock) {
          var toBlock = this._mapPosToBlock(toPos); // process first block


          var isSameBlock = toBlock && fromBlock.index === toBlock.index;
          var fromBlockRemoveBegin = fromBlock.offset;
          var fromBlockRemoveEnd = toBlock && isSameBlock ? toBlock.offset : undefined;
          fn(this._blocks[fromBlock.index], fromBlock.index, fromBlockRemoveBegin, fromBlockRemoveEnd);

          if (toBlock && !isSameBlock) {
            // process intermediate blocks
            for (var bi = fromBlock.index + 1; bi < toBlock.index; ++bi) {
              fn(this._blocks[bi], bi);
            } // process last block


            fn(this._blocks[toBlock.index], toBlock.index, 0, toBlock.offset);
          }
        }
      }
      /**
        @override
      */

    }, {
      key: "remove",
      value: function remove() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;

        var removeDetails = _get(_getPrototypeOf(MaskedPattern.prototype), "remove", this).call(this, fromPos, toPos);

        this._forEachBlocksInRange(fromPos, toPos, function (b, _, bFromPos, bToPos) {
          removeDetails.aggregate(b.remove(bFromPos, bToPos));
        });

        return removeDetails;
      }
      /**
        @override
      */

    }, {
      key: "nearestInputPos",
      value: function nearestInputPos(cursorPos) {
        var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : DIRECTION.NONE;
        // TODO refactor - extract alignblock
        var beginBlockData = this._mapPosToBlock(cursorPos) || {
          index: 0,
          offset: 0
        };
        var beginBlockOffset = beginBlockData.offset,
            beginBlockIndex = beginBlockData.index;
        var beginBlock = this._blocks[beginBlockIndex];
        if (!beginBlock) return cursorPos;
        var beginBlockCursorPos = beginBlockOffset; // if position inside block - try to adjust it

        if (beginBlockCursorPos !== 0 && beginBlockCursorPos < beginBlock.value.length) {
          beginBlockCursorPos = beginBlock.nearestInputPos(beginBlockOffset, forceDirection(direction));
        }

        var cursorAtRight = beginBlockCursorPos === beginBlock.value.length;
        var cursorAtLeft = beginBlockCursorPos === 0; //  cursor is INSIDE first block (not at bounds)

        if (!cursorAtLeft && !cursorAtRight) return this._blockStartPos(beginBlockIndex) + beginBlockCursorPos;
        var searchBlockIndex = cursorAtRight ? beginBlockIndex + 1 : beginBlockIndex;

        if (direction === DIRECTION.NONE) {
          // NONE direction used to calculate start input position if no chars were removed
          // FOR NONE:
          // -
          // input|any
          // ->
          //  any|input
          // <-
          //  filled-input|any
          // check if first block at left is input
          if (searchBlockIndex > 0) {
            var blockIndexAtLeft = searchBlockIndex - 1;
            var blockAtLeft = this._blocks[blockIndexAtLeft];
            var blockInputPos = blockAtLeft.nearestInputPos(0, DIRECTION.NONE); // is input

            if (!blockAtLeft.value.length || blockInputPos !== blockAtLeft.value.length) {
              return this._blockStartPos(searchBlockIndex);
            }
          } // ->


          var firstInputAtRight = searchBlockIndex;

          for (var bi = firstInputAtRight; bi < this._blocks.length; ++bi) {
            var _block3 = this._blocks[bi];

            var _blockInputPos = _block3.nearestInputPos(0, DIRECTION.NONE);

            if (_blockInputPos !== _block3.value.length) {
              return this._blockStartPos(bi) + _blockInputPos;
            }
          }

          return this.value.length;
        }

        if (direction === DIRECTION.LEFT || direction === DIRECTION.FORCE_LEFT) {
          // -
          //  any|filled-input
          // <-
          //  any|first not empty is not-len-aligned
          //  not-0-aligned|any
          // ->
          //  any|not-len-aligned or end
          // check if first block at right is filled input
          var firstFilledBlockIndexAtRight;

          for (var _bi = searchBlockIndex; _bi < this._blocks.length; ++_bi) {
            if (this._blocks[_bi].value) {
              firstFilledBlockIndexAtRight = _bi;
              break;
            }
          }

          if (firstFilledBlockIndexAtRight != null) {
            var filledBlock = this._blocks[firstFilledBlockIndexAtRight];

            var _blockInputPos2 = filledBlock.nearestInputPos(0, DIRECTION.RIGHT);

            if (_blockInputPos2 === 0 && filledBlock.unmaskedValue.length) {
              // filled block is input
              return this._blockStartPos(firstFilledBlockIndexAtRight) + _blockInputPos2;
            }
          } // <-
          // find this vars


          var firstFilledInputBlockIndex = -1;
          var firstEmptyInputBlockIndex; // TODO consider nested empty inputs

          for (var _bi2 = searchBlockIndex - 1; _bi2 >= 0; --_bi2) {
            var _block4 = this._blocks[_bi2];

            var _blockInputPos3 = _block4.nearestInputPos(_block4.value.length, DIRECTION.FORCE_LEFT);

            if (firstEmptyInputBlockIndex == null && (!_block4.value || _blockInputPos3 !== 0)) {
              firstEmptyInputBlockIndex = _bi2;
            }

            if (_blockInputPos3 !== 0) {
              if (_blockInputPos3 !== _block4.value.length) {
                // aligned inside block - return immediately
                return this._blockStartPos(_bi2) + _blockInputPos3;
              } else {
                // found filled
                firstFilledInputBlockIndex = _bi2;
                break;
              }
            }
          }

          if (direction === DIRECTION.LEFT) {
            // try find first empty input before start searching position only when not forced
            for (var _bi3 = firstFilledInputBlockIndex + 1; _bi3 <= Math.min(searchBlockIndex, this._blocks.length - 1); ++_bi3) {
              var _block5 = this._blocks[_bi3];

              var _blockInputPos4 = _block5.nearestInputPos(0, DIRECTION.NONE);

              var blockAlignedPos = this._blockStartPos(_bi3) + _blockInputPos4; // if block is empty and last or not lazy input


              if ((!_block5.value.length && blockAlignedPos === this.value.length || _blockInputPos4 !== _block5.value.length) && blockAlignedPos <= cursorPos) {
                return blockAlignedPos;
              }
            }
          } // process overflow


          if (firstFilledInputBlockIndex >= 0) {
            return this._blockStartPos(firstFilledInputBlockIndex) + this._blocks[firstFilledInputBlockIndex].value.length;
          } // for lazy if has aligned left inside fixed and has came to the start - use start position


          if (direction === DIRECTION.FORCE_LEFT || this.lazy && !this.extractInput() && !isInput(this._blocks[searchBlockIndex])) {
            return 0;
          }

          if (firstEmptyInputBlockIndex != null) {
            return this._blockStartPos(firstEmptyInputBlockIndex);
          } // find first input


          for (var _bi4 = searchBlockIndex; _bi4 < this._blocks.length; ++_bi4) {
            var _block6 = this._blocks[_bi4];

            var _blockInputPos5 = _block6.nearestInputPos(0, DIRECTION.NONE); // is input


            if (!_block6.value.length || _blockInputPos5 !== _block6.value.length) {
              return this._blockStartPos(_bi4) + _blockInputPos5;
            }
          }

          return 0;
        }

        if (direction === DIRECTION.RIGHT || direction === DIRECTION.FORCE_RIGHT) {
          // ->
          //  any|not-len-aligned and filled
          //  any|not-len-aligned
          // <-
          var firstInputBlockAlignedIndex;
          var firstInputBlockAlignedPos;

          for (var _bi5 = searchBlockIndex; _bi5 < this._blocks.length; ++_bi5) {
            var _block7 = this._blocks[_bi5];

            var _blockInputPos6 = _block7.nearestInputPos(0, DIRECTION.NONE);

            if (_blockInputPos6 !== _block7.value.length) {
              firstInputBlockAlignedPos = this._blockStartPos(_bi5) + _blockInputPos6;
              firstInputBlockAlignedIndex = _bi5;
              break;
            }
          }

          if (firstInputBlockAlignedIndex != null && firstInputBlockAlignedPos != null) {
            for (var _bi6 = firstInputBlockAlignedIndex; _bi6 < this._blocks.length; ++_bi6) {
              var _block8 = this._blocks[_bi6];

              var _blockInputPos7 = _block8.nearestInputPos(0, DIRECTION.FORCE_RIGHT);

              if (_blockInputPos7 !== _block8.value.length) {
                return this._blockStartPos(_bi6) + _blockInputPos7;
              }
            }

            return direction === DIRECTION.FORCE_RIGHT ? this.value.length : firstInputBlockAlignedPos;
          }

          for (var _bi7 = Math.min(searchBlockIndex, this._blocks.length - 1); _bi7 >= 0; --_bi7) {
            var _block9 = this._blocks[_bi7];

            var _blockInputPos8 = _block9.nearestInputPos(_block9.value.length, DIRECTION.LEFT);

            if (_blockInputPos8 !== 0) {
              var alignedPos = this._blockStartPos(_bi7) + _blockInputPos8;

              if (alignedPos >= cursorPos) return alignedPos;
              break;
            }
          }
        }

        return cursorPos;
      }
      /** Get block by name */

    }, {
      key: "maskedBlock",
      value: function maskedBlock(name) {
        return this.maskedBlocks(name)[0];
      }
      /** Get all blocks by name */

    }, {
      key: "maskedBlocks",
      value: function maskedBlocks(name) {
        var _this4 = this;

        var indices = this._maskedBlocks[name];
        if (!indices) return [];
        return indices.map(function (gi) {
          return _this4._blocks[gi];
        });
      }
    }, {
      key: "state",
      get: function get$$1() {
        return _objectSpread({}, _get(_getPrototypeOf(MaskedPattern.prototype), "state", this), {
          _blocks: this._blocks.map(function (b) {
            return b.state;
          })
        });
      },
      set: function set$$1(state) {
        var _blocks = state._blocks,
            maskedState = _objectWithoutProperties(state, ["_blocks"]);

        this._blocks.forEach(function (b, bi) {
          return b.state = _blocks[bi];
        });

        _set(_getPrototypeOf(MaskedPattern.prototype), "state", maskedState, this, true);
      }
    }, {
      key: "isComplete",
      get: function get$$1() {
        return this._blocks.every(function (b) {
          return b.isComplete;
        });
      }
    }, {
      key: "unmaskedValue",
      get: function get$$1() {
        return this._blocks.reduce(function (str, b) {
          return str += b.unmaskedValue;
        }, '');
      },
      set: function set$$1(unmaskedValue) {
        _set(_getPrototypeOf(MaskedPattern.prototype), "unmaskedValue", unmaskedValue, this, true);
      }
      /**
        @override
      */

    }, {
      key: "value",
      get: function get$$1() {
        // TODO return _value when not in change?
        return this._blocks.reduce(function (str, b) {
          return str += b.value;
        }, '');
      },
      set: function set$$1(value) {
        _set(_getPrototypeOf(MaskedPattern.prototype), "value", value, this, true);
      }
    }]);

    return MaskedPattern;
  }(Masked);
  MaskedPattern.DEFAULTS = {
    lazy: true,
    placeholderChar: '_'
  };
  MaskedPattern.STOP_CHAR = '`';
  MaskedPattern.ESCAPE_CHAR = '\\';
  MaskedPattern.InputDefinition = PatternInputDefinition;
  MaskedPattern.FixedDefinition = PatternFixedDefinition;

  function isInput(block) {
    if (!block) return false;
    var value = block.value;
    return !value || block.nearestInputPos(0, DIRECTION.NONE) !== value.length;
  }

  /** Pattern which accepts ranges */

  var MaskedRange =
  /*#__PURE__*/
  function (_MaskedPattern) {
    _inherits(MaskedRange, _MaskedPattern);

    function MaskedRange() {
      _classCallCheck(this, MaskedRange);

      return _possibleConstructorReturn(this, _getPrototypeOf(MaskedRange).apply(this, arguments));
    }

    _createClass(MaskedRange, [{
      key: "_update",

      /**
        @override
      */
      value: function _update(opts) {
        // TODO type
        var maxLength = String(opts.to).length;
        if (opts.maxLength != null) maxLength = Math.max(maxLength, opts.maxLength);
        opts.maxLength = maxLength;
        var toStr = String(opts.to).padStart(maxLength, '0');
        var fromStr = String(opts.from).padStart(maxLength, '0');
        var sameCharsCount = 0;

        while (sameCharsCount < toStr.length && toStr[sameCharsCount] === fromStr[sameCharsCount]) {
          ++sameCharsCount;
        }

        opts.mask = toStr.slice(0, sameCharsCount).replace(/0/g, '\\0') + '0'.repeat(maxLength - sameCharsCount);

        _get(_getPrototypeOf(MaskedRange.prototype), "_update", this).call(this, opts);
      }
      /**
        @override
      */

    }, {
      key: "doValidate",

      /**
        @override
      */
      value: function doValidate() {
        var _get2;

        var str = this.value;
        var minstr = '';
        var maxstr = '';

        var _ref = str.match(/^(\D*)(\d*)(\D*)/) || [],
            _ref2 = _slicedToArray(_ref, 3),
            placeholder = _ref2[1],
            num = _ref2[2];

        if (num) {
          minstr = '0'.repeat(placeholder.length) + num;
          maxstr = '9'.repeat(placeholder.length) + num;
        }

        var firstNonZero = str.search(/[^0]/);
        if (firstNonZero === -1 && str.length <= this._matchFrom) return true;
        minstr = minstr.padEnd(this.maxLength, '0');
        maxstr = maxstr.padEnd(this.maxLength, '9');

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return this.from <= Number(maxstr) && Number(minstr) <= this.to && (_get2 = _get(_getPrototypeOf(MaskedRange.prototype), "doValidate", this)).call.apply(_get2, [this].concat(args));
      }
    }, {
      key: "_matchFrom",

      /**
        Optionally sets max length of pattern.
        Used when pattern length is longer then `to` param length. Pads zeros at start in this case.
      */

      /** Min bound */

      /** Max bound */
      get: function get$$1() {
        return this.maxLength - String(this.from).length;
      }
    }, {
      key: "isComplete",
      get: function get$$1() {
        return _get(_getPrototypeOf(MaskedRange.prototype), "isComplete", this) && Boolean(this.value);
      }
    }]);

    return MaskedRange;
  }(MaskedPattern);

  /** Date mask */

  var MaskedDate =
  /*#__PURE__*/
  function (_MaskedPattern) {
    _inherits(MaskedDate, _MaskedPattern);

    /** Parse string to Date */

    /** Format Date to string */

    /** Pattern mask for date according to {@link MaskedDate#format} */

    /** Start date */

    /** End date */

    /**
      @param {Object} opts
    */
    function MaskedDate(opts) {
      _classCallCheck(this, MaskedDate);

      return _possibleConstructorReturn(this, _getPrototypeOf(MaskedDate).call(this, _objectSpread({}, MaskedDate.DEFAULTS, opts)));
    }
    /**
      @override
    */


    _createClass(MaskedDate, [{
      key: "_update",
      value: function _update(opts) {
        if (opts.mask === Date) delete opts.mask;

        if (opts.pattern) {
          opts.mask = opts.pattern;
          delete opts.pattern;
        }

        var blocks = opts.blocks;
        opts.blocks = _extends({}, MaskedDate.GET_DEFAULT_BLOCKS()); // adjust year block

        if (opts.min) opts.blocks.Y.from = opts.min.getFullYear();
        if (opts.max) opts.blocks.Y.to = opts.max.getFullYear();

        if (opts.min && opts.max && opts.blocks.Y.from === opts.blocks.Y.to) {
          opts.blocks.m.from = opts.min.getMonth() + 1;
          opts.blocks.m.to = opts.max.getMonth() + 1;

          if (opts.blocks.m.from === opts.blocks.m.to) {
            opts.blocks.d.from = opts.min.getDate();
            opts.blocks.d.to = opts.max.getDate();
          }
        }

        _extends(opts.blocks, blocks);

        _get(_getPrototypeOf(MaskedDate.prototype), "_update", this).call(this, opts);
      }
      /**
        @override
      */

    }, {
      key: "doValidate",
      value: function doValidate() {
        var _get2;

        var date = this.date;

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return (_get2 = _get(_getPrototypeOf(MaskedDate.prototype), "doValidate", this)).call.apply(_get2, [this].concat(args)) && (!this.isComplete || this.isDateExist(this.value) && date != null && (this.min == null || this.min <= date) && (this.max == null || date <= this.max));
      }
      /** Checks if date is exists */

    }, {
      key: "isDateExist",
      value: function isDateExist(str) {
        return this.format(this.parse(str)) === str;
      }
      /** Parsed Date */

    }, {
      key: "date",
      get: function get$$1() {
        return this.isComplete ? this.parse(this.value) : null;
      },
      set: function set(date) {
        this.value = this.format(date);
      }
      /**
        @override
      */

    }, {
      key: "typedValue",
      get: function get$$1() {
        return this.date;
      },
      set: function set(value) {
        this.date = value;
      }
    }]);

    return MaskedDate;
  }(MaskedPattern);
  MaskedDate.DEFAULTS = {
    pattern: 'd{.}`m{.}`Y',
    format: function format(date) {
      var day = String(date.getDate()).padStart(2, '0');
      var month = String(date.getMonth() + 1).padStart(2, '0');
      var year = date.getFullYear();
      return [day, month, year].join('.');
    },
    parse: function parse(str) {
      var _str$split = str.split('.'),
          _str$split2 = _slicedToArray(_str$split, 3),
          day = _str$split2[0],
          month = _str$split2[1],
          year = _str$split2[2];

      return new Date(year, month - 1, day);
    }
  };

  MaskedDate.GET_DEFAULT_BLOCKS = function () {
    return {
      d: {
        mask: MaskedRange,
        from: 1,
        to: 31,
        maxLength: 2
      },
      m: {
        mask: MaskedRange,
        from: 1,
        to: 12,
        maxLength: 2
      },
      Y: {
        mask: MaskedRange,
        from: 1900,
        to: 9999
      }
    };
  };

  /**
    Generic element API to use with mask
    @interface
  */
  var MaskElement =
  /*#__PURE__*/
  function () {
    function MaskElement() {
      _classCallCheck(this, MaskElement);
    }

    _createClass(MaskElement, [{
      key: "select",

      /** Safely sets element selection */
      value: function select(start, end) {
        if (start == null || end == null || start === this.selectionStart && end === this.selectionEnd) return;

        try {
          this._unsafeSelect(start, end);
        } catch (e) {}
      }
      /** Should be overriden in subclasses */

    }, {
      key: "_unsafeSelect",
      value: function _unsafeSelect(start, end) {}
      /** Should be overriden in subclasses */

    }, {
      key: "bindEvents",

      /** Should be overriden in subclasses */
      value: function bindEvents(handlers) {}
      /** Should be overriden in subclasses */

    }, {
      key: "unbindEvents",
      value: function unbindEvents() {}
    }, {
      key: "selectionStart",

      /** */

      /** */

      /** */

      /** Safely returns selection start */
      get: function get() {
        var start;

        try {
          start = this._unsafeSelectionStart;
        } catch (e) {}

        return start != null ? start : this.value.length;
      }
      /** Safely returns selection end */

    }, {
      key: "selectionEnd",
      get: function get() {
        var end;

        try {
          end = this._unsafeSelectionEnd;
        } catch (e) {}

        return end != null ? end : this.value.length;
      }
    }, {
      key: "isActive",
      get: function get() {
        return false;
      }
    }]);

    return MaskElement;
  }();

  /** Bridge between HTMLElement and {@link Masked} */

  var HTMLMaskElement =
  /*#__PURE__*/
  function (_MaskElement) {
    _inherits(HTMLMaskElement, _MaskElement);

    /** Mapping between HTMLElement events and mask internal events */

    /** HTMLElement to use mask on */

    /**
      @param {HTMLInputElement|HTMLTextAreaElement} input
    */
    function HTMLMaskElement(input) {
      var _this;

      _classCallCheck(this, HTMLMaskElement);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(HTMLMaskElement).call(this));
      _this.input = input;
      _this._handlers = {};
      return _this;
    }
    /**
      Is element in focus
      @readonly
    */


    _createClass(HTMLMaskElement, [{
      key: "_unsafeSelect",

      /**
        Sets HTMLElement selection
        @override
      */
      value: function _unsafeSelect(start, end) {
        this.input.setSelectionRange(start, end);
      }
      /**
        HTMLElement value
        @override
      */

    }, {
      key: "bindEvents",

      /**
        Binds HTMLElement events to mask internal events
        @override
      */
      value: function bindEvents(handlers) {
        var _this2 = this;

        Object.keys(handlers).forEach(function (event) {
          return _this2._toggleEventHandler(HTMLMaskElement.EVENTS_MAP[event], handlers[event]);
        });
      }
      /**
        Unbinds HTMLElement events to mask internal events
        @override
      */

    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        var _this3 = this;

        Object.keys(this._handlers).forEach(function (event) {
          return _this3._toggleEventHandler(event);
        });
      }
      /** */

    }, {
      key: "_toggleEventHandler",
      value: function _toggleEventHandler(event, handler) {
        if (this._handlers[event]) {
          this.input.removeEventListener(event, this._handlers[event]);
          delete this._handlers[event];
        }

        if (handler) {
          this.input.addEventListener(event, handler);
          this._handlers[event] = handler;
        }
      }
    }, {
      key: "isActive",
      get: function get() {
        return this.input === document.activeElement;
      }
      /**
        Returns HTMLElement selection start
        @override
      */

    }, {
      key: "_unsafeSelectionStart",
      get: function get() {
        return this.input.selectionStart;
      }
      /**
        Returns HTMLElement selection end
        @override
      */

    }, {
      key: "_unsafeSelectionEnd",
      get: function get() {
        return this.input.selectionEnd;
      }
    }, {
      key: "value",
      get: function get() {
        return this.input.value;
      },
      set: function set(value) {
        this.input.value = value;
      }
    }]);

    return HTMLMaskElement;
  }(MaskElement);
  HTMLMaskElement.EVENTS_MAP = {
    selectionChange: 'keydown',
    input: 'input',
    drop: 'drop',
    click: 'click',
    focus: 'focus',
    commit: 'change'
  };

  /** Listens to element events and controls changes between element and {@link Masked} */

  var InputMask =
  /*#__PURE__*/
  function () {
    /**
      View element
      @readonly
    */

    /**
      Internal {@link Masked} model
      @readonly
    */

    /**
      @param {MaskElement|HTMLInputElement|HTMLTextAreaElement} el
      @param {Object} opts
    */
    function InputMask(el, opts) {
      _classCallCheck(this, InputMask);

      this.el = el instanceof MaskElement ? el : new HTMLMaskElement(el);
      this.masked = createMask(opts);
      this._listeners = {};
      this._value = '';
      this._unmaskedValue = '';
      this._saveSelection = this._saveSelection.bind(this);
      this._onInput = this._onInput.bind(this);
      this._onChange = this._onChange.bind(this);
      this._onDrop = this._onDrop.bind(this);
      this.alignCursor = this.alignCursor.bind(this);
      this.alignCursorFriendly = this.alignCursorFriendly.bind(this);

      this._bindEvents(); // refresh


      this.updateValue();

      this._onChange();
    }
    /** Read or update mask */


    _createClass(InputMask, [{
      key: "_bindEvents",

      /**
        Starts listening to element events
        @protected
      */
      value: function _bindEvents() {
        this.el.bindEvents({
          selectionChange: this._saveSelection,
          input: this._onInput,
          drop: this._onDrop,
          click: this.alignCursorFriendly,
          focus: this.alignCursorFriendly,
          commit: this._onChange
        });
      }
      /**
        Stops listening to element events
        @protected
       */

    }, {
      key: "_unbindEvents",
      value: function _unbindEvents() {
        this.el.unbindEvents();
      }
      /**
        Fires custom event
        @protected
       */

    }, {
      key: "_fireEvent",
      value: function _fireEvent(ev) {
        var listeners = this._listeners[ev];
        if (!listeners) return;
        listeners.forEach(function (l) {
          return l();
        });
      }
      /**
        Current selection start
        @readonly
      */

    }, {
      key: "_saveSelection",

      /**
        Stores current selection
        @protected
      */
      value: function _saveSelection()
      /* ev */
      {
        if (this.value !== this.el.value) {
          console.warn('Element value was changed outside of mask. Syncronize mask using `mask.updateValue()` to work properly.'); // eslint-disable-line no-console
        }

        this._selection = {
          start: this.selectionStart,
          end: this.cursorPos
        };
      }
      /** Syncronizes model value from view */

    }, {
      key: "updateValue",
      value: function updateValue() {
        this.masked.value = this.el.value;
      }
      /** Syncronizes view from model value, fires change events */

    }, {
      key: "updateControl",
      value: function updateControl() {
        var newUnmaskedValue = this.masked.unmaskedValue;
        var newValue = this.masked.value;
        var isChanged = this.unmaskedValue !== newUnmaskedValue || this.value !== newValue;
        this._unmaskedValue = newUnmaskedValue;
        this._value = newValue;
        if (this.el.value !== newValue) this.el.value = newValue;
        if (isChanged) this._fireChangeEvents();
      }
      /** Updates options with deep equal check, recreates @{link Masked} model if mask type changes */

    }, {
      key: "updateOptions",
      value: function updateOptions(opts) {
        opts = _objectSpread({}, opts);
        this.mask = opts.mask;
        delete opts.mask; // check if changed

        if (!objectIncludes(this.masked, opts)) {
          this.masked.updateOptions(opts);
        }

        this.updateControl();
      }
      /** Updates cursor */

    }, {
      key: "updateCursor",
      value: function updateCursor(cursorPos) {
        if (cursorPos == null) return;
        this.cursorPos = cursorPos; // also queue change cursor for mobile browsers

        this._delayUpdateCursor(cursorPos);
      }
      /**
        Delays cursor update to support mobile browsers
        @private
      */

    }, {
      key: "_delayUpdateCursor",
      value: function _delayUpdateCursor(cursorPos) {
        var _this = this;

        this._abortUpdateCursor();

        this._changingCursorPos = cursorPos;
        this._cursorChanging = setTimeout(function () {
          if (!_this.el) return; // if was destroyed

          _this.cursorPos = _this._changingCursorPos;

          _this._abortUpdateCursor();
        }, 10);
      }
      /**
        Fires custom events
        @protected
      */

    }, {
      key: "_fireChangeEvents",
      value: function _fireChangeEvents() {
        this._fireEvent('accept');

        if (this.masked.isComplete) this._fireEvent('complete');
      }
      /**
        Aborts delayed cursor update
        @private
      */

    }, {
      key: "_abortUpdateCursor",
      value: function _abortUpdateCursor() {
        if (this._cursorChanging) {
          clearTimeout(this._cursorChanging);
          delete this._cursorChanging;
        }
      }
      /** Aligns cursor to nearest available position */

    }, {
      key: "alignCursor",
      value: function alignCursor() {
        this.cursorPos = this.masked.nearestInputPos(this.cursorPos, DIRECTION.LEFT);
      }
      /** Aligns cursor only if selection is empty */

    }, {
      key: "alignCursorFriendly",
      value: function alignCursorFriendly() {
        if (this.selectionStart !== this.cursorPos) return;
        this.alignCursor();
      }
      /** Adds listener on custom event */

    }, {
      key: "on",
      value: function on(ev, handler) {
        if (!this._listeners[ev]) this._listeners[ev] = [];

        this._listeners[ev].push(handler);

        return this;
      }
      /** Removes custom event listener */

    }, {
      key: "off",
      value: function off(ev, handler) {
        if (!this._listeners[ev]) return;

        if (!handler) {
          delete this._listeners[ev];
          return;
        }

        var hIndex = this._listeners[ev].indexOf(handler);

        if (hIndex >= 0) this._listeners[ev].splice(hIndex, 1);
        return this;
      }
      /** Handles view input event */

    }, {
      key: "_onInput",
      value: function _onInput() {
        this._abortUpdateCursor(); // fix strange IE behavior


        if (!this._selection) return this.updateValue();
        var details = new ActionDetails( // new state
        this.el.value, this.cursorPos, // old state
        this.value, this._selection);
        var offset = this.masked.splice(details.startChangePos, details.removed.length, details.inserted, details.removeDirection).offset;
        var cursorPos = this.masked.nearestInputPos(details.startChangePos + offset, details.removeDirection);
        this.updateControl();
        this.updateCursor(cursorPos);
      }
      /** Handles view change event and commits model value */

    }, {
      key: "_onChange",
      value: function _onChange() {
        if (this.value !== this.el.value) {
          this.updateValue();
        }

        this.masked.doCommit();
        this.updateControl();
      }
      /** Handles view drop event, prevents by default */

    }, {
      key: "_onDrop",
      value: function _onDrop(ev) {
        ev.preventDefault();
        ev.stopPropagation();
      }
      /** Unbind view events and removes element reference */

    }, {
      key: "destroy",
      value: function destroy() {
        this._unbindEvents(); // $FlowFixMe why not do so?


        this._listeners.length = 0;
        delete this.el;
      }
    }, {
      key: "mask",
      get: function get() {
        return this.masked.mask;
      },
      set: function set(mask) {
        if (mask == null || mask === this.masked.mask || mask === Date && this.masked instanceof MaskedDate) return;

        if (this.masked.constructor === maskedClass(mask)) {
          this.masked.updateOptions({
            mask: mask
          });
          return;
        }

        var masked = createMask({
          mask: mask
        });
        masked.unmaskedValue = this.masked.unmaskedValue;
        this.masked = masked;
      }
      /** Raw value */

    }, {
      key: "value",
      get: function get() {
        return this._value;
      },
      set: function set(str) {
        this.masked.value = str;
        this.updateControl();
        this.alignCursor();
      }
      /** Unmasked value */

    }, {
      key: "unmaskedValue",
      get: function get() {
        return this._unmaskedValue;
      },
      set: function set(str) {
        this.masked.unmaskedValue = str;
        this.updateControl();
        this.alignCursor();
      }
      /** Typed unmasked value */

    }, {
      key: "typedValue",
      get: function get() {
        return this.masked.typedValue;
      },
      set: function set(val) {
        this.masked.typedValue = val;
        this.updateControl();
        this.alignCursor();
      }
    }, {
      key: "selectionStart",
      get: function get() {
        return this._cursorChanging ? this._changingCursorPos : this.el.selectionStart;
      }
      /** Current cursor position */

    }, {
      key: "cursorPos",
      get: function get() {
        return this._cursorChanging ? this._changingCursorPos : this.el.selectionEnd;
      },
      set: function set(pos) {
        if (!this.el.isActive) return;
        this.el.select(pos, pos);

        this._saveSelection();
      }
    }]);

    return InputMask;
  }();

  /** Pattern which validates enum values */

  var MaskedEnum =
  /*#__PURE__*/
  function (_MaskedPattern) {
    _inherits(MaskedEnum, _MaskedPattern);

    function MaskedEnum() {
      _classCallCheck(this, MaskedEnum);

      return _possibleConstructorReturn(this, _getPrototypeOf(MaskedEnum).apply(this, arguments));
    }

    _createClass(MaskedEnum, [{
      key: "_update",

      /**
        @override
        @param {Object} opts
      */
      value: function _update(opts) {
        // TODO type
        if (opts.enum) opts.mask = '*'.repeat(opts.enum[0].length);

        _get(_getPrototypeOf(MaskedEnum.prototype), "_update", this).call(this, opts);
      }
      /**
        @override
      */

    }, {
      key: "doValidate",
      value: function doValidate() {
        var _this = this,
            _get2;

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return this.enum.some(function (e) {
          return e.indexOf(_this.unmaskedValue) >= 0;
        }) && (_get2 = _get(_getPrototypeOf(MaskedEnum.prototype), "doValidate", this)).call.apply(_get2, [this].concat(args));
      }
    }]);

    return MaskedEnum;
  }(MaskedPattern);

  /**
    Number mask
    @param {Object} opts
    @param {string} opts.radix - Single char
    @param {string} opts.thousandsSeparator - Single char
    @param {Array<string>} opts.mapToRadix - Array of single chars
    @param {number} opts.min
    @param {number} opts.max
    @param {number} opts.scale - Digits after point
    @param {boolean} opts.signed - Allow negative
    @param {boolean} opts.normalizeZeros - Flag to remove leading and trailing zeros in the end of editing
    @param {boolean} opts.padFractionalZeros - Flag to pad trailing zeros after point in the end of editing
  */
  var MaskedNumber =
  /*#__PURE__*/
  function (_Masked) {
    _inherits(MaskedNumber, _Masked);

    /** Single char */

    /** Single char */

    /** Array of single chars */

    /** */

    /** */

    /** Digits after point */

    /** */

    /** Flag to remove leading and trailing zeros in the end of editing */

    /** Flag to pad trailing zeros after point in the end of editing */
    function MaskedNumber(opts) {
      _classCallCheck(this, MaskedNumber);

      return _possibleConstructorReturn(this, _getPrototypeOf(MaskedNumber).call(this, _objectSpread({}, MaskedNumber.DEFAULTS, opts)));
    }
    /**
      @override
    */


    _createClass(MaskedNumber, [{
      key: "_update",
      value: function _update(opts) {
        _get(_getPrototypeOf(MaskedNumber.prototype), "_update", this).call(this, opts);

        this._updateRegExps();
      }
      /** */

    }, {
      key: "_updateRegExps",
      value: function _updateRegExps() {
        // use different regexp to process user input (more strict, input suffix) and tail shifting
        var start = '^';
        var midInput = '';
        var mid = '';

        if (this.allowNegative) {
          midInput += '([+|\\-]?|([+|\\-]?(0|([1-9]+\\d*))))';
          mid += '[+|\\-]?';
        } else {
          midInput += '(0|([1-9]+\\d*))';
        }

        mid += '\\d*';
        var end = (this.scale ? '(' + escapeRegExp(this.radix) + '\\d{0,' + this.scale + '})?' : '') + '$';
        this._numberRegExpInput = new RegExp(start + midInput + end);
        this._numberRegExp = new RegExp(start + mid + end);
        this._mapToRadixRegExp = new RegExp('[' + this.mapToRadix.map(escapeRegExp).join('') + ']', 'g');
        this._thousandsSeparatorRegExp = new RegExp(escapeRegExp(this.thousandsSeparator), 'g');
      }
      /**
        @override
      */

    }, {
      key: "extractTail",
      value: function extractTail() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;

        var tail = _get(_getPrototypeOf(MaskedNumber.prototype), "extractTail", this).call(this, fromPos, toPos); // $FlowFixMe no ideas


        return _objectSpread({}, tail, {
          value: this._removeThousandsSeparators(tail.value)
        });
      }
      /** */

    }, {
      key: "_removeThousandsSeparators",
      value: function _removeThousandsSeparators(value) {
        return value.replace(this._thousandsSeparatorRegExp, '');
      }
      /** */

    }, {
      key: "_insertThousandsSeparators",
      value: function _insertThousandsSeparators(value) {
        // https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
        var parts = value.split(this.radix);
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, this.thousandsSeparator);
        return parts.join(this.radix);
      }
      /**
        @override
      */

    }, {
      key: "doPrepare",
      value: function doPrepare(str) {
        var _get2;

        for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        return (_get2 = _get(_getPrototypeOf(MaskedNumber.prototype), "doPrepare", this)).call.apply(_get2, [this, this._removeThousandsSeparators(str.replace(this._mapToRadixRegExp, this.radix))].concat(args));
      }
      /** */

    }, {
      key: "_separatorsCount",
      value: function _separatorsCount() {
        var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._value;

        var rawValueLength = this._removeThousandsSeparators(value).length;

        var valueWithSeparatorsLength = rawValueLength;

        for (var pos = 0; pos <= valueWithSeparatorsLength; ++pos) {
          if (this._value[pos] === this.thousandsSeparator) ++valueWithSeparatorsLength;
        }

        return valueWithSeparatorsLength - rawValueLength;
      }
      /**
        @override
      */

    }, {
      key: "_appendCharRaw",
      value: function _appendCharRaw(ch) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        if (!this.thousandsSeparator) return _get(_getPrototypeOf(MaskedNumber.prototype), "_appendCharRaw", this).call(this, ch, flags);

        var previousBeforeTailSeparatorsCount = this._separatorsCount(flags.tail && this._beforeTailState ? this._beforeTailState._value : this._value);

        this._value = this._removeThousandsSeparators(this.value);

        var appendDetails = _get(_getPrototypeOf(MaskedNumber.prototype), "_appendCharRaw", this).call(this, ch, flags);

        this._value = this._insertThousandsSeparators(this._value);

        var beforeTailSeparatorsCount = this._separatorsCount(flags.tail && this._beforeTailState ? this._beforeTailState._value : this._value);

        appendDetails.tailShift += beforeTailSeparatorsCount - previousBeforeTailSeparatorsCount;
        return appendDetails;
      }
      /**
        @override
      */

    }, {
      key: "remove",
      value: function remove() {
        var fromPos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var toPos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value.length;
        var valueBeforePos = this.value.slice(0, fromPos);
        var valueAfterPos = this.value.slice(toPos);

        var previousBeforeTailSeparatorsCount = this._separatorsCount(valueBeforePos);

        this._value = this._insertThousandsSeparators(this._removeThousandsSeparators(valueBeforePos + valueAfterPos));

        var beforeTailSeparatorsCount = this._separatorsCount(valueBeforePos);

        return new ChangeDetails({
          tailShift: beforeTailSeparatorsCount - previousBeforeTailSeparatorsCount
        });
      }
      /**
        @override
      */

    }, {
      key: "nearestInputPos",
      value: function nearestInputPos(cursorPos, direction) {
        if (!direction) return cursorPos;
        var nextPos = indexInDirection(cursorPos, direction);
        if (this.value[nextPos] === this.thousandsSeparator) cursorPos = posInDirection(cursorPos, direction);
        return cursorPos;
      }
      /**
        @override
      */

    }, {
      key: "doValidate",
      value: function doValidate(flags) {
        var regexp = flags.input ? this._numberRegExpInput : this._numberRegExp; // validate as string

        var valid = regexp.test(this._removeThousandsSeparators(this.value));

        if (valid) {
          // validate as number
          var number = this.number;
          valid = valid && !isNaN(number) && ( // check min bound for negative values
          this.min == null || this.min >= 0 || this.min <= this.number) && ( // check max bound for positive values
          this.max == null || this.max <= 0 || this.number <= this.max);
        }

        return valid && _get(_getPrototypeOf(MaskedNumber.prototype), "doValidate", this).call(this, flags);
      }
      /**
        @override
      */

    }, {
      key: "doCommit",
      value: function doCommit() {
        var number = this.number;
        var validnum = number; // check bounds

        if (this.min != null) validnum = Math.max(validnum, this.min);
        if (this.max != null) validnum = Math.min(validnum, this.max);
        if (validnum !== number) this.unmaskedValue = String(validnum);
        var formatted = this.value;
        if (this.normalizeZeros) formatted = this._normalizeZeros(formatted);
        if (this.padFractionalZeros) formatted = this._padFractionalZeros(formatted);
        this._value = this._insertThousandsSeparators(formatted);

        _get(_getPrototypeOf(MaskedNumber.prototype), "doCommit", this).call(this);
      }
      /** */

    }, {
      key: "_normalizeZeros",
      value: function _normalizeZeros(value) {
        var parts = this._removeThousandsSeparators(value).split(this.radix); // remove leading zeros


        parts[0] = parts[0].replace(/^(\D*)(0*)(\d*)/, function (match, sign, zeros, num) {
          return sign + num;
        }); // add leading zero

        if (value.length && !/\d$/.test(parts[0])) parts[0] = parts[0] + '0';

        if (parts.length > 1) {
          parts[1] = parts[1].replace(/0*$/, ''); // remove trailing zeros

          if (!parts[1].length) parts.length = 1; // remove fractional
        }

        return this._insertThousandsSeparators(parts.join(this.radix));
      }
      /** */

    }, {
      key: "_padFractionalZeros",
      value: function _padFractionalZeros(value) {
        if (!value) return value;
        var parts = value.split(this.radix);
        if (parts.length < 2) parts.push('');
        parts[1] = parts[1].padEnd(this.scale, '0');
        return parts.join(this.radix);
      }
      /**
        @override
      */

    }, {
      key: "unmaskedValue",
      get: function get$$1() {
        return this._removeThousandsSeparators(this._normalizeZeros(this.value)).replace(this.radix, '.');
      },
      set: function set$$1(unmaskedValue) {
        _set(_getPrototypeOf(MaskedNumber.prototype), "unmaskedValue", unmaskedValue.replace('.', this.radix), this, true);
      }
      /** Parsed Number */

    }, {
      key: "number",
      get: function get$$1() {
        return Number(this.unmaskedValue);
      },
      set: function set$$1(number) {
        this.unmaskedValue = String(number);
      }
      /**
        @override
      */

    }, {
      key: "typedValue",
      get: function get$$1() {
        return this.number;
      },
      set: function set$$1(value) {
        this.number = value;
      }
      /**
        Is negative allowed
        @readonly
      */

    }, {
      key: "allowNegative",
      get: function get$$1() {
        return this.signed || this.min != null && this.min < 0 || this.max != null && this.max < 0;
      }
    }]);

    return MaskedNumber;
  }(Masked);
  MaskedNumber.DEFAULTS = {
    radix: ',',
    thousandsSeparator: '',
    mapToRadix: ['.'],
    scale: 2,
    signed: false,
    normalizeZeros: true,
    padFractionalZeros: false
  };

  /** Masking by RegExp */

  var MaskedRegExp =
  /*#__PURE__*/
  function (_Masked) {
    _inherits(MaskedRegExp, _Masked);

    function MaskedRegExp() {
      _classCallCheck(this, MaskedRegExp);

      return _possibleConstructorReturn(this, _getPrototypeOf(MaskedRegExp).apply(this, arguments));
    }

    _createClass(MaskedRegExp, [{
      key: "_update",

      /**
        @override
        @param {Object} opts
      */
      value: function _update(opts) {
        opts.validate = function (value) {
          return value.search(opts.mask) >= 0;
        };

        _get(_getPrototypeOf(MaskedRegExp.prototype), "_update", this).call(this, opts);
      }
    }]);

    return MaskedRegExp;
  }(Masked);

  /** Masking by custom Function */

  var MaskedFunction =
  /*#__PURE__*/
  function (_Masked) {
    _inherits(MaskedFunction, _Masked);

    function MaskedFunction() {
      _classCallCheck(this, MaskedFunction);

      return _possibleConstructorReturn(this, _getPrototypeOf(MaskedFunction).apply(this, arguments));
    }

    _createClass(MaskedFunction, [{
      key: "_update",

      /**
        @override
        @param {Object} opts
      */
      value: function _update(opts) {
        opts.validate = opts.mask;

        _get(_getPrototypeOf(MaskedFunction.prototype), "_update", this).call(this, opts);
      }
    }]);

    return MaskedFunction;
  }(Masked);

  /** Dynamic mask for choosing apropriate mask in run-time */
  var MaskedDynamic =
  /*#__PURE__*/
  function (_Masked) {
    _inherits(MaskedDynamic, _Masked);

    /** Currently chosen mask */

    /** Compliled {@link Masked} options */

    /** Chooses {@link Masked} depending on input value */

    /**
      @param {Object} opts
    */
    function MaskedDynamic(opts) {
      var _this;

      _classCallCheck(this, MaskedDynamic);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(MaskedDynamic).call(this, _objectSpread({}, MaskedDynamic.DEFAULTS, opts)));
      _this.currentMask = null;
      return _this;
    }
    /**
      @override
    */


    _createClass(MaskedDynamic, [{
      key: "_update",
      value: function _update(opts) {
        _get(_getPrototypeOf(MaskedDynamic.prototype), "_update", this).call(this, opts); // mask could be totally dynamic with only `dispatch` option


        this.compiledMasks = Array.isArray(opts.mask) ? opts.mask.map(function (m) {
          return createMask(m);
        }) : [];
      }
      /**
        @override
      */

    }, {
      key: "_appendCharRaw",
      value: function _appendCharRaw() {
        var details = this._applyDispatch.apply(this, arguments);

        if (this.currentMask) {
          var _this$currentMask;

          details.aggregate((_this$currentMask = this.currentMask)._appendChar.apply(_this$currentMask, arguments));
        }

        return details;
      }
      /**
        @override
      */

    }, {
      key: "_storeBeforeTailState",
      value: function _storeBeforeTailState() {
        _get(_getPrototypeOf(MaskedDynamic.prototype), "_storeBeforeTailState", this).call(this);

        if (this.currentMask) this.currentMask._storeBeforeTailState();
      }
      /**
        @override
      */

    }, {
      key: "_restoreBeforeTailState",
      value: function _restoreBeforeTailState() {
        _get(_getPrototypeOf(MaskedDynamic.prototype), "_restoreBeforeTailState", this).call(this);

        if (this.currentMask) this.currentMask._restoreBeforeTailState();
      }
      /**
        @override
      */

    }, {
      key: "_resetBeforeTailState",
      value: function _resetBeforeTailState() {
        _get(_getPrototypeOf(MaskedDynamic.prototype), "_resetBeforeTailState", this).call(this);

        if (this.currentMask) this.currentMask._resetBeforeTailState();
      }
    }, {
      key: "_applyDispatch",
      value: function _applyDispatch() {
        var appended = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        var prevValueBeforeTail = flags.tail && this._beforeTailState ? this._beforeTailState._value : this.value;
        var inputValue = this.rawInputValue;
        var insertValue = flags.tail && this._beforeTailState ? // $FlowFixMe - tired to fight with type system
        this._beforeTailState._rawInputValue : inputValue;
        var tailValue = inputValue.slice(insertValue.length);
        var prevMask = this.currentMask;
        var details = new ChangeDetails();
        var prevMaskState = prevMask && prevMask.state;
        var prevMaskBeforeTailState = prevMask && prevMask._beforeTailState;
        this.currentMask = this.doDispatch(appended, flags); // restore state after dispatch

        if (this.currentMask) {
          if (this.currentMask !== prevMask) {
            // if mask changed reapply input
            this.currentMask.reset(); // $FlowFixMe - it's ok, we don't change current mask above

            var d = this.currentMask.append(insertValue, {
              raw: true
            });
            details.tailShift = d.inserted.length - prevValueBeforeTail.length;

            this._storeBeforeTailState();

            if (tailValue) {
              // $FlowFixMe - it's ok, we don't change current mask above
              details.tailShift += this.currentMask.append(tailValue, {
                raw: true,
                tail: true
              }).tailShift;
            }
          } else {
            // Dispatch can do something bad with state, so
            // restore prev mask state
            this.currentMask.state = prevMaskState;
            this.currentMask._beforeTailState = prevMaskBeforeTailState;
          }
        }

        return details;
      }
      /**
        @override
      */

    }, {
      key: "doDispatch",
      value: function doDispatch(appended) {
        var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return this.dispatch(appended, this, flags);
      }
      /**
        @override
      */

    }, {
      key: "doValidate",
      value: function doValidate() {
        var _get2, _this$currentMask2;

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return (_get2 = _get(_getPrototypeOf(MaskedDynamic.prototype), "doValidate", this)).call.apply(_get2, [this].concat(args)) && (!this.currentMask || (_this$currentMask2 = this.currentMask).doValidate.apply(_this$currentMask2, args));
      }
      /**
        @override
      */

    }, {
      key: "reset",
      value: function reset() {
        if (this.currentMask) this.currentMask.reset();
        this.compiledMasks.forEach(function (m) {
          return m.reset();
        });
      }
      /**
        @override
      */

    }, {
      key: "remove",

      /**
        @override
      */
      value: function remove() {
        var details = new ChangeDetails();

        if (this.currentMask) {
          var _this$currentMask3;

          details.aggregate((_this$currentMask3 = this.currentMask).remove.apply(_this$currentMask3, arguments)) // update with dispatch
          .aggregate(this._applyDispatch());
        }

        return details;
      }
      /**
        @override
      */

    }, {
      key: "extractInput",

      /**
        @override
      */
      value: function extractInput() {
        var _this$currentMask4;

        return this.currentMask ? (_this$currentMask4 = this.currentMask).extractInput.apply(_this$currentMask4, arguments) : '';
      }
      /**
        @override
      */

    }, {
      key: "extractTail",
      value: function extractTail() {
        var _this$currentMask5, _get3;

        for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        return this.currentMask ? (_this$currentMask5 = this.currentMask).extractTail.apply(_this$currentMask5, args) : (_get3 = _get(_getPrototypeOf(MaskedDynamic.prototype), "extractTail", this)).call.apply(_get3, [this].concat(args));
      }
      /**
        @override
      */

    }, {
      key: "doCommit",
      value: function doCommit() {
        if (this.currentMask) this.currentMask.doCommit();

        _get(_getPrototypeOf(MaskedDynamic.prototype), "doCommit", this).call(this);
      }
      /**
        @override
      */

    }, {
      key: "nearestInputPos",
      value: function nearestInputPos() {
        var _this$currentMask6, _get4;

        for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
          args[_key3] = arguments[_key3];
        }

        return this.currentMask ? (_this$currentMask6 = this.currentMask).nearestInputPos.apply(_this$currentMask6, args) : (_get4 = _get(_getPrototypeOf(MaskedDynamic.prototype), "nearestInputPos", this)).call.apply(_get4, [this].concat(args));
      }
    }, {
      key: "value",
      get: function get$$1() {
        return this.currentMask ? this.currentMask.value : '';
      },
      set: function set$$1(value) {
        _set(_getPrototypeOf(MaskedDynamic.prototype), "value", value, this, true);
      }
      /**
        @override
      */

    }, {
      key: "unmaskedValue",
      get: function get$$1() {
        return this.currentMask ? this.currentMask.unmaskedValue : '';
      },
      set: function set$$1(unmaskedValue) {
        _set(_getPrototypeOf(MaskedDynamic.prototype), "unmaskedValue", unmaskedValue, this, true);
      }
      /**
        @override
      */

    }, {
      key: "typedValue",
      get: function get$$1() {
        return this.currentMask ? this.currentMask.typedValue : '';
      } // probably typedValue should not be used with dynamic
      ,
      set: function set$$1(value) {
        var unmaskedValue = String(value); // double check it

        if (this.currentMask) {
          this.currentMask.typedValue = value;
          unmaskedValue = this.currentMask.unmaskedValue;
        }

        this.unmaskedValue = unmaskedValue;
      }
      /**
        @override
      */

    }, {
      key: "isComplete",
      get: function get$$1() {
        return !!this.currentMask && this.currentMask.isComplete;
      }
    }, {
      key: "state",
      get: function get$$1() {
        return _objectSpread({}, _get(_getPrototypeOf(MaskedDynamic.prototype), "state", this), {
          _rawInputValue: this.rawInputValue,
          compiledMasks: this.compiledMasks.map(function (m) {
            return m.state;
          }),
          currentMaskRef: this.currentMask,
          currentMask: this.currentMask && this.currentMask.state
        });
      },
      set: function set$$1(state) {
        var compiledMasks = state.compiledMasks,
            currentMaskRef = state.currentMaskRef,
            currentMask = state.currentMask,
            maskedState = _objectWithoutProperties(state, ["compiledMasks", "currentMaskRef", "currentMask"]);

        this.compiledMasks.forEach(function (m, mi) {
          return m.state = compiledMasks[mi];
        });

        if (currentMaskRef != null) {
          this.currentMask = currentMaskRef;
          this.currentMask.state = currentMask;
        }

        _set(_getPrototypeOf(MaskedDynamic.prototype), "state", maskedState, this, true);
      }
    }]);

    return MaskedDynamic;
  }(Masked);
  MaskedDynamic.DEFAULTS = {
    dispatch: function dispatch(appended, masked, flags) {
      if (!masked.compiledMasks.length) return;
      var inputValue = masked.rawInputValue; // simulate input

      var inputs = masked.compiledMasks.map(function (m, index) {
        m.rawInputValue = inputValue;
        m.append(appended, flags);
        var weight = m.rawInputValue.length;
        return {
          weight: weight,
          index: index
        };
      }); // pop masks with longer values first

      inputs.sort(function (i1, i2) {
        return i2.weight - i1.weight;
      });
      return masked.compiledMasks[inputs[0].index];
    }
  };

  /**
   * Applies mask on element.
   * @constructor
   * @param {HTMLInputElement|HTMLTextAreaElement|MaskElement} el - Element to apply mask
   * @param {Object} opts - Custom mask options
   * @return {InputMask}
   */

  function IMask(el) {
    var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    // currently available only for input-like elements
    return new InputMask(el, opts);
  }
  /** {@link InputMask} */

  IMask.InputMask = InputMask;
  /** {@link Masked} */

  IMask.Masked = Masked;
  /** {@link MaskedPattern} */

  IMask.MaskedPattern = MaskedPattern;
  /** {@link MaskedEnum} */

  IMask.MaskedEnum = MaskedEnum;
  /** {@link MaskedRange} */

  IMask.MaskedRange = MaskedRange;
  /** {@link MaskedNumber} */

  IMask.MaskedNumber = MaskedNumber;
  /** {@link MaskedDate} */

  IMask.MaskedDate = MaskedDate;
  /** {@link MaskedRegExp} */

  IMask.MaskedRegExp = MaskedRegExp;
  /** {@link MaskedFunction} */

  IMask.MaskedFunction = MaskedFunction;
  /** {@link MaskedDynamic} */

  IMask.MaskedDynamic = MaskedDynamic;
  /** {@link createMask} */

  IMask.createMask = createMask;
  /** {@link MaskElement} */

  IMask.MaskElement = MaskElement;
  /** {@link HTMLMaskElement} */

  IMask.HTMLMaskElement = HTMLMaskElement;
  g.IMask = IMask;

  return IMask;

})));
//# sourceMappingURL=imask.js.map

},{}],"C:\\Users\\Алексей\\AppData\\Roaming\\npm\\node_modules\\parcel-bundler\\node_modules\\process\\browser.js":[function(require,module,exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout() {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
})();
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch (e) {
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch (e) {
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }
}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e) {
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e) {
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }
}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while (len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) {
    return [];
};

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () {
    return '/';
};
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function () {
    return 0;
};
},{}],"..\\node_modules\\jquery\\dist\\jquery.js":[function(require,module,exports) {
var global = arguments[3];
var process = require("process");
var define;
/*!
 * jQuery JavaScript Library v3.3.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2018-01-20T17:24Z
 */
( function( global, factory ) {

	"use strict";

	if ( typeof module === "object" && typeof module.exports === "object" ) {

		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
// throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
// arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
// enough that all such attempts are guarded in a try block.
"use strict";

var arr = [];

var document = window.document;

var getProto = Object.getPrototypeOf;

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var fnToString = hasOwn.toString;

var ObjectFunctionString = fnToString.call( Object );

var support = {};

var isFunction = function isFunction( obj ) {

      // Support: Chrome <=57, Firefox <=52
      // In some browsers, typeof returns "function" for HTML <object> elements
      // (i.e., `typeof document.createElement( "object" ) === "function"`).
      // We don't want to classify *any* DOM node as a function.
      return typeof obj === "function" && typeof obj.nodeType !== "number";
  };


var isWindow = function isWindow( obj ) {
		return obj != null && obj === obj.window;
	};




	var preservedScriptAttributes = {
		type: true,
		src: true,
		noModule: true
	};

	function DOMEval( code, doc, node ) {
		doc = doc || document;

		var i,
			script = doc.createElement( "script" );

		script.text = code;
		if ( node ) {
			for ( i in preservedScriptAttributes ) {
				if ( node[ i ] ) {
					script[ i ] = node[ i ];
				}
			}
		}
		doc.head.appendChild( script ).parentNode.removeChild( script );
	}


function toType( obj ) {
	if ( obj == null ) {
		return obj + "";
	}

	// Support: Android <=2.3 only (functionish RegExp)
	return typeof obj === "object" || typeof obj === "function" ?
		class2type[ toString.call( obj ) ] || "object" :
		typeof obj;
}
/* global Symbol */
// Defining this global in .eslintrc.json would create a danger of using the global
// unguarded in another place, it seems safer to define global only for this module



var
	version = "3.3.1",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {

		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android <=4.0 only
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

jQuery.fn = jQuery.prototype = {

	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {

		// Return all the elements in a clean array
		if ( num == null ) {
			return slice.call( this );
		}

		// Return just the one element from the set
		return num < 0 ? this[ num + this.length ] : this[ num ];
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	each: function( callback ) {
		return jQuery.each( this, callback );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map( this, function( elem, i ) {
			return callback.call( elem, i, elem );
		} ) );
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor();
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[ 0 ] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !isFunction( target ) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {

		// Only deal with non-null/undefined values
		if ( ( options = arguments[ i ] ) != null ) {

			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
					( copyIsArray = Array.isArray( copy ) ) ) ) {

					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && Array.isArray( src ) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject( src ) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend( {

	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isPlainObject: function( obj ) {
		var proto, Ctor;

		// Detect obvious negatives
		// Use toString instead of jQuery.type to catch host objects
		if ( !obj || toString.call( obj ) !== "[object Object]" ) {
			return false;
		}

		proto = getProto( obj );

		// Objects with no prototype (e.g., `Object.create( null )`) are plain
		if ( !proto ) {
			return true;
		}

		// Objects with prototype are plain iff they were constructed by a global Object function
		Ctor = hasOwn.call( proto, "constructor" ) && proto.constructor;
		return typeof Ctor === "function" && fnToString.call( Ctor ) === ObjectFunctionString;
	},

	isEmptyObject: function( obj ) {

		/* eslint-disable no-unused-vars */
		// See https://github.com/eslint/eslint/issues/6125
		var name;

		for ( name in obj ) {
			return false;
		}
		return true;
	},

	// Evaluates a script in a global context
	globalEval: function( code ) {
		DOMEval( code );
	},

	each: function( obj, callback ) {
		var length, i = 0;

		if ( isArrayLike( obj ) ) {
			length = obj.length;
			for ( ; i < length; i++ ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else {
			for ( i in obj ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		}

		return obj;
	},

	// Support: Android <=4.0 only
	trim: function( text ) {
		return text == null ?
			"" :
			( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArrayLike( Object( arr ) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	// Support: Android <=4.0 only, PhantomJS 1 only
	// push.apply(_, arraylike) throws on ancient WebKit
	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var length, value,
			i = 0,
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArrayLike( elems ) ) {
			length = elems.length;
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
} );

if ( typeof Symbol === "function" ) {
	jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( i, name ) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

	// Support: real iOS 8.2 only (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = !!obj && "length" in obj && obj.length,
		type = toType( obj );

	if ( isFunction( obj ) || isWindow( obj ) ) {
		return false;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.3.3
 * https://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2016-08-08
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// https://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",

	// http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
		"*\\]",

	pseudos = ":(" + identifier + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + identifier + ")" ),
		"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
		"TAG": new RegExp( "^(" + identifier + "|[*])" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,

	// CSS escapes
	// http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// CSS string/identifier serialization
	// https://drafts.csswg.org/cssom/#common-serializing-idioms
	rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
	fcssescape = function( ch, asCodePoint ) {
		if ( asCodePoint ) {

			// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
			if ( ch === "\0" ) {
				return "\uFFFD";
			}

			// Control characters and (dependent upon position) numbers get escaped as code points
			return ch.slice( 0, -1 ) + "\\" + ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
		}

		// Other potentially-special ASCII characters get backslash-escaped
		return "\\" + ch;
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	},

	disabledAncestor = addCombinator(
		function( elem ) {
			return elem.disabled === true && ("form" in elem || "label" in elem);
		},
		{ dir: "parentNode", next: "legend" }
	);

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var m, i, elem, nid, match, groups, newSelector,
		newContext = context && context.ownerDocument,

		// nodeType defaults to 9, since context defaults to document
		nodeType = context ? context.nodeType : 9;

	results = results || [];

	// Return early from calls with invalid selector or context
	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	// Try to shortcut find operations (as opposed to filters) in HTML documents
	if ( !seed ) {

		if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
			setDocument( context );
		}
		context = context || document;

		if ( documentIsHTML ) {

			// If the selector is sufficiently simple, try using a "get*By*" DOM method
			// (excepting DocumentFragment context, where the methods don't exist)
			if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {

				// ID selector
				if ( (m = match[1]) ) {

					// Document context
					if ( nodeType === 9 ) {
						if ( (elem = context.getElementById( m )) ) {

							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}

					// Element context
					} else {

						// Support: IE, Opera, Webkit
						// TODO: identify versions
						// getElementById can match elements by name instead of ID
						if ( newContext && (elem = newContext.getElementById( m )) &&
							contains( context, elem ) &&
							elem.id === m ) {

							results.push( elem );
							return results;
						}
					}

				// Type selector
				} else if ( match[2] ) {
					push.apply( results, context.getElementsByTagName( selector ) );
					return results;

				// Class selector
				} else if ( (m = match[3]) && support.getElementsByClassName &&
					context.getElementsByClassName ) {

					push.apply( results, context.getElementsByClassName( m ) );
					return results;
				}
			}

			// Take advantage of querySelectorAll
			if ( support.qsa &&
				!compilerCache[ selector + " " ] &&
				(!rbuggyQSA || !rbuggyQSA.test( selector )) ) {

				if ( nodeType !== 1 ) {
					newContext = context;
					newSelector = selector;

				// qSA looks outside Element context, which is not what we want
				// Thanks to Andrew Dupont for this workaround technique
				// Support: IE <=8
				// Exclude object elements
				} else if ( context.nodeName.toLowerCase() !== "object" ) {

					// Capture the context ID, setting it first if necessary
					if ( (nid = context.getAttribute( "id" )) ) {
						nid = nid.replace( rcssescape, fcssescape );
					} else {
						context.setAttribute( "id", (nid = expando) );
					}

					// Prefix every selector in the list
					groups = tokenize( selector );
					i = groups.length;
					while ( i-- ) {
						groups[i] = "#" + nid + " " + toSelector( groups[i] );
					}
					newSelector = groups.join( "," );

					// Expand context for sibling selectors
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
						context;
				}

				if ( newSelector ) {
					try {
						push.apply( results,
							newContext.querySelectorAll( newSelector )
						);
						return results;
					} catch ( qsaError ) {
					} finally {
						if ( nid === expando ) {
							context.removeAttribute( "id" );
						}
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created element and returns a boolean result
 */
function assert( fn ) {
	var el = document.createElement("fieldset");

	try {
		return !!fn( el );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( el.parentNode ) {
			el.parentNode.removeChild( el );
		}
		// release memory in IE
		el = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = arr.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			a.sourceIndex - b.sourceIndex;

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for :enabled/:disabled
 * @param {Boolean} disabled true for :disabled; false for :enabled
 */
function createDisabledPseudo( disabled ) {

	// Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
	return function( elem ) {

		// Only certain elements can match :enabled or :disabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
		if ( "form" in elem ) {

			// Check for inherited disabledness on relevant non-disabled elements:
			// * listed form-associated elements in a disabled fieldset
			//   https://html.spec.whatwg.org/multipage/forms.html#category-listed
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
			// * option elements in a disabled optgroup
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
			// All such elements have a "form" property.
			if ( elem.parentNode && elem.disabled === false ) {

				// Option elements defer to a parent optgroup if present
				if ( "label" in elem ) {
					if ( "label" in elem.parentNode ) {
						return elem.parentNode.disabled === disabled;
					} else {
						return elem.disabled === disabled;
					}
				}

				// Support: IE 6 - 11
				// Use the isDisabled shortcut property to check for disabled fieldset ancestors
				return elem.isDisabled === disabled ||

					// Where there is no isDisabled, check manually
					/* jshint -W018 */
					elem.isDisabled !== !disabled &&
						disabledAncestor( elem ) === disabled;
			}

			return elem.disabled === disabled;

		// Try to winnow out elements that can't be disabled before trusting the disabled property.
		// Some victims get caught in our net (label, legend, menu, track), but it shouldn't
		// even exist on them, let alone have a boolean value.
		} else if ( "label" in elem ) {
			return elem.disabled === disabled;
		}

		// Remaining elements are neither :enabled nor :disabled
		return false;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, subWindow,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// Return early if doc is invalid or already selected
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Update global variables
	document = doc;
	docElem = document.documentElement;
	documentIsHTML = !isXML( document );

	// Support: IE 9-11, Edge
	// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
	if ( preferredDoc !== document &&
		(subWindow = document.defaultView) && subWindow.top !== subWindow ) {

		// Support: IE 11, Edge
		if ( subWindow.addEventListener ) {
			subWindow.addEventListener( "unload", unloadHandler, false );

		// Support: IE 9 - 10 only
		} else if ( subWindow.attachEvent ) {
			subWindow.attachEvent( "onunload", unloadHandler );
		}
	}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( el ) {
		el.className = "i";
		return !el.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( el ) {
		el.appendChild( document.createComment("") );
		return !el.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( document.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programmatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( el ) {
		docElem.appendChild( el ).id = expando;
		return !document.getElementsByName || !document.getElementsByName( expando ).length;
	});

	// ID filter and find
	if ( support.getById ) {
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var elem = context.getElementById( id );
				return elem ? [ elem ] : [];
			}
		};
	} else {
		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" &&
					elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};

		// Support: IE 6 - 7 only
		// getElementById is not reliable as a find shortcut
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var node, i, elems,
					elem = context.getElementById( id );

				if ( elem ) {

					// Verify the id attribute
					node = elem.getAttributeNode("id");
					if ( node && node.value === id ) {
						return [ elem ];
					}

					// Fall back on getElementsByName
					elems = context.getElementsByName( id );
					i = 0;
					while ( (elem = elems[i++]) ) {
						node = elem.getAttributeNode("id");
						if ( node && node.value === id ) {
							return [ elem ];
						}
					}
				}

				return [];
			}
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See https://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( el ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// https://bugs.jquery.com/ticket/12359
			docElem.appendChild( el ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\r\\' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( el.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !el.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
			if ( !el.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !el.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibling-combinator selector` fails
			if ( !el.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

		assert(function( el ) {
			el.innerHTML = "<a href='' disabled='disabled'></a>" +
				"<select disabled='disabled'><option/></select>";

			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = document.createElement("input");
			input.setAttribute( "type", "hidden" );
			el.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( el.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( el.querySelectorAll(":enabled").length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Support: IE9-11+
			// IE's :disabled selector does not pick up the children of disabled fieldsets
			docElem.appendChild( el ).disabled = true;
			if ( el.querySelectorAll(":disabled").length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			el.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( el ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( el, "*" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( el, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully self-exclusive
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === document ? -1 :
				b === document ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return document;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		!compilerCache[ expr + " " ] &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch (e) {}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.escape = function( sel ) {
	return (sel + "").replace( rcssescape, fcssescape );
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[6] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, uniqueCache, outerCache, node, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType,
						diff = false;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) {

										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {

							// Seek `elem` from a previously-cached index

							// ...in a gzip-friendly way
							node = parent;
							outerCache = node[ expando ] || (node[ expando ] = {});

							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ node.uniqueID ] ||
								(outerCache[ node.uniqueID ] = {});

							cache = uniqueCache[ type ] || [];
							nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
							diff = nodeIndex && cache[ 2 ];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						} else {
							// Use previously-cached element index if available
							if ( useCache ) {
								// ...in a gzip-friendly way
								node = elem;
								outerCache = node[ expando ] || (node[ expando ] = {});

								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									(outerCache[ node.uniqueID ] = {});

								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex;
							}

							// xml :nth-child(...)
							// or :nth-last-child(...) or :nth(-last)?-of-type(...)
							if ( diff === false ) {
								// Use the same loop as above to seek `elem` from the start
								while ( (node = ++nodeIndex && node && node[ dir ] ||
									(diff = nodeIndex = 0) || start.pop()) ) {

									if ( ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) &&
										++diff ) {

										// Cache the index of each encountered element
										if ( useCache ) {
											outerCache = node[ expando ] || (node[ expando ] = {});

											// Support: IE <9 only
											// Defend against cloned attroperties (jQuery gh-1709)
											uniqueCache = outerCache[ node.uniqueID ] ||
												(outerCache[ node.uniqueID ] = {});

											uniqueCache[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": createDisabledPseudo( false ),
		"disabled": createDisabledPseudo( true ),

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		skip = combinator.next,
		key = skip || dir,
		checkNonElements = base && key === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
			return false;
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, uniqueCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});

						// Support: IE <9 only
						// Defend against cloned attroperties (jQuery gh-1709)
						uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});

						if ( skip && skip === elem.nodeName.toLowerCase() ) {
							elem = elem[ dir ] || elem;
						} else if ( (oldCache = uniqueCache[ key ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							uniqueCache[ key ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
			return false;
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context === document || context || outermost;
			}

			// Add elements passing elementMatchers directly to results
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					if ( !context && elem.ownerDocument !== document ) {
						setDocument( elem );
						xml = !documentIsHTML;
					}
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context || document, xml) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// `i` is now the count of elements visited above, and adding it to `matchedCount`
			// makes the latter nonnegative.
			matchedCount += i;

			// Apply set filters to unmatched elements
			// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
			// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
			// no element matchers and no seed.
			// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
			// case, which will result in a "00" `matchedCount` that differs from `i` but is also
			// numerically zero.
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( (selector = compiled.selector || selector) );

	results = results || [];

	// Try to minimize operations if there is only one selector in the list and no seed
	// (the latter of which guarantees us context)
	if ( match.length === 1 ) {

		// Reduce context if the leading compound selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
				context.nodeType === 9 && documentIsHTML && Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
				)) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( el ) {
	// Should return 1, but returns 4 (following)
	return el.compareDocumentPosition( document.createElement("fieldset") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( el ) {
	el.innerHTML = "<a href='#'></a>";
	return el.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( el ) {
	el.innerHTML = "<input/>";
	el.firstChild.setAttribute( "value", "" );
	return el.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( el ) {
	return el.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;

// Deprecated
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;
jQuery.escapeSelector = Sizzle.escape;




var dir = function( elem, dir, until ) {
	var matched = [],
		truncate = until !== undefined;

	while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
		if ( elem.nodeType === 1 ) {
			if ( truncate && jQuery( elem ).is( until ) ) {
				break;
			}
			matched.push( elem );
		}
	}
	return matched;
};


var siblings = function( n, elem ) {
	var matched = [];

	for ( ; n; n = n.nextSibling ) {
		if ( n.nodeType === 1 && n !== elem ) {
			matched.push( n );
		}
	}

	return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;



function nodeName( elem, name ) {

  return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();

};
var rsingleTag = ( /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i );



// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			return !!qualifier.call( elem, i, elem ) !== not;
		} );
	}

	// Single element
	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		} );
	}

	// Arraylike of elements (jQuery, arguments, Array)
	if ( typeof qualifier !== "string" ) {
		return jQuery.grep( elements, function( elem ) {
			return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
		} );
	}

	// Filtered directly for both simple and complex selectors
	return jQuery.filter( qualifier, elements, not );
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	if ( elems.length === 1 && elem.nodeType === 1 ) {
		return jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [];
	}

	return jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
		return elem.nodeType === 1;
	} ) );
};

jQuery.fn.extend( {
	find: function( selector ) {
		var i, ret,
			len = this.length,
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter( function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			} ) );
		}

		ret = this.pushStack( [] );

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		return len > 1 ? jQuery.uniqueSort( ret ) : ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow( this, selector || [], false ) );
	},
	not: function( selector ) {
		return this.pushStack( winnow( this, selector || [], true ) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	// Shortcut simple #id case for speed
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,

	init = jQuery.fn.init = function( selector, context, root ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Method init() accepts an alternate rootjQuery
		// so migrate can support jQuery.sub (gh-2101)
		root = root || rootjQuery;

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[ 0 ] === "<" &&
				selector[ selector.length - 1 ] === ">" &&
				selector.length >= 3 ) {

				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && ( match[ 1 ] || !context ) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[ 1 ] ) {
					context = context instanceof jQuery ? context[ 0 ] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[ 1 ],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {

							// Properties of context are called as methods if possible
							if ( isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[ 2 ] );

					if ( elem ) {

						// Inject the element directly into the jQuery object
						this[ 0 ] = elem;
						this.length = 1;
					}
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || root ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this[ 0 ] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( isFunction( selector ) ) {
			return root.ready !== undefined ?
				root.ready( selector ) :

				// Execute immediately if ready is not present
				selector( jQuery );
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.fn.extend( {
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter( function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[ i ] ) ) {
					return true;
				}
			}
		} );
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			targets = typeof selectors !== "string" && jQuery( selectors );

		// Positional selectors never match, since there's no _selection_ context
		if ( !rneedsContext.test( selectors ) ) {
			for ( ; i < l; i++ ) {
				for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

					// Always skip document fragments
					if ( cur.nodeType < 11 && ( targets ?
						targets.index( cur ) > -1 :

						// Don't pass non-elements to Sizzle
						cur.nodeType === 1 &&
							jQuery.find.matchesSelector( cur, selectors ) ) ) {

						matched.push( cur );
						break;
					}
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.uniqueSort(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter( selector )
		);
	}
} );

function sibling( cur, dir ) {
	while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each( {
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return siblings( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return siblings( elem.firstChild );
	},
	contents: function( elem ) {
        if ( nodeName( elem, "iframe" ) ) {
            return elem.contentDocument;
        }

        // Support: IE 9 - 11 only, iOS 7 only, Android Browser <=4.3 only
        // Treat the template element as a regular one in browsers that
        // don't support it.
        if ( nodeName( elem, "template" ) ) {
            elem = elem.content || elem;
        }

        return jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {

			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.uniqueSort( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
} );
var rnothtmlwhite = ( /[^\x20\t\r\n\f]+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
	var object = {};
	jQuery.each( options.match( rnothtmlwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	} );
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		createOptions( options ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,

		// Last fire value for non-forgettable lists
		memory,

		// Flag to know if list was already fired
		fired,

		// Flag to prevent firing
		locked,

		// Actual callback list
		list = [],

		// Queue of execution data for repeatable lists
		queue = [],

		// Index of currently firing callback (modified by add/remove as needed)
		firingIndex = -1,

		// Fire callbacks
		fire = function() {

			// Enforce single-firing
			locked = locked || options.once;

			// Execute callbacks for all pending executions,
			// respecting firingIndex overrides and runtime changes
			fired = firing = true;
			for ( ; queue.length; firingIndex = -1 ) {
				memory = queue.shift();
				while ( ++firingIndex < list.length ) {

					// Run callback and check for early termination
					if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
						options.stopOnFalse ) {

						// Jump to end and forget the data so .add doesn't re-fire
						firingIndex = list.length;
						memory = false;
					}
				}
			}

			// Forget the data if we're done with it
			if ( !options.memory ) {
				memory = false;
			}

			firing = false;

			// Clean up if we're done firing for good
			if ( locked ) {

				// Keep an empty list if we have data for future add calls
				if ( memory ) {
					list = [];

				// Otherwise, this object is spent
				} else {
					list = "";
				}
			}
		},

		// Actual Callbacks object
		self = {

			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {

					// If we have memory from a past run, we should fire after adding
					if ( memory && !firing ) {
						firingIndex = list.length - 1;
						queue.push( memory );
					}

					( function add( args ) {
						jQuery.each( args, function( _, arg ) {
							if ( isFunction( arg ) ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && toType( arg ) !== "string" ) {

								// Inspect recursively
								add( arg );
							}
						} );
					} )( arguments );

					if ( memory && !firing ) {
						fire();
					}
				}
				return this;
			},

			// Remove a callback from the list
			remove: function() {
				jQuery.each( arguments, function( _, arg ) {
					var index;
					while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
						list.splice( index, 1 );

						// Handle firing indexes
						if ( index <= firingIndex ) {
							firingIndex--;
						}
					}
				} );
				return this;
			},

			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ?
					jQuery.inArray( fn, list ) > -1 :
					list.length > 0;
			},

			// Remove all callbacks from the list
			empty: function() {
				if ( list ) {
					list = [];
				}
				return this;
			},

			// Disable .fire and .add
			// Abort any current/pending executions
			// Clear all callbacks and values
			disable: function() {
				locked = queue = [];
				list = memory = "";
				return this;
			},
			disabled: function() {
				return !list;
			},

			// Disable .fire
			// Also disable .add unless we have memory (since it would have no effect)
			// Abort any pending executions
			lock: function() {
				locked = queue = [];
				if ( !memory && !firing ) {
					list = memory = "";
				}
				return this;
			},
			locked: function() {
				return !!locked;
			},

			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( !locked ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					queue.push( args );
					if ( !firing ) {
						fire();
					}
				}
				return this;
			},

			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},

			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


function Identity( v ) {
	return v;
}
function Thrower( ex ) {
	throw ex;
}

function adoptValue( value, resolve, reject, noValue ) {
	var method;

	try {

		// Check for promise aspect first to privilege synchronous behavior
		if ( value && isFunction( ( method = value.promise ) ) ) {
			method.call( value ).done( resolve ).fail( reject );

		// Other thenables
		} else if ( value && isFunction( ( method = value.then ) ) ) {
			method.call( value, resolve, reject );

		// Other non-thenables
		} else {

			// Control `resolve` arguments by letting Array#slice cast boolean `noValue` to integer:
			// * false: [ value ].slice( 0 ) => resolve( value )
			// * true: [ value ].slice( 1 ) => resolve()
			resolve.apply( undefined, [ value ].slice( noValue ) );
		}

	// For Promises/A+, convert exceptions into rejections
	// Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
	// Deferred#then to conditionally suppress rejection.
	} catch ( value ) {

		// Support: Android 4.0 only
		// Strict mode functions invoked without .call/.apply get global-object context
		reject.apply( undefined, [ value ] );
	}
}

jQuery.extend( {

	Deferred: function( func ) {
		var tuples = [

				// action, add listener, callbacks,
				// ... .then handlers, argument index, [final state]
				[ "notify", "progress", jQuery.Callbacks( "memory" ),
					jQuery.Callbacks( "memory" ), 2 ],
				[ "resolve", "done", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 0, "resolved" ],
				[ "reject", "fail", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 1, "rejected" ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				"catch": function( fn ) {
					return promise.then( null, fn );
				},

				// Keep pipe for back-compat
				pipe: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;

					return jQuery.Deferred( function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {

							// Map tuples (progress, done, fail) to arguments (done, fail, progress)
							var fn = isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];

							// deferred.progress(function() { bind to newDefer or newDefer.notify })
							// deferred.done(function() { bind to newDefer or newDefer.resolve })
							// deferred.fail(function() { bind to newDefer or newDefer.reject })
							deferred[ tuple[ 1 ] ]( function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && isFunction( returned.promise ) ) {
									returned.promise()
										.progress( newDefer.notify )
										.done( newDefer.resolve )
										.fail( newDefer.reject );
								} else {
									newDefer[ tuple[ 0 ] + "With" ](
										this,
										fn ? [ returned ] : arguments
									);
								}
							} );
						} );
						fns = null;
					} ).promise();
				},
				then: function( onFulfilled, onRejected, onProgress ) {
					var maxDepth = 0;
					function resolve( depth, deferred, handler, special ) {
						return function() {
							var that = this,
								args = arguments,
								mightThrow = function() {
									var returned, then;

									// Support: Promises/A+ section 2.3.3.3.3
									// https://promisesaplus.com/#point-59
									// Ignore double-resolution attempts
									if ( depth < maxDepth ) {
										return;
									}

									returned = handler.apply( that, args );

									// Support: Promises/A+ section 2.3.1
									// https://promisesaplus.com/#point-48
									if ( returned === deferred.promise() ) {
										throw new TypeError( "Thenable self-resolution" );
									}

									// Support: Promises/A+ sections 2.3.3.1, 3.5
									// https://promisesaplus.com/#point-54
									// https://promisesaplus.com/#point-75
									// Retrieve `then` only once
									then = returned &&

										// Support: Promises/A+ section 2.3.4
										// https://promisesaplus.com/#point-64
										// Only check objects and functions for thenability
										( typeof returned === "object" ||
											typeof returned === "function" ) &&
										returned.then;

									// Handle a returned thenable
									if ( isFunction( then ) ) {

										// Special processors (notify) just wait for resolution
										if ( special ) {
											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special )
											);

										// Normal processors (resolve) also hook into progress
										} else {

											// ...and disregard older resolution values
											maxDepth++;

											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special ),
												resolve( maxDepth, deferred, Identity,
													deferred.notifyWith )
											);
										}

									// Handle all other returned values
									} else {

										// Only substitute handlers pass on context
										// and multiple values (non-spec behavior)
										if ( handler !== Identity ) {
											that = undefined;
											args = [ returned ];
										}

										// Process the value(s)
										// Default process is resolve
										( special || deferred.resolveWith )( that, args );
									}
								},

								// Only normal processors (resolve) catch and reject exceptions
								process = special ?
									mightThrow :
									function() {
										try {
											mightThrow();
										} catch ( e ) {

											if ( jQuery.Deferred.exceptionHook ) {
												jQuery.Deferred.exceptionHook( e,
													process.stackTrace );
											}

											// Support: Promises/A+ section 2.3.3.3.4.1
											// https://promisesaplus.com/#point-61
											// Ignore post-resolution exceptions
											if ( depth + 1 >= maxDepth ) {

												// Only substitute handlers pass on context
												// and multiple values (non-spec behavior)
												if ( handler !== Thrower ) {
													that = undefined;
													args = [ e ];
												}

												deferred.rejectWith( that, args );
											}
										}
									};

							// Support: Promises/A+ section 2.3.3.3.1
							// https://promisesaplus.com/#point-57
							// Re-resolve promises immediately to dodge false rejection from
							// subsequent errors
							if ( depth ) {
								process();
							} else {

								// Call an optional hook to record the stack, in case of exception
								// since it's otherwise lost when execution goes async
								if ( jQuery.Deferred.getStackHook ) {
									process.stackTrace = jQuery.Deferred.getStackHook();
								}
								window.setTimeout( process );
							}
						};
					}

					return jQuery.Deferred( function( newDefer ) {

						// progress_handlers.add( ... )
						tuples[ 0 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onProgress ) ?
									onProgress :
									Identity,
								newDefer.notifyWith
							)
						);

						// fulfilled_handlers.add( ... )
						tuples[ 1 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onFulfilled ) ?
									onFulfilled :
									Identity
							)
						);

						// rejected_handlers.add( ... )
						tuples[ 2 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onRejected ) ?
									onRejected :
									Thrower
							)
						);
					} ).promise();
				},

				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 5 ];

			// promise.progress = list.add
			// promise.done = list.add
			// promise.fail = list.add
			promise[ tuple[ 1 ] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(
					function() {

						// state = "resolved" (i.e., fulfilled)
						// state = "rejected"
						state = stateString;
					},

					// rejected_callbacks.disable
					// fulfilled_callbacks.disable
					tuples[ 3 - i ][ 2 ].disable,

					// rejected_handlers.disable
					// fulfilled_handlers.disable
					tuples[ 3 - i ][ 3 ].disable,

					// progress_callbacks.lock
					tuples[ 0 ][ 2 ].lock,

					// progress_handlers.lock
					tuples[ 0 ][ 3 ].lock
				);
			}

			// progress_handlers.fire
			// fulfilled_handlers.fire
			// rejected_handlers.fire
			list.add( tuple[ 3 ].fire );

			// deferred.notify = function() { deferred.notifyWith(...) }
			// deferred.resolve = function() { deferred.resolveWith(...) }
			// deferred.reject = function() { deferred.rejectWith(...) }
			deferred[ tuple[ 0 ] ] = function() {
				deferred[ tuple[ 0 ] + "With" ]( this === deferred ? undefined : this, arguments );
				return this;
			};

			// deferred.notifyWith = list.fireWith
			// deferred.resolveWith = list.fireWith
			// deferred.rejectWith = list.fireWith
			deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
		} );

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( singleValue ) {
		var

			// count of uncompleted subordinates
			remaining = arguments.length,

			// count of unprocessed arguments
			i = remaining,

			// subordinate fulfillment data
			resolveContexts = Array( i ),
			resolveValues = slice.call( arguments ),

			// the master Deferred
			master = jQuery.Deferred(),

			// subordinate callback factory
			updateFunc = function( i ) {
				return function( value ) {
					resolveContexts[ i ] = this;
					resolveValues[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( !( --remaining ) ) {
						master.resolveWith( resolveContexts, resolveValues );
					}
				};
			};

		// Single- and empty arguments are adopted like Promise.resolve
		if ( remaining <= 1 ) {
			adoptValue( singleValue, master.done( updateFunc( i ) ).resolve, master.reject,
				!remaining );

			// Use .then() to unwrap secondary thenables (cf. gh-3000)
			if ( master.state() === "pending" ||
				isFunction( resolveValues[ i ] && resolveValues[ i ].then ) ) {

				return master.then();
			}
		}

		// Multiple arguments are aggregated like Promise.all array elements
		while ( i-- ) {
			adoptValue( resolveValues[ i ], updateFunc( i ), master.reject );
		}

		return master.promise();
	}
} );


// These usually indicate a programmer mistake during development,
// warn about them ASAP rather than swallowing them by default.
var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;

jQuery.Deferred.exceptionHook = function( error, stack ) {

	// Support: IE 8 - 9 only
	// Console exists when dev tools are open, which can happen at any time
	if ( window.console && window.console.warn && error && rerrorNames.test( error.name ) ) {
		window.console.warn( "jQuery.Deferred exception: " + error.message, error.stack, stack );
	}
};




jQuery.readyException = function( error ) {
	window.setTimeout( function() {
		throw error;
	} );
};




// The deferred used on DOM ready
var readyList = jQuery.Deferred();

jQuery.fn.ready = function( fn ) {

	readyList
		.then( fn )

		// Wrap jQuery.readyException in a function so that the lookup
		// happens at the time of error handling instead of callback
		// registration.
		.catch( function( error ) {
			jQuery.readyException( error );
		} );

	return this;
};

jQuery.extend( {

	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );
	}
} );

jQuery.ready.then = readyList.then;

// The ready event handler and self cleanup method
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed );
	window.removeEventListener( "load", completed );
	jQuery.ready();
}

// Catch cases where $(document).ready() is called
// after the browser event has already occurred.
// Support: IE <=9 - 10 only
// Older IE sometimes signals "interactive" too soon
if ( document.readyState === "complete" ||
	( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

	// Handle it asynchronously to allow scripts the opportunity to delay ready
	window.setTimeout( jQuery.ready );

} else {

	// Use the handy event callback
	document.addEventListener( "DOMContentLoaded", completed );

	// A fallback to window.onload, that will always work
	window.addEventListener( "load", completed );
}




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( toType( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			access( elems, fn, i, key[ i ], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {

			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn(
					elems[ i ], key, raw ?
					value :
					value.call( elems[ i ], i, fn( elems[ i ], key ) )
				);
			}
		}
	}

	if ( chainable ) {
		return elems;
	}

	// Gets
	if ( bulk ) {
		return fn.call( elems );
	}

	return len ? fn( elems[ 0 ], key ) : emptyGet;
};


// Matches dashed string for camelizing
var rmsPrefix = /^-ms-/,
	rdashAlpha = /-([a-z])/g;

// Used by camelCase as callback to replace()
function fcamelCase( all, letter ) {
	return letter.toUpperCase();
}

// Convert dashed to camelCase; used by the css and data modules
// Support: IE <=9 - 11, Edge 12 - 15
// Microsoft forgot to hump their vendor prefix (#9572)
function camelCase( string ) {
	return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
}
var acceptData = function( owner ) {

	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

	cache: function( owner ) {

		// Check if the owner object already has a cache
		var value = owner[ this.expando ];

		// If not, create one
		if ( !value ) {
			value = {};

			// We can accept data for non-element nodes in modern browsers,
			// but we should not, see #8335.
			// Always return an empty object.
			if ( acceptData( owner ) ) {

				// If it is a node unlikely to be stringify-ed or looped over
				// use plain assignment
				if ( owner.nodeType ) {
					owner[ this.expando ] = value;

				// Otherwise secure it in a non-enumerable property
				// configurable must be true to allow the property to be
				// deleted when data is removed
				} else {
					Object.defineProperty( owner, this.expando, {
						value: value,
						configurable: true
					} );
				}
			}
		}

		return value;
	},
	set: function( owner, data, value ) {
		var prop,
			cache = this.cache( owner );

		// Handle: [ owner, key, value ] args
		// Always use camelCase key (gh-2257)
		if ( typeof data === "string" ) {
			cache[ camelCase( data ) ] = value;

		// Handle: [ owner, { properties } ] args
		} else {

			// Copy the properties one-by-one to the cache object
			for ( prop in data ) {
				cache[ camelCase( prop ) ] = data[ prop ];
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		return key === undefined ?
			this.cache( owner ) :

			// Always use camelCase key (gh-2257)
			owner[ this.expando ] && owner[ this.expando ][ camelCase( key ) ];
	},
	access: function( owner, key, value ) {

		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				( ( key && typeof key === "string" ) && value === undefined ) ) {

			return this.get( owner, key );
		}

		// When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i,
			cache = owner[ this.expando ];

		if ( cache === undefined ) {
			return;
		}

		if ( key !== undefined ) {

			// Support array or space separated string of keys
			if ( Array.isArray( key ) ) {

				// If key is an array of keys...
				// We always set camelCase keys, so remove that.
				key = key.map( camelCase );
			} else {
				key = camelCase( key );

				// If a key with the spaces exists, use it.
				// Otherwise, create an array by matching non-whitespace
				key = key in cache ?
					[ key ] :
					( key.match( rnothtmlwhite ) || [] );
			}

			i = key.length;

			while ( i-- ) {
				delete cache[ key[ i ] ];
			}
		}

		// Remove the expando if there's no more data
		if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

			// Support: Chrome <=35 - 45
			// Webkit & Blink performance suffers when deleting properties
			// from DOM nodes, so set to undefined instead
			// https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
			if ( owner.nodeType ) {
				owner[ this.expando ] = undefined;
			} else {
				delete owner[ this.expando ];
			}
		}
	},
	hasData: function( owner ) {
		var cache = owner[ this.expando ];
		return cache !== undefined && !jQuery.isEmptyObject( cache );
	}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /[A-Z]/g;

function getData( data ) {
	if ( data === "true" ) {
		return true;
	}

	if ( data === "false" ) {
		return false;
	}

	if ( data === "null" ) {
		return null;
	}

	// Only convert to a number if it doesn't change the string
	if ( data === +data + "" ) {
		return +data;
	}

	if ( rbrace.test( data ) ) {
		return JSON.parse( data );
	}

	return data;
}

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = getData( data );
			} catch ( e ) {}

			// Make sure we set the data so it isn't changed later
			dataUser.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend( {
	hasData: function( elem ) {
		return dataUser.hasData( elem ) || dataPriv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return dataUser.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		dataUser.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to dataPriv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return dataPriv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		dataPriv.remove( elem, name );
	}
} );

jQuery.fn.extend( {
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = dataUser.get( elem );

				if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE 11 only
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = camelCase( name.slice( 5 ) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					dataPriv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each( function() {
				dataUser.set( this, key );
			} );
		}

		return access( this, function( value ) {
			var data;

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {

				// Attempt to get data from the cache
				// The key will always be camelCased in Data
				data = dataUser.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each( function() {

				// We always store the camelCased key
				dataUser.set( this, key, value );
			} );
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each( function() {
			dataUser.remove( this, key );
		} );
	}
} );


jQuery.extend( {
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = dataPriv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || Array.isArray( data ) ) {
					queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
			empty: jQuery.Callbacks( "once memory" ).add( function() {
				dataPriv.remove( elem, [ type + "queue", key ] );
			} )
		} );
	}
} );

jQuery.fn.extend( {
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[ 0 ], type );
		}

		return data === undefined ?
			this :
			this.each( function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			} );
	},
	dequeue: function( type ) {
		return this.each( function() {
			jQuery.dequeue( this, type );
		} );
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},

	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHiddenWithinTree = function( elem, el ) {

		// isHiddenWithinTree might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;

		// Inline style trumps all
		return elem.style.display === "none" ||
			elem.style.display === "" &&

			// Otherwise, check computed style
			// Support: Firefox <=43 - 45
			// Disconnected elements can have computed display: none, so first confirm that elem is
			// in the document.
			jQuery.contains( elem.ownerDocument, elem ) &&

			jQuery.css( elem, "display" ) === "none";
	};

var swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};




function adjustCSS( elem, prop, valueParts, tween ) {
	var adjusted, scale,
		maxIterations = 20,
		currentValue = tween ?
			function() {
				return tween.cur();
			} :
			function() {
				return jQuery.css( elem, prop, "" );
			},
		initial = currentValue(),
		unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

		// Starting value computation is required for potential unit mismatches
		initialInUnit = ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
			rcssNum.exec( jQuery.css( elem, prop ) );

	if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

		// Support: Firefox <=54
		// Halve the iteration target value to prevent interference from CSS upper bounds (gh-2144)
		initial = initial / 2;

		// Trust units reported by jQuery.css
		unit = unit || initialInUnit[ 3 ];

		// Iteratively approximate from a nonzero starting point
		initialInUnit = +initial || 1;

		while ( maxIterations-- ) {

			// Evaluate and update our best guess (doubling guesses that zero out).
			// Finish if the scale equals or crosses 1 (making the old*new product non-positive).
			jQuery.style( elem, prop, initialInUnit + unit );
			if ( ( 1 - scale ) * ( 1 - ( scale = currentValue() / initial || 0.5 ) ) <= 0 ) {
				maxIterations = 0;
			}
			initialInUnit = initialInUnit / scale;

		}

		initialInUnit = initialInUnit * 2;
		jQuery.style( elem, prop, initialInUnit + unit );

		// Make sure we update the tween properties later on
		valueParts = valueParts || [];
	}

	if ( valueParts ) {
		initialInUnit = +initialInUnit || +initial || 0;

		// Apply relative offset (+=/-=) if specified
		adjusted = valueParts[ 1 ] ?
			initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
			+valueParts[ 2 ];
		if ( tween ) {
			tween.unit = unit;
			tween.start = initialInUnit;
			tween.end = adjusted;
		}
	}
	return adjusted;
}


var defaultDisplayMap = {};

function getDefaultDisplay( elem ) {
	var temp,
		doc = elem.ownerDocument,
		nodeName = elem.nodeName,
		display = defaultDisplayMap[ nodeName ];

	if ( display ) {
		return display;
	}

	temp = doc.body.appendChild( doc.createElement( nodeName ) );
	display = jQuery.css( temp, "display" );

	temp.parentNode.removeChild( temp );

	if ( display === "none" ) {
		display = "block";
	}
	defaultDisplayMap[ nodeName ] = display;

	return display;
}

function showHide( elements, show ) {
	var display, elem,
		values = [],
		index = 0,
		length = elements.length;

	// Determine new display value for elements that need to change
	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		display = elem.style.display;
		if ( show ) {

			// Since we force visibility upon cascade-hidden elements, an immediate (and slow)
			// check is required in this first loop unless we have a nonempty display value (either
			// inline or about-to-be-restored)
			if ( display === "none" ) {
				values[ index ] = dataPriv.get( elem, "display" ) || null;
				if ( !values[ index ] ) {
					elem.style.display = "";
				}
			}
			if ( elem.style.display === "" && isHiddenWithinTree( elem ) ) {
				values[ index ] = getDefaultDisplay( elem );
			}
		} else {
			if ( display !== "none" ) {
				values[ index ] = "none";

				// Remember what we're overwriting
				dataPriv.set( elem, "display", display );
			}
		}
	}

	// Set the display of the elements in a second loop to avoid constant reflow
	for ( index = 0; index < length; index++ ) {
		if ( values[ index ] != null ) {
			elements[ index ].style.display = values[ index ];
		}
	}

	return elements;
}

jQuery.fn.extend( {
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each( function() {
			if ( isHiddenWithinTree( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		} );
	}
} );
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([a-z][^\/\0>\x20\t\r\n\f]+)/i );

var rscriptType = ( /^$|^module$|\/(?:java|ecma)script/i );



// We have to close these tags to support XHTML (#13200)
var wrapMap = {

	// Support: IE <=9 only
	option: [ 1, "<select multiple='multiple'>", "</select>" ],

	// XHTML parsers do not magically insert elements in the
	// same way that tag soup parsers do. So we cannot shorten
	// this by omitting <tbody> or other required elements.
	thead: [ 1, "<table>", "</table>" ],
	col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
	tr: [ 2, "<table><tbody>", "</tbody></table>" ],
	td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

	_default: [ 0, "", "" ]
};

// Support: IE <=9 only
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;


function getAll( context, tag ) {

	// Support: IE <=9 - 11 only
	// Use typeof to avoid zero-argument method invocation on host objects (#15151)
	var ret;

	if ( typeof context.getElementsByTagName !== "undefined" ) {
		ret = context.getElementsByTagName( tag || "*" );

	} else if ( typeof context.querySelectorAll !== "undefined" ) {
		ret = context.querySelectorAll( tag || "*" );

	} else {
		ret = [];
	}

	if ( tag === undefined || tag && nodeName( context, tag ) ) {
		return jQuery.merge( [ context ], ret );
	}

	return ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		dataPriv.set(
			elems[ i ],
			"globalEval",
			!refElements || dataPriv.get( refElements[ i ], "globalEval" )
		);
	}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
	var elem, tmp, tag, wrap, contains, j,
		fragment = context.createDocumentFragment(),
		nodes = [],
		i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		elem = elems[ i ];

		if ( elem || elem === 0 ) {

			// Add nodes directly
			if ( toType( elem ) === "object" ) {

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

			// Convert non-html into a text node
			} else if ( !rhtml.test( elem ) ) {
				nodes.push( context.createTextNode( elem ) );

			// Convert html into DOM nodes
			} else {
				tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

				// Deserialize a standard representation
				tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
				wrap = wrapMap[ tag ] || wrapMap._default;
				tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

				// Descend through wrappers to the right content
				j = wrap[ 0 ];
				while ( j-- ) {
					tmp = tmp.lastChild;
				}

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, tmp.childNodes );

				// Remember the top-level container
				tmp = fragment.firstChild;

				// Ensure the created nodes are orphaned (#12392)
				tmp.textContent = "";
			}
		}
	}

	// Remove wrapper from fragment
	fragment.textContent = "";

	i = 0;
	while ( ( elem = nodes[ i++ ] ) ) {

		// Skip elements already in the context collection (trac-4087)
		if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
			if ( ignored ) {
				ignored.push( elem );
			}
			continue;
		}

		contains = jQuery.contains( elem.ownerDocument, elem );

		// Append to fragment
		tmp = getAll( fragment.appendChild( elem ), "script" );

		// Preserve script evaluation history
		if ( contains ) {
			setGlobalEval( tmp );
		}

		// Capture executables
		if ( scripts ) {
			j = 0;
			while ( ( elem = tmp[ j++ ] ) ) {
				if ( rscriptType.test( elem.type || "" ) ) {
					scripts.push( elem );
				}
			}
		}
	}

	return fragment;
}


( function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Android 4.0 - 4.3 only
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Android <=4.1 only
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE <=11 only
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
} )();
var documentElement = document.documentElement;



var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

// Support: IE <=9 only
// See #13393 for more info
function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
	var origFn, type;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {

		// ( types-Object, selector, data )
		if ( typeof selector !== "string" ) {

			// ( types-Object, data )
			data = data || selector;
			selector = undefined;
		}
		for ( type in types ) {
			on( elem, type, selector, data, types[ type ], one );
		}
		return elem;
	}

	if ( data == null && fn == null ) {

		// ( types, fn )
		fn = selector;
		data = selector = undefined;
	} else if ( fn == null ) {
		if ( typeof selector === "string" ) {

			// ( types, selector, fn )
			fn = data;
			data = undefined;
		} else {

			// ( types, data, fn )
			fn = data;
			data = selector;
			selector = undefined;
		}
	}
	if ( fn === false ) {
		fn = returnFalse;
	} else if ( !fn ) {
		return elem;
	}

	if ( one === 1 ) {
		origFn = fn;
		fn = function( event ) {

			// Can use an empty set, since event contains the info
			jQuery().off( event );
			return origFn.apply( this, arguments );
		};

		// Use same guid so caller can remove using origFn
		fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return elem.each( function() {
		jQuery.event.add( this, types, fn, data, selector );
	} );
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Ensure that invalid selectors throw exceptions at attach time
		// Evaluate against documentElement in case elem is a non-element node (e.g., document)
		if ( selector ) {
			jQuery.find.matchesSelector( documentElement, selector );
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !( events = elemData.events ) ) {
			events = elemData.events = {};
		}
		if ( !( eventHandle = elemData.handle ) ) {
			eventHandle = elemData.handle = function( e ) {

				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend( {
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join( "." )
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !( handlers = events[ type ] ) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup ||
					special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

		if ( !elemData || !( events = elemData.events ) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[ 2 ] &&
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector ||
						selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown ||
					special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove data and the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			dataPriv.remove( elem, "handle events" );
		}
	},

	dispatch: function( nativeEvent ) {

		// Make a writable jQuery.Event from the native event object
		var event = jQuery.event.fix( nativeEvent );

		var i, j, ret, matched, handleObj, handlerQueue,
			args = new Array( arguments.length ),
			handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[ 0 ] = event;

		for ( i = 1; i < arguments.length; i++ ) {
			args[ i ] = arguments[ i ];
		}

		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( ( handleObj = matched.handlers[ j++ ] ) &&
				!event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or 2) have namespace(s)
				// a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.rnamespace || event.rnamespace.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
						handleObj.handler ).apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( ( event.result = ret ) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, handleObj, sel, matchedHandlers, matchedSelectors,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		if ( delegateCount &&

			// Support: IE <=9
			// Black-hole SVG <use> instance trees (trac-13180)
			cur.nodeType &&

			// Support: Firefox <=42
			// Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
			// https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
			// Support: IE 11 only
			// ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
			!( event.type === "click" && event.button >= 1 ) ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && !( event.type === "click" && cur.disabled === true ) ) {
					matchedHandlers = [];
					matchedSelectors = {};
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matchedSelectors[ sel ] === undefined ) {
							matchedSelectors[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) > -1 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matchedSelectors[ sel ] ) {
							matchedHandlers.push( handleObj );
						}
					}
					if ( matchedHandlers.length ) {
						handlerQueue.push( { elem: cur, handlers: matchedHandlers } );
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		cur = this;
		if ( delegateCount < handlers.length ) {
			handlerQueue.push( { elem: cur, handlers: handlers.slice( delegateCount ) } );
		}

		return handlerQueue;
	},

	addProp: function( name, hook ) {
		Object.defineProperty( jQuery.Event.prototype, name, {
			enumerable: true,
			configurable: true,

			get: isFunction( hook ) ?
				function() {
					if ( this.originalEvent ) {
							return hook( this.originalEvent );
					}
				} :
				function() {
					if ( this.originalEvent ) {
							return this.originalEvent[ name ];
					}
				},

			set: function( value ) {
				Object.defineProperty( this, name, {
					enumerable: true,
					configurable: true,
					writable: true,
					value: value
				} );
			}
		} );
	},

	fix: function( originalEvent ) {
		return originalEvent[ jQuery.expando ] ?
			originalEvent :
			new jQuery.Event( originalEvent );
	},

	special: {
		load: {

			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {

			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					this.focus();
					return false;
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {

			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( this.type === "checkbox" && this.click && nodeName( this, "input" ) ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	}
};

jQuery.removeEvent = function( elem, type, handle ) {

	// This "if" is needed for plain objects
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle );
	}
};

jQuery.Event = function( src, props ) {

	// Allow instantiation without the 'new' keyword
	if ( !( this instanceof jQuery.Event ) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&

				// Support: Android <=2.3 only
				src.returnValue === false ?
			returnTrue :
			returnFalse;

		// Create target properties
		// Support: Safari <=6 - 7 only
		// Target should not be a text node (#504, #13143)
		this.target = ( src.target && src.target.nodeType === 3 ) ?
			src.target.parentNode :
			src.target;

		this.currentTarget = src.currentTarget;
		this.relatedTarget = src.relatedTarget;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || Date.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	constructor: jQuery.Event,
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,
	isSimulated: false,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && !this.isSimulated ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Includes all common event props including KeyEvent and MouseEvent specific props
jQuery.each( {
	altKey: true,
	bubbles: true,
	cancelable: true,
	changedTouches: true,
	ctrlKey: true,
	detail: true,
	eventPhase: true,
	metaKey: true,
	pageX: true,
	pageY: true,
	shiftKey: true,
	view: true,
	"char": true,
	charCode: true,
	key: true,
	keyCode: true,
	button: true,
	buttons: true,
	clientX: true,
	clientY: true,
	offsetX: true,
	offsetY: true,
	pointerId: true,
	pointerType: true,
	screenX: true,
	screenY: true,
	targetTouches: true,
	toElement: true,
	touches: true,

	which: function( event ) {
		var button = event.button;

		// Add which for key events
		if ( event.which == null && rkeyEvent.test( event.type ) ) {
			return event.charCode != null ? event.charCode : event.keyCode;
		}

		// Add which for click: 1 === left; 2 === middle; 3 === right
		if ( !event.which && button !== undefined && rmouseEvent.test( event.type ) ) {
			if ( button & 1 ) {
				return 1;
			}

			if ( button & 2 ) {
				return 3;
			}

			if ( button & 4 ) {
				return 2;
			}

			return 0;
		}

		return event.which;
	}
}, jQuery.event.addProp );

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://bugs.chromium.org/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mouseenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
} );

jQuery.fn.extend( {

	on: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn );
	},
	one: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {

			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ?
					handleObj.origType + "." + handleObj.namespace :
					handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {

			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {

			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each( function() {
			jQuery.event.remove( this, types, fn, selector );
		} );
	}
} );


var

	/* eslint-disable max-len */

	// See https://github.com/eslint/eslint/issues/3229
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,

	/* eslint-enable */

	// Support: IE <=10 - 11, Edge 12 - 13 only
	// In IE/Edge using regex groups here causes severe slowdowns.
	// See https://connect.microsoft.com/IE/feedback/details/1736512/
	rnoInnerhtml = /<script|<style|<link/i,

	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// Prefer a tbody over its parent table for containing new rows
function manipulationTarget( elem, content ) {
	if ( nodeName( elem, "table" ) &&
		nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {

		return jQuery( elem ).children( "tbody" )[ 0 ] || elem;
	}

	return elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	if ( ( elem.type || "" ).slice( 0, 5 ) === "true/" ) {
		elem.type = elem.type.slice( 5 );
	} else {
		elem.removeAttribute( "type" );
	}

	return elem;
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( dataPriv.hasData( src ) ) {
		pdataOld = dataPriv.access( src );
		pdataCur = dataPriv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( dataUser.hasData( src ) ) {
		udataOld = dataUser.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		dataUser.set( dest, udataCur );
	}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

function domManip( collection, args, callback, ignored ) {

	// Flatten any nested arrays
	args = concat.apply( [], args );

	var fragment, first, scripts, hasScripts, node, doc,
		i = 0,
		l = collection.length,
		iNoClone = l - 1,
		value = args[ 0 ],
		valueIsFunction = isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( valueIsFunction ||
			( l > 1 && typeof value === "string" &&
				!support.checkClone && rchecked.test( value ) ) ) {
		return collection.each( function( index ) {
			var self = collection.eq( index );
			if ( valueIsFunction ) {
				args[ 0 ] = value.call( this, index, self.html() );
			}
			domManip( self, args, callback, ignored );
		} );
	}

	if ( l ) {
		fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
		first = fragment.firstChild;

		if ( fragment.childNodes.length === 1 ) {
			fragment = first;
		}

		// Require either new content or an interest in ignored elements to invoke the callback
		if ( first || ignored ) {
			scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
			hasScripts = scripts.length;

			// Use the original fragment for the last item
			// instead of the first because it can end up
			// being emptied incorrectly in certain situations (#8070).
			for ( ; i < l; i++ ) {
				node = fragment;

				if ( i !== iNoClone ) {
					node = jQuery.clone( node, true, true );

					// Keep references to cloned scripts for later restoration
					if ( hasScripts ) {

						// Support: Android <=4.0 only, PhantomJS 1 only
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( scripts, getAll( node, "script" ) );
					}
				}

				callback.call( collection[ i ], node, i );
			}

			if ( hasScripts ) {
				doc = scripts[ scripts.length - 1 ].ownerDocument;

				// Reenable scripts
				jQuery.map( scripts, restoreScript );

				// Evaluate executable scripts on first document insertion
				for ( i = 0; i < hasScripts; i++ ) {
					node = scripts[ i ];
					if ( rscriptType.test( node.type || "" ) &&
						!dataPriv.access( node, "globalEval" ) &&
						jQuery.contains( doc, node ) ) {

						if ( node.src && ( node.type || "" ).toLowerCase()  !== "module" ) {

							// Optional AJAX dependency, but won't run scripts if not present
							if ( jQuery._evalUrl ) {
								jQuery._evalUrl( node.src );
							}
						} else {
							DOMEval( node.textContent.replace( rcleanScript, "" ), doc, node );
						}
					}
				}
			}
		}
	}

	return collection;
}

function remove( elem, selector, keepData ) {
	var node,
		nodes = selector ? jQuery.filter( selector, elem ) : elem,
		i = 0;

	for ( ; ( node = nodes[ i ] ) != null; i++ ) {
		if ( !keepData && node.nodeType === 1 ) {
			jQuery.cleanData( getAll( node ) );
		}

		if ( node.parentNode ) {
			if ( keepData && jQuery.contains( node.ownerDocument, node ) ) {
				setGlobalEval( getAll( node, "script" ) );
			}
			node.parentNode.removeChild( node );
		}
	}

	return elem;
}

jQuery.extend( {
	htmlPrefilter: function( html ) {
		return html.replace( rxhtmlTag, "<$1></$2>" );
	},

	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = jQuery.contains( elem.ownerDocument, elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	cleanData: function( elems ) {
		var data, elem, type,
			special = jQuery.event.special,
			i = 0;

		for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
			if ( acceptData( elem ) ) {
				if ( ( data = elem[ dataPriv.expando ] ) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataPriv.expando ] = undefined;
				}
				if ( elem[ dataUser.expando ] ) {

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataUser.expando ] = undefined;
				}
			}
		}
	}
} );

jQuery.fn.extend( {
	detach: function( selector ) {
		return remove( this, selector, true );
	},

	remove: function( selector ) {
		return remove( this, selector );
	},

	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each( function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				} );
		}, null, value, arguments.length );
	},

	append: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		} );
	},

	prepend: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		} );
	},

	before: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		} );
	},

	after: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		} );
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; ( elem = this[ i ] ) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map( function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		} );
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = jQuery.htmlPrefilter( value );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch ( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var ignored = [];

		// Make the changes, replacing each non-ignored context element with the new content
		return domManip( this, arguments, function( elem ) {
			var parent = this.parentNode;

			if ( jQuery.inArray( this, ignored ) < 0 ) {
				jQuery.cleanData( getAll( this ) );
				if ( parent ) {
					parent.replaceChild( elem, this );
				}
			}

		// Force callback invocation
		}, ignored );
	}
} );

jQuery.each( {
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: Android <=4.0 only, PhantomJS 1 only
			// .get() because push.apply(_, arraylike) throws on ancient WebKit
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
} );
var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

		// Support: IE <=11 only, Firefox <=30 (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		var view = elem.ownerDocument.defaultView;

		if ( !view || !view.opener ) {
			view = window;
		}

		return view.getComputedStyle( elem );
	};

var rboxStyle = new RegExp( cssExpand.join( "|" ), "i" );



( function() {

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computeStyleTests() {

		// This is a singleton, we need to execute it only once
		if ( !div ) {
			return;
		}

		container.style.cssText = "position:absolute;left:-11111px;width:60px;" +
			"margin-top:1px;padding:0;border:0";
		div.style.cssText =
			"position:relative;display:block;box-sizing:border-box;overflow:scroll;" +
			"margin:auto;border:1px;padding:1px;" +
			"width:60%;top:1%";
		documentElement.appendChild( container ).appendChild( div );

		var divStyle = window.getComputedStyle( div );
		pixelPositionVal = divStyle.top !== "1%";

		// Support: Android 4.0 - 4.3 only, Firefox <=3 - 44
		reliableMarginLeftVal = roundPixelMeasures( divStyle.marginLeft ) === 12;

		// Support: Android 4.0 - 4.3 only, Safari <=9.1 - 10.1, iOS <=7.0 - 9.3
		// Some styles come back with percentage values, even though they shouldn't
		div.style.right = "60%";
		pixelBoxStylesVal = roundPixelMeasures( divStyle.right ) === 36;

		// Support: IE 9 - 11 only
		// Detect misreporting of content dimensions for box-sizing:border-box elements
		boxSizingReliableVal = roundPixelMeasures( divStyle.width ) === 36;

		// Support: IE 9 only
		// Detect overflow:scroll screwiness (gh-3699)
		div.style.position = "absolute";
		scrollboxSizeVal = div.offsetWidth === 36 || "absolute";

		documentElement.removeChild( container );

		// Nullify the div so it wouldn't be stored in the memory and
		// it will also be a sign that checks already performed
		div = null;
	}

	function roundPixelMeasures( measure ) {
		return Math.round( parseFloat( measure ) );
	}

	var pixelPositionVal, boxSizingReliableVal, scrollboxSizeVal, pixelBoxStylesVal,
		reliableMarginLeftVal,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	// Finish early in limited (non-browser) environments
	if ( !div.style ) {
		return;
	}

	// Support: IE <=9 - 11 only
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	jQuery.extend( support, {
		boxSizingReliable: function() {
			computeStyleTests();
			return boxSizingReliableVal;
		},
		pixelBoxStyles: function() {
			computeStyleTests();
			return pixelBoxStylesVal;
		},
		pixelPosition: function() {
			computeStyleTests();
			return pixelPositionVal;
		},
		reliableMarginLeft: function() {
			computeStyleTests();
			return reliableMarginLeftVal;
		},
		scrollboxSize: function() {
			computeStyleTests();
			return scrollboxSizeVal;
		}
	} );
} )();


function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,

		// Support: Firefox 51+
		// Retrieving style before computed somehow
		// fixes an issue with getting wrong values
		// on detached elements
		style = elem.style;

	computed = computed || getStyles( elem );

	// getPropertyValue is needed for:
	//   .css('filter') (IE 9 only, #12537)
	//   .css('--customProperty) (#3144)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];

		if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// A tribute to the "awesome hack by Dean Edwards"
		// Android Browser returns percentage for some values,
		// but width seems to be reliably pixels.
		// This is against the CSSOM draft spec:
		// https://drafts.csswg.org/cssom/#resolved-values
		if ( !support.pixelBoxStyles() && rnumnonpx.test( ret ) && rboxStyle.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?

		// Support: IE <=9 - 11 only
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {

	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {

				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return ( this.get = hookFn ).apply( this, arguments );
		}
	};
}


var

	// Swappable if display is none or starts with table
	// except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rcustomProp = /^--/,
	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	},

	cssPrefixes = [ "Webkit", "Moz", "ms" ],
	emptyStyle = document.createElement( "div" ).style;

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( name ) {

	// Shortcut for names that are not vendor prefixed
	if ( name in emptyStyle ) {
		return name;
	}

	// Check for vendor prefixed names
	var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in emptyStyle ) {
			return name;
		}
	}
}

// Return a property mapped along what jQuery.cssProps suggests or to
// a vendor prefixed property.
function finalPropName( name ) {
	var ret = jQuery.cssProps[ name ];
	if ( !ret ) {
		ret = jQuery.cssProps[ name ] = vendorPropName( name ) || name;
	}
	return ret;
}

function setPositiveNumber( elem, value, subtract ) {

	// Any relative (+/-) values have already been
	// normalized at this point
	var matches = rcssNum.exec( value );
	return matches ?

		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
		value;
}

function boxModelAdjustment( elem, dimension, box, isBorderBox, styles, computedVal ) {
	var i = dimension === "width" ? 1 : 0,
		extra = 0,
		delta = 0;

	// Adjustment may not be necessary
	if ( box === ( isBorderBox ? "border" : "content" ) ) {
		return 0;
	}

	for ( ; i < 4; i += 2 ) {

		// Both box models exclude margin
		if ( box === "margin" ) {
			delta += jQuery.css( elem, box + cssExpand[ i ], true, styles );
		}

		// If we get here with a content-box, we're seeking "padding" or "border" or "margin"
		if ( !isBorderBox ) {

			// Add padding
			delta += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// For "border" or "margin", add border
			if ( box !== "padding" ) {
				delta += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );

			// But still keep track of it otherwise
			} else {
				extra += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}

		// If we get here with a border-box (content + padding + border), we're seeking "content" or
		// "padding" or "margin"
		} else {

			// For "content", subtract padding
			if ( box === "content" ) {
				delta -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// For "content" or "padding", subtract border
			if ( box !== "margin" ) {
				delta -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	// Account for positive content-box scroll gutter when requested by providing computedVal
	if ( !isBorderBox && computedVal >= 0 ) {

		// offsetWidth/offsetHeight is a rounded sum of content, padding, scroll gutter, and border
		// Assuming integer scroll gutter, subtract the rest and round down
		delta += Math.max( 0, Math.ceil(
			elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
			computedVal -
			delta -
			extra -
			0.5
		) );
	}

	return delta;
}

function getWidthOrHeight( elem, dimension, extra ) {

	// Start with computed style
	var styles = getStyles( elem ),
		val = curCSS( elem, dimension, styles ),
		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
		valueIsBorderBox = isBorderBox;

	// Support: Firefox <=54
	// Return a confounding non-pixel value or feign ignorance, as appropriate.
	if ( rnumnonpx.test( val ) ) {
		if ( !extra ) {
			return val;
		}
		val = "auto";
	}

	// Check for style in case a browser which returns unreliable values
	// for getComputedStyle silently falls back to the reliable elem.style
	valueIsBorderBox = valueIsBorderBox &&
		( support.boxSizingReliable() || val === elem.style[ dimension ] );

	// Fall back to offsetWidth/offsetHeight when value is "auto"
	// This happens for inline elements with no explicit setting (gh-3571)
	// Support: Android <=4.1 - 4.3 only
	// Also use offsetWidth/offsetHeight for misreported inline dimensions (gh-3602)
	if ( val === "auto" ||
		!parseFloat( val ) && jQuery.css( elem, "display", false, styles ) === "inline" ) {

		val = elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ];

		// offsetWidth/offsetHeight provide border-box values
		valueIsBorderBox = true;
	}

	// Normalize "" and auto
	val = parseFloat( val ) || 0;

	// Adjust for the element's box model
	return ( val +
		boxModelAdjustment(
			elem,
			dimension,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles,

			// Provide the current computed size to request scroll gutter calculation (gh-3589)
			val
		)
	) + "px";
}

jQuery.extend( {

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"animationIterationCount": true,
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = camelCase( name ),
			isCustomProp = rcustomProp.test( name ),
			style = elem.style;

		// Make sure that we're working with the right name. We don't
		// want to query the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
				value = adjustCSS( elem, name, ret );

				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add the unit (except for certain CSS properties)
			if ( type === "number" ) {
				value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
			}

			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !( "set" in hooks ) ||
				( value = hooks.set( elem, value, extra ) ) !== undefined ) {

				if ( isCustomProp ) {
					style.setProperty( name, value );
				} else {
					style[ name ] = value;
				}
			}

		} else {

			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks &&
				( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = camelCase( name ),
			isCustomProp = rcustomProp.test( name );

		// Make sure that we're working with the right name. We don't
		// want to modify the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || isFinite( num ) ? num || 0 : val;
		}

		return val;
	}
} );

jQuery.each( [ "height", "width" ], function( i, dimension ) {
	jQuery.cssHooks[ dimension ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&

					// Support: Safari 8+
					// Table columns in Safari have non-zero offsetWidth & zero
					// getBoundingClientRect().width unless display is changed.
					// Support: IE <=11 only
					// Running getBoundingClientRect on a disconnected node
					// in IE throws an error.
					( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
						swap( elem, cssShow, function() {
							return getWidthOrHeight( elem, dimension, extra );
						} ) :
						getWidthOrHeight( elem, dimension, extra );
			}
		},

		set: function( elem, value, extra ) {
			var matches,
				styles = getStyles( elem ),
				isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
				subtract = extra && boxModelAdjustment(
					elem,
					dimension,
					extra,
					isBorderBox,
					styles
				);

			// Account for unreliable border-box dimensions by comparing offset* to computed and
			// faking a content-box to get border and padding (gh-3699)
			if ( isBorderBox && support.scrollboxSize() === styles.position ) {
				subtract -= Math.ceil(
					elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
					parseFloat( styles[ dimension ] ) -
					boxModelAdjustment( elem, dimension, "border", false, styles ) -
					0.5
				);
			}

			// Convert to pixels if value adjustment is needed
			if ( subtract && ( matches = rcssNum.exec( value ) ) &&
				( matches[ 3 ] || "px" ) !== "px" ) {

				elem.style[ dimension ] = value;
				value = jQuery.css( elem, dimension );
			}

			return setPositiveNumber( elem, value, subtract );
		}
	};
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
	function( elem, computed ) {
		if ( computed ) {
			return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
				elem.getBoundingClientRect().left -
					swap( elem, { marginLeft: 0 }, function() {
						return elem.getBoundingClientRect().left;
					} )
				) + "px";
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each( {
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split( " " ) : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( prefix !== "margin" ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
} );

jQuery.fn.extend( {
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( Array.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	}
} );


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || jQuery.easing._default;
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			// Use a property on the element directly when it is not a DOM element,
			// or when there is no matching style property that exists.
			if ( tween.elem.nodeType !== 1 ||
				tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );

			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {

			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.nodeType === 1 &&
				( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null ||
					jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE <=9 only
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	},
	_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, inProgress,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rrun = /queueHooks$/;

function schedule() {
	if ( inProgress ) {
		if ( document.hidden === false && window.requestAnimationFrame ) {
			window.requestAnimationFrame( schedule );
		} else {
			window.setTimeout( schedule, jQuery.fx.interval );
		}

		jQuery.fx.tick();
	}
}

// Animations created synchronously will run synchronously
function createFxNow() {
	window.setTimeout( function() {
		fxNow = undefined;
	} );
	return ( fxNow = Date.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
		isBox = "width" in props || "height" in props,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHiddenWithinTree( elem ),
		dataShow = dataPriv.get( elem, "fxshow" );

	// Queue-skipping animations hijack the fx hooks
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always( function() {

			// Ensure the complete handler is called before this completes
			anim.always( function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			} );
		} );
	}

	// Detect show/hide animations
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.test( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// Pretend to be hidden if this is a "show" and
				// there is still data from a stopped show/hide
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;

				// Ignore all other no-op show/hide data
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
		}
	}

	// Bail out if this is a no-op like .hide().hide()
	propTween = !jQuery.isEmptyObject( props );
	if ( !propTween && jQuery.isEmptyObject( orig ) ) {
		return;
	}

	// Restrict "overflow" and "display" styles during box animations
	if ( isBox && elem.nodeType === 1 ) {

		// Support: IE <=9 - 11, Edge 12 - 15
		// Record all 3 overflow attributes because IE does not infer the shorthand
		// from identically-valued overflowX and overflowY and Edge just mirrors
		// the overflowX value there.
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Identify a display type, preferring old show/hide data over the CSS cascade
		restoreDisplay = dataShow && dataShow.display;
		if ( restoreDisplay == null ) {
			restoreDisplay = dataPriv.get( elem, "display" );
		}
		display = jQuery.css( elem, "display" );
		if ( display === "none" ) {
			if ( restoreDisplay ) {
				display = restoreDisplay;
			} else {

				// Get nonempty value(s) by temporarily forcing visibility
				showHide( [ elem ], true );
				restoreDisplay = elem.style.display || restoreDisplay;
				display = jQuery.css( elem, "display" );
				showHide( [ elem ] );
			}
		}

		// Animate inline elements as inline-block
		if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
			if ( jQuery.css( elem, "float" ) === "none" ) {

				// Restore the original display value at the end of pure show/hide animations
				if ( !propTween ) {
					anim.done( function() {
						style.display = restoreDisplay;
					} );
					if ( restoreDisplay == null ) {
						display = style.display;
						restoreDisplay = display === "none" ? "" : display;
					}
				}
				style.display = "inline-block";
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always( function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		} );
	}

	// Implement show/hide animations
	propTween = false;
	for ( prop in orig ) {

		// General show/hide setup for this element animation
		if ( !propTween ) {
			if ( dataShow ) {
				if ( "hidden" in dataShow ) {
					hidden = dataShow.hidden;
				}
			} else {
				dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
			}

			// Store hidden/visible for toggle so `.stop().toggle()` "reverses"
			if ( toggle ) {
				dataShow.hidden = !hidden;
			}

			// Show elements before animating them
			if ( hidden ) {
				showHide( [ elem ], true );
			}

			/* eslint-disable no-loop-func */

			anim.done( function() {

			/* eslint-enable no-loop-func */

				// The final step of a "hide" animation is actually hiding the element
				if ( !hidden ) {
					showHide( [ elem ] );
				}
				dataPriv.remove( elem, "fxshow" );
				for ( prop in orig ) {
					jQuery.style( elem, prop, orig[ prop ] );
				}
			} );
		}

		// Per-property setup
		propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
		if ( !( prop in dataShow ) ) {
			dataShow[ prop ] = propTween.start;
			if ( hidden ) {
				propTween.end = propTween.start;
				propTween.start = 0;
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( Array.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = Animation.prefilters.length,
		deferred = jQuery.Deferred().always( function() {

			// Don't match elem in the :animated selector
			delete tick.elem;
		} ),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

				// Support: Android 2.3 only
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ] );

			// If there's more to do, yield
			if ( percent < 1 && length ) {
				return remaining;
			}

			// If this was an empty animation, synthesize a final progress notification
			if ( !length ) {
				deferred.notifyWith( elem, [ animation, 1, 0 ] );
			}

			// Resolve the animation and report its conclusion
			deferred.resolveWith( elem, [ animation ] );
			return false;
		},
		animation = deferred.promise( {
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, {
				specialEasing: {},
				easing: jQuery.easing._default
			}, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,

					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.notifyWith( elem, [ animation, 1, 0 ] );
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		} ),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length; index++ ) {
		result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			if ( isFunction( result.stop ) ) {
				jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
					result.stop.bind( result );
			}
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	// Attach callbacks from options
	animation
		.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		} )
	);

	return animation;
}

jQuery.Animation = jQuery.extend( Animation, {

	tweeners: {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value );
			adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
			return tween;
		} ]
	},

	tweener: function( props, callback ) {
		if ( isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.match( rnothtmlwhite );
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length; index++ ) {
			prop = props[ index ];
			Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
			Animation.tweeners[ prop ].unshift( callback );
		}
	},

	prefilters: [ defaultPrefilter ],

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			Animation.prefilters.unshift( callback );
		} else {
			Animation.prefilters.push( callback );
		}
	}
} );

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !isFunction( easing ) && easing
	};

	// Go to the end state if fx are off
	if ( jQuery.fx.off ) {
		opt.duration = 0;

	} else {
		if ( typeof opt.duration !== "number" ) {
			if ( opt.duration in jQuery.fx.speeds ) {
				opt.duration = jQuery.fx.speeds[ opt.duration ];

			} else {
				opt.duration = jQuery.fx.speeds._default;
			}
		}
	}

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend( {
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHiddenWithinTree ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate( { opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {

				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || dataPriv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each( function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = dataPriv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this &&
					( type == null || timers[ index ].queue === type ) ) {

					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		} );
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each( function() {
			var index,
				data = dataPriv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		} );
	}
} );

jQuery.each( [ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
} );

// Generate shortcuts for custom animations
jQuery.each( {
	slideDown: genFx( "show" ),
	slideUp: genFx( "hide" ),
	slideToggle: genFx( "toggle" ),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = Date.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];

		// Run the timer and safely remove it when done (allowing for external removal)
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	jQuery.fx.start();
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
	if ( inProgress ) {
		return;
	}

	inProgress = true;
	schedule();
};

jQuery.fx.stop = function() {
	inProgress = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,

	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// https://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = window.setTimeout( next, time );
		hooks.stop = function() {
			window.clearTimeout( timeout );
		};
	} );
};


( function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: Android <=4.3 only
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE <=11 only
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: IE <=11 only
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
} )();


var boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each( function() {
			jQuery.removeAttr( this, name );
		} );
	}
} );

jQuery.extend( {
	attr: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set attributes on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		// Attribute hooks are determined by the lowercase version
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			hooks = jQuery.attrHooks[ name.toLowerCase() ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
		}

		if ( value !== undefined ) {
			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;
			}

			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			elem.setAttribute( name, value + "" );
			return value;
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		ret = jQuery.find.attr( elem, name );

		// Non-existent attributes return null, we normalize to undefined
		return ret == null ? undefined : ret;
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	},

	removeAttr: function( elem, value ) {
		var name,
			i = 0,

			// Attribute names can contain non-HTML whitespace characters
			// https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
			attrNames = value && value.match( rnothtmlwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( ( name = attrNames[ i++ ] ) ) {
				elem.removeAttribute( name );
			}
		}
	}
} );

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {

			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};

jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle,
			lowercaseName = name.toLowerCase();

		if ( !isXML ) {

			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ lowercaseName ];
			attrHandle[ lowercaseName ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				lowercaseName :
				null;
			attrHandle[ lowercaseName ] = handle;
		}
		return ret;
	};
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each( function() {
			delete this[ jQuery.propFix[ name ] || name ];
		} );
	}
} );

jQuery.extend( {
	prop: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			return ( elem[ name ] = value );
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		return elem[ name ];
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {

				// Support: IE <=9 - 11 only
				// elem.tabIndex doesn't always return the
				// correct value when it hasn't been explicitly set
				// https://web.archive.org/web/20141116233347/http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				if ( tabindex ) {
					return parseInt( tabindex, 10 );
				}

				if (
					rfocusable.test( elem.nodeName ) ||
					rclickable.test( elem.nodeName ) &&
					elem.href
				) {
					return 0;
				}

				return -1;
			}
		}
	},

	propFix: {
		"for": "htmlFor",
		"class": "className"
	}
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
// eslint rule "no-unused-expressions" is disabled for this code
// since it considers such accessions noop
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		},
		set: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent ) {
				parent.selectedIndex;

				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
		}
	};
}

jQuery.each( [
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
} );




	// Strip and collapse whitespace according to HTML spec
	// https://infra.spec.whatwg.org/#strip-and-collapse-ascii-whitespace
	function stripAndCollapse( value ) {
		var tokens = value.match( rnothtmlwhite ) || [];
		return tokens.join( " " );
	}


function getClass( elem ) {
	return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

function classesToArray( value ) {
	if ( Array.isArray( value ) ) {
		return value;
	}
	if ( typeof value === "string" ) {
		return value.match( rnothtmlwhite ) || [];
	}
	return [];
}

jQuery.fn.extend( {
	addClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		classes = classesToArray( value );

		if ( classes.length ) {
			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( !arguments.length ) {
			return this.attr( "class", "" );
		}

		classes = classesToArray( value );

		if ( classes.length ) {
			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );

				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {

						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value,
			isValidValue = type === "string" || Array.isArray( value );

		if ( typeof stateVal === "boolean" && isValidValue ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( isFunction( value ) ) {
			return this.each( function( i ) {
				jQuery( this ).toggleClass(
					value.call( this, i, getClass( this ), stateVal ),
					stateVal
				);
			} );
		}

		return this.each( function() {
			var className, i, self, classNames;

			if ( isValidValue ) {

				// Toggle individual class names
				i = 0;
				self = jQuery( this );
				classNames = classesToArray( value );

				while ( ( className = classNames[ i++ ] ) ) {

					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( value === undefined || type === "boolean" ) {
				className = getClass( this );
				if ( className ) {

					// Store className if set
					dataPriv.set( this, "__className__", className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				if ( this.setAttribute ) {
					this.setAttribute( "class",
						className || value === false ?
						"" :
						dataPriv.get( this, "__className__" ) || ""
					);
				}
			}
		} );
	},

	hasClass: function( selector ) {
		var className, elem,
			i = 0;

		className = " " + selector + " ";
		while ( ( elem = this[ i++ ] ) ) {
			if ( elem.nodeType === 1 &&
				( " " + stripAndCollapse( getClass( elem ) ) + " " ).indexOf( className ) > -1 ) {
					return true;
			}
		}

		return false;
	}
} );




var rreturn = /\r/g;

jQuery.fn.extend( {
	val: function( value ) {
		var hooks, ret, valueIsFunction,
			elem = this[ 0 ];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] ||
					jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks &&
					"get" in hooks &&
					( ret = hooks.get( elem, "value" ) ) !== undefined
				) {
					return ret;
				}

				ret = elem.value;

				// Handle most common string cases
				if ( typeof ret === "string" ) {
					return ret.replace( rreturn, "" );
				}

				// Handle cases where value is null/undef or number
				return ret == null ? "" : ret;
			}

			return;
		}

		valueIsFunction = isFunction( value );

		return this.each( function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( valueIsFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( Array.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				} );
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		} );
	}
} );

jQuery.extend( {
	valHooks: {
		option: {
			get: function( elem ) {

				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :

					// Support: IE <=10 - 11 only
					// option.text throws exceptions (#14686, #14858)
					// Strip and collapse whitespace
					// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
					stripAndCollapse( jQuery.text( elem ) );
			}
		},
		select: {
			get: function( elem ) {
				var value, option, i,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one",
					values = one ? null : [],
					max = one ? index + 1 : options.length;

				if ( index < 0 ) {
					i = max;

				} else {
					i = one ? index : 0;
				}

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// Support: IE <=9 only
					// IE8-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&

							// Don't return options that are disabled or in a disabled optgroup
							!option.disabled &&
							( !option.parentNode.disabled ||
								!nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];

					/* eslint-disable no-cond-assign */

					if ( option.selected =
						jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
					) {
						optionSet = true;
					}

					/* eslint-enable no-cond-assign */
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( Array.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute( "value" ) === null ? "on" : elem.value;
		};
	}
} );




// Return jQuery for attributes-only inclusion


support.focusin = "onfocusin" in window;


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	stopPropagationCallback = function( e ) {
		e.stopPropagation();
	};

jQuery.extend( jQuery.event, {

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special, lastElement,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

		cur = lastElement = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf( "." ) > -1 ) {

			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split( "." );
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf( ":" ) < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join( "." );
		event.rnamespace = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === ( elem.ownerDocument || document ) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {
			lastElement = cur;
			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
				dataPriv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( ( !special._default ||
				special._default.apply( eventPath.pop(), data ) === false ) &&
				acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && isFunction( elem[ type ] ) && !isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;

					if ( event.isPropagationStopped() ) {
						lastElement.addEventListener( type, stopPropagationCallback );
					}

					elem[ type ]();

					if ( event.isPropagationStopped() ) {
						lastElement.removeEventListener( type, stopPropagationCallback );
					}

					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	// Piggyback on a donor event to simulate a different one
	// Used only for `focus(in | out)` events
	simulate: function( type, elem, event ) {
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true
			}
		);

		jQuery.event.trigger( e, null, elem );
	}

} );

jQuery.fn.extend( {

	trigger: function( type, data ) {
		return this.each( function() {
			jQuery.event.trigger( type, data, this );
		} );
	},
	triggerHandler: function( type, data ) {
		var elem = this[ 0 ];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
} );


// Support: Firefox <=44
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
	jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
			jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
		};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					dataPriv.remove( doc, fix );

				} else {
					dataPriv.access( doc, fix, attaches );
				}
			}
		};
	} );
}
var location = window.location;

var nonce = Date.now();

var rquery = ( /\?/ );



// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE 9 - 11 only
	// IE throws on parseFromString with invalid input.
	try {
		xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( Array.isArray( obj ) ) {

		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {

				// Treat each array item as a scalar.
				add( prefix, v );

			} else {

				// Item is non-scalar (array or object), encode its numeric index.
				buildParams(
					prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
					v,
					traditional,
					add
				);
			}
		} );

	} else if ( !traditional && toType( obj ) === "object" ) {

		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {

		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, valueOrFunction ) {

			// If value is a function, invoke it and use its return value
			var value = isFunction( valueOrFunction ) ?
				valueOrFunction() :
				valueOrFunction;

			s[ s.length ] = encodeURIComponent( key ) + "=" +
				encodeURIComponent( value == null ? "" : value );
		};

	// If an array was passed in, assume that it is an array of form elements.
	if ( Array.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		} );

	} else {

		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" );
};

jQuery.fn.extend( {
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map( function() {

			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		} )
		.filter( function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		} )
		.map( function( i, elem ) {
			var val = jQuery( this ).val();

			if ( val == null ) {
				return null;
			}

			if ( Array.isArray( val ) ) {
				return jQuery.map( val, function( val ) {
					return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
				} );
			}

			return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		} ).get();
	}
} );


var
	r20 = /%20/g,
	rhash = /#.*$/,
	rantiCache = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Anchor tag for parsing the document origin
	originAnchor = document.createElement( "a" );
	originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnothtmlwhite ) || [];

		if ( isFunction( func ) ) {

			// For each dataType in the dataTypeExpression
			while ( ( dataType = dataTypes[ i++ ] ) ) {

				// Prepend if requested
				if ( dataType[ 0 ] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

				// Otherwise append
				} else {
					( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" &&
				!seekingTransport && !inspected[ dataTypeOrTransport ] ) {

				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		} );
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {

		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}

		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},

		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

			// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {

								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s.throws ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return {
								state: "parsererror",
								error: conv ? e : "No conversion from " + prev + " to " + current
							};
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend( {

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: location.href,
		type: "GET",
		isLocal: rlocalProtocol.test( location.protocol ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",

		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /\bxml\b/,
			html: /\bhtml/,
			json: /\bjson\b/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": JSON.parse,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,

			// URL without anti-cache param
			cacheURL,

			// Response headers
			responseHeadersString,
			responseHeaders,

			// timeout handle
			timeoutTimer,

			// Url cleanup var
			urlAnchor,

			// Request state (becomes false upon send and true upon completion)
			completed,

			// To know if global events are to be dispatched
			fireGlobals,

			// Loop variable
			i,

			// uncached part of the url
			uncached,

			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),

			// Callbacks context
			callbackContext = s.context || s,

			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context &&
				( callbackContext.nodeType || callbackContext.jquery ) ?
					jQuery( callbackContext ) :
					jQuery.event,

			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks( "once memory" ),

			// Status-dependent callbacks
			statusCode = s.statusCode || {},

			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},

			// Default abort message
			strAbort = "canceled",

			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( completed ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
								responseHeaders[ match[ 1 ].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return completed ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					if ( completed == null ) {
						name = requestHeadersNames[ name.toLowerCase() ] =
							requestHeadersNames[ name.toLowerCase() ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( completed == null ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( completed ) {

							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						} else {

							// Lazy-add the new callbacks in a way that preserves old ones
							for ( code in map ) {
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR );

		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || location.href ) + "" )
			.replace( rprotocol, location.protocol + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = ( s.dataType || "*" ).toLowerCase().match( rnothtmlwhite ) || [ "" ];

		// A cross-domain request is in order when the origin doesn't match the current origin.
		if ( s.crossDomain == null ) {
			urlAnchor = document.createElement( "a" );

			// Support: IE <=8 - 11, Edge 12 - 15
			// IE throws exception on accessing the href property if url is malformed,
			// e.g. http://example.com:80x/
			try {
				urlAnchor.href = s.url;

				// Support: IE <=8 - 11 only
				// Anchor's host property isn't correctly set when s.url is relative
				urlAnchor.href = urlAnchor.href;
				s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
					urlAnchor.protocol + "//" + urlAnchor.host;
			} catch ( e ) {

				// If there is an error parsing the URL, assume it is crossDomain,
				// it can be rejected by the transport if it is invalid
				s.crossDomain = true;
			}
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( completed ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger( "ajaxStart" );
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		// Remove hash to simplify url manipulation
		cacheURL = s.url.replace( rhash, "" );

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// Remember the hash so we can put it back
			uncached = s.url.slice( cacheURL.length );

			// If data is available and should be processed, append data to url
			if ( s.data && ( s.processData || typeof s.data === "string" ) ) {
				cacheURL += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data;

				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add or update anti-cache param if needed
			if ( s.cache === false ) {
				cacheURL = cacheURL.replace( rantiCache, "$1" );
				uncached = ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ( nonce++ ) + uncached;
			}

			// Put hash and anti-cache on the URL that will be requested (gh-1732)
			s.url = cacheURL + uncached;

		// Change '%20' to '+' if this is encoded form body content (gh-2658)
		} else if ( s.data && s.processData &&
			( s.contentType || "" ).indexOf( "application/x-www-form-urlencoded" ) === 0 ) {
			s.data = s.data.replace( r20, "+" );
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
				s.accepts[ s.dataTypes[ 0 ] ] +
					( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend &&
			( s.beforeSend.call( callbackContext, jqXHR, s ) === false || completed ) ) {

			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		completeDeferred.add( s.complete );
		jqXHR.done( s.success );
		jqXHR.fail( s.error );

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}

			// If request was aborted inside ajaxSend, stop there
			if ( completed ) {
				return jqXHR;
			}

			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = window.setTimeout( function() {
					jqXHR.abort( "timeout" );
				}, s.timeout );
			}

			try {
				completed = false;
				transport.send( requestHeaders, done );
			} catch ( e ) {

				// Rethrow post-completion exceptions
				if ( completed ) {
					throw e;
				}

				// Propagate others as results
				done( -1, e );
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Ignore repeat invocations
			if ( completed ) {
				return;
			}

			completed = true;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				window.clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader( "Last-Modified" );
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader( "etag" );
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {

				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger( "ajaxStop" );
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
} );

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {

		// Shift arguments if data argument was omitted
		if ( isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		// The url can be an options object (which then must have .url)
		return jQuery.ajax( jQuery.extend( {
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		}, jQuery.isPlainObject( url ) && url ) );
	};
} );


jQuery._evalUrl = function( url ) {
	return jQuery.ajax( {
		url: url,

		// Make this explicit, since user can override this through ajaxSetup (#11264)
		type: "GET",
		dataType: "script",
		cache: true,
		async: false,
		global: false,
		"throws": true
	} );
};


jQuery.fn.extend( {
	wrapAll: function( html ) {
		var wrap;

		if ( this[ 0 ] ) {
			if ( isFunction( html ) ) {
				html = html.call( this[ 0 ] );
			}

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map( function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			} ).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapInner( html.call( this, i ) );
			} );
		}

		return this.each( function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		} );
	},

	wrap: function( html ) {
		var htmlIsFunction = isFunction( html );

		return this.each( function( i ) {
			jQuery( this ).wrapAll( htmlIsFunction ? html.call( this, i ) : html );
		} );
	},

	unwrap: function( selector ) {
		this.parent( selector ).not( "body" ).each( function() {
			jQuery( this ).replaceWith( this.childNodes );
		} );
		return this;
	}
} );


jQuery.expr.pseudos.hidden = function( elem ) {
	return !jQuery.expr.pseudos.visible( elem );
};
jQuery.expr.pseudos.visible = function( elem ) {
	return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
};




jQuery.ajaxSettings.xhr = function() {
	try {
		return new window.XMLHttpRequest();
	} catch ( e ) {}
};

var xhrSuccessStatus = {

		// File protocol always yields status code 0, assume 200
		0: 200,

		// Support: IE <=9 only
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
	var callback, errorCallback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr();

				xhr.open(
					options.type,
					options.url,
					options.async,
					options.username,
					options.password
				);

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
					headers[ "X-Requested-With" ] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							callback = errorCallback = xhr.onload =
								xhr.onerror = xhr.onabort = xhr.ontimeout =
									xhr.onreadystatechange = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {

								// Support: IE <=9 only
								// On a manual native abort, IE9 throws
								// errors on any property access that is not readyState
								if ( typeof xhr.status !== "number" ) {
									complete( 0, "error" );
								} else {
									complete(

										// File: protocol always yields status 0; see #8605, #14207
										xhr.status,
										xhr.statusText
									);
								}
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,

									// Support: IE <=9 only
									// IE9 has no XHR2 but throws on binary (trac-11426)
									// For XHR2 non-text, let the caller handle it (gh-2498)
									( xhr.responseType || "text" ) !== "text"  ||
									typeof xhr.responseText !== "string" ?
										{ binary: xhr.response } :
										{ text: xhr.responseText },
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				errorCallback = xhr.onerror = xhr.ontimeout = callback( "error" );

				// Support: IE 9 only
				// Use onreadystatechange to replace onabort
				// to handle uncaught aborts
				if ( xhr.onabort !== undefined ) {
					xhr.onabort = errorCallback;
				} else {
					xhr.onreadystatechange = function() {

						// Check readyState before timeout as it changes
						if ( xhr.readyState === 4 ) {

							// Allow onerror to be called first,
							// but that will not handle a native abort
							// Also, save errorCallback to a variable
							// as xhr.onerror cannot be accessed
							window.setTimeout( function() {
								if ( callback ) {
									errorCallback();
								}
							} );
						}
					};
				}

				// Create the abort callback
				callback = callback( "abort" );

				try {

					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {

					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




// Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)
jQuery.ajaxPrefilter( function( s ) {
	if ( s.crossDomain ) {
		s.contents.script = false;
	}
} );

// Install script dataType
jQuery.ajaxSetup( {
	accepts: {
		script: "text/javascript, application/javascript, " +
			"application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /\b(?:java|ecma)script\b/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery( "<script>" ).prop( {
					charset: s.scriptCharset,
					src: s.url
				} ).on(
					"load error",
					callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					}
				);

				// Use native DOM manipulation to avoid our domManip AJAX trickery
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" &&
				( s.contentType || "" )
					.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
				rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters[ "script json" ] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// Force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always( function() {

			// If previous value didn't exist - remove it
			if ( overwritten === undefined ) {
				jQuery( window ).removeProp( callbackName );

			// Otherwise restore preexisting value
			} else {
				window[ callbackName ] = overwritten;
			}

			// Save back as free
			if ( s[ callbackName ] ) {

				// Make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// Save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		} );

		// Delegate to script
		return "script";
	}
} );




// Support: Safari 8 only
// In Safari 8 documents created via document.implementation.createHTMLDocument
// collapse sibling forms: the second one becomes a child of the first one.
// Because of that, this security measure has to be disabled in Safari 8.
// https://bugs.webkit.org/show_bug.cgi?id=137337
support.createHTMLDocument = ( function() {
	var body = document.implementation.createHTMLDocument( "" ).body;
	body.innerHTML = "<form></form><form></form>";
	return body.childNodes.length === 2;
} )();


// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( typeof data !== "string" ) {
		return [];
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}

	var base, parsed, scripts;

	if ( !context ) {

		// Stop scripts or inline event handlers from being executed immediately
		// by using document.implementation
		if ( support.createHTMLDocument ) {
			context = document.implementation.createHTMLDocument( "" );

			// Set the base href for the created document
			// so any parsed elements with URLs
			// are based on the document's URL (gh-2965)
			base = context.createElement( "base" );
			base.href = document.location.href;
			context.head.appendChild( base );
		} else {
			context = document;
		}
	}

	parsed = rsingleTag.exec( data );
	scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[ 1 ] ) ];
	}

	parsed = buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	var selector, type, response,
		self = this,
		off = url.indexOf( " " );

	if ( off > -1 ) {
		selector = stripAndCollapse( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax( {
			url: url,

			// If "type" variable is undefined, then "GET" method will be used.
			// Make value of this field explicit since
			// user can override it through ajaxSetup method
			type: type || "GET",
			dataType: "html",
			data: params
		} ).done( function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		// If the request succeeds, this function gets "data", "status", "jqXHR"
		// but they are ignored because response was set above.
		// If it fails, this function gets "jqXHR", "status", "error"
		} ).always( callback && function( jqXHR, status ) {
			self.each( function() {
				callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
			} );
		} );
	}

	return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [
	"ajaxStart",
	"ajaxStop",
	"ajaxComplete",
	"ajaxError",
	"ajaxSuccess",
	"ajaxSend"
], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
} );




jQuery.expr.pseudos.animated = function( elem ) {
	return jQuery.grep( jQuery.timers, function( fn ) {
		return elem === fn.elem;
	} ).length;
};




jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( isFunction( options ) ) {

			// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
			options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend( {

	// offset() relates an element's border box to the document origin
	offset: function( options ) {

		// Preserve chaining for setter
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each( function( i ) {
					jQuery.offset.setOffset( this, options, i );
				} );
		}

		var rect, win,
			elem = this[ 0 ];

		if ( !elem ) {
			return;
		}

		// Return zeros for disconnected and hidden (display: none) elements (gh-2310)
		// Support: IE <=11 only
		// Running getBoundingClientRect on a
		// disconnected node in IE throws an error
		if ( !elem.getClientRects().length ) {
			return { top: 0, left: 0 };
		}

		// Get document-relative position by adding viewport scroll to viewport-relative gBCR
		rect = elem.getBoundingClientRect();
		win = elem.ownerDocument.defaultView;
		return {
			top: rect.top + win.pageYOffset,
			left: rect.left + win.pageXOffset
		};
	},

	// position() relates an element's margin box to its offset parent's padding box
	// This corresponds to the behavior of CSS absolute positioning
	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset, doc,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// position:fixed elements are offset from the viewport, which itself always has zero offset
		if ( jQuery.css( elem, "position" ) === "fixed" ) {

			// Assume position:fixed implies availability of getBoundingClientRect
			offset = elem.getBoundingClientRect();

		} else {
			offset = this.offset();

			// Account for the *real* offset parent, which can be the document or its root element
			// when a statically positioned element is identified
			doc = elem.ownerDocument;
			offsetParent = elem.offsetParent || doc.documentElement;
			while ( offsetParent &&
				( offsetParent === doc.body || offsetParent === doc.documentElement ) &&
				jQuery.css( offsetParent, "position" ) === "static" ) {

				offsetParent = offsetParent.parentNode;
			}
			if ( offsetParent && offsetParent !== elem && offsetParent.nodeType === 1 ) {

				// Incorporate borders into its offset, since they are outside its content origin
				parentOffset = jQuery( offsetParent ).offset();
				parentOffset.top += jQuery.css( offsetParent, "borderTopWidth", true );
				parentOffset.left += jQuery.css( offsetParent, "borderLeftWidth", true );
			}
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	// This method will return documentElement in the following cases:
	// 1) For the element inside the iframe without offsetParent, this method will return
	//    documentElement of the parent window
	// 2) For the hidden or detached element
	// 3) For body or html element, i.e. in case of the html node - it will return itself
	//
	// but those exceptions were never presented as a real life use-cases
	// and might be considered as more preferable results.
	//
	// This logic, however, is not guaranteed and can change at any point in the future
	offsetParent: function() {
		return this.map( function() {
			var offsetParent = this.offsetParent;

			while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || documentElement;
		} );
	}
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {

			// Coalesce documents and windows
			var win;
			if ( isWindow( elem ) ) {
				win = elem;
			} else if ( elem.nodeType === 9 ) {
				win = elem.defaultView;
			}

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : win.pageXOffset,
					top ? val : win.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length );
	};
} );

// Support: Safari <=7 - 9.1, Chrome <=37 - 49
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );

				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
		function( defaultExtra, funcName ) {

		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( isWindow( elem ) ) {

					// $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
					return funcName.indexOf( "outer" ) === 0 ?
						elem[ "inner" + name ] :
						elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?

					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable );
		};
	} );
} );


jQuery.each( ( "blur focus focusin focusout resize scroll click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup contextmenu" ).split( " " ),
	function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
} );

jQuery.fn.extend( {
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	}
} );




jQuery.fn.extend( {

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {

		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ?
			this.off( selector, "**" ) :
			this.off( types, selector || "**", fn );
	}
} );

// Bind a function to a context, optionally partially applying any
// arguments.
// jQuery.proxy is deprecated to promote standards (specifically Function#bind)
// However, it is not slated for removal any time soon
jQuery.proxy = function( fn, context ) {
	var tmp, args, proxy;

	if ( typeof context === "string" ) {
		tmp = fn[ context ];
		context = fn;
		fn = tmp;
	}

	// Quick check to determine if target is callable, in the spec
	// this throws a TypeError, but we will just return undefined.
	if ( !isFunction( fn ) ) {
		return undefined;
	}

	// Simulated bind
	args = slice.call( arguments, 2 );
	proxy = function() {
		return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
	};

	// Set the guid of unique handler to the same of original handler, so it can be removed
	proxy.guid = fn.guid = fn.guid || jQuery.guid++;

	return proxy;
};

jQuery.holdReady = function( hold ) {
	if ( hold ) {
		jQuery.readyWait++;
	} else {
		jQuery.ready( true );
	}
};
jQuery.isArray = Array.isArray;
jQuery.parseJSON = JSON.parse;
jQuery.nodeName = nodeName;
jQuery.isFunction = isFunction;
jQuery.isWindow = isWindow;
jQuery.camelCase = camelCase;
jQuery.type = toType;

jQuery.now = Date.now;

jQuery.isNumeric = function( obj ) {

	// As of jQuery 3.0, isNumeric is limited to
	// strings and numbers (primitives or objects)
	// that can be coerced to finite numbers (gh-2662)
	var type = jQuery.type( obj );
	return ( type === "number" || type === "string" ) &&

		// parseFloat NaNs numeric-cast false positives ("")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		!isNaN( obj - parseFloat( obj ) );
};




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	} );
}




var

	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( !noGlobal ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;
} );

},{"process":"C:\\Users\\Алексей\\AppData\\Roaming\\npm\\node_modules\\parcel-bundler\\node_modules\\process\\browser.js"}],"scripts\\constructors\\formConstr.js":[function(require,module,exports) {
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.VideoUploadingForm = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _formSerialize = require('form-serialize');

var _formSerialize2 = _interopRequireDefault(_formSerialize);

var _imask = require('imask');

var _imask2 = _interopRequireDefault(_imask);

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FormConstr = function () {
    function FormConstr(form) {
        (0, _classCallCheck3.default)(this, FormConstr);

        this.form = form;
        this.name;
        this.email;
        this.phone;
        this.linkUploaded;
        this.uploaded;
        this.description;
        this.checkToS;
        this.inputsValues;
        this.formValuesAreValid = false;
    }

    (0, _createClass3.default)(FormConstr, [{
        key: 'initForm',
        value: function initForm() {

            var phoneMask = new _imask2.default(this.form.phone, {
                mask: '+{7}(000)000-00-00'
            });
        }
    }, {
        key: 'initInputsNames',
        value: function initInputsNames() {

            var name = this.form.name;
            name.removeAttribute('name');
            name.setAttribute('name', 'ListInfo[name]');
            var nameValue = name.value.trim();
            this.name = name;

            var email = this.form.email;
            email.removeAttribute('name');
            email.setAttribute('name', 'ListInfo[email]');
            var emailValue = email.value.trim();
            this.email = email;

            var phone = this.form.phone;
            phone.removeAttribute('name');
            phone.setAttribute('name', 'ListInfo[phone]');
            var phoneValue = phone.value.trim();
            this.phone = phone;

            var linkUploaded = this.form.linkUploaded;
            linkUploaded.removeAttribute('name');
            linkUploaded.setAttribute('name', 'ListInfo[link_uploaded]');
            var linkUploadedValue = linkUploaded.value.trim();
            this.linkUploaded = linkUploaded;

            var uploaded = this.form.uploaded;
            uploaded.removeAttribute('name');
            uploaded.setAttribute('name', 'ListInfo[uploaded]');
            var uploadedValue = uploaded.value.trim();
            this.uploaded = uploaded;

            var description = this.form.description;
            description.removeAttribute('name');
            description.setAttribute('name', 'ListInfo[description]');
            var descriptionValue = description.value;
            this.description = description;

            var checkToS = this.form.ToS;
            var checkToSValue = checkToS.value;
            this.checkToS = checkToS;

            return {
                name: [name, nameValue],
                email: [email, emailValue],
                phone: [phone, phoneValue],
                linkUploaded: [linkUploaded, linkUploadedValue],
                uploaded: [uploaded, uploadedValue],
                description: [description, descriptionValue],
                checkToS: [checkToS, checkToSValue]
            };
        }
    }, {
        key: 'showAlert',
        value: function showAlert(message, className, form, inputBox, input) {
            var div = document.createElement('div');

            div.className = 'video-uploading-form__helper alert alert-' + className;
            div.appendChild(document.createTextNode(message));

            inputBox.appendChild(div, input);

            document.querySelectorAll('.alert').forEach(function (alert) {
                alert.style.cursor = 'pointer';
                alert.addEventListener('click', function () {
                    this.remove();
                });
            });
            setTimeout(function () {
                return document.querySelector('.alert').remove();
            }, 5000);
        }
    }, {
        key: 'returnInputBox',
        value: function returnInputBox(type) {
            return document.querySelector('.video-uploading-form__input-box--' + type);
        }
    }, {
        key: 'validateInput',
        value: function validateInput(input) {

            var regExp = void 0;

            switch (input[0]) {
                case this.name:
                    if (input[1].length == '') {
                        return false;
                    }
                    regExp = /^[А-ЯЁа-яёA-Za-z\s]+$/;
                    return regExp.test(input[1]);
                    break;
                case this.email:
                    regExp = /\S+@\S+\.\S+/;
                    if (regExp.test(input[1])) {
                        return true;
                    }
                    break;
                case this.phone:
                    if (input[1].length == 16) {
                        return true;
                    }
                    break;
                case this.linkUploaded:
                    regExp = /(?:http|https):\/\/((?:[\w-]+)(?:\.[\w-]+)+)(?:[\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
                    var booVar = regExp.test(input[1]);
                    if (booVar) {
                        return true;
                    }
                    if (!booVar) {
                        return false;
                    }
                    break;
                default:
                    return true;
            }
        }
    }, {
        key: 'failValidateScrollUpToFormTop',
        value: function failValidateScrollUpToFormTop() {
            var destination = (0, _jquery2.default)(this.form).offset().top - 80;
            (0, _jquery2.default)('html').animate({ scrollTop: destination }, 400);
            return false;
        }
    }, {
        key: 'validateForm',
        value: function validateForm() {
            var _this = this;

            for (var _len = arguments.length, inputs = Array(_len), _key = 0; _key < _len; _key++) {
                inputs[_key] = arguments[_key];
            }

            inputs.map(function (input) {
                if (!_this.validateInput(input)) {

                    switch (input[0]) {
                        case _this.name:
                            _this.showAlert('Введите валидное имя (только буквы)', 'warning', _this.form, _this.returnInputBox('name'), input[0]);
                            _this.formValuesAreValid = false;
                            _this.failValidateScrollUpToFormTop();
                            break;
                        case _this.email:
                            _this.showAlert('Введите валидный адрес эл. почты', 'warning', _this.form, _this.returnInputBox('email'), input[0]);
                            _this.formValuesAreValid = false;
                            _this.failValidateScrollUpToFormTop();
                            break;
                        case _this.phone:
                            _this.showAlert('Введите свой телефон', 'warning', _this.form, _this.returnInputBox('phone'), input[0]);
                            _this.formValuesAreValid = false;
                            _this.failValidateScrollUpToFormTop();
                            break;
                        case _this.linkUploaded:
                            _this.showAlert('Укажите ссылку или загрузите видео ниже', 'warning', _this.form, _this.returnInputBox('linkUploaded'), input[0]);
                            _this.failValidateScrollUpToFormTop();
                            break;
                        case _this.uploaded:
                            _this.showAlert('Загрузите ваше видео', 'warning', _this.form, _this.returnInputBox('uploaded'), input[0]);
                            break;
                        default:
                            break;
                    }
                } else if (_this.validateInput(input)) {

                    switch (input[0]) {
                        case _this.name:
                            _this.formValuesAreValid = true;
                            break;
                        case _this.email:
                            _this.formValuesAreValid = true;
                            break;
                        case _this.phone:
                            _this.formValuesAreValid = true;
                            break;
                        case _this.linkUploaded:
                            break;
                        case _this.uploaded:
                            break;
                        default:
                            break;
                    }
                }

                if (input[1].trim() == '') {
                    switch (input[0]) {
                        case _this.name:
                            _this.showAlert('Необходимо ввести имя', 'danger', _this.form, _this.returnInputBox('name'), input[0]);
                            _this.formValuesAreValid = false;
                            _this.failValidateScrollUpToFormTop();
                            break;
                        case _this.email:
                            _this.showAlert('Необходимо ввести адрес эл. почты', 'danger', _this.form, _this.returnInputBox('email'), input[0]);
                            _this.formValuesAreValid = false;
                            _this.failValidateScrollUpToFormTop();
                            break;
                        case _this.phone:
                            _this.showAlert('Необходимо ввести свой телефон', 'danger', _this.form, _this.returnInputBox('phone'), input[0]);
                            _this.formValuesAreValid = false;
                            _this.failValidateScrollUpToFormTop();
                            break;
                        case _this.linkUploaded:
                            _this.showAlert('Укажите ссылку на загруженное видео', 'danger', _this.form, _this.returnInputBox('linkUploaded'), input[0]);
                            break;
                        case _this.uploaded:
                            _this.showAlert('Необходимо загрузить видео', 'danger', _this.form, _this.returnInputBox('uploaded'), input[0]);
                            break;
                        default:
                            break;
                    }
                }
            });

            if (this.inputsValues.name[1] == '') {
                return;
            }
            var anyNumRegExp = /-?\d+\.?\d*/;
            if (anyNumRegExp.test(this.inputsValues.name[1])) {
                return;
            }

            if (this.inputsValues.email[1] == '') {
                return;
            }

            if (this.inputsValues.linkUploaded[1] == '' && this.inputsValues.uploaded[1] == '') {
                // this.modalNoFileError()
                return;
            }

            if (this.checkToS.checked && this.formValuesAreValid) {
                this.fetchPostForm();
            }
            if (!this.checkToS.checked) {
                this.modalToSError();
            }
        }
    }, {
        key: 'fetchPostForm',
        value: function fetchPostForm() {
            var _this2 = this;

            var submitButton = this.form.querySelector('.video-uploading-form__submit');
            submitButton.setAttribute('disabled', true);
            submitButton.style.cursor = 'not-allowed';

            fetch('blue/frontend/web/send', {
                method: 'POST',
                body: JSON.stringify({
                    ListInfo: {
                        name: this.inputsValues.name[1],
                        email: this.inputsValues.email[1],
                        phone: this.inputsValues.phone[1],
                        link_uploaded: this.inputsValues.linkUploaded[1],
                        uploaded: this.inputsValues.uploaded[1],
                        description: this.inputsValues.description[1]
                    }
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            }).then(function (res) {
                return res.json();
            }).then(function (data) {
                _this2.modalResolveReject(data.success);
                submitButton.removeAttribute('disabled');
                submitButton.style.cursor = '';
            }).catch(function (err) {
                return console.log(err);
            });
        }
    }, {
        key: 'modalNoFileError',
        value: function modalNoFileError() {
            (0, _jquery2.default)(".modalResolveReject").html('<div class="m-overlay" id="bg-close"><h5>Необходимо загрузить видеофайл или указать ссылку!<i id="close-m"></i></h5></div>').show("slow");
            (0, _jquery2.default)("#close-m").click(function () {
                (0, _jquery2.default)(".modalResolveReject").hide("slow");
            });
            (0, _jquery2.default)("#bg-close").click(function (event) {
                event.preventDefault();
                (0, _jquery2.default)(".modalResolveReject").hide();
            });
        }
    }, {
        key: 'modalToSError',
        value: function modalToSError() {
            (0, _jquery2.default)(".modalResolveReject").html('<div class="m-overlay" id="bg-close"><h5>Нужно принять пользовательское соглашение!<i id="close-m"></i></h5></div>').show("slow");
            (0, _jquery2.default)("#close-m").click(function () {
                (0, _jquery2.default)(".modalResolveReject").hide("slow");
            });
            (0, _jquery2.default)("#bg-close").click(function (event) {
                event.preventDefault();
                (0, _jquery2.default)(".modalResolveReject").hide();
            });
        }
    }, {
        key: 'modalResolveReject',
        value: function modalResolveReject(data) {
            if (data) {
                (0, _jquery2.default)(".modalResolveReject").html('<div class="m-overlay" id="bg-close"><h5>Спасибо!<br/> Видео успешно загружено! <i id="close-m"></i></h5></div>').show("slow");
                (0, _jquery2.default)("#close-m").click(function () {
                    (0, _jquery2.default)(".modalResolveReject").hide("slow");
                    location.reload();
                });
                (0, _jquery2.default)("#bg-close").click(function (event) {
                    event.preventDefault();
                    (0, _jquery2.default)(".modalResolveReject").hide();
                    location.reload();
                });
            }
        }
    }, {
        key: 'submitForm',
        value: function submitForm() {
            var _this3 = this;

            this.form.addEventListener('submit', function (e) {

                e.preventDefault();

                _this3.inputsValues = _this3.initInputsNames();

                _this3.validateForm(_this3.inputsValues.name, _this3.inputsValues.email, _this3.inputsValues.phone, _this3.inputsValues.linkUploaded, _this3.inputsValues.uploaded, _this3.inputsValues.description);
            });
        }
    }]);
    return FormConstr;
}();

var VideoUploadingForm = exports.VideoUploadingForm = new FormConstr(document.querySelector('.video-uploading-form'));
},{"babel-runtime/helpers/classCallCheck":"..\\node_modules\\babel-runtime\\helpers\\classCallCheck.js","babel-runtime/helpers/createClass":"..\\node_modules\\babel-runtime\\helpers\\createClass.js","form-serialize":"..\\node_modules\\form-serialize\\index.js","imask":"..\\node_modules\\imask\\dist\\imask.js","jquery":"..\\node_modules\\jquery\\dist\\jquery.js"}],"..\\node_modules\\magnific-popup\\dist\\jquery.magnific-popup.js":[function(require,module,exports) {
var define;
/*! Magnific Popup - v1.1.0 - 2016-02-20
* http://dimsemenov.com/plugins/magnific-popup/
* Copyright (c) 2016 Dmitry Semenov; */
;(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module. 
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// Node/CommonJS 
		factory(require('jquery'));
	} else {
		// Browser globals 
		factory(window.jQuery || window.Zepto);
	}
})(function ($) {

	/*>>core*/
	/**
  * 
  * Magnific Popup Core JS file
  * 
  */

	/**
  * Private static constants
  */
	var CLOSE_EVENT = 'Close',
	    BEFORE_CLOSE_EVENT = 'BeforeClose',
	    AFTER_CLOSE_EVENT = 'AfterClose',
	    BEFORE_APPEND_EVENT = 'BeforeAppend',
	    MARKUP_PARSE_EVENT = 'MarkupParse',
	    OPEN_EVENT = 'Open',
	    CHANGE_EVENT = 'Change',
	    NS = 'mfp',
	    EVENT_NS = '.' + NS,
	    READY_CLASS = 'mfp-ready',
	    REMOVING_CLASS = 'mfp-removing',
	    PREVENT_CLOSE_CLASS = 'mfp-prevent-close';

	/**
  * Private vars 
  */
	/*jshint -W079 */
	var mfp,
	    // As we have only one instance of MagnificPopup object, we define it locally to not to use 'this'
	MagnificPopup = function () {},
	    _isJQ = !!window.jQuery,
	    _prevStatus,
	    _window = $(window),
	    _document,
	    _prevContentType,
	    _wrapClasses,
	    _currPopupType;

	/**
  * Private functions
  */
	var _mfpOn = function (name, f) {
		mfp.ev.on(NS + name + EVENT_NS, f);
	},
	    _getEl = function (className, appendTo, html, raw) {
		var el = document.createElement('div');
		el.className = 'mfp-' + className;
		if (html) {
			el.innerHTML = html;
		}
		if (!raw) {
			el = $(el);
			if (appendTo) {
				el.appendTo(appendTo);
			}
		} else if (appendTo) {
			appendTo.appendChild(el);
		}
		return el;
	},
	    _mfpTrigger = function (e, data) {
		mfp.ev.triggerHandler(NS + e, data);

		if (mfp.st.callbacks) {
			// converts "mfpEventName" to "eventName" callback and triggers it if it's present
			e = e.charAt(0).toLowerCase() + e.slice(1);
			if (mfp.st.callbacks[e]) {
				mfp.st.callbacks[e].apply(mfp, $.isArray(data) ? data : [data]);
			}
		}
	},
	    _getCloseBtn = function (type) {
		if (type !== _currPopupType || !mfp.currTemplate.closeBtn) {
			mfp.currTemplate.closeBtn = $(mfp.st.closeMarkup.replace('%title%', mfp.st.tClose));
			_currPopupType = type;
		}
		return mfp.currTemplate.closeBtn;
	},

	// Initialize Magnific Popup only when called at least once
	_checkInstance = function () {
		if (!$.magnificPopup.instance) {
			/*jshint -W020 */
			mfp = new MagnificPopup();
			mfp.init();
			$.magnificPopup.instance = mfp;
		}
	},

	// CSS transition detection, http://stackoverflow.com/questions/7264899/detect-css-transitions-using-javascript-and-without-modernizr
	supportsTransitions = function () {
		var s = document.createElement('p').style,
		    // 's' for style. better to create an element if body yet to exist
		v = ['ms', 'O', 'Moz', 'Webkit']; // 'v' for vendor

		if (s['transition'] !== undefined) {
			return true;
		}

		while (v.length) {
			if (v.pop() + 'Transition' in s) {
				return true;
			}
		}

		return false;
	};

	/**
  * Public functions
  */
	MagnificPopup.prototype = {

		constructor: MagnificPopup,

		/**
   * Initializes Magnific Popup plugin. 
   * This function is triggered only once when $.fn.magnificPopup or $.magnificPopup is executed
   */
		init: function () {
			var appVersion = navigator.appVersion;
			mfp.isLowIE = mfp.isIE8 = document.all && !document.addEventListener;
			mfp.isAndroid = /android/gi.test(appVersion);
			mfp.isIOS = /iphone|ipad|ipod/gi.test(appVersion);
			mfp.supportsTransition = supportsTransitions();

			// We disable fixed positioned lightbox on devices that don't handle it nicely.
			// If you know a better way of detecting this - let me know.
			mfp.probablyMobile = mfp.isAndroid || mfp.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent);
			_document = $(document);

			mfp.popupsCache = {};
		},

		/**
   * Opens popup
   * @param  data [description]
   */
		open: function (data) {

			var i;

			if (data.isObj === false) {
				// convert jQuery collection to array to avoid conflicts later
				mfp.items = data.items.toArray();

				mfp.index = 0;
				var items = data.items,
				    item;
				for (i = 0; i < items.length; i++) {
					item = items[i];
					if (item.parsed) {
						item = item.el[0];
					}
					if (item === data.el[0]) {
						mfp.index = i;
						break;
					}
				}
			} else {
				mfp.items = $.isArray(data.items) ? data.items : [data.items];
				mfp.index = data.index || 0;
			}

			// if popup is already opened - we just update the content
			if (mfp.isOpen) {
				mfp.updateItemHTML();
				return;
			}

			mfp.types = [];
			_wrapClasses = '';
			if (data.mainEl && data.mainEl.length) {
				mfp.ev = data.mainEl.eq(0);
			} else {
				mfp.ev = _document;
			}

			if (data.key) {
				if (!mfp.popupsCache[data.key]) {
					mfp.popupsCache[data.key] = {};
				}
				mfp.currTemplate = mfp.popupsCache[data.key];
			} else {
				mfp.currTemplate = {};
			}

			mfp.st = $.extend(true, {}, $.magnificPopup.defaults, data);
			mfp.fixedContentPos = mfp.st.fixedContentPos === 'auto' ? !mfp.probablyMobile : mfp.st.fixedContentPos;

			if (mfp.st.modal) {
				mfp.st.closeOnContentClick = false;
				mfp.st.closeOnBgClick = false;
				mfp.st.showCloseBtn = false;
				mfp.st.enableEscapeKey = false;
			}

			// Building markup
			// main containers are created only once
			if (!mfp.bgOverlay) {

				// Dark overlay
				mfp.bgOverlay = _getEl('bg').on('click' + EVENT_NS, function () {
					mfp.close();
				});

				mfp.wrap = _getEl('wrap').attr('tabindex', -1).on('click' + EVENT_NS, function (e) {
					if (mfp._checkIfClose(e.target)) {
						mfp.close();
					}
				});

				mfp.container = _getEl('container', mfp.wrap);
			}

			mfp.contentContainer = _getEl('content');
			if (mfp.st.preloader) {
				mfp.preloader = _getEl('preloader', mfp.container, mfp.st.tLoading);
			}

			// Initializing modules
			var modules = $.magnificPopup.modules;
			for (i = 0; i < modules.length; i++) {
				var n = modules[i];
				n = n.charAt(0).toUpperCase() + n.slice(1);
				mfp['init' + n].call(mfp);
			}
			_mfpTrigger('BeforeOpen');

			if (mfp.st.showCloseBtn) {
				// Close button
				if (!mfp.st.closeBtnInside) {
					mfp.wrap.append(_getCloseBtn());
				} else {
					_mfpOn(MARKUP_PARSE_EVENT, function (e, template, values, item) {
						values.close_replaceWith = _getCloseBtn(item.type);
					});
					_wrapClasses += ' mfp-close-btn-in';
				}
			}

			if (mfp.st.alignTop) {
				_wrapClasses += ' mfp-align-top';
			}

			if (mfp.fixedContentPos) {
				mfp.wrap.css({
					overflow: mfp.st.overflowY,
					overflowX: 'hidden',
					overflowY: mfp.st.overflowY
				});
			} else {
				mfp.wrap.css({
					top: _window.scrollTop(),
					position: 'absolute'
				});
			}
			if (mfp.st.fixedBgPos === false || mfp.st.fixedBgPos === 'auto' && !mfp.fixedContentPos) {
				mfp.bgOverlay.css({
					height: _document.height(),
					position: 'absolute'
				});
			}

			if (mfp.st.enableEscapeKey) {
				// Close on ESC key
				_document.on('keyup' + EVENT_NS, function (e) {
					if (e.keyCode === 27) {
						mfp.close();
					}
				});
			}

			_window.on('resize' + EVENT_NS, function () {
				mfp.updateSize();
			});

			if (!mfp.st.closeOnContentClick) {
				_wrapClasses += ' mfp-auto-cursor';
			}

			if (_wrapClasses) mfp.wrap.addClass(_wrapClasses);

			// this triggers recalculation of layout, so we get it once to not to trigger twice
			var windowHeight = mfp.wH = _window.height();

			var windowStyles = {};

			if (mfp.fixedContentPos) {
				if (mfp._hasScrollBar(windowHeight)) {
					var s = mfp._getScrollbarSize();
					if (s) {
						windowStyles.marginRight = s;
					}
				}
			}

			if (mfp.fixedContentPos) {
				if (!mfp.isIE7) {
					windowStyles.overflow = 'hidden';
				} else {
					// ie7 double-scroll bug
					$('body, html').css('overflow', 'hidden');
				}
			}

			var classesToadd = mfp.st.mainClass;
			if (mfp.isIE7) {
				classesToadd += ' mfp-ie7';
			}
			if (classesToadd) {
				mfp._addClassToMFP(classesToadd);
			}

			// add content
			mfp.updateItemHTML();

			_mfpTrigger('BuildControls');

			// remove scrollbar, add margin e.t.c
			$('html').css(windowStyles);

			// add everything to DOM
			mfp.bgOverlay.add(mfp.wrap).prependTo(mfp.st.prependTo || $(document.body));

			// Save last focused element
			mfp._lastFocusedEl = document.activeElement;

			// Wait for next cycle to allow CSS transition
			setTimeout(function () {

				if (mfp.content) {
					mfp._addClassToMFP(READY_CLASS);
					mfp._setFocus();
				} else {
					// if content is not defined (not loaded e.t.c) we add class only for BG
					mfp.bgOverlay.addClass(READY_CLASS);
				}

				// Trap the focus in popup
				_document.on('focusin' + EVENT_NS, mfp._onFocusIn);
			}, 16);

			mfp.isOpen = true;
			mfp.updateSize(windowHeight);
			_mfpTrigger(OPEN_EVENT);

			return data;
		},

		/**
   * Closes the popup
   */
		close: function () {
			if (!mfp.isOpen) return;
			_mfpTrigger(BEFORE_CLOSE_EVENT);

			mfp.isOpen = false;
			// for CSS3 animation
			if (mfp.st.removalDelay && !mfp.isLowIE && mfp.supportsTransition) {
				mfp._addClassToMFP(REMOVING_CLASS);
				setTimeout(function () {
					mfp._close();
				}, mfp.st.removalDelay);
			} else {
				mfp._close();
			}
		},

		/**
   * Helper for close() function
   */
		_close: function () {
			_mfpTrigger(CLOSE_EVENT);

			var classesToRemove = REMOVING_CLASS + ' ' + READY_CLASS + ' ';

			mfp.bgOverlay.detach();
			mfp.wrap.detach();
			mfp.container.empty();

			if (mfp.st.mainClass) {
				classesToRemove += mfp.st.mainClass + ' ';
			}

			mfp._removeClassFromMFP(classesToRemove);

			if (mfp.fixedContentPos) {
				var windowStyles = { marginRight: '' };
				if (mfp.isIE7) {
					$('body, html').css('overflow', '');
				} else {
					windowStyles.overflow = '';
				}
				$('html').css(windowStyles);
			}

			_document.off('keyup' + EVENT_NS + ' focusin' + EVENT_NS);
			mfp.ev.off(EVENT_NS);

			// clean up DOM elements that aren't removed
			mfp.wrap.attr('class', 'mfp-wrap').removeAttr('style');
			mfp.bgOverlay.attr('class', 'mfp-bg');
			mfp.container.attr('class', 'mfp-container');

			// remove close button from target element
			if (mfp.st.showCloseBtn && (!mfp.st.closeBtnInside || mfp.currTemplate[mfp.currItem.type] === true)) {
				if (mfp.currTemplate.closeBtn) mfp.currTemplate.closeBtn.detach();
			}

			if (mfp.st.autoFocusLast && mfp._lastFocusedEl) {
				$(mfp._lastFocusedEl).focus(); // put tab focus back
			}
			mfp.currItem = null;
			mfp.content = null;
			mfp.currTemplate = null;
			mfp.prevHeight = 0;

			_mfpTrigger(AFTER_CLOSE_EVENT);
		},

		updateSize: function (winHeight) {

			if (mfp.isIOS) {
				// fixes iOS nav bars https://github.com/dimsemenov/Magnific-Popup/issues/2
				var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
				var height = window.innerHeight * zoomLevel;
				mfp.wrap.css('height', height);
				mfp.wH = height;
			} else {
				mfp.wH = winHeight || _window.height();
			}
			// Fixes #84: popup incorrectly positioned with position:relative on body
			if (!mfp.fixedContentPos) {
				mfp.wrap.css('height', mfp.wH);
			}

			_mfpTrigger('Resize');
		},

		/**
   * Set content of popup based on current index
   */
		updateItemHTML: function () {
			var item = mfp.items[mfp.index];

			// Detach and perform modifications
			mfp.contentContainer.detach();

			if (mfp.content) mfp.content.detach();

			if (!item.parsed) {
				item = mfp.parseEl(mfp.index);
			}

			var type = item.type;

			_mfpTrigger('BeforeChange', [mfp.currItem ? mfp.currItem.type : '', type]);
			// BeforeChange event works like so:
			// _mfpOn('BeforeChange', function(e, prevType, newType) { });

			mfp.currItem = item;

			if (!mfp.currTemplate[type]) {
				var markup = mfp.st[type] ? mfp.st[type].markup : false;

				// allows to modify markup
				_mfpTrigger('FirstMarkupParse', markup);

				if (markup) {
					mfp.currTemplate[type] = $(markup);
				} else {
					// if there is no markup found we just define that template is parsed
					mfp.currTemplate[type] = true;
				}
			}

			if (_prevContentType && _prevContentType !== item.type) {
				mfp.container.removeClass('mfp-' + _prevContentType + '-holder');
			}

			var newContent = mfp['get' + type.charAt(0).toUpperCase() + type.slice(1)](item, mfp.currTemplate[type]);
			mfp.appendContent(newContent, type);

			item.preloaded = true;

			_mfpTrigger(CHANGE_EVENT, item);
			_prevContentType = item.type;

			// Append container back after its content changed
			mfp.container.prepend(mfp.contentContainer);

			_mfpTrigger('AfterChange');
		},

		/**
   * Set HTML content of popup
   */
		appendContent: function (newContent, type) {
			mfp.content = newContent;

			if (newContent) {
				if (mfp.st.showCloseBtn && mfp.st.closeBtnInside && mfp.currTemplate[type] === true) {
					// if there is no markup, we just append close button element inside
					if (!mfp.content.find('.mfp-close').length) {
						mfp.content.append(_getCloseBtn());
					}
				} else {
					mfp.content = newContent;
				}
			} else {
				mfp.content = '';
			}

			_mfpTrigger(BEFORE_APPEND_EVENT);
			mfp.container.addClass('mfp-' + type + '-holder');

			mfp.contentContainer.append(mfp.content);
		},

		/**
   * Creates Magnific Popup data object based on given data
   * @param  {int} index Index of item to parse
   */
		parseEl: function (index) {
			var item = mfp.items[index],
			    type;

			if (item.tagName) {
				item = { el: $(item) };
			} else {
				type = item.type;
				item = { data: item, src: item.src };
			}

			if (item.el) {
				var types = mfp.types;

				// check for 'mfp-TYPE' class
				for (var i = 0; i < types.length; i++) {
					if (item.el.hasClass('mfp-' + types[i])) {
						type = types[i];
						break;
					}
				}

				item.src = item.el.attr('data-mfp-src');
				if (!item.src) {
					item.src = item.el.attr('href');
				}
			}

			item.type = type || mfp.st.type || 'inline';
			item.index = index;
			item.parsed = true;
			mfp.items[index] = item;
			_mfpTrigger('ElementParse', item);

			return mfp.items[index];
		},

		/**
   * Initializes single popup or a group of popups
   */
		addGroup: function (el, options) {
			var eHandler = function (e) {
				e.mfpEl = this;
				mfp._openClick(e, el, options);
			};

			if (!options) {
				options = {};
			}

			var eName = 'click.magnificPopup';
			options.mainEl = el;

			if (options.items) {
				options.isObj = true;
				el.off(eName).on(eName, eHandler);
			} else {
				options.isObj = false;
				if (options.delegate) {
					el.off(eName).on(eName, options.delegate, eHandler);
				} else {
					options.items = el;
					el.off(eName).on(eName, eHandler);
				}
			}
		},
		_openClick: function (e, el, options) {
			var midClick = options.midClick !== undefined ? options.midClick : $.magnificPopup.defaults.midClick;

			if (!midClick && (e.which === 2 || e.ctrlKey || e.metaKey || e.altKey || e.shiftKey)) {
				return;
			}

			var disableOn = options.disableOn !== undefined ? options.disableOn : $.magnificPopup.defaults.disableOn;

			if (disableOn) {
				if ($.isFunction(disableOn)) {
					if (!disableOn.call(mfp)) {
						return true;
					}
				} else {
					// else it's number
					if (_window.width() < disableOn) {
						return true;
					}
				}
			}

			if (e.type) {
				e.preventDefault();

				// This will prevent popup from closing if element is inside and popup is already opened
				if (mfp.isOpen) {
					e.stopPropagation();
				}
			}

			options.el = $(e.mfpEl);
			if (options.delegate) {
				options.items = el.find(options.delegate);
			}
			mfp.open(options);
		},

		/**
   * Updates text on preloader
   */
		updateStatus: function (status, text) {

			if (mfp.preloader) {
				if (_prevStatus !== status) {
					mfp.container.removeClass('mfp-s-' + _prevStatus);
				}

				if (!text && status === 'loading') {
					text = mfp.st.tLoading;
				}

				var data = {
					status: status,
					text: text
				};
				// allows to modify status
				_mfpTrigger('UpdateStatus', data);

				status = data.status;
				text = data.text;

				mfp.preloader.html(text);

				mfp.preloader.find('a').on('click', function (e) {
					e.stopImmediatePropagation();
				});

				mfp.container.addClass('mfp-s-' + status);
				_prevStatus = status;
			}
		},

		/*
  	"Private" helpers that aren't private at all
   */
		// Check to close popup or not
		// "target" is an element that was clicked
		_checkIfClose: function (target) {

			if ($(target).hasClass(PREVENT_CLOSE_CLASS)) {
				return;
			}

			var closeOnContent = mfp.st.closeOnContentClick;
			var closeOnBg = mfp.st.closeOnBgClick;

			if (closeOnContent && closeOnBg) {
				return true;
			} else {

				// We close the popup if click is on close button or on preloader. Or if there is no content.
				if (!mfp.content || $(target).hasClass('mfp-close') || mfp.preloader && target === mfp.preloader[0]) {
					return true;
				}

				// if click is outside the content
				if (target !== mfp.content[0] && !$.contains(mfp.content[0], target)) {
					if (closeOnBg) {
						// last check, if the clicked element is in DOM, (in case it's removed onclick)
						if ($.contains(document, target)) {
							return true;
						}
					}
				} else if (closeOnContent) {
					return true;
				}
			}
			return false;
		},
		_addClassToMFP: function (cName) {
			mfp.bgOverlay.addClass(cName);
			mfp.wrap.addClass(cName);
		},
		_removeClassFromMFP: function (cName) {
			this.bgOverlay.removeClass(cName);
			mfp.wrap.removeClass(cName);
		},
		_hasScrollBar: function (winHeight) {
			return (mfp.isIE7 ? _document.height() : document.body.scrollHeight) > (winHeight || _window.height());
		},
		_setFocus: function () {
			(mfp.st.focus ? mfp.content.find(mfp.st.focus).eq(0) : mfp.wrap).focus();
		},
		_onFocusIn: function (e) {
			if (e.target !== mfp.wrap[0] && !$.contains(mfp.wrap[0], e.target)) {
				mfp._setFocus();
				return false;
			}
		},
		_parseMarkup: function (template, values, item) {
			var arr;
			if (item.data) {
				values = $.extend(item.data, values);
			}
			_mfpTrigger(MARKUP_PARSE_EVENT, [template, values, item]);

			$.each(values, function (key, value) {
				if (value === undefined || value === false) {
					return true;
				}
				arr = key.split('_');
				if (arr.length > 1) {
					var el = template.find(EVENT_NS + '-' + arr[0]);

					if (el.length > 0) {
						var attr = arr[1];
						if (attr === 'replaceWith') {
							if (el[0] !== value[0]) {
								el.replaceWith(value);
							}
						} else if (attr === 'img') {
							if (el.is('img')) {
								el.attr('src', value);
							} else {
								el.replaceWith($('<img>').attr('src', value).attr('class', el.attr('class')));
							}
						} else {
							el.attr(arr[1], value);
						}
					}
				} else {
					template.find(EVENT_NS + '-' + key).html(value);
				}
			});
		},

		_getScrollbarSize: function () {
			// thx David
			if (mfp.scrollbarSize === undefined) {
				var scrollDiv = document.createElement("div");
				scrollDiv.style.cssText = 'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
				document.body.appendChild(scrollDiv);
				mfp.scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
				document.body.removeChild(scrollDiv);
			}
			return mfp.scrollbarSize;
		}

	}; /* MagnificPopup core prototype end */

	/**
  * Public static functions
  */
	$.magnificPopup = {
		instance: null,
		proto: MagnificPopup.prototype,
		modules: [],

		open: function (options, index) {
			_checkInstance();

			if (!options) {
				options = {};
			} else {
				options = $.extend(true, {}, options);
			}

			options.isObj = true;
			options.index = index || 0;
			return this.instance.open(options);
		},

		close: function () {
			return $.magnificPopup.instance && $.magnificPopup.instance.close();
		},

		registerModule: function (name, module) {
			if (module.options) {
				$.magnificPopup.defaults[name] = module.options;
			}
			$.extend(this.proto, module.proto);
			this.modules.push(name);
		},

		defaults: {

			// Info about options is in docs:
			// http://dimsemenov.com/plugins/magnific-popup/documentation.html#options

			disableOn: 0,

			key: null,

			midClick: false,

			mainClass: '',

			preloader: true,

			focus: '', // CSS selector of input to focus after popup is opened

			closeOnContentClick: false,

			closeOnBgClick: true,

			closeBtnInside: true,

			showCloseBtn: true,

			enableEscapeKey: true,

			modal: false,

			alignTop: false,

			removalDelay: 0,

			prependTo: null,

			fixedContentPos: 'auto',

			fixedBgPos: 'auto',

			overflowY: 'auto',

			closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',

			tClose: 'Close (Esc)',

			tLoading: 'Loading...',

			autoFocusLast: true

		}
	};

	$.fn.magnificPopup = function (options) {
		_checkInstance();

		var jqEl = $(this);

		// We call some API method of first param is a string
		if (typeof options === "string") {

			if (options === 'open') {
				var items,
				    itemOpts = _isJQ ? jqEl.data('magnificPopup') : jqEl[0].magnificPopup,
				    index = parseInt(arguments[1], 10) || 0;

				if (itemOpts.items) {
					items = itemOpts.items[index];
				} else {
					items = jqEl;
					if (itemOpts.delegate) {
						items = items.find(itemOpts.delegate);
					}
					items = items.eq(index);
				}
				mfp._openClick({ mfpEl: items }, jqEl, itemOpts);
			} else {
				if (mfp.isOpen) mfp[options].apply(mfp, Array.prototype.slice.call(arguments, 1));
			}
		} else {
			// clone options obj
			options = $.extend(true, {}, options);

			/*
    * As Zepto doesn't support .data() method for objects
    * and it works only in normal browsers
    * we assign "options" object directly to the DOM element. FTW!
    */
			if (_isJQ) {
				jqEl.data('magnificPopup', options);
			} else {
				jqEl[0].magnificPopup = options;
			}

			mfp.addGroup(jqEl, options);
		}
		return jqEl;
	};

	/*>>core*/

	/*>>inline*/

	var INLINE_NS = 'inline',
	    _hiddenClass,
	    _inlinePlaceholder,
	    _lastInlineElement,
	    _putInlineElementsBack = function () {
		if (_lastInlineElement) {
			_inlinePlaceholder.after(_lastInlineElement.addClass(_hiddenClass)).detach();
			_lastInlineElement = null;
		}
	};

	$.magnificPopup.registerModule(INLINE_NS, {
		options: {
			hiddenClass: 'hide', // will be appended with `mfp-` prefix
			markup: '',
			tNotFound: 'Content not found'
		},
		proto: {

			initInline: function () {
				mfp.types.push(INLINE_NS);

				_mfpOn(CLOSE_EVENT + '.' + INLINE_NS, function () {
					_putInlineElementsBack();
				});
			},

			getInline: function (item, template) {

				_putInlineElementsBack();

				if (item.src) {
					var inlineSt = mfp.st.inline,
					    el = $(item.src);

					if (el.length) {

						// If target element has parent - we replace it with placeholder and put it back after popup is closed
						var parent = el[0].parentNode;
						if (parent && parent.tagName) {
							if (!_inlinePlaceholder) {
								_hiddenClass = inlineSt.hiddenClass;
								_inlinePlaceholder = _getEl(_hiddenClass);
								_hiddenClass = 'mfp-' + _hiddenClass;
							}
							// replace target inline element with placeholder
							_lastInlineElement = el.after(_inlinePlaceholder).detach().removeClass(_hiddenClass);
						}

						mfp.updateStatus('ready');
					} else {
						mfp.updateStatus('error', inlineSt.tNotFound);
						el = $('<div>');
					}

					item.inlineElement = el;
					return el;
				}

				mfp.updateStatus('ready');
				mfp._parseMarkup(template, {}, item);
				return template;
			}
		}
	});

	/*>>inline*/

	/*>>ajax*/
	var AJAX_NS = 'ajax',
	    _ajaxCur,
	    _removeAjaxCursor = function () {
		if (_ajaxCur) {
			$(document.body).removeClass(_ajaxCur);
		}
	},
	    _destroyAjaxRequest = function () {
		_removeAjaxCursor();
		if (mfp.req) {
			mfp.req.abort();
		}
	};

	$.magnificPopup.registerModule(AJAX_NS, {

		options: {
			settings: null,
			cursor: 'mfp-ajax-cur',
			tError: '<a href="%url%">The content</a> could not be loaded.'
		},

		proto: {
			initAjax: function () {
				mfp.types.push(AJAX_NS);
				_ajaxCur = mfp.st.ajax.cursor;

				_mfpOn(CLOSE_EVENT + '.' + AJAX_NS, _destroyAjaxRequest);
				_mfpOn('BeforeChange.' + AJAX_NS, _destroyAjaxRequest);
			},
			getAjax: function (item) {

				if (_ajaxCur) {
					$(document.body).addClass(_ajaxCur);
				}

				mfp.updateStatus('loading');

				var opts = $.extend({
					url: item.src,
					success: function (data, textStatus, jqXHR) {
						var temp = {
							data: data,
							xhr: jqXHR
						};

						_mfpTrigger('ParseAjax', temp);

						mfp.appendContent($(temp.data), AJAX_NS);

						item.finished = true;

						_removeAjaxCursor();

						mfp._setFocus();

						setTimeout(function () {
							mfp.wrap.addClass(READY_CLASS);
						}, 16);

						mfp.updateStatus('ready');

						_mfpTrigger('AjaxContentAdded');
					},
					error: function () {
						_removeAjaxCursor();
						item.finished = item.loadError = true;
						mfp.updateStatus('error', mfp.st.ajax.tError.replace('%url%', item.src));
					}
				}, mfp.st.ajax.settings);

				mfp.req = $.ajax(opts);

				return '';
			}
		}
	});

	/*>>ajax*/

	/*>>image*/
	var _imgInterval,
	    _getTitle = function (item) {
		if (item.data && item.data.title !== undefined) return item.data.title;

		var src = mfp.st.image.titleSrc;

		if (src) {
			if ($.isFunction(src)) {
				return src.call(mfp, item);
			} else if (item.el) {
				return item.el.attr(src) || '';
			}
		}
		return '';
	};

	$.magnificPopup.registerModule('image', {

		options: {
			markup: '<div class="mfp-figure">' + '<div class="mfp-close"></div>' + '<figure>' + '<div class="mfp-img"></div>' + '<figcaption>' + '<div class="mfp-bottom-bar">' + '<div class="mfp-title"></div>' + '<div class="mfp-counter"></div>' + '</div>' + '</figcaption>' + '</figure>' + '</div>',
			cursor: 'mfp-zoom-out-cur',
			titleSrc: 'title',
			verticalFit: true,
			tError: '<a href="%url%">The image</a> could not be loaded.'
		},

		proto: {
			initImage: function () {
				var imgSt = mfp.st.image,
				    ns = '.image';

				mfp.types.push('image');

				_mfpOn(OPEN_EVENT + ns, function () {
					if (mfp.currItem.type === 'image' && imgSt.cursor) {
						$(document.body).addClass(imgSt.cursor);
					}
				});

				_mfpOn(CLOSE_EVENT + ns, function () {
					if (imgSt.cursor) {
						$(document.body).removeClass(imgSt.cursor);
					}
					_window.off('resize' + EVENT_NS);
				});

				_mfpOn('Resize' + ns, mfp.resizeImage);
				if (mfp.isLowIE) {
					_mfpOn('AfterChange', mfp.resizeImage);
				}
			},
			resizeImage: function () {
				var item = mfp.currItem;
				if (!item || !item.img) return;

				if (mfp.st.image.verticalFit) {
					var decr = 0;
					// fix box-sizing in ie7/8
					if (mfp.isLowIE) {
						decr = parseInt(item.img.css('padding-top'), 10) + parseInt(item.img.css('padding-bottom'), 10);
					}
					item.img.css('max-height', mfp.wH - decr);
				}
			},
			_onImageHasSize: function (item) {
				if (item.img) {

					item.hasSize = true;

					if (_imgInterval) {
						clearInterval(_imgInterval);
					}

					item.isCheckingImgSize = false;

					_mfpTrigger('ImageHasSize', item);

					if (item.imgHidden) {
						if (mfp.content) mfp.content.removeClass('mfp-loading');

						item.imgHidden = false;
					}
				}
			},

			/**
    * Function that loops until the image has size to display elements that rely on it asap
    */
			findImageSize: function (item) {

				var counter = 0,
				    img = item.img[0],
				    mfpSetInterval = function (delay) {

					if (_imgInterval) {
						clearInterval(_imgInterval);
					}
					// decelerating interval that checks for size of an image
					_imgInterval = setInterval(function () {
						if (img.naturalWidth > 0) {
							mfp._onImageHasSize(item);
							return;
						}

						if (counter > 200) {
							clearInterval(_imgInterval);
						}

						counter++;
						if (counter === 3) {
							mfpSetInterval(10);
						} else if (counter === 40) {
							mfpSetInterval(50);
						} else if (counter === 100) {
							mfpSetInterval(500);
						}
					}, delay);
				};

				mfpSetInterval(1);
			},

			getImage: function (item, template) {

				var guard = 0,


				// image load complete handler
				onLoadComplete = function () {
					if (item) {
						if (item.img[0].complete) {
							item.img.off('.mfploader');

							if (item === mfp.currItem) {
								mfp._onImageHasSize(item);

								mfp.updateStatus('ready');
							}

							item.hasSize = true;
							item.loaded = true;

							_mfpTrigger('ImageLoadComplete');
						} else {
							// if image complete check fails 200 times (20 sec), we assume that there was an error.
							guard++;
							if (guard < 200) {
								setTimeout(onLoadComplete, 100);
							} else {
								onLoadError();
							}
						}
					}
				},


				// image error handler
				onLoadError = function () {
					if (item) {
						item.img.off('.mfploader');
						if (item === mfp.currItem) {
							mfp._onImageHasSize(item);
							mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
						}

						item.hasSize = true;
						item.loaded = true;
						item.loadError = true;
					}
				},
				    imgSt = mfp.st.image;

				var el = template.find('.mfp-img');
				if (el.length) {
					var img = document.createElement('img');
					img.className = 'mfp-img';
					if (item.el && item.el.find('img').length) {
						img.alt = item.el.find('img').attr('alt');
					}
					item.img = $(img).on('load.mfploader', onLoadComplete).on('error.mfploader', onLoadError);
					img.src = item.src;

					// without clone() "error" event is not firing when IMG is replaced by new IMG
					// TODO: find a way to avoid such cloning
					if (el.is('img')) {
						item.img = item.img.clone();
					}

					img = item.img[0];
					if (img.naturalWidth > 0) {
						item.hasSize = true;
					} else if (!img.width) {
						item.hasSize = false;
					}
				}

				mfp._parseMarkup(template, {
					title: _getTitle(item),
					img_replaceWith: item.img
				}, item);

				mfp.resizeImage();

				if (item.hasSize) {
					if (_imgInterval) clearInterval(_imgInterval);

					if (item.loadError) {
						template.addClass('mfp-loading');
						mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src));
					} else {
						template.removeClass('mfp-loading');
						mfp.updateStatus('ready');
					}
					return template;
				}

				mfp.updateStatus('loading');
				item.loading = true;

				if (!item.hasSize) {
					item.imgHidden = true;
					template.addClass('mfp-loading');
					mfp.findImageSize(item);
				}

				return template;
			}
		}
	});

	/*>>image*/

	/*>>zoom*/
	var hasMozTransform,
	    getHasMozTransform = function () {
		if (hasMozTransform === undefined) {
			hasMozTransform = document.createElement('p').style.MozTransform !== undefined;
		}
		return hasMozTransform;
	};

	$.magnificPopup.registerModule('zoom', {

		options: {
			enabled: false,
			easing: 'ease-in-out',
			duration: 300,
			opener: function (element) {
				return element.is('img') ? element : element.find('img');
			}
		},

		proto: {

			initZoom: function () {
				var zoomSt = mfp.st.zoom,
				    ns = '.zoom',
				    image;

				if (!zoomSt.enabled || !mfp.supportsTransition) {
					return;
				}

				var duration = zoomSt.duration,
				    getElToAnimate = function (image) {
					var newImg = image.clone().removeAttr('style').removeAttr('class').addClass('mfp-animated-image'),
					    transition = 'all ' + zoomSt.duration / 1000 + 's ' + zoomSt.easing,
					    cssObj = {
						position: 'fixed',
						zIndex: 9999,
						left: 0,
						top: 0,
						'-webkit-backface-visibility': 'hidden'
					},
					    t = 'transition';

					cssObj['-webkit-' + t] = cssObj['-moz-' + t] = cssObj['-o-' + t] = cssObj[t] = transition;

					newImg.css(cssObj);
					return newImg;
				},
				    showMainContent = function () {
					mfp.content.css('visibility', 'visible');
				},
				    openTimeout,
				    animatedImg;

				_mfpOn('BuildControls' + ns, function () {
					if (mfp._allowZoom()) {

						clearTimeout(openTimeout);
						mfp.content.css('visibility', 'hidden');

						// Basically, all code below does is clones existing image, puts in on top of the current one and animated it

						image = mfp._getItemToZoom();

						if (!image) {
							showMainContent();
							return;
						}

						animatedImg = getElToAnimate(image);

						animatedImg.css(mfp._getOffset());

						mfp.wrap.append(animatedImg);

						openTimeout = setTimeout(function () {
							animatedImg.css(mfp._getOffset(true));
							openTimeout = setTimeout(function () {

								showMainContent();

								setTimeout(function () {
									animatedImg.remove();
									image = animatedImg = null;
									_mfpTrigger('ZoomAnimationEnded');
								}, 16); // avoid blink when switching images
							}, duration); // this timeout equals animation duration
						}, 16); // by adding this timeout we avoid short glitch at the beginning of animation


						// Lots of timeouts...
					}
				});
				_mfpOn(BEFORE_CLOSE_EVENT + ns, function () {
					if (mfp._allowZoom()) {

						clearTimeout(openTimeout);

						mfp.st.removalDelay = duration;

						if (!image) {
							image = mfp._getItemToZoom();
							if (!image) {
								return;
							}
							animatedImg = getElToAnimate(image);
						}

						animatedImg.css(mfp._getOffset(true));
						mfp.wrap.append(animatedImg);
						mfp.content.css('visibility', 'hidden');

						setTimeout(function () {
							animatedImg.css(mfp._getOffset());
						}, 16);
					}
				});

				_mfpOn(CLOSE_EVENT + ns, function () {
					if (mfp._allowZoom()) {
						showMainContent();
						if (animatedImg) {
							animatedImg.remove();
						}
						image = null;
					}
				});
			},

			_allowZoom: function () {
				return mfp.currItem.type === 'image';
			},

			_getItemToZoom: function () {
				if (mfp.currItem.hasSize) {
					return mfp.currItem.img;
				} else {
					return false;
				}
			},

			// Get element postion relative to viewport
			_getOffset: function (isLarge) {
				var el;
				if (isLarge) {
					el = mfp.currItem.img;
				} else {
					el = mfp.st.zoom.opener(mfp.currItem.el || mfp.currItem);
				}

				var offset = el.offset();
				var paddingTop = parseInt(el.css('padding-top'), 10);
				var paddingBottom = parseInt(el.css('padding-bottom'), 10);
				offset.top -= $(window).scrollTop() - paddingTop;

				/*
    	Animating left + top + width/height looks glitchy in Firefox, but perfect in Chrome. And vice-versa.
    	 */
				var obj = {
					width: el.width(),
					// fix Zepto height+padding issue
					height: (_isJQ ? el.innerHeight() : el[0].offsetHeight) - paddingBottom - paddingTop
				};

				// I hate to do this, but there is no another option
				if (getHasMozTransform()) {
					obj['-moz-transform'] = obj['transform'] = 'translate(' + offset.left + 'px,' + offset.top + 'px)';
				} else {
					obj.left = offset.left;
					obj.top = offset.top;
				}
				return obj;
			}

		}
	});

	/*>>zoom*/

	/*>>iframe*/

	var IFRAME_NS = 'iframe',
	    _emptyPage = '//about:blank',
	    _fixIframeBugs = function (isShowing) {
		if (mfp.currTemplate[IFRAME_NS]) {
			var el = mfp.currTemplate[IFRAME_NS].find('iframe');
			if (el.length) {
				// reset src after the popup is closed to avoid "video keeps playing after popup is closed" bug
				if (!isShowing) {
					el[0].src = _emptyPage;
				}

				// IE8 black screen bug fix
				if (mfp.isIE8) {
					el.css('display', isShowing ? 'block' : 'none');
				}
			}
		}
	};

	$.magnificPopup.registerModule(IFRAME_NS, {

		options: {
			markup: '<div class="mfp-iframe-scaler">' + '<div class="mfp-close"></div>' + '<iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe>' + '</div>',

			srcAction: 'iframe_src',

			// we don't care and support only one default type of URL by default
			patterns: {
				youtube: {
					index: 'youtube.com',
					id: 'v=',
					src: '//www.youtube.com/embed/%id%?autoplay=1'
				},
				vimeo: {
					index: 'vimeo.com/',
					id: '/',
					src: '//player.vimeo.com/video/%id%?autoplay=1'
				},
				gmaps: {
					index: '//maps.google.',
					src: '%id%&output=embed'
				}
			}
		},

		proto: {
			initIframe: function () {
				mfp.types.push(IFRAME_NS);

				_mfpOn('BeforeChange', function (e, prevType, newType) {
					if (prevType !== newType) {
						if (prevType === IFRAME_NS) {
							_fixIframeBugs(); // iframe if removed
						} else if (newType === IFRAME_NS) {
							_fixIframeBugs(true); // iframe is showing
						}
					} // else {
					// iframe source is switched, don't do anything
					//}
				});

				_mfpOn(CLOSE_EVENT + '.' + IFRAME_NS, function () {
					_fixIframeBugs();
				});
			},

			getIframe: function (item, template) {
				var embedSrc = item.src;
				var iframeSt = mfp.st.iframe;

				$.each(iframeSt.patterns, function () {
					if (embedSrc.indexOf(this.index) > -1) {
						if (this.id) {
							if (typeof this.id === 'string') {
								embedSrc = embedSrc.substr(embedSrc.lastIndexOf(this.id) + this.id.length, embedSrc.length);
							} else {
								embedSrc = this.id.call(this, embedSrc);
							}
						}
						embedSrc = this.src.replace('%id%', embedSrc);
						return false; // break;
					}
				});

				var dataObj = {};
				if (iframeSt.srcAction) {
					dataObj[iframeSt.srcAction] = embedSrc;
				}
				mfp._parseMarkup(template, dataObj, item);

				mfp.updateStatus('ready');

				return template;
			}
		}
	});

	/*>>iframe*/

	/*>>gallery*/
	/**
  * Get looped index depending on number of slides
  */
	var _getLoopedId = function (index) {
		var numSlides = mfp.items.length;
		if (index > numSlides - 1) {
			return index - numSlides;
		} else if (index < 0) {
			return numSlides + index;
		}
		return index;
	},
	    _replaceCurrTotal = function (text, curr, total) {
		return text.replace(/%curr%/gi, curr + 1).replace(/%total%/gi, total);
	};

	$.magnificPopup.registerModule('gallery', {

		options: {
			enabled: false,
			arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
			preload: [0, 2],
			navigateByImgClick: true,
			arrows: true,

			tPrev: 'Previous (Left arrow key)',
			tNext: 'Next (Right arrow key)',
			tCounter: '%curr% of %total%'
		},

		proto: {
			initGallery: function () {

				var gSt = mfp.st.gallery,
				    ns = '.mfp-gallery';

				mfp.direction = true; // true - next, false - prev

				if (!gSt || !gSt.enabled) return false;

				_wrapClasses += ' mfp-gallery';

				_mfpOn(OPEN_EVENT + ns, function () {

					if (gSt.navigateByImgClick) {
						mfp.wrap.on('click' + ns, '.mfp-img', function () {
							if (mfp.items.length > 1) {
								mfp.next();
								return false;
							}
						});
					}

					_document.on('keydown' + ns, function (e) {
						if (e.keyCode === 37) {
							mfp.prev();
						} else if (e.keyCode === 39) {
							mfp.next();
						}
					});
				});

				_mfpOn('UpdateStatus' + ns, function (e, data) {
					if (data.text) {
						data.text = _replaceCurrTotal(data.text, mfp.currItem.index, mfp.items.length);
					}
				});

				_mfpOn(MARKUP_PARSE_EVENT + ns, function (e, element, values, item) {
					var l = mfp.items.length;
					values.counter = l > 1 ? _replaceCurrTotal(gSt.tCounter, item.index, l) : '';
				});

				_mfpOn('BuildControls' + ns, function () {
					if (mfp.items.length > 1 && gSt.arrows && !mfp.arrowLeft) {
						var markup = gSt.arrowMarkup,
						    arrowLeft = mfp.arrowLeft = $(markup.replace(/%title%/gi, gSt.tPrev).replace(/%dir%/gi, 'left')).addClass(PREVENT_CLOSE_CLASS),
						    arrowRight = mfp.arrowRight = $(markup.replace(/%title%/gi, gSt.tNext).replace(/%dir%/gi, 'right')).addClass(PREVENT_CLOSE_CLASS);

						arrowLeft.click(function () {
							mfp.prev();
						});
						arrowRight.click(function () {
							mfp.next();
						});

						mfp.container.append(arrowLeft.add(arrowRight));
					}
				});

				_mfpOn(CHANGE_EVENT + ns, function () {
					if (mfp._preloadTimeout) clearTimeout(mfp._preloadTimeout);

					mfp._preloadTimeout = setTimeout(function () {
						mfp.preloadNearbyImages();
						mfp._preloadTimeout = null;
					}, 16);
				});

				_mfpOn(CLOSE_EVENT + ns, function () {
					_document.off(ns);
					mfp.wrap.off('click' + ns);
					mfp.arrowRight = mfp.arrowLeft = null;
				});
			},
			next: function () {
				mfp.direction = true;
				mfp.index = _getLoopedId(mfp.index + 1);
				mfp.updateItemHTML();
			},
			prev: function () {
				mfp.direction = false;
				mfp.index = _getLoopedId(mfp.index - 1);
				mfp.updateItemHTML();
			},
			goTo: function (newIndex) {
				mfp.direction = newIndex >= mfp.index;
				mfp.index = newIndex;
				mfp.updateItemHTML();
			},
			preloadNearbyImages: function () {
				var p = mfp.st.gallery.preload,
				    preloadBefore = Math.min(p[0], mfp.items.length),
				    preloadAfter = Math.min(p[1], mfp.items.length),
				    i;

				for (i = 1; i <= (mfp.direction ? preloadAfter : preloadBefore); i++) {
					mfp._preloadItem(mfp.index + i);
				}
				for (i = 1; i <= (mfp.direction ? preloadBefore : preloadAfter); i++) {
					mfp._preloadItem(mfp.index - i);
				}
			},
			_preloadItem: function (index) {
				index = _getLoopedId(index);

				if (mfp.items[index].preloaded) {
					return;
				}

				var item = mfp.items[index];
				if (!item.parsed) {
					item = mfp.parseEl(index);
				}

				_mfpTrigger('LazyLoad', item);

				if (item.type === 'image') {
					item.img = $('<img class="mfp-img" />').on('load.mfploader', function () {
						item.hasSize = true;
					}).on('error.mfploader', function () {
						item.hasSize = true;
						item.loadError = true;
						_mfpTrigger('LazyLoadError', item);
					}).attr('src', item.src);
				}

				item.preloaded = true;
			}
		}
	});

	/*>>gallery*/

	/*>>retina*/

	var RETINA_NS = 'retina';

	$.magnificPopup.registerModule(RETINA_NS, {
		options: {
			replaceSrc: function (item) {
				return item.src.replace(/\.\w+$/, function (m) {
					return '@2x' + m;
				});
			},
			ratio: 1 // Function or number.  Set to 1 to disable.
		},
		proto: {
			initRetina: function () {
				if (window.devicePixelRatio > 1) {

					var st = mfp.st.retina,
					    ratio = st.ratio;

					ratio = !isNaN(ratio) ? ratio : ratio();

					if (ratio > 1) {
						_mfpOn('ImageHasSize' + '.' + RETINA_NS, function (e, item) {
							item.img.css({
								'max-width': item.img[0].naturalWidth / ratio,
								'width': '100%'
							});
						});
						_mfpOn('ElementParse' + '.' + RETINA_NS, function (e, item) {
							item.src = st.replaceSrc(item, ratio);
						});
					}
				}
			}
		}
	});

	/*>>retina*/
	_checkInstance();
});
},{"jquery":"..\\node_modules\\jquery\\dist\\jquery.js"}],"common_libraries\\jquery-global.js":[function(require,module,exports) {
'use strict';

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.jQuery = _jquery2.default;
window.J = _jquery2.default;
},{"jquery":"..\\node_modules\\jquery\\dist\\jquery.js"}],"..\\node_modules\\mediaelement\\build\\mediaelement-and-player.js":[function(require,module,exports) {
var global = arguments[3];
var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * MediaElement.js
 * http://www.mediaelementjs.com/
 *
 * Wrapper that mimics native HTML5 MediaElement (audio and video)
 * using a variety of technologies (pure JavaScript, Flash, iframe)
 *
 * Copyright 2010-2017, John Dyer (http://j.hn/)
 * License: MIT
 *
 */(function e(t, n, r) {
	function s(o, u) {
		if (!n[o]) {
			if (!t[o]) {
				var a = typeof require == "function" && require;if (!u && a) return a(o, !0);if (i) return i(o, !0);var f = new Error("Cannot find module '" + o + "'");throw f.code = "MODULE_NOT_FOUND", f;
			}var l = n[o] = { exports: {} };t[o][0].call(l.exports, function (e) {
				var n = t[o][1][e];return s(n ? n : e);
			}, l, l.exports, e, t, n, r);
		}return n[o].exports;
	}var i = typeof require == "function" && require;for (var o = 0; o < r.length; o++) {
		s(r[o]);
	}return s;
})({ 1: [function (_dereq_, module, exports) {}, {}], 2: [function (_dereq_, module, exports) {
		(function (global) {
			var topLevel = typeof global !== 'undefined' ? global : typeof window !== 'undefined' ? window : {};
			var minDoc = _dereq_(1);

			var doccy;

			if (typeof document !== 'undefined') {
				doccy = document;
			} else {
				doccy = topLevel['__GLOBAL_DOCUMENT_CACHE@4'];

				if (!doccy) {
					doccy = topLevel['__GLOBAL_DOCUMENT_CACHE@4'] = minDoc;
				}
			}

			module.exports = doccy;
		}).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
	}, { "1": 1 }], 3: [function (_dereq_, module, exports) {
		(function (global) {
			var win;

			if (typeof window !== "undefined") {
				win = window;
			} else if (typeof global !== "undefined") {
				win = global;
			} else if (typeof self !== "undefined") {
				win = self;
			} else {
				win = {};
			}

			module.exports = win;
		}).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
	}, {}], 4: [function (_dereq_, module, exports) {
		(function (root) {

			// Store setTimeout reference so promise-polyfill will be unaffected by
			// other code modifying setTimeout (like sinon.useFakeTimers())
			var setTimeoutFunc = setTimeout;

			function noop() {}

			// Polyfill for Function.prototype.bind
			function bind(fn, thisArg) {
				return function () {
					fn.apply(thisArg, arguments);
				};
			}

			function Promise(fn) {
				if (_typeof2(this) !== 'object') throw new TypeError('Promises must be constructed via new');
				if (typeof fn !== 'function') throw new TypeError('not a function');
				this._state = 0;
				this._handled = false;
				this._value = undefined;
				this._deferreds = [];

				doResolve(fn, this);
			}

			function handle(self, deferred) {
				while (self._state === 3) {
					self = self._value;
				}
				if (self._state === 0) {
					self._deferreds.push(deferred);
					return;
				}
				self._handled = true;
				Promise._immediateFn(function () {
					var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
					if (cb === null) {
						(self._state === 1 ? resolve : reject)(deferred.promise, self._value);
						return;
					}
					var ret;
					try {
						ret = cb(self._value);
					} catch (e) {
						reject(deferred.promise, e);
						return;
					}
					resolve(deferred.promise, ret);
				});
			}

			function resolve(self, newValue) {
				try {
					// Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
					if (newValue === self) throw new TypeError('A promise cannot be resolved with itself.');
					if (newValue && ((typeof newValue === "undefined" ? "undefined" : _typeof2(newValue)) === 'object' || typeof newValue === 'function')) {
						var then = newValue.then;
						if (newValue instanceof Promise) {
							self._state = 3;
							self._value = newValue;
							finale(self);
							return;
						} else if (typeof then === 'function') {
							doResolve(bind(then, newValue), self);
							return;
						}
					}
					self._state = 1;
					self._value = newValue;
					finale(self);
				} catch (e) {
					reject(self, e);
				}
			}

			function reject(self, newValue) {
				self._state = 2;
				self._value = newValue;
				finale(self);
			}

			function finale(self) {
				if (self._state === 2 && self._deferreds.length === 0) {
					Promise._immediateFn(function () {
						if (!self._handled) {
							Promise._unhandledRejectionFn(self._value);
						}
					});
				}

				for (var i = 0, len = self._deferreds.length; i < len; i++) {
					handle(self, self._deferreds[i]);
				}
				self._deferreds = null;
			}

			function Handler(onFulfilled, onRejected, promise) {
				this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
				this.onRejected = typeof onRejected === 'function' ? onRejected : null;
				this.promise = promise;
			}

			/**
    * Take a potentially misbehaving resolver function and make sure
    * onFulfilled and onRejected are only called once.
    *
    * Makes no guarantees about asynchrony.
    */
			function doResolve(fn, self) {
				var done = false;
				try {
					fn(function (value) {
						if (done) return;
						done = true;
						resolve(self, value);
					}, function (reason) {
						if (done) return;
						done = true;
						reject(self, reason);
					});
				} catch (ex) {
					if (done) return;
					done = true;
					reject(self, ex);
				}
			}

			Promise.prototype['catch'] = function (onRejected) {
				return this.then(null, onRejected);
			};

			Promise.prototype.then = function (onFulfilled, onRejected) {
				var prom = new this.constructor(noop);

				handle(this, new Handler(onFulfilled, onRejected, prom));
				return prom;
			};

			Promise.all = function (arr) {
				var args = Array.prototype.slice.call(arr);

				return new Promise(function (resolve, reject) {
					if (args.length === 0) return resolve([]);
					var remaining = args.length;

					function res(i, val) {
						try {
							if (val && ((typeof val === "undefined" ? "undefined" : _typeof2(val)) === 'object' || typeof val === 'function')) {
								var then = val.then;
								if (typeof then === 'function') {
									then.call(val, function (val) {
										res(i, val);
									}, reject);
									return;
								}
							}
							args[i] = val;
							if (--remaining === 0) {
								resolve(args);
							}
						} catch (ex) {
							reject(ex);
						}
					}

					for (var i = 0; i < args.length; i++) {
						res(i, args[i]);
					}
				});
			};

			Promise.resolve = function (value) {
				if (value && (typeof value === "undefined" ? "undefined" : _typeof2(value)) === 'object' && value.constructor === Promise) {
					return value;
				}

				return new Promise(function (resolve) {
					resolve(value);
				});
			};

			Promise.reject = function (value) {
				return new Promise(function (resolve, reject) {
					reject(value);
				});
			};

			Promise.race = function (values) {
				return new Promise(function (resolve, reject) {
					for (var i = 0, len = values.length; i < len; i++) {
						values[i].then(resolve, reject);
					}
				});
			};

			// Use polyfill for setImmediate for performance gains
			Promise._immediateFn = typeof setImmediate === 'function' && function (fn) {
				setImmediate(fn);
			} || function (fn) {
				setTimeoutFunc(fn, 0);
			};

			Promise._unhandledRejectionFn = function _unhandledRejectionFn(err) {
				if (typeof console !== 'undefined' && console) {
					console.warn('Possible Unhandled Promise Rejection:', err); // eslint-disable-line no-console
				}
			};

			/**
    * Set the immediate function to execute callbacks
    * @param fn {function} Function to execute
    * @deprecated
    */
			Promise._setImmediateFn = function _setImmediateFn(fn) {
				Promise._immediateFn = fn;
			};

			/**
    * Change the function to execute on unhandled rejection
    * @param {function} fn Function to execute on unhandled rejection
    * @deprecated
    */
			Promise._setUnhandledRejectionFn = function _setUnhandledRejectionFn(fn) {
				Promise._unhandledRejectionFn = fn;
			};

			if (typeof module !== 'undefined' && module.exports) {
				module.exports = Promise;
			} else if (!root.Promise) {
				root.Promise = Promise;
			}
		})(this);
	}, {}], 5: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});

		var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
			return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		} : function (obj) {
			return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		};

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _en = _dereq_(15);

		var _general = _dereq_(27);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		var i18n = { lang: 'en', en: _en.EN };

		i18n.language = function () {
			for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
				args[_key] = arguments[_key];
			}

			if (args !== null && args !== undefined && args.length) {

				if (typeof args[0] !== 'string') {
					throw new TypeError('Language code must be a string value');
				}

				if (!/^[a-z]{2,3}((\-|_)[a-z]{2})?$/i.test(args[0])) {
					throw new TypeError('Language code must have format 2-3 letters and. optionally, hyphen, underscore followed by 2 more letters');
				}

				i18n.lang = args[0];

				if (i18n[args[0]] === undefined) {
					args[1] = args[1] !== null && args[1] !== undefined && _typeof(args[1]) === 'object' ? args[1] : {};
					i18n[args[0]] = !(0, _general.isObjectEmpty)(args[1]) ? args[1] : _en.EN;
				} else if (args[1] !== null && args[1] !== undefined && _typeof(args[1]) === 'object') {
					i18n[args[0]] = args[1];
				}
			}

			return i18n.lang;
		};

		i18n.t = function (message) {
			var pluralParam = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

			if (typeof message === 'string' && message.length) {

				var str = void 0,
				    pluralForm = void 0;

				var language = i18n.language();

				var _plural = function _plural(input, number, form) {

					if ((typeof input === 'undefined' ? 'undefined' : _typeof(input)) !== 'object' || typeof number !== 'number' || typeof form !== 'number') {
						return input;
					}

					var _pluralForms = function () {
						return [function () {
							return arguments.length <= 1 ? undefined : arguments[1];
						}, function () {
							return (arguments.length <= 0 ? undefined : arguments[0]) === 1 ? arguments.length <= 1 ? undefined : arguments[1] : arguments.length <= 2 ? undefined : arguments[2];
						}, function () {
							return (arguments.length <= 0 ? undefined : arguments[0]) === 0 || (arguments.length <= 0 ? undefined : arguments[0]) === 1 ? arguments.length <= 1 ? undefined : arguments[1] : arguments.length <= 2 ? undefined : arguments[2];
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) % 10 === 1 && (arguments.length <= 0 ? undefined : arguments[0]) % 100 !== 11) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) !== 0) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else {
								return arguments.length <= 3 ? undefined : arguments[3];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 1 || (arguments.length <= 0 ? undefined : arguments[0]) === 11) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) === 2 || (arguments.length <= 0 ? undefined : arguments[0]) === 12) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) > 2 && (arguments.length <= 0 ? undefined : arguments[0]) < 20) {
								return arguments.length <= 3 ? undefined : arguments[3];
							} else {
								return arguments.length <= 4 ? undefined : arguments[4];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 1) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) === 0 || (arguments.length <= 0 ? undefined : arguments[0]) % 100 > 0 && (arguments.length <= 0 ? undefined : arguments[0]) % 100 < 20) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else {
								return arguments.length <= 3 ? undefined : arguments[3];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) % 10 === 1 && (arguments.length <= 0 ? undefined : arguments[0]) % 100 !== 11) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) % 10 >= 2 && ((arguments.length <= 0 ? undefined : arguments[0]) % 100 < 10 || (arguments.length <= 0 ? undefined : arguments[0]) % 100 >= 20)) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else {
								return [3];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) % 10 === 1 && (arguments.length <= 0 ? undefined : arguments[0]) % 100 !== 11) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) % 10 >= 2 && (arguments.length <= 0 ? undefined : arguments[0]) % 10 <= 4 && ((arguments.length <= 0 ? undefined : arguments[0]) % 100 < 10 || (arguments.length <= 0 ? undefined : arguments[0]) % 100 >= 20)) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else {
								return arguments.length <= 3 ? undefined : arguments[3];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 1) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) >= 2 && (arguments.length <= 0 ? undefined : arguments[0]) <= 4) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else {
								return arguments.length <= 3 ? undefined : arguments[3];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 1) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) % 10 >= 2 && (arguments.length <= 0 ? undefined : arguments[0]) % 10 <= 4 && ((arguments.length <= 0 ? undefined : arguments[0]) % 100 < 10 || (arguments.length <= 0 ? undefined : arguments[0]) % 100 >= 20)) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else {
								return arguments.length <= 3 ? undefined : arguments[3];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) % 100 === 1) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) % 100 === 2) {
								return arguments.length <= 3 ? undefined : arguments[3];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) % 100 === 3 || (arguments.length <= 0 ? undefined : arguments[0]) % 100 === 4) {
								return arguments.length <= 4 ? undefined : arguments[4];
							} else {
								return arguments.length <= 1 ? undefined : arguments[1];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 1) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) === 2) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) > 2 && (arguments.length <= 0 ? undefined : arguments[0]) < 7) {
								return arguments.length <= 3 ? undefined : arguments[3];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) > 6 && (arguments.length <= 0 ? undefined : arguments[0]) < 11) {
								return arguments.length <= 4 ? undefined : arguments[4];
							} else {
								return arguments.length <= 5 ? undefined : arguments[5];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 0) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) === 1) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) === 2) {
								return arguments.length <= 3 ? undefined : arguments[3];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) % 100 >= 3 && (arguments.length <= 0 ? undefined : arguments[0]) % 100 <= 10) {
								return arguments.length <= 4 ? undefined : arguments[4];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) % 100 >= 11) {
								return arguments.length <= 5 ? undefined : arguments[5];
							} else {
								return arguments.length <= 6 ? undefined : arguments[6];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 1) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) === 0 || (arguments.length <= 0 ? undefined : arguments[0]) % 100 > 1 && (arguments.length <= 0 ? undefined : arguments[0]) % 100 < 11) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) % 100 > 10 && (arguments.length <= 0 ? undefined : arguments[0]) % 100 < 20) {
								return arguments.length <= 3 ? undefined : arguments[3];
							} else {
								return arguments.length <= 4 ? undefined : arguments[4];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) % 10 === 1) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) % 10 === 2) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else {
								return arguments.length <= 3 ? undefined : arguments[3];
							}
						}, function () {
							return (arguments.length <= 0 ? undefined : arguments[0]) !== 11 && (arguments.length <= 0 ? undefined : arguments[0]) % 10 === 1 ? arguments.length <= 1 ? undefined : arguments[1] : arguments.length <= 2 ? undefined : arguments[2];
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 1) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) % 10 >= 2 && (arguments.length <= 0 ? undefined : arguments[0]) % 10 <= 4 && ((arguments.length <= 0 ? undefined : arguments[0]) % 100 < 10 || (arguments.length <= 0 ? undefined : arguments[0]) % 100 >= 20)) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else {
								return arguments.length <= 3 ? undefined : arguments[3];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 1) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) === 2) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) !== 8 && (arguments.length <= 0 ? undefined : arguments[0]) !== 11) {
								return arguments.length <= 3 ? undefined : arguments[3];
							} else {
								return arguments.length <= 4 ? undefined : arguments[4];
							}
						}, function () {
							return (arguments.length <= 0 ? undefined : arguments[0]) === 0 ? arguments.length <= 1 ? undefined : arguments[1] : arguments.length <= 2 ? undefined : arguments[2];
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 1) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) === 2) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) === 3) {
								return arguments.length <= 3 ? undefined : arguments[3];
							} else {
								return arguments.length <= 4 ? undefined : arguments[4];
							}
						}, function () {
							if ((arguments.length <= 0 ? undefined : arguments[0]) === 0) {
								return arguments.length <= 1 ? undefined : arguments[1];
							} else if ((arguments.length <= 0 ? undefined : arguments[0]) === 1) {
								return arguments.length <= 2 ? undefined : arguments[2];
							} else {
								return arguments.length <= 3 ? undefined : arguments[3];
							}
						}];
					}();

					return _pluralForms[form].apply(null, [number].concat(input));
				};

				if (i18n[language] !== undefined) {
					str = i18n[language][message];
					if (pluralParam !== null && typeof pluralParam === 'number') {
						pluralForm = i18n[language]['mejs.plural-form'];
						str = _plural.apply(null, [str, pluralParam, pluralForm]);
					}
				}

				if (!str && i18n.en) {
					str = i18n.en[message];
					if (pluralParam !== null && typeof pluralParam === 'number') {
						pluralForm = i18n.en['mejs.plural-form'];
						str = _plural.apply(null, [str, pluralParam, pluralForm]);
					}
				}

				str = str || message;

				if (pluralParam !== null && typeof pluralParam === 'number') {
					str = str.replace('%1', pluralParam);
				}

				return (0, _general.escapeHTML)(str);
			}

			return message;
		};

		_mejs2.default.i18n = i18n;

		if (typeof mejsL10n !== 'undefined') {
			_mejs2.default.i18n.language(mejsL10n.language, mejsL10n.strings);
		}

		exports.default = i18n;
	}, { "15": 15, "27": 27, "7": 7 }], 6: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});

		var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
			return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		} : function (obj) {
			return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		};

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _general = _dereq_(27);

		var _media2 = _dereq_(28);

		var _renderer = _dereq_(8);

		var _constants = _dereq_(25);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		function _classCallCheck(instance, Constructor) {
			if (!(instance instanceof Constructor)) {
				throw new TypeError("Cannot call a class as a function");
			}
		}

		var MediaElement = function MediaElement(idOrNode, options, sources) {
			var _this = this;

			_classCallCheck(this, MediaElement);

			var t = this;

			sources = Array.isArray(sources) ? sources : null;

			t.defaults = {
				renderers: [],

				fakeNodeName: 'mediaelementwrapper',

				pluginPath: 'build/',

				shimScriptAccess: 'sameDomain'
			};

			options = Object.assign(t.defaults, options);

			t.mediaElement = _document2.default.createElement(options.fakeNodeName);

			var id = idOrNode,
			    error = false;

			if (typeof idOrNode === 'string') {
				t.mediaElement.originalNode = _document2.default.getElementById(idOrNode);
			} else {
				t.mediaElement.originalNode = idOrNode;
				id = idOrNode.id;
			}

			if (t.mediaElement.originalNode === undefined || t.mediaElement.originalNode === null) {
				return null;
			}

			t.mediaElement.options = options;
			id = id || 'mejs_' + Math.random().toString().slice(2);

			t.mediaElement.originalNode.setAttribute('id', id + '_from_mejs');

			var tagName = t.mediaElement.originalNode.tagName.toLowerCase();
			if (['video', 'audio'].indexOf(tagName) > -1 && !t.mediaElement.originalNode.getAttribute('preload')) {
				t.mediaElement.originalNode.setAttribute('preload', 'none');
			}

			t.mediaElement.originalNode.parentNode.insertBefore(t.mediaElement, t.mediaElement.originalNode);

			t.mediaElement.appendChild(t.mediaElement.originalNode);

			var processURL = function processURL(url, type) {
				if (_window2.default.location.protocol === 'https:' && url.indexOf('http:') === 0 && _constants.IS_IOS && _mejs2.default.html5media.mediaTypes.indexOf(type) > -1) {
					var xhr = new XMLHttpRequest();
					xhr.onreadystatechange = function () {
						if (this.readyState === 4 && this.status === 200) {
							var _url = _window2.default.URL || _window2.default.webkitURL,
							    blobUrl = _url.createObjectURL(this.response);
							t.mediaElement.originalNode.setAttribute('src', blobUrl);
							return blobUrl;
						}
						return url;
					};
					xhr.open('GET', url);
					xhr.responseType = 'blob';
					xhr.send();
				}

				return url;
			};

			var mediaFiles = void 0;

			if (sources !== null) {
				mediaFiles = sources;
			} else if (t.mediaElement.originalNode !== null) {

				mediaFiles = [];

				switch (t.mediaElement.originalNode.nodeName.toLowerCase()) {
					case 'iframe':
						mediaFiles.push({
							type: '',
							src: t.mediaElement.originalNode.getAttribute('src')
						});
						break;
					case 'audio':
					case 'video':
						var _sources = t.mediaElement.originalNode.children.length,
						    nodeSource = t.mediaElement.originalNode.getAttribute('src');

						if (nodeSource) {
							var node = t.mediaElement.originalNode,
							    type = (0, _media2.formatType)(nodeSource, node.getAttribute('type'));
							mediaFiles.push({
								type: type,
								src: processURL(nodeSource, type)
							});
						}

						for (var i = 0; i < _sources; i++) {
							var n = t.mediaElement.originalNode.children[i];
							if (n.tagName.toLowerCase() === 'source') {
								var src = n.getAttribute('src'),
								    _type = (0, _media2.formatType)(src, n.getAttribute('type'));
								mediaFiles.push({ type: _type, src: processURL(src, _type) });
							}
						}
						break;
				}
			}

			t.mediaElement.id = id;
			t.mediaElement.renderers = {};
			t.mediaElement.events = {};
			t.mediaElement.promises = [];
			t.mediaElement.renderer = null;
			t.mediaElement.rendererName = null;

			t.mediaElement.changeRenderer = function (rendererName, mediaFiles) {

				var t = _this,
				    media = Object.keys(mediaFiles[0]).length > 2 ? mediaFiles[0] : mediaFiles[0].src;

				if (t.mediaElement.renderer !== undefined && t.mediaElement.renderer !== null && t.mediaElement.renderer.name === rendererName) {
					t.mediaElement.renderer.pause();
					if (t.mediaElement.renderer.stop) {
						t.mediaElement.renderer.stop();
					}
					t.mediaElement.renderer.show();
					t.mediaElement.renderer.setSrc(media);
					return true;
				}

				if (t.mediaElement.renderer !== undefined && t.mediaElement.renderer !== null) {
					t.mediaElement.renderer.pause();
					if (t.mediaElement.renderer.stop) {
						t.mediaElement.renderer.stop();
					}
					t.mediaElement.renderer.hide();
				}

				var newRenderer = t.mediaElement.renderers[rendererName],
				    newRendererType = null;

				if (newRenderer !== undefined && newRenderer !== null) {
					newRenderer.show();
					newRenderer.setSrc(media);
					t.mediaElement.renderer = newRenderer;
					t.mediaElement.rendererName = rendererName;
					return true;
				}

				var rendererArray = t.mediaElement.options.renderers.length ? t.mediaElement.options.renderers : _renderer.renderer.order;

				for (var _i = 0, total = rendererArray.length; _i < total; _i++) {
					var index = rendererArray[_i];

					if (index === rendererName) {
						var rendererList = _renderer.renderer.renderers;
						newRendererType = rendererList[index];

						var renderOptions = Object.assign(newRendererType.options, t.mediaElement.options);
						newRenderer = newRendererType.create(t.mediaElement, renderOptions, mediaFiles);
						newRenderer.name = rendererName;

						t.mediaElement.renderers[newRendererType.name] = newRenderer;
						t.mediaElement.renderer = newRenderer;
						t.mediaElement.rendererName = rendererName;
						newRenderer.show();
						return true;
					}
				}

				return false;
			};

			t.mediaElement.setSize = function (width, height) {
				if (t.mediaElement.renderer !== undefined && t.mediaElement.renderer !== null) {
					t.mediaElement.renderer.setSize(width, height);
				}
			};

			t.mediaElement.generateError = function (message, urlList) {
				message = message || '';
				urlList = Array.isArray(urlList) ? urlList : [];
				var event = (0, _general.createEvent)('error', t.mediaElement);
				event.message = message;
				event.urls = urlList;
				t.mediaElement.dispatchEvent(event);
				error = true;
			};

			var props = _mejs2.default.html5media.properties,
			    methods = _mejs2.default.html5media.methods,
			    addProperty = function addProperty(obj, name, onGet, onSet) {
				var oldValue = obj[name];
				var getFn = function getFn() {
					return onGet.apply(obj, [oldValue]);
				},
				    setFn = function setFn(newValue) {
					oldValue = onSet.apply(obj, [newValue]);
					return oldValue;
				};

				Object.defineProperty(obj, name, {
					get: getFn,
					set: setFn
				});
			},
			    assignGettersSetters = function assignGettersSetters(propName) {
				if (propName !== 'src') {

					var capName = '' + propName.substring(0, 1).toUpperCase() + propName.substring(1),
					    getFn = function getFn() {
						return t.mediaElement.renderer !== undefined && t.mediaElement.renderer !== null && typeof t.mediaElement.renderer['get' + capName] === 'function' ? t.mediaElement.renderer['get' + capName]() : null;
					},
					    setFn = function setFn(value) {
						if (t.mediaElement.renderer !== undefined && t.mediaElement.renderer !== null && typeof t.mediaElement.renderer['set' + capName] === 'function') {
							t.mediaElement.renderer['set' + capName](value);
						}
					};

					addProperty(t.mediaElement, propName, getFn, setFn);
					t.mediaElement['get' + capName] = getFn;
					t.mediaElement['set' + capName] = setFn;
				}
			},
			    getSrc = function getSrc() {
				return t.mediaElement.renderer !== undefined && t.mediaElement.renderer !== null ? t.mediaElement.renderer.getSrc() : null;
			},
			    setSrc = function setSrc(value) {
				var mediaFiles = [];

				if (typeof value === 'string') {
					mediaFiles.push({
						src: value,
						type: value ? (0, _media2.getTypeFromFile)(value) : ''
					});
				} else if ((typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value.src !== undefined) {
					var _src = (0, _media2.absolutizeUrl)(value.src),
					    _type2 = value.type,
					    media = Object.assign(value, {
						src: _src,
						type: (_type2 === '' || _type2 === null || _type2 === undefined) && _src ? (0, _media2.getTypeFromFile)(_src) : _type2
					});
					mediaFiles.push(media);
				} else if (Array.isArray(value)) {
					for (var _i2 = 0, total = value.length; _i2 < total; _i2++) {

						var _src2 = (0, _media2.absolutizeUrl)(value[_i2].src),
						    _type3 = value[_i2].type,
						    _media = Object.assign(value[_i2], {
							src: _src2,
							type: (_type3 === '' || _type3 === null || _type3 === undefined) && _src2 ? (0, _media2.getTypeFromFile)(_src2) : _type3
						});

						mediaFiles.push(_media);
					}
				}

				var renderInfo = _renderer.renderer.select(mediaFiles, t.mediaElement.options.renderers.length ? t.mediaElement.options.renderers : []),
				    event = void 0;

				if (!t.mediaElement.paused) {
					t.mediaElement.pause();
					event = (0, _general.createEvent)('pause', t.mediaElement);
					t.mediaElement.dispatchEvent(event);
				}
				t.mediaElement.originalNode.src = mediaFiles[0].src || '';

				if (renderInfo === null && mediaFiles[0].src) {
					t.mediaElement.generateError('No renderer found', mediaFiles);
					return;
				}

				return mediaFiles[0].src ? t.mediaElement.changeRenderer(renderInfo.rendererName, mediaFiles) : null;
			},
			    triggerAction = function triggerAction(methodName, args) {
				try {
					if (methodName === 'play' && t.mediaElement.rendererName === 'native_dash') {
						var response = t.mediaElement.renderer[methodName](args);
						if (response && typeof response.then === 'function') {
							response.catch(function () {
								if (t.mediaElement.paused) {
									setTimeout(function () {
										var tmpResponse = t.mediaElement.renderer.play();
										if (tmpResponse !== undefined) {
											tmpResponse.catch(function () {
												if (!t.mediaElement.renderer.paused) {
													t.mediaElement.renderer.pause();
												}
											});
										}
									}, 150);
								}
							});
						}
					} else {
						t.mediaElement.renderer[methodName](args);
					}
				} catch (e) {
					t.mediaElement.generateError(e, mediaFiles);
				}
			},
			    assignMethods = function assignMethods(methodName) {
				t.mediaElement[methodName] = function () {
					for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
						args[_key] = arguments[_key];
					}

					if (t.mediaElement.renderer !== undefined && t.mediaElement.renderer !== null && typeof t.mediaElement.renderer[methodName] === 'function') {
						if (t.mediaElement.promises.length) {
							Promise.all(t.mediaElement.promises).then(function () {
								triggerAction(methodName, args);
							}).catch(function (e) {
								t.mediaElement.generateError(e, mediaFiles);
							});
						} else {
							triggerAction(methodName, args);
						}
					}
					return null;
				};
			};

			addProperty(t.mediaElement, 'src', getSrc, setSrc);
			t.mediaElement.getSrc = getSrc;
			t.mediaElement.setSrc = setSrc;

			for (var _i3 = 0, total = props.length; _i3 < total; _i3++) {
				assignGettersSetters(props[_i3]);
			}

			for (var _i4 = 0, _total = methods.length; _i4 < _total; _i4++) {
				assignMethods(methods[_i4]);
			}

			t.mediaElement.addEventListener = function (eventName, callback) {
				t.mediaElement.events[eventName] = t.mediaElement.events[eventName] || [];

				t.mediaElement.events[eventName].push(callback);
			};
			t.mediaElement.removeEventListener = function (eventName, callback) {
				if (!eventName) {
					t.mediaElement.events = {};
					return true;
				}

				var callbacks = t.mediaElement.events[eventName];

				if (!callbacks) {
					return true;
				}

				if (!callback) {
					t.mediaElement.events[eventName] = [];
					return true;
				}

				for (var _i5 = 0; _i5 < callbacks.length; _i5++) {
					if (callbacks[_i5] === callback) {
						t.mediaElement.events[eventName].splice(_i5, 1);
						return true;
					}
				}
				return false;
			};

			t.mediaElement.dispatchEvent = function (event) {
				var callbacks = t.mediaElement.events[event.type];
				if (callbacks) {
					for (var _i6 = 0; _i6 < callbacks.length; _i6++) {
						callbacks[_i6].apply(null, [event]);
					}
				}
			};

			t.mediaElement.destroy = function () {
				var mediaElement = t.mediaElement.originalNode.cloneNode(true);
				var wrapper = t.mediaElement.parentElement;
				mediaElement.removeAttribute('id');
				mediaElement.remove();
				t.mediaElement.remove();
				wrapper.append(mediaElement);
			};

			if (mediaFiles.length) {
				t.mediaElement.src = mediaFiles;
			}

			if (t.mediaElement.promises.length) {
				Promise.all(t.mediaElement.promises).then(function () {
					if (t.mediaElement.options.success) {
						t.mediaElement.options.success(t.mediaElement, t.mediaElement.originalNode);
					}
				}).catch(function () {
					if (error && t.mediaElement.options.error) {
						t.mediaElement.options.error(t.mediaElement, t.mediaElement.originalNode);
					}
				});
			} else {
				if (t.mediaElement.options.success) {
					t.mediaElement.options.success(t.mediaElement, t.mediaElement.originalNode);
				}

				if (error && t.mediaElement.options.error) {
					t.mediaElement.options.error(t.mediaElement, t.mediaElement.originalNode);
				}
			}

			return t.mediaElement;
		};

		_window2.default.MediaElement = MediaElement;
		_mejs2.default.MediaElement = MediaElement;

		exports.default = MediaElement;
	}, { "2": 2, "25": 25, "27": 27, "28": 28, "3": 3, "7": 7, "8": 8 }], 7: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		var mejs = {};

		mejs.version = '4.2.9';

		mejs.html5media = {
			properties: ['volume', 'src', 'currentTime', 'muted', 'duration', 'paused', 'ended', 'buffered', 'error', 'networkState', 'readyState', 'seeking', 'seekable', 'currentSrc', 'preload', 'bufferedBytes', 'bufferedTime', 'initialTime', 'startOffsetTime', 'defaultPlaybackRate', 'playbackRate', 'played', 'autoplay', 'loop', 'controls'],
			readOnlyProperties: ['duration', 'paused', 'ended', 'buffered', 'error', 'networkState', 'readyState', 'seeking', 'seekable'],

			methods: ['load', 'play', 'pause', 'canPlayType'],

			events: ['loadstart', 'durationchange', 'loadedmetadata', 'loadeddata', 'progress', 'canplay', 'canplaythrough', 'suspend', 'abort', 'error', 'emptied', 'stalled', 'play', 'playing', 'pause', 'waiting', 'seeking', 'seeked', 'timeupdate', 'ended', 'ratechange', 'volumechange'],

			mediaTypes: ['audio/mp3', 'audio/ogg', 'audio/oga', 'audio/wav', 'audio/x-wav', 'audio/wave', 'audio/x-pn-wav', 'audio/mpeg', 'audio/mp4', 'video/mp4', 'video/webm', 'video/ogg', 'video/ogv']
		};

		_window2.default.mejs = mejs;

		exports.default = mejs;
	}, { "3": 3 }], 8: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});
		exports.renderer = undefined;

		var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
			return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		} : function (obj) {
			return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		};

		var _createClass = function () {
			function defineProperties(target, props) {
				for (var i = 0; i < props.length; i++) {
					var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
				}
			}return function (Constructor, protoProps, staticProps) {
				if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
			};
		}();

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		function _classCallCheck(instance, Constructor) {
			if (!(instance instanceof Constructor)) {
				throw new TypeError("Cannot call a class as a function");
			}
		}

		var Renderer = function () {
			function Renderer() {
				_classCallCheck(this, Renderer);

				this.renderers = {};
				this.order = [];
			}

			_createClass(Renderer, [{
				key: 'add',
				value: function add(renderer) {
					if (renderer.name === undefined) {
						throw new TypeError('renderer must contain at least `name` property');
					}

					this.renderers[renderer.name] = renderer;
					this.order.push(renderer.name);
				}
			}, {
				key: 'select',
				value: function select(mediaFiles) {
					var renderers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

					var renderersLength = renderers.length;

					renderers = renderers.length ? renderers : this.order;

					if (!renderersLength) {
						var rendererIndicator = [/^(html5|native)/i, /^flash/i, /iframe$/i],
						    rendererRanking = function rendererRanking(renderer) {
							for (var i = 0, total = rendererIndicator.length; i < total; i++) {
								if (rendererIndicator[i].test(renderer)) {
									return i;
								}
							}
							return rendererIndicator.length;
						};

						renderers.sort(function (a, b) {
							return rendererRanking(a) - rendererRanking(b);
						});
					}

					for (var i = 0, total = renderers.length; i < total; i++) {
						var key = renderers[i],
						    _renderer = this.renderers[key];

						if (_renderer !== null && _renderer !== undefined) {
							for (var j = 0, jl = mediaFiles.length; j < jl; j++) {
								if (typeof _renderer.canPlayType === 'function' && typeof mediaFiles[j].type === 'string' && _renderer.canPlayType(mediaFiles[j].type)) {
									return {
										rendererName: _renderer.name,
										src: mediaFiles[j].src
									};
								}
							}
						}
					}

					return null;
				}
			}, {
				key: 'order',
				set: function set(order) {
					if (!Array.isArray(order)) {
						throw new TypeError('order must be an array of strings.');
					}

					this._order = order;
				},
				get: function get() {
					return this._order;
				}
			}, {
				key: 'renderers',
				set: function set(renderers) {
					if (renderers !== null && (typeof renderers === 'undefined' ? 'undefined' : _typeof(renderers)) !== 'object') {
						throw new TypeError('renderers must be an array of objects.');
					}

					this._renderers = renderers;
				},
				get: function get() {
					return this._renderers;
				}
			}]);

			return Renderer;
		}();

		var renderer = exports.renderer = new Renderer();

		_mejs2.default.Renderers = renderer;
	}, { "7": 7 }], 9: [function (_dereq_, module, exports) {
		'use strict';

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _i18n = _dereq_(5);

		var _i18n2 = _interopRequireDefault(_i18n);

		var _player = _dereq_(16);

		var _player2 = _interopRequireDefault(_player);

		var _constants = _dereq_(25);

		var Features = _interopRequireWildcard(_constants);

		var _general = _dereq_(27);

		var _dom = _dereq_(26);

		var _media = _dereq_(28);

		function _interopRequireWildcard(obj) {
			if (obj && obj.__esModule) {
				return obj;
			} else {
				var newObj = {};if (obj != null) {
					for (var key in obj) {
						if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
					}
				}newObj.default = obj;return newObj;
			}
		}

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		Object.assign(_player.config, {
			usePluginFullScreen: true,

			fullscreenText: null,

			useFakeFullscreen: false
		});

		Object.assign(_player2.default.prototype, {
			isFullScreen: false,

			isNativeFullScreen: false,

			isInIframe: false,

			isPluginClickThroughCreated: false,

			fullscreenMode: '',

			containerSizeTimeout: null,

			buildfullscreen: function buildfullscreen(player) {
				if (!player.isVideo) {
					return;
				}

				player.isInIframe = _window2.default.location !== _window2.default.parent.location;

				player.detectFullscreenMode();

				var t = this,
				    fullscreenTitle = (0, _general.isString)(t.options.fullscreenText) ? t.options.fullscreenText : _i18n2.default.t('mejs.fullscreen'),
				    fullscreenBtn = _document2.default.createElement('div');

				fullscreenBtn.className = t.options.classPrefix + 'button ' + t.options.classPrefix + 'fullscreen-button';
				fullscreenBtn.innerHTML = '<button type="button" aria-controls="' + t.id + '" title="' + fullscreenTitle + '" aria-label="' + fullscreenTitle + '" tabindex="0"></button>';
				t.addControlElement(fullscreenBtn, 'fullscreen');

				fullscreenBtn.addEventListener('click', function () {
					var isFullScreen = Features.HAS_TRUE_NATIVE_FULLSCREEN && Features.IS_FULLSCREEN || player.isFullScreen;

					if (isFullScreen) {
						player.exitFullScreen();
					} else {
						player.enterFullScreen();
					}
				});

				player.fullscreenBtn = fullscreenBtn;

				t.options.keyActions.push({
					keys: [70],
					action: function action(player, media, key, event) {
						if (!event.ctrlKey) {
							if (typeof player.enterFullScreen !== 'undefined') {
								if (player.isFullScreen) {
									player.exitFullScreen();
								} else {
									player.enterFullScreen();
								}
							}
						}
					}
				});

				t.exitFullscreenCallback = function (e) {
					var key = e.which || e.keyCode || 0;
					if (t.options.enableKeyboard && key === 27 && (Features.HAS_TRUE_NATIVE_FULLSCREEN && Features.IS_FULLSCREEN || t.isFullScreen)) {
						player.exitFullScreen();
					}
				};

				t.globalBind('keydown', t.exitFullscreenCallback);

				t.normalHeight = 0;
				t.normalWidth = 0;

				if (Features.HAS_TRUE_NATIVE_FULLSCREEN) {
					var fullscreenChanged = function fullscreenChanged() {
						if (player.isFullScreen) {
							if (Features.isFullScreen()) {
								player.isNativeFullScreen = true;

								player.setControlsSize();
							} else {
								player.isNativeFullScreen = false;

								player.exitFullScreen();
							}
						}
					};

					player.globalBind(Features.FULLSCREEN_EVENT_NAME, fullscreenChanged);
				}
			},
			cleanfullscreen: function cleanfullscreen(player) {
				player.exitFullScreen();
				player.globalUnbind('keydown', player.exitFullscreenCallback);
			},
			detectFullscreenMode: function detectFullscreenMode() {
				var t = this,
				    isNative = t.media.rendererName !== null && /(native|html5)/i.test(t.media.rendererName);

				var mode = '';

				if (Features.HAS_TRUE_NATIVE_FULLSCREEN && isNative) {
					mode = 'native-native';
				} else if (Features.HAS_TRUE_NATIVE_FULLSCREEN && !isNative) {
					mode = 'plugin-native';
				} else if (t.usePluginFullScreen && Features.SUPPORT_POINTER_EVENTS) {
					mode = 'plugin-click';
				}

				t.fullscreenMode = mode;
				return mode;
			},
			enterFullScreen: function enterFullScreen() {
				var t = this,
				    isNative = t.media.rendererName !== null && /(html5|native)/i.test(t.media.rendererName),
				    containerStyles = getComputedStyle(t.getElement(t.container));

				if (!t.isVideo) {
					return;
				}

				if (t.options.useFakeFullscreen === false && Features.IS_IOS && Features.HAS_IOS_FULLSCREEN && typeof t.media.originalNode.webkitEnterFullscreen === 'function' && t.media.originalNode.canPlayType((0, _media.getTypeFromFile)(t.media.getSrc()))) {
					t.media.originalNode.webkitEnterFullscreen();
					return;
				}

				(0, _dom.addClass)(_document2.default.documentElement, t.options.classPrefix + 'fullscreen');
				(0, _dom.addClass)(t.getElement(t.container), t.options.classPrefix + 'container-fullscreen');

				t.normalHeight = parseFloat(containerStyles.height);
				t.normalWidth = parseFloat(containerStyles.width);

				if (t.fullscreenMode === 'native-native' || t.fullscreenMode === 'plugin-native') {
					Features.requestFullScreen(t.getElement(t.container));

					if (t.isInIframe) {
						setTimeout(function checkFullscreen() {

							if (t.isNativeFullScreen) {
								var percentErrorMargin = 0.002,
								    windowWidth = _window2.default.innerWidth || _document2.default.documentElement.clientWidth || _document2.default.body.clientWidth,
								    screenWidth = screen.width,
								    absDiff = Math.abs(screenWidth - windowWidth),
								    marginError = screenWidth * percentErrorMargin;

								if (absDiff > marginError) {
									t.exitFullScreen();
								} else {
									setTimeout(checkFullscreen, 500);
								}
							}
						}, 1000);
					}
				}

				t.getElement(t.container).style.width = '100%';
				t.getElement(t.container).style.height = '100%';

				t.containerSizeTimeout = setTimeout(function () {
					t.getElement(t.container).style.width = '100%';
					t.getElement(t.container).style.height = '100%';
					t.setControlsSize();
				}, 500);

				if (isNative) {
					t.node.style.width = '100%';
					t.node.style.height = '100%';
				} else {
					var elements = t.getElement(t.container).querySelectorAll('embed, object, video'),
					    _total = elements.length;
					for (var i = 0; i < _total; i++) {
						elements[i].style.width = '100%';
						elements[i].style.height = '100%';
					}
				}

				if (t.options.setDimensions && typeof t.media.setSize === 'function') {
					t.media.setSize(screen.width, screen.height);
				}

				var layers = t.getElement(t.layers).children,
				    total = layers.length;
				for (var _i = 0; _i < total; _i++) {
					layers[_i].style.width = '100%';
					layers[_i].style.height = '100%';
				}

				if (t.fullscreenBtn) {
					(0, _dom.removeClass)(t.fullscreenBtn, t.options.classPrefix + 'fullscreen');
					(0, _dom.addClass)(t.fullscreenBtn, t.options.classPrefix + 'unfullscreen');
				}

				t.setControlsSize();
				t.isFullScreen = true;

				var zoomFactor = Math.min(screen.width / t.width, screen.height / t.height),
				    captionText = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'captions-text');
				if (captionText) {
					captionText.style.fontSize = zoomFactor * 100 + '%';
					captionText.style.lineHeight = 'normal';
					t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'captions-position').style.bottom = (screen.height - t.normalHeight) / 2 - t.getElement(t.controls).offsetHeight / 2 + zoomFactor + 15 + 'px';
				}
				var event = (0, _general.createEvent)('enteredfullscreen', t.getElement(t.container));
				t.getElement(t.container).dispatchEvent(event);
			},
			exitFullScreen: function exitFullScreen() {
				var t = this,
				    isNative = t.media.rendererName !== null && /(native|html5)/i.test(t.media.rendererName);

				if (!t.isVideo) {
					return;
				}

				clearTimeout(t.containerSizeTimeout);

				if (Features.HAS_TRUE_NATIVE_FULLSCREEN && (Features.IS_FULLSCREEN || t.isFullScreen)) {
					Features.cancelFullScreen();
				}

				(0, _dom.removeClass)(_document2.default.documentElement, t.options.classPrefix + 'fullscreen');
				(0, _dom.removeClass)(t.getElement(t.container), t.options.classPrefix + 'container-fullscreen');

				if (t.options.setDimensions) {
					t.getElement(t.container).style.width = t.normalWidth + 'px';
					t.getElement(t.container).style.height = t.normalHeight + 'px';

					if (isNative) {
						t.node.style.width = t.normalWidth + 'px';
						t.node.style.height = t.normalHeight + 'px';
					} else {
						var elements = t.getElement(t.container).querySelectorAll('embed, object, video'),
						    _total2 = elements.length;
						for (var i = 0; i < _total2; i++) {
							elements[i].style.width = t.normalWidth + 'px';
							elements[i].style.height = t.normalHeight + 'px';
						}
					}

					if (typeof t.media.setSize === 'function') {
						t.media.setSize(t.normalWidth, t.normalHeight);
					}

					var layers = t.getElement(t.layers).children,
					    total = layers.length;
					for (var _i2 = 0; _i2 < total; _i2++) {
						layers[_i2].style.width = t.normalWidth + 'px';
						layers[_i2].style.height = t.normalHeight + 'px';
					}
				}

				if (t.fullscreenBtn) {
					(0, _dom.removeClass)(t.fullscreenBtn, t.options.classPrefix + 'unfullscreen');
					(0, _dom.addClass)(t.fullscreenBtn, t.options.classPrefix + 'fullscreen');
				}

				t.setControlsSize();
				t.isFullScreen = false;

				var captionText = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'captions-text');
				if (captionText) {
					captionText.style.fontSize = '';
					captionText.style.lineHeight = '';
					t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'captions-position').style.bottom = '';
				}
				var event = (0, _general.createEvent)('exitedfullscreen', t.getElement(t.container));
				t.getElement(t.container).dispatchEvent(event);
			}
		});
	}, { "16": 16, "2": 2, "25": 25, "26": 26, "27": 27, "28": 28, "3": 3, "5": 5 }], 10: [function (_dereq_, module, exports) {
		'use strict';

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _player = _dereq_(16);

		var _player2 = _interopRequireDefault(_player);

		var _i18n = _dereq_(5);

		var _i18n2 = _interopRequireDefault(_i18n);

		var _general = _dereq_(27);

		var _dom = _dereq_(26);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		Object.assign(_player.config, {
			playText: null,

			pauseText: null
		});

		Object.assign(_player2.default.prototype, {
			buildplaypause: function buildplaypause(player, controls, layers, media) {
				var t = this,
				    op = t.options,
				    playTitle = (0, _general.isString)(op.playText) ? op.playText : _i18n2.default.t('mejs.play'),
				    pauseTitle = (0, _general.isString)(op.pauseText) ? op.pauseText : _i18n2.default.t('mejs.pause'),
				    play = _document2.default.createElement('div');

				play.className = t.options.classPrefix + 'button ' + t.options.classPrefix + 'playpause-button ' + t.options.classPrefix + 'play';
				play.innerHTML = '<button type="button" aria-controls="' + t.id + '" title="' + playTitle + '" aria-label="' + pauseTitle + '" tabindex="0"></button>';
				play.addEventListener('click', function () {
					if (t.paused) {
						t.play();
					} else {
						t.pause();
					}
				});

				var playBtn = play.querySelector('button');
				t.addControlElement(play, 'playpause');

				function togglePlayPause(which) {
					if ('play' === which) {
						(0, _dom.removeClass)(play, t.options.classPrefix + 'play');
						(0, _dom.removeClass)(play, t.options.classPrefix + 'replay');
						(0, _dom.addClass)(play, t.options.classPrefix + 'pause');
						playBtn.setAttribute('title', pauseTitle);
						playBtn.setAttribute('aria-label', pauseTitle);
					} else {

						(0, _dom.removeClass)(play, t.options.classPrefix + 'pause');
						(0, _dom.removeClass)(play, t.options.classPrefix + 'replay');
						(0, _dom.addClass)(play, t.options.classPrefix + 'play');
						playBtn.setAttribute('title', playTitle);
						playBtn.setAttribute('aria-label', playTitle);
					}
				}

				togglePlayPause('pse');

				media.addEventListener('loadedmetadata', function () {
					if (media.rendererName.indexOf('flash') === -1) {
						togglePlayPause('pse');
					}
				});
				media.addEventListener('play', function () {
					togglePlayPause('play');
				});
				media.addEventListener('playing', function () {
					togglePlayPause('play');
				});
				media.addEventListener('pause', function () {
					togglePlayPause('pse');
				});
				media.addEventListener('ended', function () {
					if (!player.options.loop) {
						(0, _dom.removeClass)(play, t.options.classPrefix + 'pause');
						(0, _dom.removeClass)(play, t.options.classPrefix + 'play');
						(0, _dom.addClass)(play, t.options.classPrefix + 'replay');
						playBtn.setAttribute('title', playTitle);
						playBtn.setAttribute('aria-label', playTitle);
					}
				});
			}
		});
	}, { "16": 16, "2": 2, "26": 26, "27": 27, "5": 5 }], 11: [function (_dereq_, module, exports) {
		'use strict';

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _player = _dereq_(16);

		var _player2 = _interopRequireDefault(_player);

		var _i18n = _dereq_(5);

		var _i18n2 = _interopRequireDefault(_i18n);

		var _constants = _dereq_(25);

		var _time = _dereq_(30);

		var _dom = _dereq_(26);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		Object.assign(_player.config, {
			enableProgressTooltip: true,

			useSmoothHover: true,

			forceLive: false
		});

		Object.assign(_player2.default.prototype, {
			buildprogress: function buildprogress(player, controls, layers, media) {

				var lastKeyPressTime = 0,
				    mouseIsDown = false,
				    startedPaused = false;

				var t = this,
				    autoRewindInitial = player.options.autoRewind,
				    tooltip = player.options.enableProgressTooltip ? '<span class="' + t.options.classPrefix + 'time-float">' + ('<span class="' + t.options.classPrefix + 'time-float-current">00:00</span>') + ('<span class="' + t.options.classPrefix + 'time-float-corner"></span>') + '</span>' : '',
				    rail = _document2.default.createElement('div');

				rail.className = t.options.classPrefix + 'time-rail';
				rail.innerHTML = '<span class="' + t.options.classPrefix + 'time-total ' + t.options.classPrefix + 'time-slider">' + ('<span class="' + t.options.classPrefix + 'time-buffering"></span>') + ('<span class="' + t.options.classPrefix + 'time-loaded"></span>') + ('<span class="' + t.options.classPrefix + 'time-current"></span>') + ('<span class="' + t.options.classPrefix + 'time-hovered no-hover"></span>') + ('<span class="' + t.options.classPrefix + 'time-handle"><span class="' + t.options.classPrefix + 'time-handle-content"></span></span>') + ('' + tooltip) + '</span>';

				t.addControlElement(rail, 'progress');

				t.options.keyActions.push({
					keys: [37, 227],
					action: function action(player) {
						if (!isNaN(player.duration) && player.duration > 0) {
							if (player.isVideo) {
								player.showControls();
								player.startControlsTimer();
							}

							player.getElement(player.container).querySelector('.' + _player.config.classPrefix + 'time-total').focus();

							var newTime = Math.max(player.currentTime - player.options.defaultSeekBackwardInterval(player), 0);
							player.setCurrentTime(newTime);
						}
					}
				}, {
					keys: [39, 228],
					action: function action(player) {

						if (!isNaN(player.duration) && player.duration > 0) {
							if (player.isVideo) {
								player.showControls();
								player.startControlsTimer();
							}

							player.getElement(player.container).querySelector('.' + _player.config.classPrefix + 'time-total').focus();

							var newTime = Math.min(player.currentTime + player.options.defaultSeekForwardInterval(player), player.duration);
							player.setCurrentTime(newTime);
						}
					}
				});

				t.rail = controls.querySelector('.' + t.options.classPrefix + 'time-rail');
				t.total = controls.querySelector('.' + t.options.classPrefix + 'time-total');
				t.loaded = controls.querySelector('.' + t.options.classPrefix + 'time-loaded');
				t.current = controls.querySelector('.' + t.options.classPrefix + 'time-current');
				t.handle = controls.querySelector('.' + t.options.classPrefix + 'time-handle');
				t.timefloat = controls.querySelector('.' + t.options.classPrefix + 'time-float');
				t.timefloatcurrent = controls.querySelector('.' + t.options.classPrefix + 'time-float-current');
				t.slider = controls.querySelector('.' + t.options.classPrefix + 'time-slider');
				t.hovered = controls.querySelector('.' + t.options.classPrefix + 'time-hovered');
				t.buffer = controls.querySelector('.' + t.options.classPrefix + 'time-buffering');
				t.newTime = 0;
				t.forcedHandlePause = false;
				t.setTransformStyle = function (element, value) {
					element.style.transform = value;
					element.style.webkitTransform = value;
					element.style.MozTransform = value;
					element.style.msTransform = value;
					element.style.OTransform = value;
				};

				t.buffer.style.display = 'none';

				var handleMouseMove = function handleMouseMove(e) {
					var totalStyles = getComputedStyle(t.total),
					    offsetStyles = (0, _dom.offset)(t.total),
					    width = t.total.offsetWidth,
					    transform = function () {
						if (totalStyles.webkitTransform !== undefined) {
							return 'webkitTransform';
						} else if (totalStyles.mozTransform !== undefined) {
							return 'mozTransform ';
						} else if (totalStyles.oTransform !== undefined) {
							return 'oTransform';
						} else if (totalStyles.msTransform !== undefined) {
							return 'msTransform';
						} else {
							return 'transform';
						}
					}(),
					    cssMatrix = function () {
						if ('WebKitCSSMatrix' in window) {
							return 'WebKitCSSMatrix';
						} else if ('MSCSSMatrix' in window) {
							return 'MSCSSMatrix';
						} else if ('CSSMatrix' in window) {
							return 'CSSMatrix';
						}
					}();

					var percentage = 0,
					    leftPos = 0,
					    pos = 0,
					    x = void 0;

					if (e.originalEvent && e.originalEvent.changedTouches) {
						x = e.originalEvent.changedTouches[0].pageX;
					} else if (e.changedTouches) {
						x = e.changedTouches[0].pageX;
					} else {
						x = e.pageX;
					}

					if (t.getDuration()) {
						if (x < offsetStyles.left) {
							x = offsetStyles.left;
						} else if (x > width + offsetStyles.left) {
							x = width + offsetStyles.left;
						}

						pos = x - offsetStyles.left;
						percentage = pos / width;
						t.newTime = percentage <= 0.02 ? 0 : percentage * t.getDuration();

						if (mouseIsDown && t.getCurrentTime() !== null && t.newTime.toFixed(4) !== t.getCurrentTime().toFixed(4)) {
							t.setCurrentRailHandle(t.newTime);
							t.updateCurrent(t.newTime);
						}

						if (!_constants.IS_IOS && !_constants.IS_ANDROID) {
							if (pos < 0) {
								pos = 0;
							}
							if (t.options.useSmoothHover && cssMatrix !== null && typeof window[cssMatrix] !== 'undefined') {
								var matrix = new window[cssMatrix](getComputedStyle(t.handle)[transform]),
								    handleLocation = matrix.m41,
								    hoverScaleX = pos / parseFloat(getComputedStyle(t.total).width) - handleLocation / parseFloat(getComputedStyle(t.total).width);

								t.hovered.style.left = handleLocation + 'px';
								t.setTransformStyle(t.hovered, 'scaleX(' + hoverScaleX + ')');
								t.hovered.setAttribute('pos', pos);

								if (hoverScaleX >= 0) {
									(0, _dom.removeClass)(t.hovered, 'negative');
								} else {
									(0, _dom.addClass)(t.hovered, 'negative');
								}
							}

							if (t.timefloat) {
								var half = t.timefloat.offsetWidth / 2,
								    offsetContainer = mejs.Utils.offset(t.getElement(t.container)),
								    tooltipStyles = getComputedStyle(t.timefloat);

								if (x - offsetContainer.left < t.timefloat.offsetWidth) {
									leftPos = half;
								} else if (x - offsetContainer.left >= t.getElement(t.container).offsetWidth - half) {
									leftPos = t.total.offsetWidth - half;
								} else {
									leftPos = pos;
								}

								if ((0, _dom.hasClass)(t.getElement(t.container), t.options.classPrefix + 'long-video')) {
									leftPos += parseFloat(tooltipStyles.marginLeft) / 2 + t.timefloat.offsetWidth / 2;
								}

								t.timefloat.style.left = leftPos + 'px';
								t.timefloatcurrent.innerHTML = (0, _time.secondsToTimeCode)(t.newTime, player.options.alwaysShowHours, player.options.showTimecodeFrameCount, player.options.framesPerSecond, player.options.secondsDecimalLength, player.options.timeFormat);
								t.timefloat.style.display = 'block';
							}
						}
					} else if (!_constants.IS_IOS && !_constants.IS_ANDROID && t.timefloat) {
						leftPos = t.timefloat.offsetWidth + width >= t.getElement(t.container).offsetWidth ? t.timefloat.offsetWidth / 2 : 0;
						t.timefloat.style.left = leftPos + 'px';
						t.timefloat.style.left = leftPos + 'px';
						t.timefloat.style.display = 'block';
					}
				},
				    updateSlider = function updateSlider() {
					var seconds = t.getCurrentTime(),
					    timeSliderText = _i18n2.default.t('mejs.time-slider'),
					    time = (0, _time.secondsToTimeCode)(seconds, player.options.alwaysShowHours, player.options.showTimecodeFrameCount, player.options.framesPerSecond, player.options.secondsDecimalLength, player.options.timeFormat),
					    duration = t.getDuration();

					t.slider.setAttribute('role', 'slider');
					t.slider.tabIndex = 0;

					if (media.paused) {
						t.slider.setAttribute('aria-label', timeSliderText);
						t.slider.setAttribute('aria-valuemin', 0);
						t.slider.setAttribute('aria-valuemax', duration);
						t.slider.setAttribute('aria-valuenow', seconds);
						t.slider.setAttribute('aria-valuetext', time);
					} else {
						t.slider.removeAttribute('aria-label');
						t.slider.removeAttribute('aria-valuemin');
						t.slider.removeAttribute('aria-valuemax');
						t.slider.removeAttribute('aria-valuenow');
						t.slider.removeAttribute('aria-valuetext');
					}
				},
				    restartPlayer = function restartPlayer() {
					if (new Date() - lastKeyPressTime >= 1000) {
						t.play();
					}
				},
				    handleMouseup = function handleMouseup() {
					if (mouseIsDown && t.getCurrentTime() !== null && t.newTime.toFixed(4) !== t.getCurrentTime().toFixed(4)) {
						t.setCurrentTime(t.newTime);
						t.setCurrentRailHandle(t.newTime);
						t.updateCurrent(t.newTime);
					}
					if (t.forcedHandlePause) {
						t.slider.focus();
						t.play();
					}
					t.forcedHandlePause = false;
				};

				t.slider.addEventListener('focus', function () {
					player.options.autoRewind = false;
				});
				t.slider.addEventListener('blur', function () {
					player.options.autoRewind = autoRewindInitial;
				});
				t.slider.addEventListener('keydown', function (e) {
					if (new Date() - lastKeyPressTime >= 1000) {
						startedPaused = t.paused;
					}

					if (t.options.enableKeyboard && t.options.keyActions.length) {

						var keyCode = e.which || e.keyCode || 0,
						    duration = t.getDuration(),
						    seekForward = player.options.defaultSeekForwardInterval(media),
						    seekBackward = player.options.defaultSeekBackwardInterval(media);

						var seekTime = t.getCurrentTime();
						var volume = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'volume-slider');

						if (keyCode === 38 || keyCode === 40) {
							if (volume) {
								volume.style.display = 'block';
							}
							if (t.isVideo) {
								t.showControls();
								t.startControlsTimer();
							}

							var newVolume = keyCode === 38 ? Math.min(t.volume + 0.1, 1) : Math.max(t.volume - 0.1, 0),
							    mutePlayer = newVolume <= 0;
							t.setVolume(newVolume);
							t.setMuted(mutePlayer);
							return;
						} else {
							if (volume) {
								volume.style.display = 'none';
							}
						}

						switch (keyCode) {
							case 37:
								if (t.getDuration() !== Infinity) {
									seekTime -= seekBackward;
								}
								break;
							case 39:
								if (t.getDuration() !== Infinity) {
									seekTime += seekForward;
								}
								break;
							case 36:
								seekTime = 0;
								break;
							case 35:
								seekTime = duration;
								break;
							case 13:
							case 32:
								if (_constants.IS_FIREFOX) {
									if (t.paused) {
										t.play();
									} else {
										t.pause();
									}
								}
								return;
							default:
								return;
						}

						seekTime = seekTime < 0 ? 0 : seekTime >= duration ? duration : Math.floor(seekTime);
						lastKeyPressTime = new Date();
						if (!startedPaused) {
							player.pause();
						}

						if (seekTime < t.getDuration() && !startedPaused) {
							setTimeout(restartPlayer, 1100);
						}

						t.setCurrentTime(seekTime);
						player.showControls();

						e.preventDefault();
						e.stopPropagation();
					}
				});

				var events = ['mousedown', 'touchstart'];

				t.slider.addEventListener('dragstart', function () {
					return false;
				});

				for (var i = 0, total = events.length; i < total; i++) {
					t.slider.addEventListener(events[i], function (e) {
						t.forcedHandlePause = false;
						if (t.getDuration() !== Infinity) {
							if (e.which === 1 || e.which === 0) {
								if (!t.paused) {
									t.pause();
									t.forcedHandlePause = true;
								}

								mouseIsDown = true;
								handleMouseMove(e);
								var endEvents = ['mouseup', 'touchend'];

								for (var j = 0, totalEvents = endEvents.length; j < totalEvents; j++) {
									t.getElement(t.container).addEventListener(endEvents[j], function (event) {
										var target = event.target;
										if (target === t.slider || target.closest('.' + t.options.classPrefix + 'time-slider')) {
											handleMouseMove(event);
										}
									});
								}
								t.globalBind('mouseup.dur touchend.dur', function () {
									handleMouseup();
									mouseIsDown = false;
									if (t.timefloat) {
										t.timefloat.style.display = 'none';
									}
								});
							}
						}
					}, _constants.SUPPORT_PASSIVE_EVENT && events[i] === 'touchstart' ? { passive: true } : false);
				}
				t.slider.addEventListener('mouseenter', function (e) {
					if (e.target === t.slider && t.getDuration() !== Infinity) {
						t.getElement(t.container).addEventListener('mousemove', function (event) {
							var target = event.target;
							if (target === t.slider || target.closest('.' + t.options.classPrefix + 'time-slider')) {
								handleMouseMove(event);
							}
						});
						if (t.timefloat && !_constants.IS_IOS && !_constants.IS_ANDROID) {
							t.timefloat.style.display = 'block';
						}
						if (t.hovered && !_constants.IS_IOS && !_constants.IS_ANDROID && t.options.useSmoothHover) {
							(0, _dom.removeClass)(t.hovered, 'no-hover');
						}
					}
				});
				t.slider.addEventListener('mouseleave', function () {
					if (t.getDuration() !== Infinity) {
						if (!mouseIsDown) {
							if (t.timefloat) {
								t.timefloat.style.display = 'none';
							}
							if (t.hovered && t.options.useSmoothHover) {
								(0, _dom.addClass)(t.hovered, 'no-hover');
							}
						}
					}
				});

				t.broadcastCallback = function (e) {
					var broadcast = controls.querySelector('.' + t.options.classPrefix + 'broadcast');
					if (!t.options.forceLive && t.getDuration() !== Infinity) {
						if (broadcast) {
							t.slider.style.display = '';
							broadcast.remove();
						}

						player.setProgressRail(e);
						if (!t.forcedHandlePause) {
							player.setCurrentRail(e);
						}
						updateSlider();
					} else if (!broadcast || t.options.forceLive) {
						var label = _document2.default.createElement('span');
						label.className = t.options.classPrefix + 'broadcast';
						label.innerText = _i18n2.default.t('mejs.live-broadcast');
						t.slider.style.display = 'none';
						t.rail.appendChild(label);
					}
				};

				media.addEventListener('progress', t.broadcastCallback);
				media.addEventListener('timeupdate', t.broadcastCallback);
				media.addEventListener('play', function () {
					t.buffer.style.display = 'none';
				});
				media.addEventListener('playing', function () {
					t.buffer.style.display = 'none';
				});
				media.addEventListener('seeking', function () {
					t.buffer.style.display = '';
				});
				media.addEventListener('seeked', function () {
					t.buffer.style.display = 'none';
				});
				media.addEventListener('pause', function () {
					t.buffer.style.display = 'none';
				});
				media.addEventListener('waiting', function () {
					t.buffer.style.display = '';
				});
				media.addEventListener('loadeddata', function () {
					t.buffer.style.display = '';
				});
				media.addEventListener('canplay', function () {
					t.buffer.style.display = 'none';
				});
				media.addEventListener('error', function () {
					t.buffer.style.display = 'none';
				});

				t.getElement(t.container).addEventListener('controlsresize', function (e) {
					if (t.getDuration() !== Infinity) {
						player.setProgressRail(e);
						if (!t.forcedHandlePause) {
							player.setCurrentRail(e);
						}
					}
				});
			},
			cleanprogress: function cleanprogress(player, controls, layers, media) {
				media.removeEventListener('progress', player.broadcastCallback);
				media.removeEventListener('timeupdate', player.broadcastCallback);
				if (player.rail) {
					player.rail.remove();
				}
			},
			setProgressRail: function setProgressRail(e) {
				var t = this,
				    target = e !== undefined ? e.detail.target || e.target : t.media;

				var percent = null;

				if (target && target.buffered && target.buffered.length > 0 && target.buffered.end && t.getDuration()) {
					percent = target.buffered.end(target.buffered.length - 1) / t.getDuration();
				} else if (target && target.bytesTotal !== undefined && target.bytesTotal > 0 && target.bufferedBytes !== undefined) {
					percent = target.bufferedBytes / target.bytesTotal;
				} else if (e && e.lengthComputable && e.total !== 0) {
					percent = e.loaded / e.total;
				}

				if (percent !== null) {
					percent = Math.min(1, Math.max(0, percent));

					if (t.loaded) {
						t.setTransformStyle(t.loaded, 'scaleX(' + percent + ')');
					}
				}
			},
			setCurrentRailHandle: function setCurrentRailHandle(fakeTime) {
				var t = this;
				t.setCurrentRailMain(t, fakeTime);
			},
			setCurrentRail: function setCurrentRail() {
				var t = this;
				t.setCurrentRailMain(t);
			},
			setCurrentRailMain: function setCurrentRailMain(t, fakeTime) {
				if (t.getCurrentTime() !== undefined && t.getDuration()) {
					var nTime = typeof fakeTime === 'undefined' ? t.getCurrentTime() : fakeTime;

					if (t.total && t.handle) {
						var tW = parseFloat(getComputedStyle(t.total).width);

						var newWidth = Math.round(tW * nTime / t.getDuration()),
						    handlePos = newWidth - Math.round(t.handle.offsetWidth / 2);

						handlePos = handlePos < 0 ? 0 : handlePos;
						t.setTransformStyle(t.current, 'scaleX(' + newWidth / tW + ')');
						t.setTransformStyle(t.handle, 'translateX(' + handlePos + 'px)');

						if (t.options.useSmoothHover && !(0, _dom.hasClass)(t.hovered, 'no-hover')) {
							var pos = parseInt(t.hovered.getAttribute('pos'), 10);
							pos = isNaN(pos) ? 0 : pos;

							var hoverScaleX = pos / tW - handlePos / tW;

							t.hovered.style.left = handlePos + 'px';
							t.setTransformStyle(t.hovered, 'scaleX(' + hoverScaleX + ')');

							if (hoverScaleX >= 0) {
								(0, _dom.removeClass)(t.hovered, 'negative');
							} else {
								(0, _dom.addClass)(t.hovered, 'negative');
							}
						}
					}
				}
			}
		});
	}, { "16": 16, "2": 2, "25": 25, "26": 26, "30": 30, "5": 5 }], 12: [function (_dereq_, module, exports) {
		'use strict';

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _player = _dereq_(16);

		var _player2 = _interopRequireDefault(_player);

		var _time = _dereq_(30);

		var _dom = _dereq_(26);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		Object.assign(_player.config, {
			duration: 0,

			timeAndDurationSeparator: '<span> | </span>'
		});

		Object.assign(_player2.default.prototype, {
			buildcurrent: function buildcurrent(player, controls, layers, media) {
				var t = this,
				    time = _document2.default.createElement('div');

				time.className = t.options.classPrefix + 'time';
				time.setAttribute('role', 'timer');
				time.setAttribute('aria-live', 'off');
				time.innerHTML = '<span class="' + t.options.classPrefix + 'currenttime">' + (0, _time.secondsToTimeCode)(0, player.options.alwaysShowHours, player.options.showTimecodeFrameCount, player.options.framesPerSecond, player.options.secondsDecimalLength, player.options.timeFormat) + '</span>';

				t.addControlElement(time, 'current');
				player.updateCurrent();
				t.updateTimeCallback = function () {
					if (t.controlsAreVisible) {
						player.updateCurrent();
					}
				};
				media.addEventListener('timeupdate', t.updateTimeCallback);
			},
			cleancurrent: function cleancurrent(player, controls, layers, media) {
				media.removeEventListener('timeupdate', player.updateTimeCallback);
			},
			buildduration: function buildduration(player, controls, layers, media) {
				var t = this,
				    currTime = controls.lastChild.querySelector('.' + t.options.classPrefix + 'currenttime');

				if (currTime) {
					controls.querySelector('.' + t.options.classPrefix + 'time').innerHTML += t.options.timeAndDurationSeparator + '<span class="' + t.options.classPrefix + 'duration">' + ((0, _time.secondsToTimeCode)(t.options.duration, t.options.alwaysShowHours, t.options.showTimecodeFrameCount, t.options.framesPerSecond, t.options.secondsDecimalLength, t.options.timeFormat) + '</span>');
				} else {
					if (controls.querySelector('.' + t.options.classPrefix + 'currenttime')) {
						(0, _dom.addClass)(controls.querySelector('.' + t.options.classPrefix + 'currenttime').parentNode, t.options.classPrefix + 'currenttime-container');
					}

					var duration = _document2.default.createElement('div');
					duration.className = t.options.classPrefix + 'time ' + t.options.classPrefix + 'duration-container';
					duration.innerHTML = '<span class="' + t.options.classPrefix + 'duration">' + ((0, _time.secondsToTimeCode)(t.options.duration, t.options.alwaysShowHours, t.options.showTimecodeFrameCount, t.options.framesPerSecond, t.options.secondsDecimalLength, t.options.timeFormat) + '</span>');

					t.addControlElement(duration, 'duration');
				}

				t.updateDurationCallback = function () {
					if (t.controlsAreVisible) {
						player.updateDuration();
					}
				};

				media.addEventListener('timeupdate', t.updateDurationCallback);
			},
			cleanduration: function cleanduration(player, controls, layers, media) {
				media.removeEventListener('timeupdate', player.updateDurationCallback);
			},
			updateCurrent: function updateCurrent() {
				var t = this;

				var currentTime = t.getCurrentTime();

				if (isNaN(currentTime)) {
					currentTime = 0;
				}

				var timecode = (0, _time.secondsToTimeCode)(currentTime, t.options.alwaysShowHours, t.options.showTimecodeFrameCount, t.options.framesPerSecond, t.options.secondsDecimalLength, t.options.timeFormat);

				if (timecode.length > 5) {
					(0, _dom.addClass)(t.getElement(t.container), t.options.classPrefix + 'long-video');
				} else {
					(0, _dom.removeClass)(t.getElement(t.container), t.options.classPrefix + 'long-video');
				}

				if (t.getElement(t.controls).querySelector('.' + t.options.classPrefix + 'currenttime')) {
					t.getElement(t.controls).querySelector('.' + t.options.classPrefix + 'currenttime').innerText = timecode;
				}
			},
			updateDuration: function updateDuration() {
				var t = this;

				var duration = t.getDuration();

				if (t.media !== undefined && (isNaN(duration) || duration === Infinity || duration < 0)) {
					t.media.duration = t.options.duration = duration = 0;
				}

				if (t.options.duration > 0) {
					duration = t.options.duration;
				}

				var timecode = (0, _time.secondsToTimeCode)(duration, t.options.alwaysShowHours, t.options.showTimecodeFrameCount, t.options.framesPerSecond, t.options.secondsDecimalLength, t.options.timeFormat);

				if (timecode.length > 5) {
					(0, _dom.addClass)(t.getElement(t.container), t.options.classPrefix + 'long-video');
				} else {
					(0, _dom.removeClass)(t.getElement(t.container), t.options.classPrefix + 'long-video');
				}

				if (t.getElement(t.controls).querySelector('.' + t.options.classPrefix + 'duration') && duration > 0) {
					t.getElement(t.controls).querySelector('.' + t.options.classPrefix + 'duration').innerHTML = timecode;
				}
			}
		});
	}, { "16": 16, "2": 2, "26": 26, "30": 30 }], 13: [function (_dereq_, module, exports) {
		'use strict';

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _i18n = _dereq_(5);

		var _i18n2 = _interopRequireDefault(_i18n);

		var _player = _dereq_(16);

		var _player2 = _interopRequireDefault(_player);

		var _time = _dereq_(30);

		var _general = _dereq_(27);

		var _dom = _dereq_(26);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		Object.assign(_player.config, {
			startLanguage: '',

			tracksText: null,

			chaptersText: null,

			tracksAriaLive: false,

			hideCaptionsButtonWhenEmpty: true,

			toggleCaptionsButtonWhenOnlyOne: false,

			slidesSelector: ''
		});

		Object.assign(_player2.default.prototype, {
			hasChapters: false,

			buildtracks: function buildtracks(player, controls, layers, media) {

				this.findTracks();

				if (!player.tracks.length && (!player.trackFiles || !player.trackFiles.length === 0)) {
					return;
				}

				var t = this,
				    attr = t.options.tracksAriaLive ? ' role="log" aria-live="assertive" aria-atomic="false"' : '',
				    tracksTitle = (0, _general.isString)(t.options.tracksText) ? t.options.tracksText : _i18n2.default.t('mejs.captions-subtitles'),
				    chaptersTitle = (0, _general.isString)(t.options.chaptersText) ? t.options.chaptersText : _i18n2.default.t('mejs.captions-chapters'),
				    total = player.trackFiles === null ? player.tracks.length : player.trackFiles.length;

				if (t.domNode.textTracks) {
					for (var i = t.domNode.textTracks.length - 1; i >= 0; i--) {
						t.domNode.textTracks[i].mode = 'hidden';
					}
				}

				t.cleartracks(player);

				player.captions = _document2.default.createElement('div');
				player.captions.className = t.options.classPrefix + 'captions-layer ' + t.options.classPrefix + 'layer';
				player.captions.innerHTML = '<div class="' + t.options.classPrefix + 'captions-position ' + t.options.classPrefix + 'captions-position-hover"' + attr + '>' + ('<span class="' + t.options.classPrefix + 'captions-text"></span>') + '</div>';
				player.captions.style.display = 'none';
				layers.insertBefore(player.captions, layers.firstChild);

				player.captionsText = player.captions.querySelector('.' + t.options.classPrefix + 'captions-text');

				player.captionsButton = _document2.default.createElement('div');
				player.captionsButton.className = t.options.classPrefix + 'button ' + t.options.classPrefix + 'captions-button';
				player.captionsButton.innerHTML = '<button type="button" aria-controls="' + t.id + '" title="' + tracksTitle + '" aria-label="' + tracksTitle + '" tabindex="0"></button>' + ('<div class="' + t.options.classPrefix + 'captions-selector ' + t.options.classPrefix + 'offscreen">') + ('<ul class="' + t.options.classPrefix + 'captions-selector-list">') + ('<li class="' + t.options.classPrefix + 'captions-selector-list-item">') + ('<input type="radio" class="' + t.options.classPrefix + 'captions-selector-input" ') + ('name="' + player.id + '_captions" id="' + player.id + '_captions_none" ') + 'value="none" checked disabled>' + ('<label class="' + t.options.classPrefix + 'captions-selector-label ') + (t.options.classPrefix + 'captions-selected" ') + ('for="' + player.id + '_captions_none">' + _i18n2.default.t('mejs.none') + '</label>') + '</li>' + '</ul>' + '</div>';

				t.addControlElement(player.captionsButton, 'tracks');

				player.captionsButton.querySelector('.' + t.options.classPrefix + 'captions-selector-input').disabled = false;

				player.chaptersButton = _document2.default.createElement('div');
				player.chaptersButton.className = t.options.classPrefix + 'button ' + t.options.classPrefix + 'chapters-button';
				player.chaptersButton.innerHTML = '<button type="button" aria-controls="' + t.id + '" title="' + chaptersTitle + '" aria-label="' + chaptersTitle + '" tabindex="0"></button>' + ('<div class="' + t.options.classPrefix + 'chapters-selector ' + t.options.classPrefix + 'offscreen">') + ('<ul class="' + t.options.classPrefix + 'chapters-selector-list"></ul>') + '</div>';

				var subtitleCount = 0;

				for (var _i = 0; _i < total; _i++) {
					var kind = player.tracks[_i].kind,
					    src = player.tracks[_i].src;
					if (src.trim()) {
						if (kind === 'subtitles' || kind === 'captions') {
							subtitleCount++;
						} else if (kind === 'chapters' && !controls.querySelector('.' + t.options.classPrefix + 'chapter-selector')) {
							player.captionsButton.parentNode.insertBefore(player.chaptersButton, player.captionsButton);
						}
					}
				}

				player.trackToLoad = -1;
				player.selectedTrack = null;
				player.isLoadingTrack = false;

				for (var _i2 = 0; _i2 < total; _i2++) {
					var _kind = player.tracks[_i2].kind;
					if (player.tracks[_i2].src.trim() && (_kind === 'subtitles' || _kind === 'captions')) {
						player.addTrackButton(player.tracks[_i2].trackId, player.tracks[_i2].srclang, player.tracks[_i2].label);
					}
				}

				player.loadNextTrack();

				var inEvents = ['mouseenter', 'focusin'],
				    outEvents = ['mouseleave', 'focusout'];

				if (t.options.toggleCaptionsButtonWhenOnlyOne && subtitleCount === 1) {
					player.captionsButton.addEventListener('click', function (e) {
						var trackId = 'none';
						if (player.selectedTrack === null) {
							trackId = player.tracks[0].trackId;
						}
						var keyboard = e.keyCode || e.which;
						player.setTrack(trackId, typeof keyboard !== 'undefined');
					});
				} else {
					var labels = player.captionsButton.querySelectorAll('.' + t.options.classPrefix + 'captions-selector-label'),
					    captions = player.captionsButton.querySelectorAll('input[type=radio]');

					for (var _i3 = 0, _total = inEvents.length; _i3 < _total; _i3++) {
						player.captionsButton.addEventListener(inEvents[_i3], function () {
							(0, _dom.removeClass)(this.querySelector('.' + t.options.classPrefix + 'captions-selector'), t.options.classPrefix + 'offscreen');
						});
					}

					for (var _i4 = 0, _total2 = outEvents.length; _i4 < _total2; _i4++) {
						player.captionsButton.addEventListener(outEvents[_i4], function () {
							(0, _dom.addClass)(this.querySelector('.' + t.options.classPrefix + 'captions-selector'), t.options.classPrefix + 'offscreen');
						});
					}

					for (var _i5 = 0, _total3 = captions.length; _i5 < _total3; _i5++) {
						captions[_i5].addEventListener('click', function (e) {
							var keyboard = e.keyCode || e.which;
							player.setTrack(this.value, typeof keyboard !== 'undefined');
						});
					}

					for (var _i6 = 0, _total4 = labels.length; _i6 < _total4; _i6++) {
						labels[_i6].addEventListener('click', function (e) {
							var radio = (0, _dom.siblings)(this, function (el) {
								return el.tagName === 'INPUT';
							})[0],
							    event = (0, _general.createEvent)('click', radio);
							radio.dispatchEvent(event);
							e.preventDefault();
						});
					}

					player.captionsButton.addEventListener('keydown', function (e) {
						e.stopPropagation();
					});
				}

				for (var _i7 = 0, _total5 = inEvents.length; _i7 < _total5; _i7++) {
					player.chaptersButton.addEventListener(inEvents[_i7], function () {
						if (this.querySelector('.' + t.options.classPrefix + 'chapters-selector-list').children.length) {
							(0, _dom.removeClass)(this.querySelector('.' + t.options.classPrefix + 'chapters-selector'), t.options.classPrefix + 'offscreen');
						}
					});
				}

				for (var _i8 = 0, _total6 = outEvents.length; _i8 < _total6; _i8++) {
					player.chaptersButton.addEventListener(outEvents[_i8], function () {
						(0, _dom.addClass)(this.querySelector('.' + t.options.classPrefix + 'chapters-selector'), t.options.classPrefix + 'offscreen');
					});
				}

				player.chaptersButton.addEventListener('keydown', function (e) {
					e.stopPropagation();
				});

				if (!player.options.alwaysShowControls) {
					player.getElement(player.container).addEventListener('controlsshown', function () {
						(0, _dom.addClass)(player.getElement(player.container).querySelector('.' + t.options.classPrefix + 'captions-position'), t.options.classPrefix + 'captions-position-hover');
					});

					player.getElement(player.container).addEventListener('controlshidden', function () {
						if (!media.paused) {
							(0, _dom.removeClass)(player.getElement(player.container).querySelector('.' + t.options.classPrefix + 'captions-position'), t.options.classPrefix + 'captions-position-hover');
						}
					});
				} else {
					(0, _dom.addClass)(player.getElement(player.container).querySelector('.' + t.options.classPrefix + 'captions-position'), t.options.classPrefix + 'captions-position-hover');
				}

				media.addEventListener('timeupdate', function () {
					player.displayCaptions();
				});

				if (player.options.slidesSelector !== '') {
					player.slidesContainer = _document2.default.querySelectorAll(player.options.slidesSelector);

					media.addEventListener('timeupdate', function () {
						player.displaySlides();
					});
				}
			},
			cleartracks: function cleartracks(player) {
				if (player) {
					if (player.captions) {
						player.captions.remove();
					}
					if (player.chapters) {
						player.chapters.remove();
					}
					if (player.captionsText) {
						player.captionsText.remove();
					}
					if (player.captionsButton) {
						player.captionsButton.remove();
					}
					if (player.chaptersButton) {
						player.chaptersButton.remove();
					}
				}
			},
			rebuildtracks: function rebuildtracks() {
				var t = this;
				t.findTracks();
				t.buildtracks(t, t.getElement(t.controls), t.getElement(t.layers), t.media);
			},
			findTracks: function findTracks() {
				var t = this,
				    tracktags = t.trackFiles === null ? t.node.querySelectorAll('track') : t.trackFiles,
				    total = tracktags.length;

				t.tracks = [];
				for (var i = 0; i < total; i++) {
					var track = tracktags[i],
					    srclang = track.getAttribute('srclang').toLowerCase() || '',
					    trackId = t.id + '_track_' + i + '_' + track.getAttribute('kind') + '_' + srclang;
					t.tracks.push({
						trackId: trackId,
						srclang: srclang,
						src: track.getAttribute('src'),
						kind: track.getAttribute('kind'),
						label: track.getAttribute('label') || '',
						entries: [],
						isLoaded: false
					});
				}
			},
			setTrack: function setTrack(trackId, setByKeyboard) {

				var t = this,
				    radios = t.captionsButton.querySelectorAll('input[type="radio"]'),
				    captions = t.captionsButton.querySelectorAll('.' + t.options.classPrefix + 'captions-selected'),
				    track = t.captionsButton.querySelector('input[value="' + trackId + '"]');

				for (var i = 0, total = radios.length; i < total; i++) {
					radios[i].checked = false;
				}

				for (var _i9 = 0, _total7 = captions.length; _i9 < _total7; _i9++) {
					(0, _dom.removeClass)(captions[_i9], t.options.classPrefix + 'captions-selected');
				}

				track.checked = true;
				var labels = (0, _dom.siblings)(track, function (el) {
					return (0, _dom.hasClass)(el, t.options.classPrefix + 'captions-selector-label');
				});
				for (var _i10 = 0, _total8 = labels.length; _i10 < _total8; _i10++) {
					(0, _dom.addClass)(labels[_i10], t.options.classPrefix + 'captions-selected');
				}

				if (trackId === 'none') {
					t.selectedTrack = null;
					(0, _dom.removeClass)(t.captionsButton, t.options.classPrefix + 'captions-enabled');
				} else {
					for (var _i11 = 0, _total9 = t.tracks.length; _i11 < _total9; _i11++) {
						var _track = t.tracks[_i11];
						if (_track.trackId === trackId) {
							if (t.selectedTrack === null) {
								(0, _dom.addClass)(t.captionsButton, t.options.classPrefix + 'captions-enabled');
							}
							t.selectedTrack = _track;
							t.captions.setAttribute('lang', t.selectedTrack.srclang);
							t.displayCaptions();
							break;
						}
					}
				}

				var event = (0, _general.createEvent)('captionschange', t.media);
				event.detail.caption = t.selectedTrack;
				t.media.dispatchEvent(event);

				if (!setByKeyboard) {
					setTimeout(function () {
						t.getElement(t.container).focus();
					}, 500);
				}
			},
			loadNextTrack: function loadNextTrack() {
				var t = this;

				t.trackToLoad++;
				if (t.trackToLoad < t.tracks.length) {
					t.isLoadingTrack = true;
					t.loadTrack(t.trackToLoad);
				} else {
					t.isLoadingTrack = false;
					t.checkForTracks();
				}
			},
			loadTrack: function loadTrack(index) {
				var t = this,
				    track = t.tracks[index];

				if (track !== undefined && (track.src !== undefined || track.src !== "")) {
					(0, _dom.ajax)(track.src, 'text', function (d) {
						track.entries = typeof d === 'string' && /<tt\s+xml/ig.exec(d) ? _mejs2.default.TrackFormatParser.dfxp.parse(d) : _mejs2.default.TrackFormatParser.webvtt.parse(d);

						track.isLoaded = true;
						t.enableTrackButton(track);
						t.loadNextTrack();

						if (track.kind === 'slides') {
							t.setupSlides(track);
						} else if (track.kind === 'chapters' && !t.hasChapters) {
							t.drawChapters(track);
							t.hasChapters = true;
						}
					}, function () {
						t.removeTrackButton(track.trackId);
						t.loadNextTrack();
					});
				}
			},
			enableTrackButton: function enableTrackButton(track) {
				var t = this,
				    lang = track.srclang,
				    target = _document2.default.getElementById('' + track.trackId);

				if (!target) {
					return;
				}

				var label = track.label;

				if (label === '') {
					label = _i18n2.default.t(_mejs2.default.language.codes[lang]) || lang;
				}
				target.disabled = false;
				var targetSiblings = (0, _dom.siblings)(target, function (el) {
					return (0, _dom.hasClass)(el, t.options.classPrefix + 'captions-selector-label');
				});
				for (var i = 0, total = targetSiblings.length; i < total; i++) {
					targetSiblings[i].innerHTML = label;
				}

				if (t.options.startLanguage === lang) {
					target.checked = true;
					var event = (0, _general.createEvent)('click', target);
					target.dispatchEvent(event);
				}
			},
			removeTrackButton: function removeTrackButton(trackId) {
				var element = _document2.default.getElementById('' + trackId);
				if (element) {
					var button = element.closest('li');
					if (button) {
						button.remove();
					}
				}
			},
			addTrackButton: function addTrackButton(trackId, lang, label) {
				var t = this;
				if (label === '') {
					label = _i18n2.default.t(_mejs2.default.language.codes[lang]) || lang;
				}

				t.captionsButton.querySelector('ul').innerHTML += '<li class="' + t.options.classPrefix + 'captions-selector-list-item">' + ('<input type="radio" class="' + t.options.classPrefix + 'captions-selector-input" ') + ('name="' + t.id + '_captions" id="' + trackId + '" value="' + trackId + '" disabled>') + ('<label class="' + t.options.classPrefix + 'captions-selector-label"') + ('for="' + trackId + '">' + label + ' (loading)</label>') + '</li>';
			},
			checkForTracks: function checkForTracks() {
				var t = this;

				var hasSubtitles = false;

				if (t.options.hideCaptionsButtonWhenEmpty) {
					for (var i = 0, total = t.tracks.length; i < total; i++) {
						var kind = t.tracks[i].kind;
						if ((kind === 'subtitles' || kind === 'captions') && t.tracks[i].isLoaded) {
							hasSubtitles = true;
							break;
						}
					}

					t.captionsButton.style.display = hasSubtitles ? '' : 'none';
					t.setControlsSize();
				}
			},
			displayCaptions: function displayCaptions() {
				if (this.tracks === undefined) {
					return;
				}

				var t = this,
				    track = t.selectedTrack,
				    sanitize = function sanitize(html) {
					var div = _document2.default.createElement('div');
					div.innerHTML = html;

					var scripts = div.getElementsByTagName('script');
					var i = scripts.length;
					while (i--) {
						scripts[i].remove();
					}

					var allElements = div.getElementsByTagName('*');
					for (var _i12 = 0, n = allElements.length; _i12 < n; _i12++) {
						var attributesObj = allElements[_i12].attributes,
						    attributes = Array.prototype.slice.call(attributesObj);

						for (var j = 0, total = attributes.length; j < total; j++) {
							if (attributes[j].name.startsWith('on') || attributes[j].value.startsWith('javascript')) {
								allElements[_i12].remove();
							} else if (attributes[j].name === 'style') {
								allElements[_i12].removeAttribute(attributes[j].name);
							}
						}
					}
					return div.innerHTML;
				};

				if (track !== null && track.isLoaded) {
					var i = t.searchTrackPosition(track.entries, t.media.currentTime);
					if (i > -1) {
						t.captionsText.innerHTML = sanitize(track.entries[i].text);
						t.captionsText.className = t.options.classPrefix + 'captions-text ' + (track.entries[i].identifier || '');
						t.captions.style.display = '';
						t.captions.style.height = '0px';
						return;
					}
					t.captions.style.display = 'none';
				} else {
					t.captions.style.display = 'none';
				}
			},
			setupSlides: function setupSlides(track) {
				var t = this;
				t.slides = track;
				t.slides.entries.imgs = [t.slides.entries.length];
				t.showSlide(0);
			},
			showSlide: function showSlide(index) {
				var _this = this;

				var t = this;

				if (t.tracks === undefined || t.slidesContainer === undefined) {
					return;
				}

				var url = t.slides.entries[index].text;

				var img = t.slides.entries[index].imgs;

				if (img === undefined || img.fadeIn === undefined) {
					var image = _document2.default.createElement('img');
					image.src = url;
					image.addEventListener('load', function () {
						var self = _this,
						    visible = (0, _dom.siblings)(self, function (el) {
							return visible(el);
						});
						self.style.display = 'none';
						t.slidesContainer.innerHTML += self.innerHTML;
						(0, _dom.fadeIn)(t.slidesContainer.querySelector(image));
						for (var i = 0, total = visible.length; i < total; i++) {
							(0, _dom.fadeOut)(visible[i], 400);
						}
					});
					t.slides.entries[index].imgs = img = image;
				} else if (!(0, _dom.visible)(img)) {
					var _visible = (0, _dom.siblings)(self, function (el) {
						return _visible(el);
					});
					(0, _dom.fadeIn)(t.slidesContainer.querySelector(img));
					for (var i = 0, total = _visible.length; i < total; i++) {
						(0, _dom.fadeOut)(_visible[i]);
					}
				}
			},
			displaySlides: function displaySlides() {
				var t = this;

				if (this.slides === undefined) {
					return;
				}

				var slides = t.slides,
				    i = t.searchTrackPosition(slides.entries, t.media.currentTime);

				if (i > -1) {
					t.showSlide(i);
				}
			},
			drawChapters: function drawChapters(chapters) {
				var t = this,
				    total = chapters.entries.length;

				if (!total) {
					return;
				}

				t.chaptersButton.querySelector('ul').innerHTML = '';

				for (var i = 0; i < total; i++) {
					t.chaptersButton.querySelector('ul').innerHTML += '<li class="' + t.options.classPrefix + 'chapters-selector-list-item" ' + 'role="menuitemcheckbox" aria-live="polite" aria-disabled="false" aria-checked="false">' + ('<input type="radio" class="' + t.options.classPrefix + 'captions-selector-input" ') + ('name="' + t.id + '_chapters" id="' + t.id + '_chapters_' + i + '" value="' + chapters.entries[i].start + '" disabled>') + ('<label class="' + t.options.classPrefix + 'chapters-selector-label"') + ('for="' + t.id + '_chapters_' + i + '">' + chapters.entries[i].text + '</label>') + '</li>';
				}

				var radios = t.chaptersButton.querySelectorAll('input[type="radio"]'),
				    labels = t.chaptersButton.querySelectorAll('.' + t.options.classPrefix + 'chapters-selector-label');

				for (var _i13 = 0, _total10 = radios.length; _i13 < _total10; _i13++) {
					radios[_i13].disabled = false;
					radios[_i13].checked = false;
					radios[_i13].addEventListener('click', function (e) {
						var self = this,
						    listItems = t.chaptersButton.querySelectorAll('li'),
						    label = (0, _dom.siblings)(self, function (el) {
							return (0, _dom.hasClass)(el, t.options.classPrefix + 'chapters-selector-label');
						})[0];

						self.checked = true;
						self.parentNode.setAttribute('aria-checked', true);
						(0, _dom.addClass)(label, t.options.classPrefix + 'chapters-selected');
						(0, _dom.removeClass)(t.chaptersButton.querySelector('.' + t.options.classPrefix + 'chapters-selected'), t.options.classPrefix + 'chapters-selected');

						for (var _i14 = 0, _total11 = listItems.length; _i14 < _total11; _i14++) {
							listItems[_i14].setAttribute('aria-checked', false);
						}

						var keyboard = e.keyCode || e.which;
						if (typeof keyboard === 'undefined') {
							setTimeout(function () {
								t.getElement(t.container).focus();
							}, 500);
						}

						t.media.setCurrentTime(parseFloat(self.value));
						if (t.media.paused) {
							t.media.play();
						}
					});
				}

				for (var _i15 = 0, _total12 = labels.length; _i15 < _total12; _i15++) {
					labels[_i15].addEventListener('click', function (e) {
						var radio = (0, _dom.siblings)(this, function (el) {
							return el.tagName === 'INPUT';
						})[0],
						    event = (0, _general.createEvent)('click', radio);
						radio.dispatchEvent(event);
						e.preventDefault();
					});
				}
			},
			searchTrackPosition: function searchTrackPosition(tracks, currentTime) {
				var lo = 0,
				    hi = tracks.length - 1,
				    mid = void 0,
				    start = void 0,
				    stop = void 0;

				while (lo <= hi) {
					mid = lo + hi >> 1;
					start = tracks[mid].start;
					stop = tracks[mid].stop;

					if (currentTime >= start && currentTime < stop) {
						return mid;
					} else if (start < currentTime) {
						lo = mid + 1;
					} else if (start > currentTime) {
						hi = mid - 1;
					}
				}

				return -1;
			}
		});

		_mejs2.default.language = {
			codes: {
				af: 'mejs.afrikaans',
				sq: 'mejs.albanian',
				ar: 'mejs.arabic',
				be: 'mejs.belarusian',
				bg: 'mejs.bulgarian',
				ca: 'mejs.catalan',
				zh: 'mejs.chinese',
				'zh-cn': 'mejs.chinese-simplified',
				'zh-tw': 'mejs.chines-traditional',
				hr: 'mejs.croatian',
				cs: 'mejs.czech',
				da: 'mejs.danish',
				nl: 'mejs.dutch',
				en: 'mejs.english',
				et: 'mejs.estonian',
				fl: 'mejs.filipino',
				fi: 'mejs.finnish',
				fr: 'mejs.french',
				gl: 'mejs.galician',
				de: 'mejs.german',
				el: 'mejs.greek',
				ht: 'mejs.haitian-creole',
				iw: 'mejs.hebrew',
				hi: 'mejs.hindi',
				hu: 'mejs.hungarian',
				is: 'mejs.icelandic',
				id: 'mejs.indonesian',
				ga: 'mejs.irish',
				it: 'mejs.italian',
				ja: 'mejs.japanese',
				ko: 'mejs.korean',
				lv: 'mejs.latvian',
				lt: 'mejs.lithuanian',
				mk: 'mejs.macedonian',
				ms: 'mejs.malay',
				mt: 'mejs.maltese',
				no: 'mejs.norwegian',
				fa: 'mejs.persian',
				pl: 'mejs.polish',
				pt: 'mejs.portuguese',
				ro: 'mejs.romanian',
				ru: 'mejs.russian',
				sr: 'mejs.serbian',
				sk: 'mejs.slovak',
				sl: 'mejs.slovenian',
				es: 'mejs.spanish',
				sw: 'mejs.swahili',
				sv: 'mejs.swedish',
				tl: 'mejs.tagalog',
				th: 'mejs.thai',
				tr: 'mejs.turkish',
				uk: 'mejs.ukrainian',
				vi: 'mejs.vietnamese',
				cy: 'mejs.welsh',
				yi: 'mejs.yiddish'
			}
		};

		_mejs2.default.TrackFormatParser = {
			webvtt: {
				pattern: /^((?:[0-9]{1,2}:)?[0-9]{2}:[0-9]{2}([,.][0-9]{1,3})?) --\> ((?:[0-9]{1,2}:)?[0-9]{2}:[0-9]{2}([,.][0-9]{3})?)(.*)$/,

				parse: function parse(trackText) {
					var lines = trackText.split(/\r?\n/),
					    entries = [];

					var timecode = void 0,
					    text = void 0,
					    identifier = void 0;

					for (var i = 0, total = lines.length; i < total; i++) {
						timecode = this.pattern.exec(lines[i]);

						if (timecode && i < lines.length) {
							if (i - 1 >= 0 && lines[i - 1] !== '') {
								identifier = lines[i - 1];
							}
							i++;

							text = lines[i];
							i++;
							while (lines[i] !== '' && i < lines.length) {
								text = text + '\n' + lines[i];
								i++;
							}
							text = text.trim().replace(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig, "<a href='$1' target='_blank'>$1</a>");
							entries.push({
								identifier: identifier,
								start: (0, _time.convertSMPTEtoSeconds)(timecode[1]) === 0 ? 0.200 : (0, _time.convertSMPTEtoSeconds)(timecode[1]),
								stop: (0, _time.convertSMPTEtoSeconds)(timecode[3]),
								text: text,
								settings: timecode[5]
							});
						}
						identifier = '';
					}
					return entries;
				}
			},

			dfxp: {
				parse: function parse(trackText) {
					trackText = $(trackText).filter('tt');
					var container = trackText.firstChild,
					    lines = container.querySelectorAll('p'),
					    styleNode = trackText.getElementById('' + container.attr('style')),
					    entries = [];

					var styles = void 0;

					if (styleNode.length) {
						styleNode.removeAttribute('id');
						var attributes = styleNode.attributes;
						if (attributes.length) {
							styles = {};
							for (var i = 0, total = attributes.length; i < total; i++) {
								styles[attributes[i].name.split(":")[1]] = attributes[i].value;
							}
						}
					}

					for (var _i16 = 0, _total13 = lines.length; _i16 < _total13; _i16++) {
						var style = void 0,
						    _temp = {
							start: null,
							stop: null,
							style: null,
							text: null
						};

						if (lines.eq(_i16).attr('begin')) {
							_temp.start = (0, _time.convertSMPTEtoSeconds)(lines.eq(_i16).attr('begin'));
						}
						if (!_temp.start && lines.eq(_i16 - 1).attr('end')) {
							_temp.start = (0, _time.convertSMPTEtoSeconds)(lines.eq(_i16 - 1).attr('end'));
						}
						if (lines.eq(_i16).attr('end')) {
							_temp.stop = (0, _time.convertSMPTEtoSeconds)(lines.eq(_i16).attr('end'));
						}
						if (!_temp.stop && lines.eq(_i16 + 1).attr('begin')) {
							_temp.stop = (0, _time.convertSMPTEtoSeconds)(lines.eq(_i16 + 1).attr('begin'));
						}

						if (styles) {
							style = '';
							for (var _style in styles) {
								style += _style + ':' + styles[_style] + ';';
							}
						}
						if (style) {
							_temp.style = style;
						}
						if (_temp.start === 0) {
							_temp.start = 0.200;
						}
						_temp.text = lines.eq(_i16).innerHTML.trim().replace(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig, "<a href='$1' target='_blank'>$1</a>");
						entries.push(_temp);
					}
					return entries;
				}
			}
		};
	}, { "16": 16, "2": 2, "26": 26, "27": 27, "30": 30, "5": 5, "7": 7 }], 14: [function (_dereq_, module, exports) {
		'use strict';

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _player = _dereq_(16);

		var _player2 = _interopRequireDefault(_player);

		var _i18n = _dereq_(5);

		var _i18n2 = _interopRequireDefault(_i18n);

		var _constants = _dereq_(25);

		var _general = _dereq_(27);

		var _dom = _dereq_(26);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		Object.assign(_player.config, {
			muteText: null,

			unmuteText: null,

			allyVolumeControlText: null,

			hideVolumeOnTouchDevices: true,

			audioVolume: 'horizontal',

			videoVolume: 'vertical',

			startVolume: 0.8
		});

		Object.assign(_player2.default.prototype, {
			buildvolume: function buildvolume(player, controls, layers, media) {
				if ((_constants.IS_ANDROID || _constants.IS_IOS) && this.options.hideVolumeOnTouchDevices) {
					return;
				}

				var t = this,
				    mode = t.isVideo ? t.options.videoVolume : t.options.audioVolume,
				    muteText = (0, _general.isString)(t.options.muteText) ? t.options.muteText : _i18n2.default.t('mejs.mute'),
				    unmuteText = (0, _general.isString)(t.options.unmuteText) ? t.options.unmuteText : _i18n2.default.t('mejs.unmute'),
				    volumeControlText = (0, _general.isString)(t.options.allyVolumeControlText) ? t.options.allyVolumeControlText : _i18n2.default.t('mejs.volume-help-text'),
				    mute = _document2.default.createElement('div');

				mute.className = t.options.classPrefix + 'button ' + t.options.classPrefix + 'volume-button ' + t.options.classPrefix + 'mute';
				mute.innerHTML = mode === 'horizontal' ? '<button type="button" aria-controls="' + t.id + '" title="' + muteText + '" aria-label="' + muteText + '" tabindex="0"></button>' : '<button type="button" aria-controls="' + t.id + '" title="' + muteText + '" aria-label="' + muteText + '" tabindex="0"></button>' + ('<a href="javascript:void(0);" class="' + t.options.classPrefix + 'volume-slider" ') + ('aria-label="' + _i18n2.default.t('mejs.volume-slider') + '" aria-valuemin="0" aria-valuemax="100" role="slider" ') + 'aria-orientation="vertical">' + ('<span class="' + t.options.classPrefix + 'offscreen">' + volumeControlText + '</span>') + ('<div class="' + t.options.classPrefix + 'volume-total">') + ('<div class="' + t.options.classPrefix + 'volume-current"></div>') + ('<div class="' + t.options.classPrefix + 'volume-handle"></div>') + '</div>' + '</a>';

				t.addControlElement(mute, 'volume');

				t.options.keyActions.push({
					keys: [38],
					action: function action(player) {
						var volumeSlider = player.getElement(player.container).querySelector('.' + _player.config.classPrefix + 'volume-slider');
						if (volumeSlider || player.getElement(player.container).querySelector('.' + _player.config.classPrefix + 'volume-slider').matches(':focus')) {
							volumeSlider.style.display = 'block';
						}
						if (player.isVideo) {
							player.showControls();
							player.startControlsTimer();
						}

						var newVolume = Math.min(player.volume + 0.1, 1);
						player.setVolume(newVolume);
						if (newVolume > 0) {
							player.setMuted(false);
						}
					}
				}, {
					keys: [40],
					action: function action(player) {
						var volumeSlider = player.getElement(player.container).querySelector('.' + _player.config.classPrefix + 'volume-slider');
						if (volumeSlider) {
							volumeSlider.style.display = 'block';
						}

						if (player.isVideo) {
							player.showControls();
							player.startControlsTimer();
						}

						var newVolume = Math.max(player.volume - 0.1, 0);
						player.setVolume(newVolume);

						if (newVolume <= 0.1) {
							player.setMuted(true);
						}
					}
				}, {
					keys: [77],
					action: function action(player) {
						player.getElement(player.container).querySelector('.' + _player.config.classPrefix + 'volume-slider').style.display = 'block';
						if (player.isVideo) {
							player.showControls();
							player.startControlsTimer();
						}
						if (player.media.muted) {
							player.setMuted(false);
						} else {
							player.setMuted(true);
						}
					}
				});

				if (mode === 'horizontal') {
					var anchor = _document2.default.createElement('a');
					anchor.className = t.options.classPrefix + 'horizontal-volume-slider';
					anchor.href = 'javascript:void(0);';
					anchor.setAttribute('aria-label', _i18n2.default.t('mejs.volume-slider'));
					anchor.setAttribute('aria-valuemin', 0);
					anchor.setAttribute('aria-valuemax', 100);
					anchor.setAttribute('role', 'slider');
					anchor.innerHTML += '<span class="' + t.options.classPrefix + 'offscreen">' + volumeControlText + '</span>' + ('<div class="' + t.options.classPrefix + 'horizontal-volume-total">') + ('<div class="' + t.options.classPrefix + 'horizontal-volume-current"></div>') + ('<div class="' + t.options.classPrefix + 'horizontal-volume-handle"></div>') + '</div>';
					mute.parentNode.insertBefore(anchor, mute.nextSibling);
				}

				var mouseIsDown = false,
				    mouseIsOver = false,
				    modified = false,
				    updateVolumeSlider = function updateVolumeSlider() {
					var volume = Math.floor(media.volume * 100);
					volumeSlider.setAttribute('aria-valuenow', volume);
					volumeSlider.setAttribute('aria-valuetext', volume + '%');
				};

				var volumeSlider = mode === 'vertical' ? t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'volume-slider') : t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'horizontal-volume-slider'),
				    volumeTotal = mode === 'vertical' ? t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'volume-total') : t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'horizontal-volume-total'),
				    volumeCurrent = mode === 'vertical' ? t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'volume-current') : t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'horizontal-volume-current'),
				    volumeHandle = mode === 'vertical' ? t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'volume-handle') : t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'horizontal-volume-handle'),
				    positionVolumeHandle = function positionVolumeHandle(volume) {

					if (volume === null || isNaN(volume) || volume === undefined) {
						return;
					}

					volume = Math.max(0, volume);
					volume = Math.min(volume, 1);

					if (volume === 0) {
						(0, _dom.removeClass)(mute, t.options.classPrefix + 'mute');
						(0, _dom.addClass)(mute, t.options.classPrefix + 'unmute');
						var button = mute.firstElementChild;
						button.setAttribute('title', unmuteText);
						button.setAttribute('aria-label', unmuteText);
					} else {
						(0, _dom.removeClass)(mute, t.options.classPrefix + 'unmute');
						(0, _dom.addClass)(mute, t.options.classPrefix + 'mute');
						var _button = mute.firstElementChild;
						_button.setAttribute('title', muteText);
						_button.setAttribute('aria-label', muteText);
					}

					var volumePercentage = volume * 100 + '%',
					    volumeStyles = getComputedStyle(volumeHandle);

					if (mode === 'vertical') {
						volumeCurrent.style.bottom = 0;
						volumeCurrent.style.height = volumePercentage;
						volumeHandle.style.bottom = volumePercentage;
						volumeHandle.style.marginBottom = -parseFloat(volumeStyles.height) / 2 + 'px';
					} else {
						volumeCurrent.style.left = 0;
						volumeCurrent.style.width = volumePercentage;
						volumeHandle.style.left = volumePercentage;
						volumeHandle.style.marginLeft = -parseFloat(volumeStyles.width) / 2 + 'px';
					}
				},
				    handleVolumeMove = function handleVolumeMove(e) {
					var totalOffset = (0, _dom.offset)(volumeTotal),
					    volumeStyles = getComputedStyle(volumeTotal);

					modified = true;

					var volume = null;

					if (mode === 'vertical') {
						var railHeight = parseFloat(volumeStyles.height),
						    newY = e.pageY - totalOffset.top;

						volume = (railHeight - newY) / railHeight;

						if (totalOffset.top === 0 || totalOffset.left === 0) {
							return;
						}
					} else {
						var railWidth = parseFloat(volumeStyles.width),
						    newX = e.pageX - totalOffset.left;

						volume = newX / railWidth;
					}

					volume = Math.max(0, volume);
					volume = Math.min(volume, 1);

					positionVolumeHandle(volume);

					t.setMuted(volume === 0);
					t.setVolume(volume);

					e.preventDefault();
					e.stopPropagation();
				},
				    toggleMute = function toggleMute() {
					if (t.muted) {
						positionVolumeHandle(0);
						(0, _dom.removeClass)(mute, t.options.classPrefix + 'mute');
						(0, _dom.addClass)(mute, t.options.classPrefix + 'unmute');
					} else {
						positionVolumeHandle(media.volume);
						(0, _dom.removeClass)(mute, t.options.classPrefix + 'unmute');
						(0, _dom.addClass)(mute, t.options.classPrefix + 'mute');
					}
				};

				player.getElement(player.container).addEventListener('keydown', function (e) {
					var hasFocus = !!e.target.closest('.' + t.options.classPrefix + 'container');
					if (!hasFocus && mode === 'vertical') {
						volumeSlider.style.display = 'none';
					}
				});

				mute.addEventListener('mouseenter', function (e) {
					if (e.target === mute) {
						volumeSlider.style.display = 'block';
						mouseIsOver = true;
						e.preventDefault();
						e.stopPropagation();
					}
				});
				mute.addEventListener('focusin', function () {
					volumeSlider.style.display = 'block';
					mouseIsOver = true;
				});

				mute.addEventListener('focusout', function (e) {
					if ((!e.relatedTarget || e.relatedTarget && !e.relatedTarget.matches('.' + t.options.classPrefix + 'volume-slider')) && mode === 'vertical') {
						volumeSlider.style.display = 'none';
					}
				});
				mute.addEventListener('mouseleave', function () {
					mouseIsOver = false;
					if (!mouseIsDown && mode === 'vertical') {
						volumeSlider.style.display = 'none';
					}
				});
				mute.addEventListener('focusout', function () {
					mouseIsOver = false;
				});
				mute.addEventListener('keydown', function (e) {
					if (t.options.enableKeyboard && t.options.keyActions.length) {
						var keyCode = e.which || e.keyCode || 0,
						    volume = media.volume;

						switch (keyCode) {
							case 38:
								volume = Math.min(volume + 0.1, 1);
								break;
							case 40:
								volume = Math.max(0, volume - 0.1);
								break;
							default:
								return true;
						}

						mouseIsDown = false;
						positionVolumeHandle(volume);
						media.setVolume(volume);

						e.preventDefault();
						e.stopPropagation();
					}
				});
				mute.querySelector('button').addEventListener('click', function () {
					media.setMuted(!media.muted);
					var event = (0, _general.createEvent)('volumechange', media);
					media.dispatchEvent(event);
				});

				volumeSlider.addEventListener('dragstart', function () {
					return false;
				});

				volumeSlider.addEventListener('mouseover', function () {
					mouseIsOver = true;
				});
				volumeSlider.addEventListener('focusin', function () {
					volumeSlider.style.display = 'block';
					mouseIsOver = true;
				});
				volumeSlider.addEventListener('focusout', function () {
					mouseIsOver = false;
					if (!mouseIsDown && mode === 'vertical') {
						volumeSlider.style.display = 'none';
					}
				});
				volumeSlider.addEventListener('mousedown', function (e) {
					handleVolumeMove(e);
					t.globalBind('mousemove.vol', function (event) {
						var target = event.target;
						if (mouseIsDown && (target === volumeSlider || target.closest(mode === 'vertical' ? '.' + t.options.classPrefix + 'volume-slider' : '.' + t.options.classPrefix + 'horizontal-volume-slider'))) {
							handleVolumeMove(event);
						}
					});
					t.globalBind('mouseup.vol', function () {
						mouseIsDown = false;
						if (!mouseIsOver && mode === 'vertical') {
							volumeSlider.style.display = 'none';
						}
					});
					mouseIsDown = true;
					e.preventDefault();
					e.stopPropagation();
				});

				media.addEventListener('volumechange', function (e) {
					if (!mouseIsDown) {
						toggleMute();
					}
					updateVolumeSlider(e);
				});

				var rendered = false;
				media.addEventListener('rendererready', function () {
					if (!modified) {
						setTimeout(function () {
							rendered = true;
							if (player.options.startVolume === 0 || media.originalNode.muted) {
								media.setMuted(true);
								player.options.startVolume = 0;
							}
							media.setVolume(player.options.startVolume);
							t.setControlsSize();
						}, 250);
					}
				});

				media.addEventListener('loadedmetadata', function () {
					setTimeout(function () {
						if (!modified && !rendered) {
							if (player.options.startVolume === 0 || media.originalNode.muted) {
								media.setMuted(true);
							}
							media.setVolume(player.options.startVolume);
							t.setControlsSize();
						}
						rendered = false;
					}, 250);
				});

				if (player.options.startVolume === 0 || media.originalNode.muted) {
					media.setMuted(true);
					player.options.startVolume = 0;
					toggleMute();
				}

				t.getElement(t.container).addEventListener('controlsresize', function () {
					toggleMute();
				});
			}
		});
	}, { "16": 16, "2": 2, "25": 25, "26": 26, "27": 27, "5": 5 }], 15: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});
		var EN = exports.EN = {
			'mejs.plural-form': 1,

			'mejs.download-file': 'Download File',

			'mejs.install-flash': 'You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https://get.adobe.com/flashplayer/',

			'mejs.fullscreen': 'Fullscreen',

			'mejs.play': 'Play',
			'mejs.pause': 'Pause',

			'mejs.time-slider': 'Time Slider',
			'mejs.time-help-text': 'Use Left/Right Arrow keys to advance one second, Up/Down arrows to advance ten seconds.',
			'mejs.live-broadcast': 'Live Broadcast',

			'mejs.volume-help-text': 'Use Up/Down Arrow keys to increase or decrease volume.',
			'mejs.unmute': 'Unmute',
			'mejs.mute': 'Mute',
			'mejs.volume-slider': 'Volume Slider',

			'mejs.video-player': 'Video Player',
			'mejs.audio-player': 'Audio Player',

			'mejs.captions-subtitles': 'Captions/Subtitles',
			'mejs.captions-chapters': 'Chapters',
			'mejs.none': 'None',
			'mejs.afrikaans': 'Afrikaans',
			'mejs.albanian': 'Albanian',
			'mejs.arabic': 'Arabic',
			'mejs.belarusian': 'Belarusian',
			'mejs.bulgarian': 'Bulgarian',
			'mejs.catalan': 'Catalan',
			'mejs.chinese': 'Chinese',
			'mejs.chinese-simplified': 'Chinese (Simplified)',
			'mejs.chinese-traditional': 'Chinese (Traditional)',
			'mejs.croatian': 'Croatian',
			'mejs.czech': 'Czech',
			'mejs.danish': 'Danish',
			'mejs.dutch': 'Dutch',
			'mejs.english': 'English',
			'mejs.estonian': 'Estonian',
			'mejs.filipino': 'Filipino',
			'mejs.finnish': 'Finnish',
			'mejs.french': 'French',
			'mejs.galician': 'Galician',
			'mejs.german': 'German',
			'mejs.greek': 'Greek',
			'mejs.haitian-creole': 'Haitian Creole',
			'mejs.hebrew': 'Hebrew',
			'mejs.hindi': 'Hindi',
			'mejs.hungarian': 'Hungarian',
			'mejs.icelandic': 'Icelandic',
			'mejs.indonesian': 'Indonesian',
			'mejs.irish': 'Irish',
			'mejs.italian': 'Italian',
			'mejs.japanese': 'Japanese',
			'mejs.korean': 'Korean',
			'mejs.latvian': 'Latvian',
			'mejs.lithuanian': 'Lithuanian',
			'mejs.macedonian': 'Macedonian',
			'mejs.malay': 'Malay',
			'mejs.maltese': 'Maltese',
			'mejs.norwegian': 'Norwegian',
			'mejs.persian': 'Persian',
			'mejs.polish': 'Polish',
			'mejs.portuguese': 'Portuguese',
			'mejs.romanian': 'Romanian',
			'mejs.russian': 'Russian',
			'mejs.serbian': 'Serbian',
			'mejs.slovak': 'Slovak',
			'mejs.slovenian': 'Slovenian',
			'mejs.spanish': 'Spanish',
			'mejs.swahili': 'Swahili',
			'mejs.swedish': 'Swedish',
			'mejs.tagalog': 'Tagalog',
			'mejs.thai': 'Thai',
			'mejs.turkish': 'Turkish',
			'mejs.ukrainian': 'Ukrainian',
			'mejs.vietnamese': 'Vietnamese',
			'mejs.welsh': 'Welsh',
			'mejs.yiddish': 'Yiddish'
		};
	}, {}], 16: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});
		exports.config = undefined;

		var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
			return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		} : function (obj) {
			return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		};

		var _createClass = function () {
			function defineProperties(target, props) {
				for (var i = 0; i < props.length; i++) {
					var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
				}
			}return function (Constructor, protoProps, staticProps) {
				if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
			};
		}();

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _mediaelement = _dereq_(6);

		var _mediaelement2 = _interopRequireDefault(_mediaelement);

		var _default = _dereq_(17);

		var _default2 = _interopRequireDefault(_default);

		var _i18n = _dereq_(5);

		var _i18n2 = _interopRequireDefault(_i18n);

		var _constants = _dereq_(25);

		var _general = _dereq_(27);

		var _time = _dereq_(30);

		var _media = _dereq_(28);

		var _dom = _dereq_(26);

		var dom = _interopRequireWildcard(_dom);

		function _interopRequireWildcard(obj) {
			if (obj && obj.__esModule) {
				return obj;
			} else {
				var newObj = {};if (obj != null) {
					for (var key in obj) {
						if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
					}
				}newObj.default = obj;return newObj;
			}
		}

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		function _classCallCheck(instance, Constructor) {
			if (!(instance instanceof Constructor)) {
				throw new TypeError("Cannot call a class as a function");
			}
		}

		_mejs2.default.mepIndex = 0;

		_mejs2.default.players = {};

		var config = exports.config = {
			poster: '',

			showPosterWhenEnded: false,

			showPosterWhenPaused: false,

			defaultVideoWidth: 480,

			defaultVideoHeight: 270,

			videoWidth: -1,

			videoHeight: -1,

			defaultAudioWidth: 400,

			defaultAudioHeight: 40,

			defaultSeekBackwardInterval: function defaultSeekBackwardInterval(media) {
				return media.getDuration() * 0.05;
			},

			defaultSeekForwardInterval: function defaultSeekForwardInterval(media) {
				return media.getDuration() * 0.05;
			},

			setDimensions: true,

			audioWidth: -1,

			audioHeight: -1,

			loop: false,

			autoRewind: true,

			enableAutosize: true,

			timeFormat: '',

			alwaysShowHours: false,

			showTimecodeFrameCount: false,

			framesPerSecond: 25,

			alwaysShowControls: false,

			hideVideoControlsOnLoad: false,

			hideVideoControlsOnPause: false,

			clickToPlayPause: true,

			controlsTimeoutDefault: 1500,

			controlsTimeoutMouseEnter: 2500,

			controlsTimeoutMouseLeave: 1000,

			iPadUseNativeControls: false,

			iPhoneUseNativeControls: false,

			AndroidUseNativeControls: false,

			features: ['playpause', 'current', 'progress', 'duration', 'tracks', 'volume', 'fullscreen'],

			useDefaultControls: false,

			isVideo: true,

			stretching: 'auto',

			classPrefix: 'mejs__',

			enableKeyboard: true,

			pauseOtherPlayers: true,

			secondsDecimalLength: 0,

			customError: null,

			keyActions: [{
				keys: [32, 179],
				action: function action(player) {

					if (!_constants.IS_FIREFOX) {
						if (player.paused || player.ended) {
							player.play();
						} else {
							player.pause();
						}
					}
				}
			}]
		};

		_mejs2.default.MepDefaults = config;

		var MediaElementPlayer = function () {
			function MediaElementPlayer(node, o) {
				_classCallCheck(this, MediaElementPlayer);

				var t = this,
				    element = typeof node === 'string' ? _document2.default.getElementById(node) : node;

				if (!(t instanceof MediaElementPlayer)) {
					return new MediaElementPlayer(element, o);
				}

				t.node = t.media = element;

				if (!t.node) {
					return;
				}

				if (t.media.player) {
					return t.media.player;
				}

				t.hasFocus = false;

				t.controlsAreVisible = true;

				t.controlsEnabled = true;

				t.controlsTimer = null;

				t.currentMediaTime = 0;

				t.proxy = null;

				if (o === undefined) {
					var options = t.node.getAttribute('data-mejsoptions');
					o = options ? JSON.parse(options) : {};
				}

				t.options = Object.assign({}, config, o);

				if (t.options.loop && !t.media.getAttribute('loop')) {
					t.media.loop = true;
					t.node.loop = true;
				} else if (t.media.loop) {
					t.options.loop = true;
				}

				if (!t.options.timeFormat) {
					t.options.timeFormat = 'mm:ss';
					if (t.options.alwaysShowHours) {
						t.options.timeFormat = 'hh:mm:ss';
					}
					if (t.options.showTimecodeFrameCount) {
						t.options.timeFormat += ':ff';
					}
				}

				(0, _time.calculateTimeFormat)(0, t.options, t.options.framesPerSecond || 25);

				t.id = 'mep_' + _mejs2.default.mepIndex++;

				_mejs2.default.players[t.id] = t;

				t.init();

				return t;
			}

			_createClass(MediaElementPlayer, [{
				key: 'getElement',
				value: function getElement(element) {
					return element;
				}
			}, {
				key: 'init',
				value: function init() {
					var t = this,
					    playerOptions = Object.assign({}, t.options, {
						success: function success(media, domNode) {
							t._meReady(media, domNode);
						},
						error: function error(e) {
							t._handleError(e);
						}
					}),
					    tagName = t.node.tagName.toLowerCase();

					t.isDynamic = tagName !== 'audio' && tagName !== 'video' && tagName !== 'iframe';
					t.isVideo = t.isDynamic ? t.options.isVideo : tagName !== 'audio' && t.options.isVideo;
					t.mediaFiles = null;
					t.trackFiles = null;

					if (_constants.IS_IPAD && t.options.iPadUseNativeControls || _constants.IS_IPHONE && t.options.iPhoneUseNativeControls) {
						t.node.setAttribute('controls', true);

						if (_constants.IS_IPAD && t.node.getAttribute('autoplay')) {
							t.play();
						}
					} else if ((t.isVideo || !t.isVideo && (t.options.features.length || t.options.useDefaultControls)) && !(_constants.IS_ANDROID && t.options.AndroidUseNativeControls)) {
						t.node.removeAttribute('controls');
						var videoPlayerTitle = t.isVideo ? _i18n2.default.t('mejs.video-player') : _i18n2.default.t('mejs.audio-player');

						var offscreen = _document2.default.createElement('span');
						offscreen.className = t.options.classPrefix + 'offscreen';
						offscreen.innerText = videoPlayerTitle;
						t.media.parentNode.insertBefore(offscreen, t.media);

						t.container = _document2.default.createElement('div');
						t.getElement(t.container).id = t.id;
						t.getElement(t.container).className = t.options.classPrefix + 'container ' + t.options.classPrefix + 'container-keyboard-inactive ' + t.media.className;
						t.getElement(t.container).tabIndex = 0;
						t.getElement(t.container).setAttribute('role', 'application');
						t.getElement(t.container).setAttribute('aria-label', videoPlayerTitle);
						t.getElement(t.container).innerHTML = '<div class="' + t.options.classPrefix + 'inner">' + ('<div class="' + t.options.classPrefix + 'mediaelement"></div>') + ('<div class="' + t.options.classPrefix + 'layers"></div>') + ('<div class="' + t.options.classPrefix + 'controls"></div>') + '</div>';
						t.getElement(t.container).addEventListener('focus', function (e) {
							if (!t.controlsAreVisible && !t.hasFocus && t.controlsEnabled) {
								t.showControls(true);

								var btnSelector = (0, _general.isNodeAfter)(e.relatedTarget, t.getElement(t.container)) ? '.' + t.options.classPrefix + 'controls .' + t.options.classPrefix + 'button:last-child > button' : '.' + t.options.classPrefix + 'playpause-button > button',
								    button = t.getElement(t.container).querySelector(btnSelector);

								button.focus();
							}
						});
						t.node.parentNode.insertBefore(t.getElement(t.container), t.node);

						if (!t.options.features.length && !t.options.useDefaultControls) {
							t.getElement(t.container).style.background = 'transparent';
							t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'controls').style.display = 'none';
						}

						if (t.isVideo && t.options.stretching === 'fill' && !dom.hasClass(t.getElement(t.container).parentNode, t.options.classPrefix + 'fill-container')) {
							t.outerContainer = t.media.parentNode;

							var wrapper = _document2.default.createElement('div');
							wrapper.className = t.options.classPrefix + 'fill-container';
							t.getElement(t.container).parentNode.insertBefore(wrapper, t.getElement(t.container));
							wrapper.appendChild(t.getElement(t.container));
						}

						if (_constants.IS_ANDROID) {
							dom.addClass(t.getElement(t.container), t.options.classPrefix + 'android');
						}
						if (_constants.IS_IOS) {
							dom.addClass(t.getElement(t.container), t.options.classPrefix + 'ios');
						}
						if (_constants.IS_IPAD) {
							dom.addClass(t.getElement(t.container), t.options.classPrefix + 'ipad');
						}
						if (_constants.IS_IPHONE) {
							dom.addClass(t.getElement(t.container), t.options.classPrefix + 'iphone');
						}
						dom.addClass(t.getElement(t.container), t.isVideo ? t.options.classPrefix + 'video' : t.options.classPrefix + 'audio');

						if (_constants.IS_SAFARI && !_constants.IS_IOS) {

							dom.addClass(t.getElement(t.container), t.options.classPrefix + 'hide-cues');

							var cloneNode = t.node.cloneNode(),
							    children = t.node.children,
							    mediaFiles = [],
							    tracks = [];

							for (var i = 0, total = children.length; i < total; i++) {
								var childNode = children[i];

								(function () {
									switch (childNode.tagName.toLowerCase()) {
										case 'source':
											var elements = {};
											Array.prototype.slice.call(childNode.attributes).forEach(function (item) {
												elements[item.name] = item.value;
											});
											elements.type = (0, _media.formatType)(elements.src, elements.type);
											mediaFiles.push(elements);
											break;
										case 'track':
											childNode.mode = 'hidden';
											tracks.push(childNode);
											break;
										default:
											cloneNode.appendChild(childNode);
											break;
									}
								})();
							}

							t.node.remove();
							t.node = t.media = cloneNode;

							if (mediaFiles.length) {
								t.mediaFiles = mediaFiles;
							}
							if (tracks.length) {
								t.trackFiles = tracks;
							}
						}

						t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'mediaelement').appendChild(t.node);

						t.media.player = t;

						t.controls = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'controls');
						t.layers = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'layers');

						var tagType = t.isVideo ? 'video' : 'audio',
						    capsTagName = tagType.substring(0, 1).toUpperCase() + tagType.substring(1);

						if (t.options[tagType + 'Width'] > 0 || t.options[tagType + 'Width'].toString().indexOf('%') > -1) {
							t.width = t.options[tagType + 'Width'];
						} else if (t.node.style.width !== '' && t.node.style.width !== null) {
							t.width = t.node.style.width;
						} else if (t.node.getAttribute('width')) {
							t.width = t.node.getAttribute('width');
						} else {
							t.width = t.options['default' + capsTagName + 'Width'];
						}

						if (t.options[tagType + 'Height'] > 0 || t.options[tagType + 'Height'].toString().indexOf('%') > -1) {
							t.height = t.options[tagType + 'Height'];
						} else if (t.node.style.height !== '' && t.node.style.height !== null) {
							t.height = t.node.style.height;
						} else if (t.node.getAttribute('height')) {
							t.height = t.node.getAttribute('height');
						} else {
							t.height = t.options['default' + capsTagName + 'Height'];
						}

						t.initialAspectRatio = t.height >= t.width ? t.width / t.height : t.height / t.width;

						t.setPlayerSize(t.width, t.height);

						playerOptions.pluginWidth = t.width;
						playerOptions.pluginHeight = t.height;
					} else if (!t.isVideo && !t.options.features.length && !t.options.useDefaultControls) {
						t.node.style.display = 'none';
					}

					_mejs2.default.MepDefaults = playerOptions;

					new _mediaelement2.default(t.media, playerOptions, t.mediaFiles);

					if (t.getElement(t.container) !== undefined && t.options.features.length && t.controlsAreVisible && !t.options.hideVideoControlsOnLoad) {
						var event = (0, _general.createEvent)('controlsshown', t.getElement(t.container));
						t.getElement(t.container).dispatchEvent(event);
					}
				}
			}, {
				key: 'showControls',
				value: function showControls(doAnimation) {
					var t = this;

					doAnimation = doAnimation === undefined || doAnimation;

					if (t.controlsAreVisible || !t.isVideo) {
						return;
					}

					if (doAnimation) {
						(function () {
							dom.fadeIn(t.getElement(t.controls), 200, function () {
								dom.removeClass(t.getElement(t.controls), t.options.classPrefix + 'offscreen');
								var event = (0, _general.createEvent)('controlsshown', t.getElement(t.container));
								t.getElement(t.container).dispatchEvent(event);
							});

							var controls = t.getElement(t.container).querySelectorAll('.' + t.options.classPrefix + 'control');

							var _loop = function _loop(i, total) {
								dom.fadeIn(controls[i], 200, function () {
									dom.removeClass(controls[i], t.options.classPrefix + 'offscreen');
								});
							};

							for (var i = 0, total = controls.length; i < total; i++) {
								_loop(i, total);
							}
						})();
					} else {
						dom.removeClass(t.getElement(t.controls), t.options.classPrefix + 'offscreen');
						t.getElement(t.controls).style.display = '';
						t.getElement(t.controls).style.opacity = 1;

						var controls = t.getElement(t.container).querySelectorAll('.' + t.options.classPrefix + 'control');
						for (var i = 0, total = controls.length; i < total; i++) {
							dom.removeClass(controls[i], t.options.classPrefix + 'offscreen');
							controls[i].style.display = '';
						}

						var event = (0, _general.createEvent)('controlsshown', t.getElement(t.container));
						t.getElement(t.container).dispatchEvent(event);
					}

					t.controlsAreVisible = true;
					t.setControlsSize();
				}
			}, {
				key: 'hideControls',
				value: function hideControls(doAnimation, forceHide) {
					var t = this;

					doAnimation = doAnimation === undefined || doAnimation;

					if (forceHide !== true && (!t.controlsAreVisible || t.options.alwaysShowControls || t.paused && t.readyState === 4 && (!t.options.hideVideoControlsOnLoad && t.currentTime <= 0 || !t.options.hideVideoControlsOnPause && t.currentTime > 0) || t.isVideo && !t.options.hideVideoControlsOnLoad && !t.readyState || t.ended)) {
						return;
					}

					if (doAnimation) {
						(function () {
							dom.fadeOut(t.getElement(t.controls), 200, function () {
								dom.addClass(t.getElement(t.controls), t.options.classPrefix + 'offscreen');
								t.getElement(t.controls).style.display = '';
								var event = (0, _general.createEvent)('controlshidden', t.getElement(t.container));
								t.getElement(t.container).dispatchEvent(event);
							});

							var controls = t.getElement(t.container).querySelectorAll('.' + t.options.classPrefix + 'control');

							var _loop2 = function _loop2(i, total) {
								dom.fadeOut(controls[i], 200, function () {
									dom.addClass(controls[i], t.options.classPrefix + 'offscreen');
									controls[i].style.display = '';
								});
							};

							for (var i = 0, total = controls.length; i < total; i++) {
								_loop2(i, total);
							}
						})();
					} else {
						dom.addClass(t.getElement(t.controls), t.options.classPrefix + 'offscreen');
						t.getElement(t.controls).style.display = '';
						t.getElement(t.controls).style.opacity = 0;

						var controls = t.getElement(t.container).querySelectorAll('.' + t.options.classPrefix + 'control');
						for (var i = 0, total = controls.length; i < total; i++) {
							dom.addClass(controls[i], t.options.classPrefix + 'offscreen');
							controls[i].style.display = '';
						}

						var event = (0, _general.createEvent)('controlshidden', t.getElement(t.container));
						t.getElement(t.container).dispatchEvent(event);
					}

					t.controlsAreVisible = false;
				}
			}, {
				key: 'startControlsTimer',
				value: function startControlsTimer(timeout) {
					var t = this;

					timeout = typeof timeout !== 'undefined' ? timeout : t.options.controlsTimeoutDefault;

					t.killControlsTimer('start');

					t.controlsTimer = setTimeout(function () {
						t.hideControls();
						t.killControlsTimer('hide');
					}, timeout);
				}
			}, {
				key: 'killControlsTimer',
				value: function killControlsTimer() {
					var t = this;

					if (t.controlsTimer !== null) {
						clearTimeout(t.controlsTimer);
						delete t.controlsTimer;
						t.controlsTimer = null;
					}
				}
			}, {
				key: 'disableControls',
				value: function disableControls() {
					var t = this;

					t.killControlsTimer();
					t.controlsEnabled = false;
					t.hideControls(false, true);
				}
			}, {
				key: 'enableControls',
				value: function enableControls() {
					var t = this;

					t.controlsEnabled = true;
					t.showControls(false);
				}
			}, {
				key: '_setDefaultPlayer',
				value: function _setDefaultPlayer() {
					var t = this;
					if (t.proxy) {
						t.proxy.pause();
					}
					t.proxy = new _default2.default(t);
					t.media.addEventListener('loadedmetadata', function () {
						if (t.getCurrentTime() > 0 && t.currentMediaTime > 0) {
							t.setCurrentTime(t.currentMediaTime);
							if (!_constants.IS_IOS && !_constants.IS_ANDROID) {
								t.play();
							}
						}
					});
				}
			}, {
				key: '_meReady',
				value: function _meReady(media, domNode) {
					var t = this,
					    autoplayAttr = domNode.getAttribute('autoplay'),
					    autoplay = !(autoplayAttr === undefined || autoplayAttr === null || autoplayAttr === 'false'),
					    isNative = media.rendererName !== null && /(native|html5)/i.test(t.media.rendererName);

					if (t.getElement(t.controls)) {
						t.enableControls();
					}

					if (t.getElement(t.container) && t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'overlay-play')) {
						t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'overlay-play').style.display = '';
					}

					if (t.created) {
						return;
					}

					t.created = true;
					t.media = media;
					t.domNode = domNode;

					if (!(_constants.IS_ANDROID && t.options.AndroidUseNativeControls) && !(_constants.IS_IPAD && t.options.iPadUseNativeControls) && !(_constants.IS_IPHONE && t.options.iPhoneUseNativeControls)) {
						if (!t.isVideo && !t.options.features.length && !t.options.useDefaultControls) {
							if (autoplay && isNative) {
								t.play();
							}

							if (t.options.success) {

								if (typeof t.options.success === 'string') {
									_window2.default[t.options.success](t.media, t.domNode, t);
								} else {
									t.options.success(t.media, t.domNode, t);
								}
							}

							return;
						}

						t.featurePosition = {};

						t._setDefaultPlayer();

						t.buildposter(t, t.getElement(t.controls), t.getElement(t.layers), t.media);
						t.buildkeyboard(t, t.getElement(t.controls), t.getElement(t.layers), t.media);
						t.buildoverlays(t, t.getElement(t.controls), t.getElement(t.layers), t.media);

						if (t.options.useDefaultControls) {
							var defaultControls = ['playpause', 'current', 'progress', 'duration', 'tracks', 'volume', 'fullscreen'];
							t.options.features = defaultControls.concat(t.options.features.filter(function (item) {
								return defaultControls.indexOf(item) === -1;
							}));
						}

						t.buildfeatures(t, t.getElement(t.controls), t.getElement(t.layers), t.media);

						var event = (0, _general.createEvent)('controlsready', t.getElement(t.container));
						t.getElement(t.container).dispatchEvent(event);

						t.setPlayerSize(t.width, t.height);
						t.setControlsSize();

						if (t.isVideo) {
							t.clickToPlayPauseCallback = function () {

								if (t.options.clickToPlayPause) {
									var button = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'overlay-button'),
									    pressed = button.getAttribute('aria-pressed');

									if (t.paused && pressed) {
										t.pause();
									} else if (t.paused) {
										t.play();
									} else {
										t.pause();
									}

									button.setAttribute('aria-pressed', !pressed);
									t.getElement(t.container).focus();
								}
							};

							t.createIframeLayer();

							t.media.addEventListener('click', t.clickToPlayPauseCallback);

							if ((_constants.IS_ANDROID || _constants.IS_IOS) && !t.options.alwaysShowControls) {
								t.node.addEventListener('touchstart', function () {
									if (t.controlsAreVisible) {
										t.hideControls(false);
									} else {
										if (t.controlsEnabled) {
											t.showControls(false);
										}
									}
								}, _constants.SUPPORT_PASSIVE_EVENT ? { passive: true } : false);
							} else {
								t.getElement(t.container).addEventListener('mouseenter', function () {
									if (t.controlsEnabled) {
										if (!t.options.alwaysShowControls) {
											t.killControlsTimer('enter');
											t.showControls();
											t.startControlsTimer(t.options.controlsTimeoutMouseEnter);
										}
									}
								});
								t.getElement(t.container).addEventListener('mousemove', function () {
									if (t.controlsEnabled) {
										if (!t.controlsAreVisible) {
											t.showControls();
										}
										if (!t.options.alwaysShowControls) {
											t.startControlsTimer(t.options.controlsTimeoutMouseEnter);
										}
									}
								});
								t.getElement(t.container).addEventListener('mouseleave', function () {
									if (t.controlsEnabled) {
										if (!t.paused && !t.options.alwaysShowControls) {
											t.startControlsTimer(t.options.controlsTimeoutMouseLeave);
										}
									}
								});
							}

							if (t.options.hideVideoControlsOnLoad) {
								t.hideControls(false);
							}

							if (t.options.enableAutosize) {
								t.media.addEventListener('loadedmetadata', function (e) {
									var target = e !== undefined ? e.detail.target || e.target : t.media;
									if (t.options.videoHeight <= 0 && !t.domNode.getAttribute('height') && !t.domNode.style.height && target !== null && !isNaN(target.videoHeight)) {
										t.setPlayerSize(target.videoWidth, target.videoHeight);
										t.setControlsSize();
										t.media.setSize(target.videoWidth, target.videoHeight);
									}
								});
							}
						}

						t.media.addEventListener('play', function () {
							t.hasFocus = true;

							for (var playerIndex in _mejs2.default.players) {
								if (_mejs2.default.players.hasOwnProperty(playerIndex)) {
									var p = _mejs2.default.players[playerIndex];

									if (p.id !== t.id && t.options.pauseOtherPlayers && !p.paused && !p.ended) {
										p.pause();
										p.hasFocus = false;
									}
								}
							}

							if (!(_constants.IS_ANDROID || _constants.IS_IOS) && !t.options.alwaysShowControls && t.isVideo) {
								t.hideControls();
							}
						});

						t.media.addEventListener('ended', function () {
							if (t.options.autoRewind) {
								try {
									t.setCurrentTime(0);

									setTimeout(function () {
										var loadingElement = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'overlay-loading');
										if (loadingElement && loadingElement.parentNode) {
											loadingElement.parentNode.style.display = 'none';
										}
									}, 20);
								} catch (exp) {}
							}

							if (typeof t.media.renderer.stop === 'function') {
								t.media.renderer.stop();
							} else {
								t.pause();
							}

							if (t.setProgressRail) {
								t.setProgressRail();
							}
							if (t.setCurrentRail) {
								t.setCurrentRail();
							}

							if (t.options.loop) {
								t.play();
							} else if (!t.options.alwaysShowControls && t.controlsEnabled) {
								t.showControls();
							}
						});

						t.media.addEventListener('loadedmetadata', function () {

							(0, _time.calculateTimeFormat)(t.getDuration(), t.options, t.options.framesPerSecond || 25);

							if (t.updateDuration) {
								t.updateDuration();
							}
							if (t.updateCurrent) {
								t.updateCurrent();
							}

							if (!t.isFullScreen) {
								t.setPlayerSize(t.width, t.height);
								t.setControlsSize();
							}
						});

						var duration = null;
						t.media.addEventListener('timeupdate', function () {
							if (!isNaN(t.getDuration()) && duration !== t.getDuration()) {
								duration = t.getDuration();
								(0, _time.calculateTimeFormat)(duration, t.options, t.options.framesPerSecond || 25);

								if (t.updateDuration) {
									t.updateDuration();
								}
								if (t.updateCurrent) {
									t.updateCurrent();
								}

								t.setControlsSize();
							}
						});

						t.getElement(t.container).addEventListener('click', function (e) {
							dom.addClass(e.currentTarget, t.options.classPrefix + 'container-keyboard-inactive');
						});

						t.getElement(t.container).addEventListener('focusin', function (e) {
							dom.removeClass(e.currentTarget, t.options.classPrefix + 'container-keyboard-inactive');
							if (t.isVideo && !_constants.IS_ANDROID && !_constants.IS_IOS && t.controlsEnabled && !t.options.alwaysShowControls) {
								t.killControlsTimer('enter');
								t.showControls();
								t.startControlsTimer(t.options.controlsTimeoutMouseEnter);
							}
						});

						t.getElement(t.container).addEventListener('focusout', function (e) {
							setTimeout(function () {
								if (e.relatedTarget) {
									if (t.keyboardAction && !e.relatedTarget.closest('.' + t.options.classPrefix + 'container')) {
										t.keyboardAction = false;
										if (t.isVideo && !t.options.alwaysShowControls && !t.paused) {
											t.startControlsTimer(t.options.controlsTimeoutMouseLeave);
										}
									}
								}
							}, 0);
						});

						setTimeout(function () {
							t.setPlayerSize(t.width, t.height);
							t.setControlsSize();
						}, 0);

						t.globalResizeCallback = function () {
							if (!(t.isFullScreen || _constants.HAS_TRUE_NATIVE_FULLSCREEN && _document2.default.webkitIsFullScreen)) {
								t.setPlayerSize(t.width, t.height);
							}

							t.setControlsSize();
						};

						t.globalBind('resize', t.globalResizeCallback);
					}

					if (autoplay && isNative) {
						t.play();
					}

					if (t.options.success) {
						if (typeof t.options.success === 'string') {
							_window2.default[t.options.success](t.media, t.domNode, t);
						} else {
							t.options.success(t.media, t.domNode, t);
						}
					}
				}
			}, {
				key: '_handleError',
				value: function _handleError(e, media, node) {
					var t = this,
					    play = t.getElement(t.layers).querySelector('.' + t.options.classPrefix + 'overlay-play');

					if (play) {
						play.style.display = 'none';
					}

					if (t.options.error) {
						t.options.error(e, media, node);
					}

					if (t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'cannotplay')) {
						t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'cannotplay').remove();
					}

					var errorContainer = _document2.default.createElement('div');
					errorContainer.className = t.options.classPrefix + 'cannotplay';
					errorContainer.style.width = '100%';
					errorContainer.style.height = '100%';

					var errorContent = typeof t.options.customError === 'function' ? t.options.customError(t.media, t.media.originalNode) : t.options.customError,
					    imgError = '';

					if (!errorContent) {
						var poster = t.media.originalNode.getAttribute('poster');
						if (poster) {
							imgError = '<img src="' + poster + '" alt="' + _mejs2.default.i18n.t('mejs.download-file') + '">';
						}

						if (e.message) {
							errorContent = '<p>' + e.message + '</p>';
						}

						if (e.urls) {
							for (var i = 0, total = e.urls.length; i < total; i++) {
								var url = e.urls[i];
								errorContent += '<a href="' + url.src + '" data-type="' + url.type + '"><span>' + _mejs2.default.i18n.t('mejs.download-file') + ': ' + url.src + '</span></a>';
							}
						}
					}

					if (errorContent && t.getElement(t.layers).querySelector('.' + t.options.classPrefix + 'overlay-error')) {
						errorContainer.innerHTML = errorContent;
						t.getElement(t.layers).querySelector('.' + t.options.classPrefix + 'overlay-error').innerHTML = '' + imgError + errorContainer.outerHTML;
						t.getElement(t.layers).querySelector('.' + t.options.classPrefix + 'overlay-error').parentNode.style.display = 'block';
					}

					if (t.controlsEnabled) {
						t.disableControls();
					}
				}
			}, {
				key: 'setPlayerSize',
				value: function setPlayerSize(width, height) {
					var t = this;

					if (!t.options.setDimensions) {
						return false;
					}

					if (typeof width !== 'undefined') {
						t.width = width;
					}

					if (typeof height !== 'undefined') {
						t.height = height;
					}

					switch (t.options.stretching) {
						case 'fill':
							if (t.isVideo) {
								t.setFillMode();
							} else {
								t.setDimensions(t.width, t.height);
							}
							break;
						case 'responsive':
							t.setResponsiveMode();
							break;
						case 'none':
							t.setDimensions(t.width, t.height);
							break;

						default:
							if (t.hasFluidMode() === true) {
								t.setResponsiveMode();
							} else {
								t.setDimensions(t.width, t.height);
							}
							break;
					}
				}
			}, {
				key: 'hasFluidMode',
				value: function hasFluidMode() {
					var t = this;

					return t.height.toString().indexOf('%') !== -1 || t.node && t.node.style.maxWidth && t.node.style.maxWidth !== 'none' && t.node.style.maxWidth !== t.width || t.node && t.node.currentStyle && t.node.currentStyle.maxWidth === '100%';
				}
			}, {
				key: 'setResponsiveMode',
				value: function setResponsiveMode() {
					var t = this,
					    parent = function () {

						var parentEl = void 0,
						    el = t.getElement(t.container);

						while (el) {
							try {
								if (_constants.IS_FIREFOX && el.tagName.toLowerCase() === 'html' && _window2.default.self !== _window2.default.top && _window2.default.frameElement !== null) {
									return _window2.default.frameElement;
								} else {
									parentEl = el.parentElement;
								}
							} catch (e) {
								parentEl = el.parentElement;
							}

							if (parentEl && dom.visible(parentEl)) {
								return parentEl;
							}
							el = parentEl;
						}

						return null;
					}(),
					    parentStyles = parent ? getComputedStyle(parent, null) : getComputedStyle(_document2.default.body, null),
					    nativeWidth = function () {
						if (t.isVideo) {
							if (t.node.videoWidth && t.node.videoWidth > 0) {
								return t.node.videoWidth;
							} else if (t.node.getAttribute('width')) {
								return t.node.getAttribute('width');
							} else {
								return t.options.defaultVideoWidth;
							}
						} else {
							return t.options.defaultAudioWidth;
						}
					}(),
					    nativeHeight = function () {
						if (t.isVideo) {
							if (t.node.videoHeight && t.node.videoHeight > 0) {
								return t.node.videoHeight;
							} else if (t.node.getAttribute('height')) {
								return t.node.getAttribute('height');
							} else {
								return t.options.defaultVideoHeight;
							}
						} else {
							return t.options.defaultAudioHeight;
						}
					}(),
					    aspectRatio = function () {
						var ratio = 1;
						if (!t.isVideo) {
							return ratio;
						}

						if (t.node.videoWidth && t.node.videoWidth > 0 && t.node.videoHeight && t.node.videoHeight > 0) {
							ratio = t.height >= t.width ? t.node.videoWidth / t.node.videoHeight : t.node.videoHeight / t.node.videoWidth;
						} else {
							ratio = t.initialAspectRatio;
						}

						if (isNaN(ratio) || ratio < 0.01 || ratio > 100) {
							ratio = 1;
						}

						return ratio;
					}(),
					    parentHeight = parseFloat(parentStyles.height);

					var newHeight = void 0,
					    parentWidth = parseFloat(parentStyles.width);

					if (t.isVideo) {
						if (t.height === '100%') {
							newHeight = parseFloat(parentWidth * nativeHeight / nativeWidth, 10);
						} else {
							newHeight = t.height >= t.width ? parseFloat(parentWidth / aspectRatio, 10) : parseFloat(parentWidth * aspectRatio, 10);
						}
					} else {
						newHeight = nativeHeight;
					}

					if (isNaN(newHeight)) {
						newHeight = parentHeight;
					}

					if (t.getElement(t.container).parentNode.length > 0 && t.getElement(t.container).parentNode.tagName.toLowerCase() === 'body') {
						parentWidth = _window2.default.innerWidth || _document2.default.documentElement.clientWidth || _document2.default.body.clientWidth;
						newHeight = _window2.default.innerHeight || _document2.default.documentElement.clientHeight || _document2.default.body.clientHeight;
					}

					if (newHeight && parentWidth) {
						t.getElement(t.container).style.width = parentWidth + 'px';
						t.getElement(t.container).style.height = newHeight + 'px';

						t.node.style.width = '100%';
						t.node.style.height = '100%';

						if (t.isVideo && t.media.setSize) {
							t.media.setSize(parentWidth, newHeight);
						}

						var layerChildren = t.getElement(t.layers).children;
						for (var i = 0, total = layerChildren.length; i < total; i++) {
							layerChildren[i].style.width = '100%';
							layerChildren[i].style.height = '100%';
						}
					}
				}
			}, {
				key: 'setFillMode',
				value: function setFillMode() {
					var t = this;
					var isIframe = _window2.default.self !== _window2.default.top && _window2.default.frameElement !== null;
					var parent = function () {
						var parentEl = void 0,
						    el = t.getElement(t.container);

						while (el) {
							try {
								if (_constants.IS_FIREFOX && el.tagName.toLowerCase() === 'html' && _window2.default.self !== _window2.default.top && _window2.default.frameElement !== null) {
									return _window2.default.frameElement;
								} else {
									parentEl = el.parentElement;
								}
							} catch (e) {
								parentEl = el.parentElement;
							}

							if (parentEl && dom.visible(parentEl)) {
								return parentEl;
							}
							el = parentEl;
						}

						return null;
					}();
					var parentStyles = parent ? getComputedStyle(parent, null) : getComputedStyle(_document2.default.body, null);

					if (t.node.style.height !== 'none' && t.node.style.height !== t.height) {
						t.node.style.height = 'auto';
					}
					if (t.node.style.maxWidth !== 'none' && t.node.style.maxWidth !== t.width) {
						t.node.style.maxWidth = 'none';
					}

					if (t.node.style.maxHeight !== 'none' && t.node.style.maxHeight !== t.height) {
						t.node.style.maxHeight = 'none';
					}

					if (t.node.currentStyle) {
						if (t.node.currentStyle.height === '100%') {
							t.node.currentStyle.height = 'auto';
						}
						if (t.node.currentStyle.maxWidth === '100%') {
							t.node.currentStyle.maxWidth = 'none';
						}
						if (t.node.currentStyle.maxHeight === '100%') {
							t.node.currentStyle.maxHeight = 'none';
						}
					}

					if (!isIframe && !parseFloat(parentStyles.width)) {
						parent.style.width = t.media.offsetWidth + 'px';
					}

					if (!isIframe && !parseFloat(parentStyles.height)) {
						parent.style.height = t.media.offsetHeight + 'px';
					}

					parentStyles = getComputedStyle(parent);

					var parentWidth = parseFloat(parentStyles.width),
					    parentHeight = parseFloat(parentStyles.height);

					t.setDimensions('100%', '100%');

					var poster = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'poster>img');
					if (poster) {
						poster.style.display = '';
					}

					var targetElement = t.getElement(t.container).querySelectorAll('object, embed, iframe, video'),
					    initHeight = t.height,
					    initWidth = t.width,
					    scaleX1 = parentWidth,
					    scaleY1 = initHeight * parentWidth / initWidth,
					    scaleX2 = initWidth * parentHeight / initHeight,
					    scaleY2 = parentHeight,
					    bScaleOnWidth = scaleX2 > parentWidth === false,
					    finalWidth = bScaleOnWidth ? Math.floor(scaleX1) : Math.floor(scaleX2),
					    finalHeight = bScaleOnWidth ? Math.floor(scaleY1) : Math.floor(scaleY2),
					    width = bScaleOnWidth ? parentWidth + 'px' : finalWidth + 'px',
					    height = bScaleOnWidth ? finalHeight + 'px' : parentHeight + 'px';

					for (var i = 0, total = targetElement.length; i < total; i++) {
						targetElement[i].style.height = height;
						targetElement[i].style.width = width;
						if (t.media.setSize) {
							t.media.setSize(width, height);
						}

						targetElement[i].style.marginLeft = Math.floor((parentWidth - finalWidth) / 2) + 'px';
						targetElement[i].style.marginTop = 0;
					}
				}
			}, {
				key: 'setDimensions',
				value: function setDimensions(width, height) {
					var t = this;

					width = (0, _general.isString)(width) && width.indexOf('%') > -1 ? width : parseFloat(width) + 'px';
					height = (0, _general.isString)(height) && height.indexOf('%') > -1 ? height : parseFloat(height) + 'px';

					t.getElement(t.container).style.width = width;
					t.getElement(t.container).style.height = height;

					var layers = t.getElement(t.layers).children;
					for (var i = 0, total = layers.length; i < total; i++) {
						layers[i].style.width = width;
						layers[i].style.height = height;
					}
				}
			}, {
				key: 'setControlsSize',
				value: function setControlsSize() {
					var t = this;

					if (!dom.visible(t.getElement(t.container))) {
						return;
					}

					if (t.rail && dom.visible(t.rail)) {
						var totalStyles = t.total ? getComputedStyle(t.total, null) : null,
						    totalMargin = totalStyles ? parseFloat(totalStyles.marginLeft) + parseFloat(totalStyles.marginRight) : 0,
						    railStyles = getComputedStyle(t.rail),
						    railMargin = parseFloat(railStyles.marginLeft) + parseFloat(railStyles.marginRight);

						var siblingsWidth = 0;

						var siblings = dom.siblings(t.rail, function (el) {
							return el !== t.rail;
						}),
						    total = siblings.length;
						for (var i = 0; i < total; i++) {
							siblingsWidth += siblings[i].offsetWidth;
						}

						siblingsWidth += totalMargin + (totalMargin === 0 ? railMargin * 2 : railMargin) + 1;

						t.getElement(t.container).style.minWidth = siblingsWidth + 'px';

						var event = (0, _general.createEvent)('controlsresize', t.getElement(t.container));
						t.getElement(t.container).dispatchEvent(event);
					} else {
						var children = t.getElement(t.controls).children;
						var minWidth = 0;

						for (var _i = 0, _total = children.length; _i < _total; _i++) {
							minWidth += children[_i].offsetWidth;
						}

						t.getElement(t.container).style.minWidth = minWidth + 'px';
					}
				}
			}, {
				key: 'addControlElement',
				value: function addControlElement(element, key) {

					var t = this;

					if (t.featurePosition[key] !== undefined) {
						var child = t.getElement(t.controls).children[t.featurePosition[key] - 1];
						child.parentNode.insertBefore(element, child.nextSibling);
					} else {
						t.getElement(t.controls).appendChild(element);
						var children = t.getElement(t.controls).children;
						for (var i = 0, total = children.length; i < total; i++) {
							if (element === children[i]) {
								t.featurePosition[key] = i;
								break;
							}
						}
					}
				}
			}, {
				key: 'createIframeLayer',
				value: function createIframeLayer() {
					var t = this;

					if (t.isVideo && t.media.rendererName !== null && t.media.rendererName.indexOf('iframe') > -1 && !_document2.default.getElementById(t.media.id + '-iframe-overlay')) {

						var layer = _document2.default.createElement('div'),
						    target = _document2.default.getElementById(t.media.id + '_' + t.media.rendererName);

						layer.id = t.media.id + '-iframe-overlay';
						layer.className = t.options.classPrefix + 'iframe-overlay';
						layer.addEventListener('click', function (e) {
							if (t.options.clickToPlayPause) {
								if (t.paused) {
									t.play();
								} else {
									t.pause();
								}

								e.preventDefault();
								e.stopPropagation();
							}
						});

						target.parentNode.insertBefore(layer, target);
					}
				}
			}, {
				key: 'resetSize',
				value: function resetSize() {
					var t = this;

					setTimeout(function () {
						t.setPlayerSize(t.width, t.height);
						t.setControlsSize();
					}, 50);
				}
			}, {
				key: 'setPoster',
				value: function setPoster(url) {
					var t = this;

					if (t.getElement(t.container)) {
						var posterDiv = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'poster');

						if (!posterDiv) {
							posterDiv = _document2.default.createElement('div');
							posterDiv.className = t.options.classPrefix + 'poster ' + t.options.classPrefix + 'layer';
							t.getElement(t.layers).appendChild(posterDiv);
						}

						var posterImg = posterDiv.querySelector('img');

						if (!posterImg && url) {
							posterImg = _document2.default.createElement('img');
							posterImg.className = t.options.classPrefix + 'poster-img';
							posterImg.width = '100%';
							posterImg.height = '100%';
							posterDiv.style.display = '';
							posterDiv.appendChild(posterImg);
						}

						if (url) {
							posterImg.setAttribute('src', url);
							posterDiv.style.backgroundImage = 'url("' + url + '")';
							posterDiv.style.display = '';
						} else if (posterImg) {
							posterDiv.style.backgroundImage = 'none';
							posterDiv.style.display = 'none';
							posterImg.remove();
						} else {
							posterDiv.style.display = 'none';
						}
					} else if (_constants.IS_IPAD && t.options.iPadUseNativeControls || _constants.IS_IPHONE && t.options.iPhoneUseNativeControls || _constants.IS_ANDROID && t.options.AndroidUseNativeControls) {
						t.media.originalNode.poster = url;
					}
				}
			}, {
				key: 'changeSkin',
				value: function changeSkin(className) {
					var t = this;

					t.getElement(t.container).className = t.options.classPrefix + 'container ' + className;
					t.setPlayerSize(t.width, t.height);
					t.setControlsSize();
				}
			}, {
				key: 'globalBind',
				value: function globalBind(events, callback) {
					var t = this,
					    doc = t.node ? t.node.ownerDocument : _document2.default;

					events = (0, _general.splitEvents)(events, t.id);
					if (events.d) {
						var eventList = events.d.split(' ');
						for (var i = 0, total = eventList.length; i < total; i++) {
							eventList[i].split('.').reduce(function (part, e) {
								doc.addEventListener(e, callback, false);
								return e;
							}, '');
						}
					}
					if (events.w) {
						var _eventList = events.w.split(' ');
						for (var _i2 = 0, _total2 = _eventList.length; _i2 < _total2; _i2++) {
							_eventList[_i2].split('.').reduce(function (part, e) {
								_window2.default.addEventListener(e, callback, false);
								return e;
							}, '');
						}
					}
				}
			}, {
				key: 'globalUnbind',
				value: function globalUnbind(events, callback) {
					var t = this,
					    doc = t.node ? t.node.ownerDocument : _document2.default;

					events = (0, _general.splitEvents)(events, t.id);
					if (events.d) {
						var eventList = events.d.split(' ');
						for (var i = 0, total = eventList.length; i < total; i++) {
							eventList[i].split('.').reduce(function (part, e) {
								doc.removeEventListener(e, callback, false);
								return e;
							}, '');
						}
					}
					if (events.w) {
						var _eventList2 = events.w.split(' ');
						for (var _i3 = 0, _total3 = _eventList2.length; _i3 < _total3; _i3++) {
							_eventList2[_i3].split('.').reduce(function (part, e) {
								_window2.default.removeEventListener(e, callback, false);
								return e;
							}, '');
						}
					}
				}
			}, {
				key: 'buildfeatures',
				value: function buildfeatures(player, controls, layers, media) {
					var t = this;

					for (var i = 0, total = t.options.features.length; i < total; i++) {
						var feature = t.options.features[i];
						if (t['build' + feature]) {
							try {
								t['build' + feature](player, controls, layers, media);
							} catch (e) {
								console.error('error building ' + feature, e);
							}
						}
					}
				}
			}, {
				key: 'buildposter',
				value: function buildposter(player, controls, layers, media) {
					var t = this,
					    poster = _document2.default.createElement('div');

					poster.className = t.options.classPrefix + 'poster ' + t.options.classPrefix + 'layer';
					layers.appendChild(poster);

					var posterUrl = media.originalNode.getAttribute('poster');

					if (player.options.poster !== '') {
						if (posterUrl && _constants.IS_IOS) {
							media.originalNode.removeAttribute('poster');
						}
						posterUrl = player.options.poster;
					}

					if (posterUrl) {
						t.setPoster(posterUrl);
					} else if (t.media.renderer !== null && typeof t.media.renderer.getPosterUrl === 'function') {
						t.setPoster(t.media.renderer.getPosterUrl());
					} else {
						poster.style.display = 'none';
					}

					media.addEventListener('play', function () {
						poster.style.display = 'none';
					});

					media.addEventListener('playing', function () {
						poster.style.display = 'none';
					});

					if (player.options.showPosterWhenEnded && player.options.autoRewind) {
						media.addEventListener('ended', function () {
							poster.style.display = '';
						});
					}

					media.addEventListener('error', function () {
						poster.style.display = 'none';
					});

					if (player.options.showPosterWhenPaused) {
						media.addEventListener('pause', function () {
							if (!player.ended) {
								poster.style.display = '';
							}
						});
					}
				}
			}, {
				key: 'buildoverlays',
				value: function buildoverlays(player, controls, layers, media) {

					if (!player.isVideo) {
						return;
					}

					var t = this,
					    loading = _document2.default.createElement('div'),
					    error = _document2.default.createElement('div'),
					    bigPlay = _document2.default.createElement('div');

					loading.style.display = 'none';
					loading.className = t.options.classPrefix + 'overlay ' + t.options.classPrefix + 'layer';
					loading.innerHTML = '<div class="' + t.options.classPrefix + 'overlay-loading">' + ('<span class="' + t.options.classPrefix + 'overlay-loading-bg-img"></span>') + '</div>';
					layers.appendChild(loading);

					error.style.display = 'none';
					error.className = t.options.classPrefix + 'overlay ' + t.options.classPrefix + 'layer';
					error.innerHTML = '<div class="' + t.options.classPrefix + 'overlay-error"></div>';
					layers.appendChild(error);

					bigPlay.className = t.options.classPrefix + 'overlay ' + t.options.classPrefix + 'layer ' + t.options.classPrefix + 'overlay-play';
					bigPlay.innerHTML = '<div class="' + t.options.classPrefix + 'overlay-button" role="button" tabindex="0" ' + ('aria-label="' + _i18n2.default.t('mejs.play') + '" aria-pressed="false"></div>');
					bigPlay.addEventListener('click', function () {
						if (t.options.clickToPlayPause) {

							var button = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'overlay-button'),
							    pressed = button.getAttribute('aria-pressed');

							if (t.paused) {
								t.play();
							} else {
								t.pause();
							}

							button.setAttribute('aria-pressed', !!pressed);
							t.getElement(t.container).focus();
						}
					});

					bigPlay.addEventListener('keydown', function (e) {
						var keyPressed = e.keyCode || e.which || 0;

						if (keyPressed === 13 || _constants.IS_FIREFOX && keyPressed === 32) {
							var event = (0, _general.createEvent)('click', bigPlay);
							bigPlay.dispatchEvent(event);
							return false;
						}
					});

					layers.appendChild(bigPlay);

					if (t.media.rendererName !== null && (/(youtube|facebook)/i.test(t.media.rendererName) && !(t.media.originalNode.getAttribute('poster') || player.options.poster || typeof t.media.renderer.getPosterUrl === 'function' && t.media.renderer.getPosterUrl()) || _constants.IS_STOCK_ANDROID || t.media.originalNode.getAttribute('autoplay'))) {
						bigPlay.style.display = 'none';
					}

					var hasError = false;

					media.addEventListener('play', function () {
						bigPlay.style.display = 'none';
						loading.style.display = 'none';
						error.style.display = 'none';
						hasError = false;
					});
					media.addEventListener('playing', function () {
						bigPlay.style.display = 'none';
						loading.style.display = 'none';
						error.style.display = 'none';
						hasError = false;
					});
					media.addEventListener('seeking', function () {
						bigPlay.style.display = 'none';
						loading.style.display = '';
						hasError = false;
					});
					media.addEventListener('seeked', function () {
						bigPlay.style.display = t.paused && !_constants.IS_STOCK_ANDROID ? '' : 'none';
						loading.style.display = 'none';
						hasError = false;
					});
					media.addEventListener('pause', function () {
						loading.style.display = 'none';
						if (!_constants.IS_STOCK_ANDROID && !hasError) {
							bigPlay.style.display = '';
						}
						hasError = false;
					});
					media.addEventListener('waiting', function () {
						loading.style.display = '';
						hasError = false;
					});

					media.addEventListener('loadeddata', function () {
						loading.style.display = '';

						if (_constants.IS_ANDROID) {
							media.canplayTimeout = setTimeout(function () {
								if (_document2.default.createEvent) {
									var evt = _document2.default.createEvent('HTMLEvents');
									evt.initEvent('canplay', true, true);
									return media.dispatchEvent(evt);
								}
							}, 300);
						}
						hasError = false;
					});
					media.addEventListener('canplay', function () {
						loading.style.display = 'none';

						clearTimeout(media.canplayTimeout);
						hasError = false;
					});

					media.addEventListener('error', function (e) {
						t._handleError(e, t.media, t.node);
						loading.style.display = 'none';
						bigPlay.style.display = 'none';
						hasError = true;
					});

					media.addEventListener('loadedmetadata', function () {
						if (!t.controlsEnabled) {
							t.enableControls();
						}
					});

					media.addEventListener('keydown', function (e) {
						t.onkeydown(player, media, e);
						hasError = false;
					});
				}
			}, {
				key: 'buildkeyboard',
				value: function buildkeyboard(player, controls, layers, media) {

					var t = this;

					t.getElement(t.container).addEventListener('keydown', function () {
						t.keyboardAction = true;
					});

					t.globalKeydownCallback = function (event) {
						var container = _document2.default.activeElement.closest('.' + t.options.classPrefix + 'container'),
						    target = t.media.closest('.' + t.options.classPrefix + 'container');
						t.hasFocus = !!(container && target && container.id === target.id);
						return t.onkeydown(player, media, event);
					};

					t.globalClickCallback = function (event) {
						t.hasFocus = !!event.target.closest('.' + t.options.classPrefix + 'container');
					};

					t.globalBind('keydown', t.globalKeydownCallback);

					t.globalBind('click', t.globalClickCallback);
				}
			}, {
				key: 'onkeydown',
				value: function onkeydown(player, media, e) {

					if (player.hasFocus && player.options.enableKeyboard) {
						for (var i = 0, total = player.options.keyActions.length; i < total; i++) {
							var keyAction = player.options.keyActions[i];

							for (var j = 0, jl = keyAction.keys.length; j < jl; j++) {
								if (e.keyCode === keyAction.keys[j]) {
									keyAction.action(player, media, e.keyCode, e);
									e.preventDefault();
									e.stopPropagation();
									return;
								}
							}
						}
					}

					return true;
				}
			}, {
				key: 'play',
				value: function play() {
					this.proxy.play();
				}
			}, {
				key: 'pause',
				value: function pause() {
					this.proxy.pause();
				}
			}, {
				key: 'load',
				value: function load() {
					this.proxy.load();
				}
			}, {
				key: 'setCurrentTime',
				value: function setCurrentTime(time) {
					this.proxy.setCurrentTime(time);
				}
			}, {
				key: 'getCurrentTime',
				value: function getCurrentTime() {
					return this.proxy.currentTime;
				}
			}, {
				key: 'getDuration',
				value: function getDuration() {
					return this.proxy.duration;
				}
			}, {
				key: 'setVolume',
				value: function setVolume(volume) {
					this.proxy.volume = volume;
				}
			}, {
				key: 'getVolume',
				value: function getVolume() {
					return this.proxy.getVolume();
				}
			}, {
				key: 'setMuted',
				value: function setMuted(value) {
					this.proxy.setMuted(value);
				}
			}, {
				key: 'setSrc',
				value: function setSrc(src) {
					if (!this.controlsEnabled) {
						this.enableControls();
					}
					this.proxy.setSrc(src);
				}
			}, {
				key: 'getSrc',
				value: function getSrc() {
					return this.proxy.getSrc();
				}
			}, {
				key: 'canPlayType',
				value: function canPlayType(type) {
					return this.proxy.canPlayType(type);
				}
			}, {
				key: 'remove',
				value: function remove() {
					var t = this,
					    rendererName = t.media.rendererName,
					    src = t.media.originalNode.src;

					for (var featureIndex in t.options.features) {
						var feature = t.options.features[featureIndex];
						if (t['clean' + feature]) {
							try {
								t['clean' + feature](t, t.getElement(t.layers), t.getElement(t.controls), t.media);
							} catch (e) {
								console.error('error cleaning ' + feature, e);
							}
						}
					}

					var nativeWidth = t.node.getAttribute('width'),
					    nativeHeight = t.node.getAttribute('height');

					if (nativeWidth) {
						if (nativeWidth.indexOf('%') === -1) {
							nativeWidth = nativeWidth + 'px';
						}
					} else {
						nativeWidth = 'auto';
					}

					if (nativeHeight) {
						if (nativeHeight.indexOf('%') === -1) {
							nativeHeight = nativeHeight + 'px';
						}
					} else {
						nativeHeight = 'auto';
					}

					t.node.style.width = nativeWidth;
					t.node.style.height = nativeHeight;

					t.setPlayerSize(0, 0);

					if (!t.isDynamic) {
						(function () {
							t.node.setAttribute('controls', true);
							t.node.setAttribute('id', t.node.getAttribute('id').replace('_' + rendererName, '').replace('_from_mejs', ''));
							var poster = t.getElement(t.container).querySelector('.' + t.options.classPrefix + 'poster>img');
							if (poster) {
								t.node.setAttribute('poster', poster.src);
							}

							delete t.node.autoplay;

							t.node.setAttribute('src', '');
							if (t.media.canPlayType((0, _media.getTypeFromFile)(src)) !== '') {
								t.node.setAttribute('src', src);
							}

							if (rendererName && rendererName.indexOf('iframe') > -1) {
								var layer = _document2.default.getElementById(t.media.id + '-iframe-overlay');
								layer.remove();
							}

							var node = t.node.cloneNode();
							node.style.display = '';
							t.getElement(t.container).parentNode.insertBefore(node, t.getElement(t.container));
							t.node.remove();

							if (t.mediaFiles) {
								for (var i = 0, total = t.mediaFiles.length; i < total; i++) {
									var source = _document2.default.createElement('source');
									source.setAttribute('src', t.mediaFiles[i].src);
									source.setAttribute('type', t.mediaFiles[i].type);
									node.appendChild(source);
								}
							}
							if (t.trackFiles) {
								var _loop3 = function _loop3(_i4, _total4) {
									var track = t.trackFiles[_i4];
									var newTrack = _document2.default.createElement('track');
									newTrack.kind = track.kind;
									newTrack.label = track.label;
									newTrack.srclang = track.srclang;
									newTrack.src = track.src;

									node.appendChild(newTrack);
									newTrack.addEventListener('load', function () {
										this.mode = 'showing';
										node.textTracks[_i4].mode = 'showing';
									});
								};

								for (var _i4 = 0, _total4 = t.trackFiles.length; _i4 < _total4; _i4++) {
									_loop3(_i4, _total4);
								}
							}

							delete t.node;
							delete t.mediaFiles;
							delete t.trackFiles;
						})();
					} else {
						t.getElement(t.container).parentNode.insertBefore(t.node, t.getElement(t.container));
					}

					if (t.media.renderer && typeof t.media.renderer.destroy === 'function') {
						t.media.renderer.destroy();
					}

					delete _mejs2.default.players[t.id];

					if (_typeof(t.getElement(t.container)) === 'object') {
						var offscreen = t.getElement(t.container).parentNode.querySelector('.' + t.options.classPrefix + 'offscreen');
						offscreen.remove();
						t.getElement(t.container).remove();
					}
					t.globalUnbind('resize', t.globalResizeCallback);
					t.globalUnbind('keydown', t.globalKeydownCallback);
					t.globalUnbind('click', t.globalClickCallback);

					delete t.media.player;
				}
			}, {
				key: 'paused',
				get: function get() {
					return this.proxy.paused;
				}
			}, {
				key: 'muted',
				get: function get() {
					return this.proxy.muted;
				},
				set: function set(muted) {
					this.setMuted(muted);
				}
			}, {
				key: 'ended',
				get: function get() {
					return this.proxy.ended;
				}
			}, {
				key: 'readyState',
				get: function get() {
					return this.proxy.readyState;
				}
			}, {
				key: 'currentTime',
				set: function set(time) {
					this.setCurrentTime(time);
				},
				get: function get() {
					return this.getCurrentTime();
				}
			}, {
				key: 'duration',
				get: function get() {
					return this.getDuration();
				}
			}, {
				key: 'volume',
				set: function set(volume) {
					this.setVolume(volume);
				},
				get: function get() {
					return this.getVolume();
				}
			}, {
				key: 'src',
				set: function set(src) {
					this.setSrc(src);
				},
				get: function get() {
					return this.getSrc();
				}
			}]);

			return MediaElementPlayer;
		}();

		_window2.default.MediaElementPlayer = MediaElementPlayer;
		_mejs2.default.MediaElementPlayer = MediaElementPlayer;

		exports.default = MediaElementPlayer;
	}, { "17": 17, "2": 2, "25": 25, "26": 26, "27": 27, "28": 28, "3": 3, "30": 30, "5": 5, "6": 6, "7": 7 }], 17: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});

		var _createClass = function () {
			function defineProperties(target, props) {
				for (var i = 0; i < props.length; i++) {
					var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
				}
			}return function (Constructor, protoProps, staticProps) {
				if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
			};
		}();

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		function _classCallCheck(instance, Constructor) {
			if (!(instance instanceof Constructor)) {
				throw new TypeError("Cannot call a class as a function");
			}
		}

		var DefaultPlayer = function () {
			function DefaultPlayer(player) {
				_classCallCheck(this, DefaultPlayer);

				this.media = player.media;
				this.isVideo = player.isVideo;
				this.classPrefix = player.options.classPrefix;
				this.createIframeLayer = function () {
					return player.createIframeLayer();
				};
				this.setPoster = function (url) {
					return player.setPoster(url);
				};
				return this;
			}

			_createClass(DefaultPlayer, [{
				key: 'play',
				value: function play() {
					this.media.play();
				}
			}, {
				key: 'pause',
				value: function pause() {
					this.media.pause();
				}
			}, {
				key: 'load',
				value: function load() {
					var t = this;

					if (!t.isLoaded) {
						t.media.load();
					}

					t.isLoaded = true;
				}
			}, {
				key: 'setCurrentTime',
				value: function setCurrentTime(time) {
					this.media.setCurrentTime(time);
				}
			}, {
				key: 'getCurrentTime',
				value: function getCurrentTime() {
					return this.media.currentTime;
				}
			}, {
				key: 'getDuration',
				value: function getDuration() {
					return this.media.getDuration();
				}
			}, {
				key: 'setVolume',
				value: function setVolume(volume) {
					this.media.setVolume(volume);
				}
			}, {
				key: 'getVolume',
				value: function getVolume() {
					return this.media.getVolume();
				}
			}, {
				key: 'setMuted',
				value: function setMuted(value) {
					this.media.setMuted(value);
				}
			}, {
				key: 'setSrc',
				value: function setSrc(src) {
					var t = this,
					    layer = document.getElementById(t.media.id + '-iframe-overlay');

					if (layer) {
						layer.remove();
					}

					t.media.setSrc(src);
					t.createIframeLayer();
					if (t.media.renderer !== null && typeof t.media.renderer.getPosterUrl === 'function') {
						t.setPoster(t.media.renderer.getPosterUrl());
					}
				}
			}, {
				key: 'getSrc',
				value: function getSrc() {
					return this.media.getSrc();
				}
			}, {
				key: 'canPlayType',
				value: function canPlayType(type) {
					return this.media.canPlayType(type);
				}
			}, {
				key: 'paused',
				get: function get() {
					return this.media.paused;
				}
			}, {
				key: 'muted',
				set: function set(muted) {
					this.setMuted(muted);
				},
				get: function get() {
					return this.media.muted;
				}
			}, {
				key: 'ended',
				get: function get() {
					return this.media.ended;
				}
			}, {
				key: 'readyState',
				get: function get() {
					return this.media.readyState;
				}
			}, {
				key: 'currentTime',
				set: function set(time) {
					this.setCurrentTime(time);
				},
				get: function get() {
					return this.getCurrentTime();
				}
			}, {
				key: 'duration',
				get: function get() {
					return this.getDuration();
				}
			}, {
				key: 'remainingTime',
				get: function get() {
					return this.getDuration() - this.currentTime();
				}
			}, {
				key: 'volume',
				set: function set(volume) {
					this.setVolume(volume);
				},
				get: function get() {
					return this.getVolume();
				}
			}, {
				key: 'src',
				set: function set(src) {
					this.setSrc(src);
				},
				get: function get() {
					return this.getSrc();
				}
			}]);

			return DefaultPlayer;
		}();

		exports.default = DefaultPlayer;

		_window2.default.DefaultPlayer = DefaultPlayer;
	}, { "3": 3 }], 18: [function (_dereq_, module, exports) {
		'use strict';

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _player = _dereq_(16);

		var _player2 = _interopRequireDefault(_player);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		if (typeof jQuery !== 'undefined') {
			_mejs2.default.$ = _window2.default.jQuery = _window2.default.$ = jQuery;
		} else if (typeof Zepto !== 'undefined') {
			_mejs2.default.$ = _window2.default.Zepto = _window2.default.$ = Zepto;
		} else if (typeof ender !== 'undefined') {
			_mejs2.default.$ = _window2.default.ender = _window2.default.$ = ender;
		}

		(function ($) {
			if (typeof $ !== 'undefined') {
				$.fn.mediaelementplayer = function (options) {
					if (options === false) {
						this.each(function () {
							var player = $(this).data('mediaelementplayer');
							if (player) {
								player.remove();
							}
							$(this).removeData('mediaelementplayer');
						});
					} else {
						this.each(function () {
							$(this).data('mediaelementplayer', new _player2.default(this, options));
						});
					}
					return this;
				};

				$(document).ready(function () {
					$('.' + _mejs2.default.MepDefaults.classPrefix + 'player').mediaelementplayer();
				});
			}
		})(_mejs2.default.$);
	}, { "16": 16, "3": 3, "7": 7 }], 19: [function (_dereq_, module, exports) {
		'use strict';

		var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
			return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		} : function (obj) {
			return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		};

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _renderer = _dereq_(8);

		var _general = _dereq_(27);

		var _media = _dereq_(28);

		var _constants = _dereq_(25);

		var _dom = _dereq_(26);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		var NativeDash = {

			promise: null,

			load: function load(settings) {
				if (typeof dashjs !== 'undefined') {
					NativeDash.promise = new Promise(function (resolve) {
						resolve();
					}).then(function () {
						NativeDash._createPlayer(settings);
					});
				} else {
					settings.options.path = typeof settings.options.path === 'string' ? settings.options.path : 'https://cdn.dashjs.org/latest/dash.all.min.js';

					NativeDash.promise = NativeDash.promise || (0, _dom.loadScript)(settings.options.path);
					NativeDash.promise.then(function () {
						NativeDash._createPlayer(settings);
					});
				}

				return NativeDash.promise;
			},

			_createPlayer: function _createPlayer(settings) {
				var player = dashjs.MediaPlayer().create();
				_window2.default['__ready__' + settings.id](player);
				return player;
			}
		};

		var DashNativeRenderer = {
			name: 'native_dash',
			options: {
				prefix: 'native_dash',
				dash: {
					path: 'https://cdn.dashjs.org/latest/dash.all.min.js',
					debug: false,
					drm: {},

					robustnessLevel: ''
				}
			},

			canPlayType: function canPlayType(type) {
				return _constants.HAS_MSE && ['application/dash+xml'].indexOf(type.toLowerCase()) > -1;
			},

			create: function create(mediaElement, options, mediaFiles) {

				var originalNode = mediaElement.originalNode,
				    id = mediaElement.id + '_' + options.prefix,
				    autoplay = originalNode.autoplay,
				    children = originalNode.children;

				var node = null,
				    dashPlayer = null;

				originalNode.removeAttribute('type');
				for (var i = 0, total = children.length; i < total; i++) {
					children[i].removeAttribute('type');
				}

				node = originalNode.cloneNode(true);
				options = Object.assign(options, mediaElement.options);

				var props = _mejs2.default.html5media.properties,
				    events = _mejs2.default.html5media.events.concat(['click', 'mouseover', 'mouseout']).filter(function (e) {
					return e !== 'error';
				}),
				    attachNativeEvents = function attachNativeEvents(e) {
					var event = (0, _general.createEvent)(e.type, mediaElement);
					mediaElement.dispatchEvent(event);
				},
				    assignGettersSetters = function assignGettersSetters(propName) {
					var capName = '' + propName.substring(0, 1).toUpperCase() + propName.substring(1);

					node['get' + capName] = function () {
						return dashPlayer !== null ? node[propName] : null;
					};

					node['set' + capName] = function (value) {
						if (_mejs2.default.html5media.readOnlyProperties.indexOf(propName) === -1) {
							if (propName === 'src') {
								var source = (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value.src ? value.src : value;
								node[propName] = source;
								if (dashPlayer !== null) {
									dashPlayer.reset();
									for (var _i = 0, _total = events.length; _i < _total; _i++) {
										node.removeEventListener(events[_i], attachNativeEvents);
									}
									dashPlayer = NativeDash._createPlayer({
										options: options.dash,
										id: id
									});

									if (value && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && _typeof(value.drm) === 'object') {
										dashPlayer.setProtectionData(value.drm);
										if ((0, _general.isString)(options.dash.robustnessLevel) && options.dash.robustnessLevel) {
											dashPlayer.getProtectionController().setRobustnessLevel(options.dash.robustnessLevel);
										}
									}
									dashPlayer.attachSource(source);
									if (autoplay) {
										dashPlayer.play();
									}
								}
							} else {
								node[propName] = value;
							}
						}
					};
				};

				for (var _i2 = 0, _total2 = props.length; _i2 < _total2; _i2++) {
					assignGettersSetters(props[_i2]);
				}

				_window2.default['__ready__' + id] = function (_dashPlayer) {
					mediaElement.dashPlayer = dashPlayer = _dashPlayer;

					var dashEvents = dashjs.MediaPlayer.events,
					    assignEvents = function assignEvents(eventName) {
						if (eventName === 'loadedmetadata') {
							dashPlayer.getDebug().setLogToBrowserConsole(options.dash.debug);
							dashPlayer.initialize();
							dashPlayer.setScheduleWhilePaused(false);
							dashPlayer.setFastSwitchEnabled(true);
							dashPlayer.attachView(node);
							dashPlayer.setAutoPlay(false);

							if (_typeof(options.dash.drm) === 'object' && !_mejs2.default.Utils.isObjectEmpty(options.dash.drm)) {
								dashPlayer.setProtectionData(options.dash.drm);
								if ((0, _general.isString)(options.dash.robustnessLevel) && options.dash.robustnessLevel) {
									dashPlayer.getProtectionController().setRobustnessLevel(options.dash.robustnessLevel);
								}
							}
							dashPlayer.attachSource(node.getSrc());
						}

						node.addEventListener(eventName, attachNativeEvents);
					};

					for (var _i3 = 0, _total3 = events.length; _i3 < _total3; _i3++) {
						assignEvents(events[_i3]);
					}

					var assignMdashEvents = function assignMdashEvents(e) {
						if (e.type.toLowerCase() === 'error') {
							mediaElement.generateError(e.message, node.src);
							console.error(e);
						} else {
							var _event = (0, _general.createEvent)(e.type, mediaElement);
							_event.data = e;
							mediaElement.dispatchEvent(_event);
						}
					};

					for (var eventType in dashEvents) {
						if (dashEvents.hasOwnProperty(eventType)) {
							dashPlayer.on(dashEvents[eventType], function (e) {
								return assignMdashEvents(e);
							});
						}
					}
				};

				if (mediaFiles && mediaFiles.length > 0) {
					for (var _i4 = 0, _total4 = mediaFiles.length; _i4 < _total4; _i4++) {
						if (_renderer.renderer.renderers[options.prefix].canPlayType(mediaFiles[_i4].type)) {
							node.setAttribute('src', mediaFiles[_i4].src);
							if (typeof mediaFiles[_i4].drm !== 'undefined') {
								options.dash.drm = mediaFiles[_i4].drm;
							}
							break;
						}
					}
				}

				node.setAttribute('id', id);

				originalNode.parentNode.insertBefore(node, originalNode);
				originalNode.autoplay = false;
				originalNode.style.display = 'none';

				node.setSize = function (width, height) {
					node.style.width = width + 'px';
					node.style.height = height + 'px';
					return node;
				};

				node.hide = function () {
					node.pause();
					node.style.display = 'none';
					return node;
				};

				node.show = function () {
					node.style.display = '';
					return node;
				};

				node.destroy = function () {
					if (dashPlayer !== null) {
						dashPlayer.reset();
					}
				};

				var event = (0, _general.createEvent)('rendererready', node);
				mediaElement.dispatchEvent(event);

				mediaElement.promises.push(NativeDash.load({
					options: options.dash,
					id: id
				}));

				return node;
			}
		};

		_media.typeChecks.push(function (url) {
			return ~url.toLowerCase().indexOf('.mpd') ? 'application/dash+xml' : null;
		});

		_renderer.renderer.add(DashNativeRenderer);
	}, { "25": 25, "26": 26, "27": 27, "28": 28, "3": 3, "7": 7, "8": 8 }], 20: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});
		exports.PluginDetector = undefined;

		var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
			return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		} : function (obj) {
			return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		};

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _i18n = _dereq_(5);

		var _i18n2 = _interopRequireDefault(_i18n);

		var _renderer = _dereq_(8);

		var _general = _dereq_(27);

		var _constants = _dereq_(25);

		var _media = _dereq_(28);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		var PluginDetector = exports.PluginDetector = {
			plugins: [],

			hasPluginVersion: function hasPluginVersion(plugin, v) {
				var pv = PluginDetector.plugins[plugin];
				v[1] = v[1] || 0;
				v[2] = v[2] || 0;
				return pv[0] > v[0] || pv[0] === v[0] && pv[1] > v[1] || pv[0] === v[0] && pv[1] === v[1] && pv[2] >= v[2];
			},

			addPlugin: function addPlugin(p, pluginName, mimeType, activeX, axDetect) {
				PluginDetector.plugins[p] = PluginDetector.detectPlugin(pluginName, mimeType, activeX, axDetect);
			},

			detectPlugin: function detectPlugin(pluginName, mimeType, activeX, axDetect) {

				var version = [0, 0, 0],
				    description = void 0,
				    ax = void 0;

				if (_constants.NAV.plugins !== null && _constants.NAV.plugins !== undefined && _typeof(_constants.NAV.plugins[pluginName]) === 'object') {
					description = _constants.NAV.plugins[pluginName].description;
					if (description && !(typeof _constants.NAV.mimeTypes !== 'undefined' && _constants.NAV.mimeTypes[mimeType] && !_constants.NAV.mimeTypes[mimeType].enabledPlugin)) {
						version = description.replace(pluginName, '').replace(/^\s+/, '').replace(/\sr/gi, '.').split('.');
						for (var i = 0, total = version.length; i < total; i++) {
							version[i] = parseInt(version[i].match(/\d+/), 10);
						}
					}
				} else if (_window2.default.ActiveXObject !== undefined) {
					try {
						ax = new ActiveXObject(activeX);
						if (ax) {
							version = axDetect(ax);
						}
					} catch (e) {}
				}
				return version;
			}
		};

		PluginDetector.addPlugin('flash', 'Shockwave Flash', 'application/x-shockwave-flash', 'ShockwaveFlash.ShockwaveFlash', function (ax) {
			var version = [],
			    d = ax.GetVariable("$version");

			if (d) {
				d = d.split(" ")[1].split(",");
				version = [parseInt(d[0], 10), parseInt(d[1], 10), parseInt(d[2], 10)];
			}
			return version;
		});

		var FlashMediaElementRenderer = {
			create: function create(mediaElement, options, mediaFiles) {

				var flash = {};
				var isActive = false;

				flash.options = options;
				flash.id = mediaElement.id + '_' + flash.options.prefix;
				flash.mediaElement = mediaElement;
				flash.flashState = {};
				flash.flashApi = null;
				flash.flashApiStack = [];

				var props = _mejs2.default.html5media.properties,
				    assignGettersSetters = function assignGettersSetters(propName) {
					flash.flashState[propName] = null;

					var capName = '' + propName.substring(0, 1).toUpperCase() + propName.substring(1);

					flash['get' + capName] = function () {
						if (flash.flashApi !== null) {
							if (typeof flash.flashApi['get_' + propName] === 'function') {
								var value = flash.flashApi['get_' + propName]();

								if (propName === 'buffered') {
									return {
										start: function start() {
											return 0;
										},
										end: function end() {
											return value;
										},
										length: 1
									};
								}
								return value;
							} else {
								return null;
							}
						} else {
							return null;
						}
					};

					flash['set' + capName] = function (value) {
						if (propName === 'src') {
							value = (0, _media.absolutizeUrl)(value);
						}

						if (flash.flashApi !== null && flash.flashApi['set_' + propName] !== undefined) {
							try {
								flash.flashApi['set_' + propName](value);
							} catch (e) {}
						} else {
							flash.flashApiStack.push({
								type: 'set',
								propName: propName,
								value: value
							});
						}
					};
				};

				for (var i = 0, total = props.length; i < total; i++) {
					assignGettersSetters(props[i]);
				}

				var methods = _mejs2.default.html5media.methods,
				    assignMethods = function assignMethods(methodName) {
					flash[methodName] = function () {
						if (isActive) {
							if (flash.flashApi !== null) {
								if (flash.flashApi['fire_' + methodName]) {
									try {
										flash.flashApi['fire_' + methodName]();
									} catch (e) {}
								} else {}
							} else {
								flash.flashApiStack.push({
									type: 'call',
									methodName: methodName
								});
							}
						}
					};
				};
				methods.push('stop');
				for (var _i = 0, _total = methods.length; _i < _total; _i++) {
					assignMethods(methods[_i]);
				}

				var initEvents = ['rendererready'];

				for (var _i2 = 0, _total2 = initEvents.length; _i2 < _total2; _i2++) {
					var event = (0, _general.createEvent)(initEvents[_i2], flash);
					mediaElement.dispatchEvent(event);
				}

				_window2.default['__ready__' + flash.id] = function () {

					flash.flashReady = true;
					flash.flashApi = _document2.default.getElementById('__' + flash.id);

					if (flash.flashApiStack.length) {
						for (var _i3 = 0, _total3 = flash.flashApiStack.length; _i3 < _total3; _i3++) {
							var stackItem = flash.flashApiStack[_i3];

							if (stackItem.type === 'set') {
								var propName = stackItem.propName,
								    capName = '' + propName.substring(0, 1).toUpperCase() + propName.substring(1);

								flash['set' + capName](stackItem.value);
							} else if (stackItem.type === 'call') {
								flash[stackItem.methodName]();
							}
						}
					}
				};

				_window2.default['__event__' + flash.id] = function (eventName, message) {
					var event = (0, _general.createEvent)(eventName, flash);
					if (message) {
						try {
							event.data = JSON.parse(message);
							event.details.data = JSON.parse(message);
						} catch (e) {
							event.message = message;
						}
					}

					flash.mediaElement.dispatchEvent(event);
				};

				flash.flashWrapper = _document2.default.createElement('div');

				if (['always', 'sameDomain'].indexOf(flash.options.shimScriptAccess) === -1) {
					flash.options.shimScriptAccess = 'sameDomain';
				}

				var autoplay = mediaElement.originalNode.autoplay,
				    flashVars = ['uid=' + flash.id, 'autoplay=' + autoplay, 'allowScriptAccess=' + flash.options.shimScriptAccess, 'preload=' + (mediaElement.originalNode.getAttribute('preload') || '')],
				    isVideo = mediaElement.originalNode !== null && mediaElement.originalNode.tagName.toLowerCase() === 'video',
				    flashHeight = isVideo ? mediaElement.originalNode.height : 1,
				    flashWidth = isVideo ? mediaElement.originalNode.width : 1;

				if (mediaElement.originalNode.getAttribute('src')) {
					flashVars.push('src=' + mediaElement.originalNode.getAttribute('src'));
				}

				if (flash.options.enablePseudoStreaming === true) {
					flashVars.push('pseudostreamstart=' + flash.options.pseudoStreamingStartQueryParam);
					flashVars.push('pseudostreamtype=' + flash.options.pseudoStreamingType);
				}

				if (flash.options.streamDelimiter) {
					flashVars.push('streamdelimiter=' + encodeURIComponent(flash.options.streamDelimiter));
				}

				if (flash.options.proxyType) {
					flashVars.push('proxytype=' + flash.options.proxyType);
				}

				mediaElement.appendChild(flash.flashWrapper);
				mediaElement.originalNode.style.display = 'none';

				var settings = [];

				if (_constants.IS_IE || _constants.IS_EDGE) {
					var specialIEContainer = _document2.default.createElement('div');
					flash.flashWrapper.appendChild(specialIEContainer);

					if (_constants.IS_EDGE) {
						settings = ['type="application/x-shockwave-flash"', 'data="' + flash.options.pluginPath + flash.options.filename + '"', 'id="__' + flash.id + '"', 'width="' + flashWidth + '"', 'height="' + flashHeight + '\'"'];
					} else {
						settings = ['classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"', 'codebase="//download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab"', 'id="__' + flash.id + '"', 'width="' + flashWidth + '"', 'height="' + flashHeight + '"'];
					}

					if (!isVideo) {
						settings.push('style="clip: rect(0 0 0 0); position: absolute;"');
					}

					specialIEContainer.outerHTML = '<object ' + settings.join(' ') + '>' + ('<param name="movie" value="' + flash.options.pluginPath + flash.options.filename + '?x=' + new Date() + '" />') + ('<param name="flashvars" value="' + flashVars.join('&amp;') + '" />') + '<param name="quality" value="high" />' + '<param name="bgcolor" value="#000000" />' + '<param name="wmode" value="transparent" />' + ('<param name="allowScriptAccess" value="' + flash.options.shimScriptAccess + '" />') + '<param name="allowFullScreen" value="true" />' + ('<div>' + _i18n2.default.t('mejs.install-flash') + '</div>') + '</object>';
				} else {

					settings = ['id="__' + flash.id + '"', 'name="__' + flash.id + '"', 'play="true"', 'loop="false"', 'quality="high"', 'bgcolor="#000000"', 'wmode="transparent"', 'allowScriptAccess="' + flash.options.shimScriptAccess + '"', 'allowFullScreen="true"', 'type="application/x-shockwave-flash"', 'pluginspage="//www.macromedia.com/go/getflashplayer"', 'src="' + flash.options.pluginPath + flash.options.filename + '"', 'flashvars="' + flashVars.join('&') + '"'];

					if (isVideo) {
						settings.push('width="' + flashWidth + '"');
						settings.push('height="' + flashHeight + '"');
					} else {
						settings.push('style="position: fixed; left: -9999em; top: -9999em;"');
					}

					flash.flashWrapper.innerHTML = '<embed ' + settings.join(' ') + '>';
				}

				flash.flashNode = flash.flashWrapper.lastChild;

				flash.hide = function () {
					isActive = false;
					if (isVideo) {
						flash.flashNode.style.display = 'none';
					}
				};
				flash.show = function () {
					isActive = true;
					if (isVideo) {
						flash.flashNode.style.display = '';
					}
				};
				flash.setSize = function (width, height) {
					flash.flashNode.style.width = width + 'px';
					flash.flashNode.style.height = height + 'px';

					if (flash.flashApi !== null && typeof flash.flashApi.fire_setSize === 'function') {
						flash.flashApi.fire_setSize(width, height);
					}
				};

				flash.destroy = function () {
					flash.flashNode.remove();
				};

				if (mediaFiles && mediaFiles.length > 0) {
					for (var _i4 = 0, _total4 = mediaFiles.length; _i4 < _total4; _i4++) {
						if (_renderer.renderer.renderers[options.prefix].canPlayType(mediaFiles[_i4].type)) {
							flash.setSrc(mediaFiles[_i4].src);
							break;
						}
					}
				}

				return flash;
			}
		};

		var hasFlash = PluginDetector.hasPluginVersion('flash', [10, 0, 0]);

		if (hasFlash) {
			_media.typeChecks.push(function (url) {
				url = url.toLowerCase();

				if (url.startsWith('rtmp')) {
					if (~url.indexOf('.mp3')) {
						return 'audio/rtmp';
					} else {
						return 'video/rtmp';
					}
				} else if (/\.og(a|g)/i.test(url)) {
					return 'audio/ogg';
				} else if (~url.indexOf('.m3u8')) {
					return 'application/x-mpegURL';
				} else if (~url.indexOf('.mpd')) {
					return 'application/dash+xml';
				} else if (~url.indexOf('.flv')) {
					return 'video/flv';
				} else {
					return null;
				}
			});

			var FlashMediaElementVideoRenderer = {
				name: 'flash_video',
				options: {
					prefix: 'flash_video',
					filename: 'mediaelement-flash-video.swf',
					enablePseudoStreaming: false,

					pseudoStreamingStartQueryParam: 'start',

					pseudoStreamingType: 'byte',

					proxyType: '',

					streamDelimiter: ''
				},

				canPlayType: function canPlayType(type) {
					return ~['video/mp4', 'video/rtmp', 'audio/rtmp', 'rtmp/mp4', 'audio/mp4', 'video/flv', 'video/x-flv'].indexOf(type.toLowerCase());
				},

				create: FlashMediaElementRenderer.create

			};
			_renderer.renderer.add(FlashMediaElementVideoRenderer);

			var FlashMediaElementHlsVideoRenderer = {
				name: 'flash_hls',
				options: {
					prefix: 'flash_hls',
					filename: 'mediaelement-flash-video-hls.swf'
				},

				canPlayType: function canPlayType(type) {
					return ~['application/x-mpegurl', 'application/vnd.apple.mpegurl', 'audio/mpegurl', 'audio/hls', 'video/hls'].indexOf(type.toLowerCase());
				},

				create: FlashMediaElementRenderer.create
			};
			_renderer.renderer.add(FlashMediaElementHlsVideoRenderer);

			var FlashMediaElementMdashVideoRenderer = {
				name: 'flash_dash',
				options: {
					prefix: 'flash_dash',
					filename: 'mediaelement-flash-video-mdash.swf'
				},

				canPlayType: function canPlayType(type) {
					return ~['application/dash+xml'].indexOf(type.toLowerCase());
				},

				create: FlashMediaElementRenderer.create
			};
			_renderer.renderer.add(FlashMediaElementMdashVideoRenderer);

			var FlashMediaElementAudioRenderer = {
				name: 'flash_audio',
				options: {
					prefix: 'flash_audio',
					filename: 'mediaelement-flash-audio.swf'
				},

				canPlayType: function canPlayType(type) {
					return ~['audio/mp3'].indexOf(type.toLowerCase());
				},

				create: FlashMediaElementRenderer.create
			};
			_renderer.renderer.add(FlashMediaElementAudioRenderer);

			var FlashMediaElementAudioOggRenderer = {
				name: 'flash_audio_ogg',
				options: {
					prefix: 'flash_audio_ogg',
					filename: 'mediaelement-flash-audio-ogg.swf'
				},

				canPlayType: function canPlayType(type) {
					return ~['audio/ogg', 'audio/oga', 'audio/ogv'].indexOf(type.toLowerCase());
				},

				create: FlashMediaElementRenderer.create
			};
			_renderer.renderer.add(FlashMediaElementAudioOggRenderer);
		}
	}, { "2": 2, "25": 25, "27": 27, "28": 28, "3": 3, "5": 5, "7": 7, "8": 8 }], 21: [function (_dereq_, module, exports) {
		'use strict';

		var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
			return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		} : function (obj) {
			return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		};

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _renderer = _dereq_(8);

		var _general = _dereq_(27);

		var _constants = _dereq_(25);

		var _media = _dereq_(28);

		var _dom = _dereq_(26);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		var NativeFlv = {

			promise: null,

			load: function load(settings) {
				if (typeof flvjs !== 'undefined') {
					NativeFlv.promise = new Promise(function (resolve) {
						resolve();
					}).then(function () {
						NativeFlv._createPlayer(settings);
					});
				} else {
					settings.options.path = typeof settings.options.path === 'string' ? settings.options.path : 'https://cdn.jsdelivr.net/npm/flv.js@latest';

					NativeFlv.promise = NativeFlv.promise || (0, _dom.loadScript)(settings.options.path);
					NativeFlv.promise.then(function () {
						NativeFlv._createPlayer(settings);
					});
				}

				return NativeFlv.promise;
			},

			_createPlayer: function _createPlayer(settings) {
				flvjs.LoggingControl.enableDebug = settings.options.debug;
				flvjs.LoggingControl.enableVerbose = settings.options.debug;
				var player = flvjs.createPlayer(settings.options, settings.configs);
				_window2.default['__ready__' + settings.id](player);
				return player;
			}
		};

		var FlvNativeRenderer = {
			name: 'native_flv',
			options: {
				prefix: 'native_flv',
				flv: {
					path: 'https://cdn.jsdelivr.net/npm/flv.js@latest',

					cors: true,
					debug: false
				}
			},

			canPlayType: function canPlayType(type) {
				return _constants.HAS_MSE && ['video/x-flv', 'video/flv'].indexOf(type.toLowerCase()) > -1;
			},

			create: function create(mediaElement, options, mediaFiles) {

				var originalNode = mediaElement.originalNode,
				    id = mediaElement.id + '_' + options.prefix;

				var node = null,
				    flvPlayer = null;

				node = originalNode.cloneNode(true);
				options = Object.assign(options, mediaElement.options);

				var props = _mejs2.default.html5media.properties,
				    events = _mejs2.default.html5media.events.concat(['click', 'mouseover', 'mouseout']).filter(function (e) {
					return e !== 'error';
				}),
				    attachNativeEvents = function attachNativeEvents(e) {
					var event = (0, _general.createEvent)(e.type, mediaElement);
					mediaElement.dispatchEvent(event);
				},
				    assignGettersSetters = function assignGettersSetters(propName) {
					var capName = '' + propName.substring(0, 1).toUpperCase() + propName.substring(1);

					node['get' + capName] = function () {
						return flvPlayer !== null ? node[propName] : null;
					};

					node['set' + capName] = function (value) {
						if (_mejs2.default.html5media.readOnlyProperties.indexOf(propName) === -1) {
							if (propName === 'src') {
								node[propName] = (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value.src ? value.src : value;
								if (flvPlayer !== null) {
									var _flvOptions = {};
									_flvOptions.type = 'flv';
									_flvOptions.url = value;
									_flvOptions.cors = options.flv.cors;
									_flvOptions.debug = options.flv.debug;
									_flvOptions.path = options.flv.path;
									var _flvConfigs = options.flv.configs;

									flvPlayer.destroy();
									for (var i = 0, total = events.length; i < total; i++) {
										node.removeEventListener(events[i], attachNativeEvents);
									}
									flvPlayer = NativeFlv._createPlayer({
										options: _flvOptions,
										configs: _flvConfigs,
										id: id
									});
									flvPlayer.attachMediaElement(node);
									flvPlayer.load();
								}
							} else {
								node[propName] = value;
							}
						}
					};
				};

				for (var i = 0, total = props.length; i < total; i++) {
					assignGettersSetters(props[i]);
				}

				_window2.default['__ready__' + id] = function (_flvPlayer) {
					mediaElement.flvPlayer = flvPlayer = _flvPlayer;

					var flvEvents = flvjs.Events,
					    assignEvents = function assignEvents(eventName) {
						if (eventName === 'loadedmetadata') {
							flvPlayer.unload();
							flvPlayer.detachMediaElement();
							flvPlayer.attachMediaElement(node);
							flvPlayer.load();
						}

						node.addEventListener(eventName, attachNativeEvents);
					};

					for (var _i = 0, _total = events.length; _i < _total; _i++) {
						assignEvents(events[_i]);
					}

					var assignFlvEvents = function assignFlvEvents(name, data) {
						if (name === 'error') {
							var message = data[0] + ': ' + data[1] + ' ' + data[2].msg;
							mediaElement.generateError(message, node.src);
						} else {
							var _event = (0, _general.createEvent)(name, mediaElement);
							_event.data = data;
							mediaElement.dispatchEvent(_event);
						}
					};

					var _loop = function _loop(eventType) {
						if (flvEvents.hasOwnProperty(eventType)) {
							flvPlayer.on(flvEvents[eventType], function () {
								for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
									args[_key] = arguments[_key];
								}

								return assignFlvEvents(flvEvents[eventType], args);
							});
						}
					};

					for (var eventType in flvEvents) {
						_loop(eventType);
					}
				};

				if (mediaFiles && mediaFiles.length > 0) {
					for (var _i2 = 0, _total2 = mediaFiles.length; _i2 < _total2; _i2++) {
						if (_renderer.renderer.renderers[options.prefix].canPlayType(mediaFiles[_i2].type)) {
							node.setAttribute('src', mediaFiles[_i2].src);
							break;
						}
					}
				}

				node.setAttribute('id', id);

				originalNode.parentNode.insertBefore(node, originalNode);
				originalNode.autoplay = false;
				originalNode.style.display = 'none';

				var flvOptions = {};
				flvOptions.type = 'flv';
				flvOptions.url = node.src;
				flvOptions.cors = options.flv.cors;
				flvOptions.debug = options.flv.debug;
				flvOptions.path = options.flv.path;
				var flvConfigs = options.flv.configs;

				node.setSize = function (width, height) {
					node.style.width = width + 'px';
					node.style.height = height + 'px';
					return node;
				};

				node.hide = function () {
					if (flvPlayer !== null) {
						flvPlayer.pause();
					}
					node.style.display = 'none';
					return node;
				};

				node.show = function () {
					node.style.display = '';
					return node;
				};

				node.destroy = function () {
					if (flvPlayer !== null) {
						flvPlayer.destroy();
					}
				};

				var event = (0, _general.createEvent)('rendererready', node);
				mediaElement.dispatchEvent(event);

				mediaElement.promises.push(NativeFlv.load({
					options: flvOptions,
					configs: flvConfigs,
					id: id
				}));

				return node;
			}
		};

		_media.typeChecks.push(function (url) {
			return ~url.toLowerCase().indexOf('.flv') ? 'video/flv' : null;
		});

		_renderer.renderer.add(FlvNativeRenderer);
	}, { "25": 25, "26": 26, "27": 27, "28": 28, "3": 3, "7": 7, "8": 8 }], 22: [function (_dereq_, module, exports) {
		'use strict';

		var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
			return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		} : function (obj) {
			return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
		};

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _renderer = _dereq_(8);

		var _general = _dereq_(27);

		var _constants = _dereq_(25);

		var _media = _dereq_(28);

		var _dom = _dereq_(26);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		var NativeHls = {

			promise: null,

			load: function load(settings) {
				if (typeof Hls !== 'undefined') {
					NativeHls.promise = new Promise(function (resolve) {
						resolve();
					}).then(function () {
						NativeHls._createPlayer(settings);
					});
				} else {
					settings.options.path = typeof settings.options.path === 'string' ? settings.options.path : 'https://cdn.jsdelivr.net/npm/hls.js@latest';

					NativeHls.promise = NativeHls.promise || (0, _dom.loadScript)(settings.options.path);
					NativeHls.promise.then(function () {
						NativeHls._createPlayer(settings);
					});
				}

				return NativeHls.promise;
			},

			_createPlayer: function _createPlayer(settings) {
				var player = new Hls(settings.options);
				_window2.default['__ready__' + settings.id](player);
				return player;
			}
		};

		var HlsNativeRenderer = {
			name: 'native_hls',
			options: {
				prefix: 'native_hls',
				hls: {
					path: 'https://cdn.jsdelivr.net/npm/hls.js@latest',

					autoStartLoad: false,
					debug: false
				}
			},

			canPlayType: function canPlayType(type) {
				return _constants.HAS_MSE && ['application/x-mpegurl', 'application/vnd.apple.mpegurl', 'audio/mpegurl', 'audio/hls', 'video/hls'].indexOf(type.toLowerCase()) > -1;
			},

			create: function create(mediaElement, options, mediaFiles) {

				var originalNode = mediaElement.originalNode,
				    id = mediaElement.id + '_' + options.prefix,
				    preload = originalNode.getAttribute('preload'),
				    autoplay = originalNode.autoplay;

				var hlsPlayer = null,
				    node = null,
				    index = 0,
				    total = mediaFiles.length;

				node = originalNode.cloneNode(true);
				options = Object.assign(options, mediaElement.options);
				options.hls.autoStartLoad = preload && preload !== 'none' || autoplay;

				var props = _mejs2.default.html5media.properties,
				    events = _mejs2.default.html5media.events.concat(['click', 'mouseover', 'mouseout']).filter(function (e) {
					return e !== 'error';
				}),
				    attachNativeEvents = function attachNativeEvents(e) {
					var event = (0, _general.createEvent)(e.type, mediaElement);
					mediaElement.dispatchEvent(event);
				},
				    assignGettersSetters = function assignGettersSetters(propName) {
					var capName = '' + propName.substring(0, 1).toUpperCase() + propName.substring(1);

					node['get' + capName] = function () {
						return hlsPlayer !== null ? node[propName] : null;
					};

					node['set' + capName] = function (value) {
						if (_mejs2.default.html5media.readOnlyProperties.indexOf(propName) === -1) {
							if (propName === 'src') {
								node[propName] = (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value.src ? value.src : value;
								if (hlsPlayer !== null) {
									hlsPlayer.destroy();
									for (var i = 0, _total = events.length; i < _total; i++) {
										node.removeEventListener(events[i], attachNativeEvents);
									}
									hlsPlayer = NativeHls._createPlayer({
										options: options.hls,
										id: id
									});
									hlsPlayer.loadSource(value);
									hlsPlayer.attachMedia(node);
								}
							} else {
								node[propName] = value;
							}
						}
					};
				};

				for (var i = 0, _total2 = props.length; i < _total2; i++) {
					assignGettersSetters(props[i]);
				}

				_window2.default['__ready__' + id] = function (_hlsPlayer) {
					mediaElement.hlsPlayer = hlsPlayer = _hlsPlayer;
					var hlsEvents = Hls.Events,
					    assignEvents = function assignEvents(eventName) {
						if (eventName === 'loadedmetadata') {
							var url = mediaElement.originalNode.src;
							hlsPlayer.detachMedia();
							hlsPlayer.loadSource(url);
							hlsPlayer.attachMedia(node);
						}

						node.addEventListener(eventName, attachNativeEvents);
					};

					for (var _i = 0, _total3 = events.length; _i < _total3; _i++) {
						assignEvents(events[_i]);
					}

					var recoverDecodingErrorDate = void 0,
					    recoverSwapAudioCodecDate = void 0;
					var assignHlsEvents = function assignHlsEvents(name, data) {
						if (name === 'hlsError') {
							console.warn(data);
							data = data[1];

							if (data.fatal) {
								switch (data.type) {
									case 'mediaError':
										var now = new Date().getTime();
										if (!recoverDecodingErrorDate || now - recoverDecodingErrorDate > 3000) {
											recoverDecodingErrorDate = new Date().getTime();
											hlsPlayer.recoverMediaError();
										} else if (!recoverSwapAudioCodecDate || now - recoverSwapAudioCodecDate > 3000) {
											recoverSwapAudioCodecDate = new Date().getTime();
											console.warn('Attempting to swap Audio Codec and recover from media error');
											hlsPlayer.swapAudioCodec();
											hlsPlayer.recoverMediaError();
										} else {
											var message = 'Cannot recover, last media error recovery failed';
											mediaElement.generateError(message, node.src);
											console.error(message);
										}
										break;
									case 'networkError':
										if (data.details === 'manifestLoadError') {
											if (index < total && mediaFiles[index + 1] !== undefined) {
												node.setSrc(mediaFiles[index++].src);
												node.load();
												node.play();
											} else {
												var _message = 'Network error';
												mediaElement.generateError(_message, mediaFiles);
												console.error(_message);
											}
										} else {
											var _message2 = 'Network error';
											mediaElement.generateError(_message2, mediaFiles);
											console.error(_message2);
										}
										break;
									default:
										hlsPlayer.destroy();
										break;
								}
							}
						} else {
							var _event = (0, _general.createEvent)(name, mediaElement);
							_event.data = data;
							mediaElement.dispatchEvent(_event);
						}
					};

					var _loop = function _loop(eventType) {
						if (hlsEvents.hasOwnProperty(eventType)) {
							hlsPlayer.on(hlsEvents[eventType], function () {
								for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
									args[_key] = arguments[_key];
								}

								return assignHlsEvents(hlsEvents[eventType], args);
							});
						}
					};

					for (var eventType in hlsEvents) {
						_loop(eventType);
					}
				};

				if (total > 0) {
					for (; index < total; index++) {
						if (_renderer.renderer.renderers[options.prefix].canPlayType(mediaFiles[index].type)) {
							node.setAttribute('src', mediaFiles[index].src);
							break;
						}
					}
				}

				if (preload !== 'auto' && !autoplay) {
					node.addEventListener('play', function () {
						if (hlsPlayer !== null) {
							hlsPlayer.startLoad();
						}
					});

					node.addEventListener('pause', function () {
						if (hlsPlayer !== null) {
							hlsPlayer.stopLoad();
						}
					});
				}

				node.setAttribute('id', id);

				originalNode.parentNode.insertBefore(node, originalNode);
				originalNode.autoplay = false;
				originalNode.style.display = 'none';

				node.setSize = function (width, height) {
					node.style.width = width + 'px';
					node.style.height = height + 'px';
					return node;
				};

				node.hide = function () {
					node.pause();
					node.style.display = 'none';
					return node;
				};

				node.show = function () {
					node.style.display = '';
					return node;
				};

				node.destroy = function () {
					if (hlsPlayer !== null) {
						hlsPlayer.stopLoad();
						hlsPlayer.destroy();
					}
				};

				var event = (0, _general.createEvent)('rendererready', node);
				mediaElement.dispatchEvent(event);

				mediaElement.promises.push(NativeHls.load({
					options: options.hls,
					id: id
				}));

				return node;
			}
		};

		_media.typeChecks.push(function (url) {
			return ~url.toLowerCase().indexOf('.m3u8') ? 'application/x-mpegURL' : null;
		});

		_renderer.renderer.add(HlsNativeRenderer);
	}, { "25": 25, "26": 26, "27": 27, "28": 28, "3": 3, "7": 7, "8": 8 }], 23: [function (_dereq_, module, exports) {
		'use strict';

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _renderer = _dereq_(8);

		var _general = _dereq_(27);

		var _constants = _dereq_(25);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		var HtmlMediaElement = {
			name: 'html5',
			options: {
				prefix: 'html5'
			},

			canPlayType: function canPlayType(type) {

				var mediaElement = _document2.default.createElement('video');

				if (_constants.IS_ANDROID && /\/mp(3|4)$/i.test(type) || ~['application/x-mpegurl', 'vnd.apple.mpegurl', 'audio/mpegurl', 'audio/hls', 'video/hls'].indexOf(type.toLowerCase()) && _constants.SUPPORTS_NATIVE_HLS) {
					return 'yes';
				} else if (mediaElement.canPlayType) {
					return mediaElement.canPlayType(type.toLowerCase()).replace(/no/, '');
				} else {
					return '';
				}
			},

			create: function create(mediaElement, options, mediaFiles) {

				var id = mediaElement.id + '_' + options.prefix;
				var isActive = false;

				var node = null;

				if (mediaElement.originalNode === undefined || mediaElement.originalNode === null) {
					node = _document2.default.createElement('audio');
					mediaElement.appendChild(node);
				} else {
					node = mediaElement.originalNode;
				}

				node.setAttribute('id', id);

				var props = _mejs2.default.html5media.properties,
				    assignGettersSetters = function assignGettersSetters(propName) {
					var capName = '' + propName.substring(0, 1).toUpperCase() + propName.substring(1);

					node['get' + capName] = function () {
						return node[propName];
					};

					node['set' + capName] = function (value) {
						if (_mejs2.default.html5media.readOnlyProperties.indexOf(propName) === -1) {
							node[propName] = value;
						}
					};
				};

				for (var i = 0, _total = props.length; i < _total; i++) {
					assignGettersSetters(props[i]);
				}

				var events = _mejs2.default.html5media.events.concat(['click', 'mouseover', 'mouseout']).filter(function (e) {
					return e !== 'error';
				}),
				    assignEvents = function assignEvents(eventName) {
					node.addEventListener(eventName, function (e) {
						if (isActive) {
							var _event = (0, _general.createEvent)(e.type, e.target);
							mediaElement.dispatchEvent(_event);
						}
					});
				};

				for (var _i = 0, _total2 = events.length; _i < _total2; _i++) {
					assignEvents(events[_i]);
				}

				node.setSize = function (width, height) {
					node.style.width = width + 'px';
					node.style.height = height + 'px';
					return node;
				};

				node.hide = function () {
					isActive = false;
					node.style.display = 'none';

					return node;
				};

				node.show = function () {
					isActive = true;
					node.style.display = '';

					return node;
				};

				var index = 0,
				    total = mediaFiles.length;
				if (total > 0) {
					for (; index < total; index++) {
						if (_renderer.renderer.renderers[options.prefix].canPlayType(mediaFiles[index].type)) {
							node.setAttribute('src', mediaFiles[index].src);
							break;
						}
					}
				}

				node.addEventListener('error', function (e) {
					if (e.target.error.code === 4 && isActive) {
						if (index < total && mediaFiles[index + 1] !== undefined) {
							node.src = mediaFiles[index++].src;
							node.load();
							node.play();
						} else {
							mediaElement.generateError('Media error: Format(s) not supported or source(s) not found', mediaFiles);
						}
					}
				});

				var event = (0, _general.createEvent)('rendererready', node);
				mediaElement.dispatchEvent(event);

				return node;
			}
		};

		_window2.default.HtmlMediaElement = _mejs2.default.HtmlMediaElement = HtmlMediaElement;

		_renderer.renderer.add(HtmlMediaElement);
	}, { "2": 2, "25": 25, "27": 27, "3": 3, "7": 7, "8": 8 }], 24: [function (_dereq_, module, exports) {
		'use strict';

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _renderer = _dereq_(8);

		var _general = _dereq_(27);

		var _media = _dereq_(28);

		var _dom = _dereq_(26);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		var YouTubeApi = {
			isIframeStarted: false,

			isIframeLoaded: false,

			iframeQueue: [],

			enqueueIframe: function enqueueIframe(settings) {
				YouTubeApi.isLoaded = typeof YT !== 'undefined' && YT.loaded;

				if (YouTubeApi.isLoaded) {
					YouTubeApi.createIframe(settings);
				} else {
					YouTubeApi.loadIframeApi();
					YouTubeApi.iframeQueue.push(settings);
				}
			},

			loadIframeApi: function loadIframeApi() {
				if (!YouTubeApi.isIframeStarted) {
					(0, _dom.loadScript)('https://www.youtube.com/player_api');
					YouTubeApi.isIframeStarted = true;
				}
			},

			iFrameReady: function iFrameReady() {

				YouTubeApi.isLoaded = true;
				YouTubeApi.isIframeLoaded = true;

				while (YouTubeApi.iframeQueue.length > 0) {
					var settings = YouTubeApi.iframeQueue.pop();
					YouTubeApi.createIframe(settings);
				}
			},

			createIframe: function createIframe(settings) {
				return new YT.Player(settings.containerId, settings);
			},

			getYouTubeId: function getYouTubeId(url) {

				var youTubeId = '';

				if (url.indexOf('?') > 0) {
					youTubeId = YouTubeApi.getYouTubeIdFromParam(url);

					if (youTubeId === '') {
						youTubeId = YouTubeApi.getYouTubeIdFromUrl(url);
					}
				} else {
					youTubeId = YouTubeApi.getYouTubeIdFromUrl(url);
				}

				var id = youTubeId.substring(youTubeId.lastIndexOf('/') + 1);
				youTubeId = id.split('?');
				return youTubeId[0];
			},

			getYouTubeIdFromParam: function getYouTubeIdFromParam(url) {

				if (url === undefined || url === null || !url.trim().length) {
					return null;
				}

				var parts = url.split('?'),
				    parameters = parts[1].split('&');

				var youTubeId = '';

				for (var i = 0, total = parameters.length; i < total; i++) {
					var paramParts = parameters[i].split('=');
					if (paramParts[0] === 'v') {
						youTubeId = paramParts[1];
						break;
					}
				}

				return youTubeId;
			},

			getYouTubeIdFromUrl: function getYouTubeIdFromUrl(url) {

				if (url === undefined || url === null || !url.trim().length) {
					return null;
				}

				var parts = url.split('?');
				url = parts[0];
				return url.substring(url.lastIndexOf('/') + 1);
			},

			getYouTubeNoCookieUrl: function getYouTubeNoCookieUrl(url) {
				if (url === undefined || url === null || !url.trim().length || url.indexOf('//www.youtube') === -1) {
					return url;
				}

				var parts = url.split('/');
				parts[2] = parts[2].replace('.com', '-nocookie.com');
				return parts.join('/');
			}
		};

		var YouTubeIframeRenderer = {
			name: 'youtube_iframe',

			options: {
				prefix: 'youtube_iframe',

				youtube: {
					autoplay: 0,
					controls: 0,
					disablekb: 1,
					end: 0,
					loop: 0,
					modestbranding: 0,
					playsinline: 0,
					rel: 0,
					showinfo: 0,
					start: 0,
					iv_load_policy: 3,

					nocookie: false,

					imageQuality: null
				}
			},

			canPlayType: function canPlayType(type) {
				return ~['video/youtube', 'video/x-youtube'].indexOf(type.toLowerCase());
			},

			create: function create(mediaElement, options, mediaFiles) {

				var youtube = {},
				    apiStack = [],
				    readyState = 4;

				var youTubeApi = null,
				    paused = true,
				    ended = false,
				    youTubeIframe = null,
				    volume = 1;

				youtube.options = options;
				youtube.id = mediaElement.id + '_' + options.prefix;
				youtube.mediaElement = mediaElement;

				var props = _mejs2.default.html5media.properties,
				    assignGettersSetters = function assignGettersSetters(propName) {

					var capName = '' + propName.substring(0, 1).toUpperCase() + propName.substring(1);

					youtube['get' + capName] = function () {
						if (youTubeApi !== null) {
							var value = null;

							switch (propName) {
								case 'currentTime':
									return youTubeApi.getCurrentTime();
								case 'duration':
									return youTubeApi.getDuration();
								case 'volume':
									volume = youTubeApi.getVolume() / 100;
									return volume;
								case 'paused':
									return paused;
								case 'ended':
									return ended;
								case 'muted':
									return youTubeApi.isMuted();
								case 'buffered':
									var percentLoaded = youTubeApi.getVideoLoadedFraction(),
									    duration = youTubeApi.getDuration();
									return {
										start: function start() {
											return 0;
										},
										end: function end() {
											return percentLoaded * duration;
										},
										length: 1
									};
								case 'src':
									return youTubeApi.getVideoUrl();
								case 'readyState':
									return readyState;
							}

							return value;
						} else {
							return null;
						}
					};

					youtube['set' + capName] = function (value) {
						if (youTubeApi !== null) {
							switch (propName) {
								case 'src':
									var url = typeof value === 'string' ? value : value[0].src,
									    _videoId = YouTubeApi.getYouTubeId(url);

									if (mediaElement.originalNode.autoplay) {
										youTubeApi.loadVideoById(_videoId);
									} else {
										youTubeApi.cueVideoById(_videoId);
									}
									break;
								case 'currentTime':
									youTubeApi.seekTo(value);
									break;
								case 'muted':
									if (value) {
										youTubeApi.mute();
									} else {
										youTubeApi.unMute();
									}
									setTimeout(function () {
										var event = (0, _general.createEvent)('volumechange', youtube);
										mediaElement.dispatchEvent(event);
									}, 50);
									break;
								case 'volume':
									volume = value;
									youTubeApi.setVolume(value * 100);
									setTimeout(function () {
										var event = (0, _general.createEvent)('volumechange', youtube);
										mediaElement.dispatchEvent(event);
									}, 50);
									break;
								case 'readyState':
									var event = (0, _general.createEvent)('canplay', youtube);
									mediaElement.dispatchEvent(event);
									break;
								default:

									break;
							}
						} else {
							apiStack.push({ type: 'set', propName: propName, value: value });
						}
					};
				};

				for (var i = 0, total = props.length; i < total; i++) {
					assignGettersSetters(props[i]);
				}

				var methods = _mejs2.default.html5media.methods,
				    assignMethods = function assignMethods(methodName) {
					youtube[methodName] = function () {
						if (youTubeApi !== null) {
							switch (methodName) {
								case 'play':
									paused = false;
									return youTubeApi.playVideo();
								case 'pause':
									paused = true;
									return youTubeApi.pauseVideo();
								case 'load':
									return null;
							}
						} else {
							apiStack.push({ type: 'call', methodName: methodName });
						}
					};
				};

				for (var _i = 0, _total = methods.length; _i < _total; _i++) {
					assignMethods(methods[_i]);
				}

				var errorHandler = function errorHandler(error) {
					var message = '';
					switch (error.data) {
						case 2:
							message = 'The request contains an invalid parameter value. Verify that video ID has 11 characters and that contains no invalid characters, such as exclamation points or asterisks.';
							break;
						case 5:
							message = 'The requested content cannot be played in an HTML5 player or another error related to the HTML5 player has occurred.';
							break;
						case 100:
							message = 'The video requested was not found. Either video has been removed or has been marked as private.';
							break;
						case 101:
						case 105:
							message = 'The owner of the requested video does not allow it to be played in embedded players.';
							break;
						default:
							message = 'Unknown error.';
							break;
					}
					mediaElement.generateError('Code ' + error.data + ': ' + message, mediaFiles);
				};

				var youtubeContainer = _document2.default.createElement('div');
				youtubeContainer.id = youtube.id;

				if (youtube.options.youtube.nocookie) {
					mediaElement.originalNode.src = YouTubeApi.getYouTubeNoCookieUrl(mediaFiles[0].src);
				}

				mediaElement.originalNode.parentNode.insertBefore(youtubeContainer, mediaElement.originalNode);
				mediaElement.originalNode.style.display = 'none';

				var isAudio = mediaElement.originalNode.tagName.toLowerCase() === 'audio',
				    height = isAudio ? '1' : mediaElement.originalNode.height,
				    width = isAudio ? '1' : mediaElement.originalNode.width,
				    videoId = YouTubeApi.getYouTubeId(mediaFiles[0].src),
				    youtubeSettings = {
					id: youtube.id,
					containerId: youtubeContainer.id,
					videoId: videoId,
					height: height,
					width: width,
					playerVars: Object.assign({
						controls: 0,
						rel: 0,
						disablekb: 1,
						showinfo: 0,
						modestbranding: 0,
						html5: 1,
						iv_load_policy: 3
					}, youtube.options.youtube),
					origin: _window2.default.location.host,
					events: {
						onReady: function onReady(e) {
							mediaElement.youTubeApi = youTubeApi = e.target;
							mediaElement.youTubeState = {
								paused: true,
								ended: false
							};

							if (apiStack.length) {
								for (var _i2 = 0, _total2 = apiStack.length; _i2 < _total2; _i2++) {

									var stackItem = apiStack[_i2];

									if (stackItem.type === 'set') {
										var propName = stackItem.propName,
										    capName = '' + propName.substring(0, 1).toUpperCase() + propName.substring(1);

										youtube['set' + capName](stackItem.value);
									} else if (stackItem.type === 'call') {
										youtube[stackItem.methodName]();
									}
								}
							}

							youTubeIframe = youTubeApi.getIframe();

							if (mediaElement.originalNode.muted) {
								youTubeApi.mute();
							}

							var events = ['mouseover', 'mouseout'],
							    assignEvents = function assignEvents(e) {
								var newEvent = (0, _general.createEvent)(e.type, youtube);
								mediaElement.dispatchEvent(newEvent);
							};

							for (var _i3 = 0, _total3 = events.length; _i3 < _total3; _i3++) {
								youTubeIframe.addEventListener(events[_i3], assignEvents, false);
							}

							var initEvents = ['rendererready', 'loadedmetadata', 'loadeddata', 'canplay'];

							for (var _i4 = 0, _total4 = initEvents.length; _i4 < _total4; _i4++) {
								var event = (0, _general.createEvent)(initEvents[_i4], youtube);
								mediaElement.dispatchEvent(event);
							}
						},
						onStateChange: function onStateChange(e) {
							var events = [];

							switch (e.data) {
								case -1:
									events = ['loadedmetadata'];
									paused = true;
									ended = false;
									break;
								case 0:
									events = ['ended'];
									paused = false;
									ended = !youtube.options.youtube.loop;
									if (!youtube.options.youtube.loop) {
										youtube.stopInterval();
									}
									break;
								case 1:
									events = ['play', 'playing'];
									paused = false;
									ended = false;
									youtube.startInterval();
									break;
								case 2:
									events = ['pause'];
									paused = true;
									ended = false;
									youtube.stopInterval();
									break;
								case 3:
									events = ['progress'];
									ended = false;
									break;
								case 5:
									events = ['loadeddata', 'loadedmetadata', 'canplay'];
									paused = true;
									ended = false;
									break;
							}

							for (var _i5 = 0, _total5 = events.length; _i5 < _total5; _i5++) {
								var event = (0, _general.createEvent)(events[_i5], youtube);
								mediaElement.dispatchEvent(event);
							}
						},
						onError: function onError(e) {
							return errorHandler(e);
						}
					}
				};

				if (isAudio || mediaElement.originalNode.hasAttribute('playsinline')) {
					youtubeSettings.playerVars.playsinline = 1;
				}

				if (mediaElement.originalNode.controls) {
					youtubeSettings.playerVars.controls = 1;
				}
				if (mediaElement.originalNode.autoplay) {
					youtubeSettings.playerVars.autoplay = 1;
				}
				if (mediaElement.originalNode.loop) {
					youtubeSettings.playerVars.loop = 1;
				}

				if ((youtubeSettings.playerVars.loop && parseInt(youtubeSettings.playerVars.loop, 10) === 1 || mediaElement.originalNode.src.indexOf('loop=') > -1) && !youtubeSettings.playerVars.playlist && mediaElement.originalNode.src.indexOf('playlist=') === -1) {
					youtubeSettings.playerVars.playlist = YouTubeApi.getYouTubeId(mediaElement.originalNode.src);
				}

				YouTubeApi.enqueueIframe(youtubeSettings);

				youtube.onEvent = function (eventName, player, _youTubeState) {
					if (_youTubeState !== null && _youTubeState !== undefined) {
						mediaElement.youTubeState = _youTubeState;
					}
				};

				youtube.setSize = function (width, height) {
					if (youTubeApi !== null) {
						youTubeApi.setSize(width, height);
					}
				};
				youtube.hide = function () {
					youtube.stopInterval();
					youtube.pause();
					if (youTubeIframe) {
						youTubeIframe.style.display = 'none';
					}
				};
				youtube.show = function () {
					if (youTubeIframe) {
						youTubeIframe.style.display = '';
					}
				};
				youtube.destroy = function () {
					youTubeApi.destroy();
				};
				youtube.interval = null;

				youtube.startInterval = function () {
					youtube.interval = setInterval(function () {
						var event = (0, _general.createEvent)('timeupdate', youtube);
						mediaElement.dispatchEvent(event);
					}, 250);
				};
				youtube.stopInterval = function () {
					if (youtube.interval) {
						clearInterval(youtube.interval);
					}
				};
				youtube.getPosterUrl = function () {
					var quality = options.youtube.imageQuality,
					    resolutions = ['default', 'hqdefault', 'mqdefault', 'sddefault', 'maxresdefault'],
					    id = YouTubeApi.getYouTubeId(mediaElement.originalNode.src);
					return quality && resolutions.indexOf(quality) > -1 && id ? 'https://img.youtube.com/vi/' + id + '/' + quality + '.jpg' : '';
				};

				return youtube;
			}
		};

		_window2.default.onYouTubePlayerAPIReady = function () {
			YouTubeApi.iFrameReady();
		};

		_media.typeChecks.push(function (url) {
			return (/\/\/(www\.youtube|youtu\.?be)/i.test(url) ? 'video/x-youtube' : null
			);
		});

		_renderer.renderer.add(YouTubeIframeRenderer);
	}, { "2": 2, "26": 26, "27": 27, "28": 28, "3": 3, "7": 7, "8": 8 }], 25: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});
		exports.cancelFullScreen = exports.requestFullScreen = exports.isFullScreen = exports.FULLSCREEN_EVENT_NAME = exports.HAS_NATIVE_FULLSCREEN_ENABLED = exports.HAS_TRUE_NATIVE_FULLSCREEN = exports.HAS_IOS_FULLSCREEN = exports.HAS_MS_NATIVE_FULLSCREEN = exports.HAS_MOZ_NATIVE_FULLSCREEN = exports.HAS_WEBKIT_NATIVE_FULLSCREEN = exports.HAS_NATIVE_FULLSCREEN = exports.SUPPORTS_NATIVE_HLS = exports.SUPPORT_PASSIVE_EVENT = exports.SUPPORT_POINTER_EVENTS = exports.HAS_MSE = exports.IS_STOCK_ANDROID = exports.IS_SAFARI = exports.IS_FIREFOX = exports.IS_CHROME = exports.IS_EDGE = exports.IS_IE = exports.IS_ANDROID = exports.IS_IOS = exports.IS_IPOD = exports.IS_IPHONE = exports.IS_IPAD = exports.UA = exports.NAV = undefined;

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		var NAV = exports.NAV = _window2.default.navigator;
		var UA = exports.UA = NAV.userAgent.toLowerCase();
		var IS_IPAD = exports.IS_IPAD = /ipad/i.test(UA) && !_window2.default.MSStream;
		var IS_IPHONE = exports.IS_IPHONE = /iphone/i.test(UA) && !_window2.default.MSStream;
		var IS_IPOD = exports.IS_IPOD = /ipod/i.test(UA) && !_window2.default.MSStream;
		var IS_IOS = exports.IS_IOS = /ipad|iphone|ipod/i.test(UA) && !_window2.default.MSStream;
		var IS_ANDROID = exports.IS_ANDROID = /android/i.test(UA);
		var IS_IE = exports.IS_IE = /(trident|microsoft)/i.test(NAV.appName);
		var IS_EDGE = exports.IS_EDGE = 'msLaunchUri' in NAV && !('documentMode' in _document2.default);
		var IS_CHROME = exports.IS_CHROME = /chrome/i.test(UA);
		var IS_FIREFOX = exports.IS_FIREFOX = /firefox/i.test(UA);
		var IS_SAFARI = exports.IS_SAFARI = /safari/i.test(UA) && !IS_CHROME;
		var IS_STOCK_ANDROID = exports.IS_STOCK_ANDROID = /^mozilla\/\d+\.\d+\s\(linux;\su;/i.test(UA);
		var HAS_MSE = exports.HAS_MSE = 'MediaSource' in _window2.default;
		var SUPPORT_POINTER_EVENTS = exports.SUPPORT_POINTER_EVENTS = function () {
			var element = _document2.default.createElement('x'),
			    documentElement = _document2.default.documentElement,
			    getComputedStyle = _window2.default.getComputedStyle;

			if (!('pointerEvents' in element.style)) {
				return false;
			}

			element.style.pointerEvents = 'auto';
			element.style.pointerEvents = 'x';
			documentElement.appendChild(element);
			var supports = getComputedStyle && (getComputedStyle(element, '') || {}).pointerEvents === 'auto';
			element.remove();
			return !!supports;
		}();

		var SUPPORT_PASSIVE_EVENT = exports.SUPPORT_PASSIVE_EVENT = function () {
			var supportsPassive = false;
			try {
				var opts = Object.defineProperty({}, 'passive', {
					get: function get() {
						supportsPassive = true;
					}
				});
				_window2.default.addEventListener('test', null, opts);
			} catch (e) {}

			return supportsPassive;
		}();

		var html5Elements = ['source', 'track', 'audio', 'video'];
		var video = void 0;

		for (var i = 0, total = html5Elements.length; i < total; i++) {
			video = _document2.default.createElement(html5Elements[i]);
		}

		var SUPPORTS_NATIVE_HLS = exports.SUPPORTS_NATIVE_HLS = IS_SAFARI || IS_ANDROID && (IS_CHROME || IS_STOCK_ANDROID) || IS_IE && /edge/i.test(UA);

		var hasiOSFullScreen = video.webkitEnterFullscreen !== undefined;

		var hasNativeFullscreen = video.requestFullscreen !== undefined;

		if (hasiOSFullScreen && /mac os x 10_5/i.test(UA)) {
			hasNativeFullscreen = false;
			hasiOSFullScreen = false;
		}

		var hasWebkitNativeFullScreen = video.webkitRequestFullScreen !== undefined;
		var hasMozNativeFullScreen = video.mozRequestFullScreen !== undefined;
		var hasMsNativeFullScreen = video.msRequestFullscreen !== undefined;
		var hasTrueNativeFullScreen = hasWebkitNativeFullScreen || hasMozNativeFullScreen || hasMsNativeFullScreen;
		var nativeFullScreenEnabled = hasTrueNativeFullScreen;
		var fullScreenEventName = '';
		var isFullScreen = void 0,
		    requestFullScreen = void 0,
		    cancelFullScreen = void 0;

		if (hasMozNativeFullScreen) {
			nativeFullScreenEnabled = _document2.default.mozFullScreenEnabled;
		} else if (hasMsNativeFullScreen) {
			nativeFullScreenEnabled = _document2.default.msFullscreenEnabled;
		}

		if (IS_CHROME) {
			hasiOSFullScreen = false;
		}

		if (hasTrueNativeFullScreen) {
			if (hasWebkitNativeFullScreen) {
				fullScreenEventName = 'webkitfullscreenchange';
			} else if (hasMozNativeFullScreen) {
				fullScreenEventName = 'mozfullscreenchange';
			} else if (hasMsNativeFullScreen) {
				fullScreenEventName = 'MSFullscreenChange';
			}

			exports.isFullScreen = isFullScreen = function isFullScreen() {
				if (hasMozNativeFullScreen) {
					return _document2.default.mozFullScreen;
				} else if (hasWebkitNativeFullScreen) {
					return _document2.default.webkitIsFullScreen;
				} else if (hasMsNativeFullScreen) {
					return _document2.default.msFullscreenElement !== null;
				}
			};

			exports.requestFullScreen = requestFullScreen = function requestFullScreen(el) {
				if (hasWebkitNativeFullScreen) {
					el.webkitRequestFullScreen();
				} else if (hasMozNativeFullScreen) {
					el.mozRequestFullScreen();
				} else if (hasMsNativeFullScreen) {
					el.msRequestFullscreen();
				}
			};

			exports.cancelFullScreen = cancelFullScreen = function cancelFullScreen() {
				if (hasWebkitNativeFullScreen) {
					_document2.default.webkitCancelFullScreen();
				} else if (hasMozNativeFullScreen) {
					_document2.default.mozCancelFullScreen();
				} else if (hasMsNativeFullScreen) {
					_document2.default.msExitFullscreen();
				}
			};
		}

		var HAS_NATIVE_FULLSCREEN = exports.HAS_NATIVE_FULLSCREEN = hasNativeFullscreen;
		var HAS_WEBKIT_NATIVE_FULLSCREEN = exports.HAS_WEBKIT_NATIVE_FULLSCREEN = hasWebkitNativeFullScreen;
		var HAS_MOZ_NATIVE_FULLSCREEN = exports.HAS_MOZ_NATIVE_FULLSCREEN = hasMozNativeFullScreen;
		var HAS_MS_NATIVE_FULLSCREEN = exports.HAS_MS_NATIVE_FULLSCREEN = hasMsNativeFullScreen;
		var HAS_IOS_FULLSCREEN = exports.HAS_IOS_FULLSCREEN = hasiOSFullScreen;
		var HAS_TRUE_NATIVE_FULLSCREEN = exports.HAS_TRUE_NATIVE_FULLSCREEN = hasTrueNativeFullScreen;
		var HAS_NATIVE_FULLSCREEN_ENABLED = exports.HAS_NATIVE_FULLSCREEN_ENABLED = nativeFullScreenEnabled;
		var FULLSCREEN_EVENT_NAME = exports.FULLSCREEN_EVENT_NAME = fullScreenEventName;
		exports.isFullScreen = isFullScreen;
		exports.requestFullScreen = requestFullScreen;
		exports.cancelFullScreen = cancelFullScreen;

		_mejs2.default.Features = _mejs2.default.Features || {};
		_mejs2.default.Features.isiPad = IS_IPAD;
		_mejs2.default.Features.isiPod = IS_IPOD;
		_mejs2.default.Features.isiPhone = IS_IPHONE;
		_mejs2.default.Features.isiOS = _mejs2.default.Features.isiPhone || _mejs2.default.Features.isiPad;
		_mejs2.default.Features.isAndroid = IS_ANDROID;
		_mejs2.default.Features.isIE = IS_IE;
		_mejs2.default.Features.isEdge = IS_EDGE;
		_mejs2.default.Features.isChrome = IS_CHROME;
		_mejs2.default.Features.isFirefox = IS_FIREFOX;
		_mejs2.default.Features.isSafari = IS_SAFARI;
		_mejs2.default.Features.isStockAndroid = IS_STOCK_ANDROID;
		_mejs2.default.Features.hasMSE = HAS_MSE;
		_mejs2.default.Features.supportsNativeHLS = SUPPORTS_NATIVE_HLS;
		_mejs2.default.Features.supportsPointerEvents = SUPPORT_POINTER_EVENTS;
		_mejs2.default.Features.supportsPassiveEvent = SUPPORT_PASSIVE_EVENT;
		_mejs2.default.Features.hasiOSFullScreen = HAS_IOS_FULLSCREEN;
		_mejs2.default.Features.hasNativeFullscreen = HAS_NATIVE_FULLSCREEN;
		_mejs2.default.Features.hasWebkitNativeFullScreen = HAS_WEBKIT_NATIVE_FULLSCREEN;
		_mejs2.default.Features.hasMozNativeFullScreen = HAS_MOZ_NATIVE_FULLSCREEN;
		_mejs2.default.Features.hasMsNativeFullScreen = HAS_MS_NATIVE_FULLSCREEN;
		_mejs2.default.Features.hasTrueNativeFullScreen = HAS_TRUE_NATIVE_FULLSCREEN;
		_mejs2.default.Features.nativeFullScreenEnabled = HAS_NATIVE_FULLSCREEN_ENABLED;
		_mejs2.default.Features.fullScreenEventName = FULLSCREEN_EVENT_NAME;
		_mejs2.default.Features.isFullScreen = isFullScreen;
		_mejs2.default.Features.requestFullScreen = requestFullScreen;
		_mejs2.default.Features.cancelFullScreen = cancelFullScreen;
	}, { "2": 2, "3": 3, "7": 7 }], 26: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});
		exports.removeClass = exports.addClass = exports.hasClass = undefined;
		exports.loadScript = loadScript;
		exports.offset = offset;
		exports.toggleClass = toggleClass;
		exports.fadeOut = fadeOut;
		exports.fadeIn = fadeIn;
		exports.siblings = siblings;
		exports.visible = visible;
		exports.ajax = ajax;

		var _window = _dereq_(3);

		var _window2 = _interopRequireDefault(_window);

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		function loadScript(url) {
			return new Promise(function (resolve, reject) {
				var script = _document2.default.createElement('script');
				script.src = url;
				script.async = true;
				script.onload = function () {
					script.remove();
					resolve();
				};
				script.onerror = function () {
					script.remove();
					reject();
				};
				_document2.default.head.appendChild(script);
			});
		}

		function offset(el) {
			var rect = el.getBoundingClientRect(),
			    scrollLeft = _window2.default.pageXOffset || _document2.default.documentElement.scrollLeft,
			    scrollTop = _window2.default.pageYOffset || _document2.default.documentElement.scrollTop;
			return { top: rect.top + scrollTop, left: rect.left + scrollLeft };
		}

		var hasClassMethod = void 0,
		    addClassMethod = void 0,
		    removeClassMethod = void 0;

		if ('classList' in _document2.default.documentElement) {
			hasClassMethod = function hasClassMethod(el, className) {
				return el.classList !== undefined && el.classList.contains(className);
			};
			addClassMethod = function addClassMethod(el, className) {
				return el.classList.add(className);
			};
			removeClassMethod = function removeClassMethod(el, className) {
				return el.classList.remove(className);
			};
		} else {
			hasClassMethod = function hasClassMethod(el, className) {
				return new RegExp('\\b' + className + '\\b').test(el.className);
			};
			addClassMethod = function addClassMethod(el, className) {
				if (!hasClass(el, className)) {
					el.className += ' ' + className;
				}
			};
			removeClassMethod = function removeClassMethod(el, className) {
				el.className = el.className.replace(new RegExp('\\b' + className + '\\b', 'g'), '');
			};
		}

		var hasClass = exports.hasClass = hasClassMethod;
		var addClass = exports.addClass = addClassMethod;
		var removeClass = exports.removeClass = removeClassMethod;

		function toggleClass(el, className) {
			hasClass(el, className) ? removeClass(el, className) : addClass(el, className);
		}

		function fadeOut(el) {
			var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 400;
			var callback = arguments[2];

			if (!el.style.opacity) {
				el.style.opacity = 1;
			}

			var start = null;
			_window2.default.requestAnimationFrame(function animate(timestamp) {
				start = start || timestamp;
				var progress = timestamp - start;
				var opacity = parseFloat(1 - progress / duration, 2);
				el.style.opacity = opacity < 0 ? 0 : opacity;
				if (progress > duration) {
					if (callback && typeof callback === 'function') {
						callback();
					}
				} else {
					_window2.default.requestAnimationFrame(animate);
				}
			});
		}

		function fadeIn(el) {
			var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 400;
			var callback = arguments[2];

			if (!el.style.opacity) {
				el.style.opacity = 0;
			}

			var start = null;
			_window2.default.requestAnimationFrame(function animate(timestamp) {
				start = start || timestamp;
				var progress = timestamp - start;
				var opacity = parseFloat(progress / duration, 2);
				el.style.opacity = opacity > 1 ? 1 : opacity;
				if (progress > duration) {
					if (callback && typeof callback === 'function') {
						callback();
					}
				} else {
					_window2.default.requestAnimationFrame(animate);
				}
			});
		}

		function siblings(el, filter) {
			var siblings = [];
			el = el.parentNode.firstChild;
			do {
				if (!filter || filter(el)) {
					siblings.push(el);
				}
			} while (el = el.nextSibling);
			return siblings;
		}

		function visible(elem) {
			if (elem.getClientRects !== undefined && elem.getClientRects === 'function') {
				return !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
			}
			return !!(elem.offsetWidth || elem.offsetHeight);
		}

		function ajax(url, dataType, success, error) {
			var xhr = _window2.default.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');

			var type = 'application/x-www-form-urlencoded; charset=UTF-8',
			    completed = false,
			    accept = '*/'.concat('*');

			switch (dataType) {
				case 'text':
					type = 'text/plain';
					break;
				case 'json':
					type = 'application/json, text/javascript';
					break;
				case 'html':
					type = 'text/html';
					break;
				case 'xml':
					type = 'application/xml, text/xml';
					break;
			}

			if (type !== 'application/x-www-form-urlencoded') {
				accept = type + ', */*; q=0.01';
			}

			if (xhr) {
				xhr.open('GET', url, true);
				xhr.setRequestHeader('Accept', accept);
				xhr.onreadystatechange = function () {
					if (completed) {
						return;
					}

					if (xhr.readyState === 4) {
						if (xhr.status === 200) {
							completed = true;
							var data = void 0;
							switch (dataType) {
								case 'json':
									data = JSON.parse(xhr.responseText);
									break;
								case 'xml':
									data = xhr.responseXML;
									break;
								default:
									data = xhr.responseText;
									break;
							}
							success(data);
						} else if (typeof error === 'function') {
							error(xhr.status);
						}
					}
				};

				xhr.send();
			}
		}

		_mejs2.default.Utils = _mejs2.default.Utils || {};
		_mejs2.default.Utils.offset = offset;
		_mejs2.default.Utils.hasClass = hasClass;
		_mejs2.default.Utils.addClass = addClass;
		_mejs2.default.Utils.removeClass = removeClass;
		_mejs2.default.Utils.toggleClass = toggleClass;
		_mejs2.default.Utils.fadeIn = fadeIn;
		_mejs2.default.Utils.fadeOut = fadeOut;
		_mejs2.default.Utils.siblings = siblings;
		_mejs2.default.Utils.visible = visible;
		_mejs2.default.Utils.ajax = ajax;
		_mejs2.default.Utils.loadScript = loadScript;
	}, { "2": 2, "3": 3, "7": 7 }], 27: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});
		exports.escapeHTML = escapeHTML;
		exports.debounce = debounce;
		exports.isObjectEmpty = isObjectEmpty;
		exports.splitEvents = splitEvents;
		exports.createEvent = createEvent;
		exports.isNodeAfter = isNodeAfter;
		exports.isString = isString;

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		function escapeHTML(input) {

			if (typeof input !== 'string') {
				throw new Error('Argument passed must be a string');
			}

			var map = {
				'&': '&amp;',
				'<': '&lt;',
				'>': '&gt;',
				'"': '&quot;'
			};

			return input.replace(/[&<>"]/g, function (c) {
				return map[c];
			});
		}

		function debounce(func, wait) {
			var _this = this,
			    _arguments = arguments;

			var immediate = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

			if (typeof func !== 'function') {
				throw new Error('First argument must be a function');
			}

			if (typeof wait !== 'number') {
				throw new Error('Second argument must be a numeric value');
			}

			var timeout = void 0;
			return function () {
				var context = _this,
				    args = _arguments;
				var later = function later() {
					timeout = null;
					if (!immediate) {
						func.apply(context, args);
					}
				};
				var callNow = immediate && !timeout;
				clearTimeout(timeout);
				timeout = setTimeout(later, wait);

				if (callNow) {
					func.apply(context, args);
				}
			};
		}

		function isObjectEmpty(instance) {
			return Object.getOwnPropertyNames(instance).length <= 0;
		}

		function splitEvents(events, id) {
			var rwindow = /^((after|before)print|(before)?unload|hashchange|message|o(ff|n)line|page(hide|show)|popstate|resize|storage)\b/;

			var ret = { d: [], w: [] };
			(events || '').split(' ').forEach(function (v) {
				var eventName = '' + v + (id ? '.' + id : '');

				if (eventName.startsWith('.')) {
					ret.d.push(eventName);
					ret.w.push(eventName);
				} else {
					ret[rwindow.test(v) ? 'w' : 'd'].push(eventName);
				}
			});

			ret.d = ret.d.join(' ');
			ret.w = ret.w.join(' ');
			return ret;
		}

		function createEvent(eventName, target) {

			if (typeof eventName !== 'string') {
				throw new Error('Event name must be a string');
			}

			var eventFrags = eventName.match(/([a-z]+\.([a-z]+))/i),
			    detail = {
				target: target
			};

			if (eventFrags !== null) {
				eventName = eventFrags[1];
				detail.namespace = eventFrags[2];
			}

			return new window.CustomEvent(eventName, {
				detail: detail
			});
		}

		function isNodeAfter(sourceNode, targetNode) {

			return !!(sourceNode && targetNode && sourceNode.compareDocumentPosition(targetNode) & 2);
		}

		function isString(value) {
			return typeof value === 'string';
		}

		_mejs2.default.Utils = _mejs2.default.Utils || {};
		_mejs2.default.Utils.escapeHTML = escapeHTML;
		_mejs2.default.Utils.debounce = debounce;
		_mejs2.default.Utils.isObjectEmpty = isObjectEmpty;
		_mejs2.default.Utils.splitEvents = splitEvents;
		_mejs2.default.Utils.createEvent = createEvent;
		_mejs2.default.Utils.isNodeAfter = isNodeAfter;
		_mejs2.default.Utils.isString = isString;
	}, { "7": 7 }], 28: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});
		exports.typeChecks = undefined;
		exports.absolutizeUrl = absolutizeUrl;
		exports.formatType = formatType;
		exports.getMimeFromType = getMimeFromType;
		exports.getTypeFromFile = getTypeFromFile;
		exports.getExtension = getExtension;
		exports.normalizeExtension = normalizeExtension;

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		var _general = _dereq_(27);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		var typeChecks = exports.typeChecks = [];

		function absolutizeUrl(url) {

			if (typeof url !== 'string') {
				throw new Error('`url` argument must be a string');
			}

			var el = document.createElement('div');
			el.innerHTML = '<a href="' + (0, _general.escapeHTML)(url) + '">x</a>';
			return el.firstChild.href;
		}

		function formatType(url) {
			var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

			return url && !type ? getTypeFromFile(url) : type;
		}

		function getMimeFromType(type) {

			if (typeof type !== 'string') {
				throw new Error('`type` argument must be a string');
			}

			return type && type.indexOf(';') > -1 ? type.substr(0, type.indexOf(';')) : type;
		}

		function getTypeFromFile(url) {

			if (typeof url !== 'string') {
				throw new Error('`url` argument must be a string');
			}

			for (var i = 0, total = typeChecks.length; i < total; i++) {
				var type = typeChecks[i](url);

				if (type) {
					return type;
				}
			}

			var ext = getExtension(url),
			    normalizedExt = normalizeExtension(ext);

			var mime = 'video/mp4';

			if (normalizedExt) {
				if (~['mp4', 'm4v', 'ogg', 'ogv', 'webm', 'flv', 'mpeg', 'mov'].indexOf(normalizedExt)) {
					mime = 'video/' + normalizedExt;
				} else if (~['mp3', 'oga', 'wav', 'mid', 'midi'].indexOf(normalizedExt)) {
					mime = 'audio/' + normalizedExt;
				}
			}

			return mime;
		}

		function getExtension(url) {

			if (typeof url !== 'string') {
				throw new Error('`url` argument must be a string');
			}

			var baseUrl = url.split('?')[0],
			    baseName = baseUrl.split('\\').pop().split('/').pop();
			return ~baseName.indexOf('.') ? baseName.substring(baseName.lastIndexOf('.') + 1) : '';
		}

		function normalizeExtension(extension) {

			if (typeof extension !== 'string') {
				throw new Error('`extension` argument must be a string');
			}

			switch (extension) {
				case 'mp4':
				case 'm4v':
					return 'mp4';
				case 'webm':
				case 'webma':
				case 'webmv':
					return 'webm';
				case 'ogg':
				case 'oga':
				case 'ogv':
					return 'ogg';
				default:
					return extension;
			}
		}

		_mejs2.default.Utils = _mejs2.default.Utils || {};
		_mejs2.default.Utils.typeChecks = typeChecks;
		_mejs2.default.Utils.absolutizeUrl = absolutizeUrl;
		_mejs2.default.Utils.formatType = formatType;
		_mejs2.default.Utils.getMimeFromType = getMimeFromType;
		_mejs2.default.Utils.getTypeFromFile = getTypeFromFile;
		_mejs2.default.Utils.getExtension = getExtension;
		_mejs2.default.Utils.normalizeExtension = normalizeExtension;
	}, { "27": 27, "7": 7 }], 29: [function (_dereq_, module, exports) {
		'use strict';

		var _document = _dereq_(2);

		var _document2 = _interopRequireDefault(_document);

		var _promisePolyfill = _dereq_(4);

		var _promisePolyfill2 = _interopRequireDefault(_promisePolyfill);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		(function (arr) {
			arr.forEach(function (item) {
				if (item.hasOwnProperty('remove')) {
					return;
				}
				Object.defineProperty(item, 'remove', {
					configurable: true,
					enumerable: true,
					writable: true,
					value: function remove() {
						this.parentNode.removeChild(this);
					}
				});
			});
		})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

		(function () {

			if (typeof window.CustomEvent === 'function') {
				return false;
			}

			function CustomEvent(event, params) {
				params = params || { bubbles: false, cancelable: false, detail: undefined };
				var evt = _document2.default.createEvent('CustomEvent');
				evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
				return evt;
			}

			CustomEvent.prototype = window.Event.prototype;
			window.CustomEvent = CustomEvent;
		})();

		if (typeof Object.assign !== 'function') {
			Object.assign = function (target) {

				if (target === null || target === undefined) {
					throw new TypeError('Cannot convert undefined or null to object');
				}

				var to = Object(target);

				for (var index = 1, total = arguments.length; index < total; index++) {
					var nextSource = arguments[index];

					if (nextSource !== null) {
						for (var nextKey in nextSource) {
							if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
				}
				return to;
			};
		}

		if (!String.prototype.startsWith) {
			String.prototype.startsWith = function (searchString, position) {
				position = position || 0;
				return this.substr(position, searchString.length) === searchString;
			};
		}

		if (!Element.prototype.matches) {
			Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector || function (s) {
				var matches = (this.document || this.ownerDocument).querySelectorAll(s),
				    i = matches.length - 1;
				while (--i >= 0 && matches.item(i) !== this) {}
				return i > -1;
			};
		}

		if (window.Element && !Element.prototype.closest) {
			Element.prototype.closest = function (s) {
				var matches = (this.document || this.ownerDocument).querySelectorAll(s),
				    i = void 0,
				    el = this;
				do {
					i = matches.length;
					while (--i >= 0 && matches.item(i) !== el) {}
				} while (i < 0 && (el = el.parentElement));
				return el;
			};
		}

		(function () {
			var lastTime = 0;
			var vendors = ['ms', 'moz', 'webkit', 'o'];
			for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
				window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
				window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
			}

			if (!window.requestAnimationFrame) window.requestAnimationFrame = function (callback) {
				var currTime = new Date().getTime();
				var timeToCall = Math.max(0, 16 - (currTime - lastTime));
				var id = window.setTimeout(function () {
					callback(currTime + timeToCall);
				}, timeToCall);
				lastTime = currTime + timeToCall;
				return id;
			};

			if (!window.cancelAnimationFrame) window.cancelAnimationFrame = function (id) {
				clearTimeout(id);
			};
		})();

		if (/firefox/i.test(navigator.userAgent)) {
			var getComputedStyle = window.getComputedStyle;
			window.getComputedStyle = function (el, pseudoEl) {
				var t = getComputedStyle(el, pseudoEl);
				return t === null ? { getPropertyValue: function getPropertyValue() {} } : t;
			};
		}

		if (!window.Promise) {
			window.Promise = _promisePolyfill2.default;
		}

		(function (constructor) {
			if (constructor && constructor.prototype && constructor.prototype.children === null) {
				Object.defineProperty(constructor.prototype, 'children', {
					get: function get() {
						var i = 0,
						    node = void 0,
						    nodes = this.childNodes,
						    children = [];
						while (node = nodes[i++]) {
							if (node.nodeType === 1) {
								children.push(node);
							}
						}
						return children;
					}
				});
			}
		})(window.Node || window.Element);
	}, { "2": 2, "4": 4 }], 30: [function (_dereq_, module, exports) {
		'use strict';

		Object.defineProperty(exports, "__esModule", {
			value: true
		});
		exports.isDropFrame = isDropFrame;
		exports.secondsToTimeCode = secondsToTimeCode;
		exports.timeCodeToSeconds = timeCodeToSeconds;
		exports.calculateTimeFormat = calculateTimeFormat;
		exports.convertSMPTEtoSeconds = convertSMPTEtoSeconds;

		var _mejs = _dereq_(7);

		var _mejs2 = _interopRequireDefault(_mejs);

		function _interopRequireDefault(obj) {
			return obj && obj.__esModule ? obj : { default: obj };
		}

		function isDropFrame() {
			var fps = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 25;

			return !(fps % 1 === 0);
		}
		function secondsToTimeCode(time) {
			var forceHours = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
			var showFrameCount = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
			var fps = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 25;
			var secondsDecimalLength = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
			var timeFormat = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 'hh:mm:ss';

			time = !time || typeof time !== 'number' || time < 0 ? 0 : time;

			var dropFrames = Math.round(fps * 0.066666),
			    timeBase = Math.round(fps),
			    framesPer24Hours = Math.round(fps * 3600) * 24,
			    framesPer10Minutes = Math.round(fps * 600),
			    frameSep = isDropFrame(fps) ? ';' : ':',
			    hours = void 0,
			    minutes = void 0,
			    seconds = void 0,
			    frames = void 0,
			    f = Math.round(time * fps);

			if (isDropFrame(fps)) {

				if (f < 0) {
					f = framesPer24Hours + f;
				}

				f = f % framesPer24Hours;

				var d = Math.floor(f / framesPer10Minutes);
				var m = f % framesPer10Minutes;
				f = f + dropFrames * 9 * d;
				if (m > dropFrames) {
					f = f + dropFrames * Math.floor((m - dropFrames) / Math.round(timeBase * 60 - dropFrames));
				}

				var timeBaseDivision = Math.floor(f / timeBase);

				hours = Math.floor(Math.floor(timeBaseDivision / 60) / 60);
				minutes = Math.floor(timeBaseDivision / 60) % 60;

				if (showFrameCount) {
					seconds = timeBaseDivision % 60;
				} else {
					seconds = Math.floor(f / timeBase % 60).toFixed(secondsDecimalLength);
				}
			} else {
				hours = Math.floor(time / 3600) % 24;
				minutes = Math.floor(time / 60) % 60;
				if (showFrameCount) {
					seconds = Math.floor(time % 60);
				} else {
					seconds = Math.floor(time % 60).toFixed(secondsDecimalLength);
				}
			}
			hours = hours <= 0 ? 0 : hours;
			minutes = minutes <= 0 ? 0 : minutes;
			seconds = seconds <= 0 ? 0 : seconds;

			seconds = seconds === 60 ? 0 : seconds;
			minutes = minutes === 60 ? 0 : minutes;

			var timeFormatFrags = timeFormat.split(':');
			var timeFormatSettings = {};
			for (var i = 0, total = timeFormatFrags.length; i < total; ++i) {
				var unique = '';
				for (var j = 0, t = timeFormatFrags[i].length; j < t; j++) {
					if (unique.indexOf(timeFormatFrags[i][j]) < 0) {
						unique += timeFormatFrags[i][j];
					}
				}
				if (~['f', 's', 'm', 'h'].indexOf(unique)) {
					timeFormatSettings[unique] = timeFormatFrags[i].length;
				}
			}

			var result = forceHours || hours > 0 ? (hours < 10 && timeFormatSettings.h > 1 ? '0' + hours : hours) + ':' : '';
			result += (minutes < 10 && timeFormatSettings.m > 1 ? '0' + minutes : minutes) + ':';
			result += '' + (seconds < 10 && timeFormatSettings.s > 1 ? '0' + seconds : seconds);

			if (showFrameCount) {
				frames = (f % timeBase).toFixed(0);
				frames = frames <= 0 ? 0 : frames;
				result += frames < 10 && timeFormatSettings.f ? frameSep + '0' + frames : '' + frameSep + frames;
			}

			return result;
		}

		function timeCodeToSeconds(time) {
			var fps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 25;

			if (typeof time !== 'string') {
				throw new TypeError('Time must be a string');
			}

			if (time.indexOf(';') > 0) {
				time = time.replace(';', ':');
			}

			if (!/\d{2}(\:\d{2}){0,3}/i.test(time)) {
				throw new TypeError('Time code must have the format `00:00:00`');
			}

			var parts = time.split(':');

			var output = void 0,
			    hours = 0,
			    minutes = 0,
			    seconds = 0,
			    frames = 0,
			    totalMinutes = 0,
			    dropFrames = Math.round(fps * 0.066666),
			    timeBase = Math.round(fps),
			    hFrames = timeBase * 3600,
			    mFrames = timeBase * 60;

			switch (parts.length) {
				default:
				case 1:
					seconds = parseInt(parts[0], 10);
					break;
				case 2:
					minutes = parseInt(parts[0], 10);
					seconds = parseInt(parts[1], 10);
					break;
				case 3:
					hours = parseInt(parts[0], 10);
					minutes = parseInt(parts[1], 10);
					seconds = parseInt(parts[2], 10);
					break;
				case 4:
					hours = parseInt(parts[0], 10);
					minutes = parseInt(parts[1], 10);
					seconds = parseInt(parts[2], 10);
					frames = parseInt(parts[3], 10);
					break;
			}

			if (isDropFrame(fps)) {
				totalMinutes = 60 * hours + minutes;
				output = hFrames * hours + mFrames * minutes + timeBase * seconds + frames - dropFrames * (totalMinutes - Math.floor(totalMinutes / 10));
			} else {
				output = (hFrames * hours + mFrames * minutes + fps * seconds + frames) / fps;
			}

			return parseFloat(output.toFixed(3));
		}

		function calculateTimeFormat(time, options) {
			var fps = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 25;

			time = !time || typeof time !== 'number' || time < 0 ? 0 : time;

			var hours = Math.floor(time / 3600) % 24,
			    minutes = Math.floor(time / 60) % 60,
			    seconds = Math.floor(time % 60),
			    frames = Math.floor((time % 1 * fps).toFixed(3)),
			    lis = [[frames, 'f'], [seconds, 's'], [minutes, 'm'], [hours, 'h']];

			var format = options.timeFormat,
			    firstTwoPlaces = format[1] === format[0],
			    separatorIndex = firstTwoPlaces ? 2 : 1,
			    separator = format.length < separatorIndex ? format[separatorIndex] : ':',
			    firstChar = format[0],
			    required = false;

			for (var i = 0, len = lis.length; i < len; i++) {
				if (~format.indexOf(lis[i][1])) {
					required = true;
				} else if (required) {
					var hasNextValue = false;
					for (var j = i; j < len; j++) {
						if (lis[j][0] > 0) {
							hasNextValue = true;
							break;
						}
					}

					if (!hasNextValue) {
						break;
					}

					if (!firstTwoPlaces) {
						format = firstChar + format;
					}
					format = lis[i][1] + separator + format;
					if (firstTwoPlaces) {
						format = lis[i][1] + format;
					}
					firstChar = lis[i][1];
				}
			}

			options.timeFormat = format;
		}

		function convertSMPTEtoSeconds(SMPTE) {

			if (typeof SMPTE !== 'string') {
				throw new TypeError('Argument must be a string value');
			}

			SMPTE = SMPTE.replace(',', '.');

			var decimalLen = ~SMPTE.indexOf('.') ? SMPTE.split('.')[1].length : 0;

			var secs = 0,
			    multiplier = 1;

			SMPTE = SMPTE.split(':').reverse();

			for (var i = 0, total = SMPTE.length; i < total; i++) {
				multiplier = 1;
				if (i > 0) {
					multiplier = Math.pow(60, i);
				}
				secs += Number(SMPTE[i]) * multiplier;
			}
			return Number(secs.toFixed(decimalLen));
		}

		_mejs2.default.Utils = _mejs2.default.Utils || {};
		_mejs2.default.Utils.secondsToTimeCode = secondsToTimeCode;
		_mejs2.default.Utils.timeCodeToSeconds = timeCodeToSeconds;
		_mejs2.default.Utils.calculateTimeFormat = calculateTimeFormat;
		_mejs2.default.Utils.convertSMPTEtoSeconds = convertSMPTEtoSeconds;
	}, { "7": 7 }] }, {}, [29, 6, 5, 15, 23, 20, 19, 21, 22, 24, 16, 18, 17, 9, 10, 11, 12, 13, 14]);
},{}],"..\\node_modules\\mediaelement\\full.js":[function(require,module,exports) {
module.exports = require('./build/mediaelement-and-player.js');
},{"./build/mediaelement-and-player.js":"..\\node_modules\\mediaelement\\build\\mediaelement-and-player.js"}],"scripts\\constructors\\showVideosConstr.js":[function(require,module,exports) {
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ShowVideosSection = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _magnificPopup = require('magnific-popup');

var _magnificPopup2 = _interopRequireDefault(_magnificPopup);

require('../../common_libraries/jquery-global');

var _mediaelement = require('mediaelement');

var _mediaelement2 = _interopRequireDefault(_mediaelement);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var J = jQuery.noConflict();

var ShowVideosConstr = function () {
    function ShowVideosConstr(videosSection) {
        (0, _classCallCheck3.default)(this, ShowVideosConstr);

        this.videosSection = videosSection;
        this.videoCardsContainer;
        this.siteUrl;
        this.siteHash;
    }

    (0, _createClass3.default)(ShowVideosConstr, [{
        key: 'initShowVideosSection',
        value: function initShowVideosSection() {
            this.siteUrl = window.location.href, this.siteHash = window.location.hash, this.fetchGetVideosData();
        }
    }, {
        key: 'fetchGetVideosData',
        value: function fetchGetVideosData() {
            var _this = this;

            fetch('https://2019.m24.ru/blue/frontend/web/get').then(function (res) {
                return res.json();
            }).then(function (data) {
                return _this.showVideoCardsFromData(data);
            }).catch(function (err) {
                return console.log(err);
            });
        }
    }, {
        key: 'showVideoCardsFromData',
        value: function showVideoCardsFromData(videoCards) {
            var _this2 = this;

            this.videoCardsContainer = document.getElementById('participants');

            if (!videoCards.length) {
                this.videoCardsContainer.visibility = 'hidden';
            }

            videoCards.forEach(function (videoCard) {

                var videoCardCorrectTitr = videoCard.titr;

                if (/"/.test(videoCard.titr)) {
                    var videoCardTitr = videoCard.titr.replace(/"/g, "\'");
                    videoCard.titr = videoCardTitr;
                    videoCardCorrectTitr = videoCardTitr.replace(/'/g, "\"");
                }

                var divCol = document.createElement('div');
                divCol.className = 'col-md-4 col-sm-12';
                divCol.innerHTML = '\n               <div class="entry">\n                  <a class="ptVid" href="#vid-' + videoCard.id + '" data-name="' + videoCard.name + '" data-title="' + videoCard.titr + '" data-desc="' + videoCard.comment + '" data-video-url="' + videoCard.film + '">\n                  <span class="c-img">\n                      <img alt="" src="' + videoCard.film_img + '" />\n                  </span>\n                  <span class="c-info">\n                      <span class="c-titr">' + videoCardCorrectTitr + '</span>\n                      <span class="c-comment">\n                          ' + videoCard.comment + '\n                      </span>\n                  </span>\n                  </a>\n               </div>';

                _this2.videoCardsContainer.appendChild(divCol);

                _this2.detailVideoCardPopup();
            });

            this.openVideoCardWithSiteHash();
        }
    }, {
        key: 'detailVideoCardPopup',
        value: function detailVideoCardPopup() {

            J('.ptVid').each(function () {

                var video = '';
                if (J(this).data('video-url').indexOf('m3u8') > -1) {
                    video = '<video class="video popup-video" data-id="' + this.getAttribute('href') + '" autoplay width="980" height="525"><source src="' + J(this).data('video-url') + '" /></video>';
                } else {
                    video = '<iframe src="' + J(this).data('video-url') + '?autoplay=true" width="980" height="525" />';
                }

                J(this).magnificPopup({
                    items: {
                        src: '<div class="popup popup-open" style="background-color: white;"><h2 class="popup-title">' + J(this).data('title') + '</h2>' + video + '<p class="popup-desc">' + J(this).data('name') + '</p><div id="share" class="popup-share"></div></div>',
                        type: 'inline'
                    },
                    removalDelay: 350,
                    mainClass: 'mfp-fade',
                    callbacks: {
                        open: function open() {

                            var thisPopup = J.magnificPopup.instance.contentContainer[0].querySelector('.popup');
                            var thisPopupH2 = thisPopup.querySelector('H2');

                            if (/'/.test(thisPopupH2.textContent)) {
                                var thisPopupH2TextContent = thisPopupH2.textContent.replace(/'/g, "\"");
                                thisPopupH2.textContent = thisPopupH2TextContent;
                            }

                            // let nav = document.querySelector('.thenav')
                            // nav.querySelector('NAV').style.marginRight = '17px'

                            var magnificPopup = J.magnificPopup.instance;
                            var $video = magnificPopup.contentContainer.find('.video');

                            var currentVideoCardImage = void 0;
                            var currentVideoCardH2 = magnificPopup.content[0].querySelector('H2').textContent;

                            var detaiVideoCard = J($video).data('id');
                            history.pushState(null, '', '/' + detaiVideoCard);

                            location.hash = detaiVideoCard;

                            currentVideoCardImage = $('[href="' + detaiVideoCard + '"] .c-img img')[0].src;

                            if ($video.length) {
                                $video.mediaelementplayer();
                            }

                            var share = Ya.share2('share', {
                                content: {
                                    url: 'https://2019.m24.ru/' + location.hash,
                                    title: '\u0423\u0447\u0430\u0441\u0442\u043D\u0438\u043A \u0441\u043F\u0435\u0446\u043F\u0440\u043E\u0435\u043A\u0442\u0430 \u041C\u043E\u0441\u043A\u0432\u0430 24 "\u0421\u0435\u043C\u0435\u0439\u043D\u044B\u0439 \u043E\u0433\u043E\u043D\u0451\u043A": ' + currentVideoCardH2
                                    // image: currentVideoCardImage
                                },
                                theme: {
                                    services: 'vkontakte,facebook,twitter,odnoklassniki'
                                }
                            });
                        },
                        close: function close() {

                            var thisPopup = J.magnificPopup.instance.contentContainer[0].querySelector('.popup');
                            history.pushState(null, '', '/');
                            // let nav = document.querySelector('.thenav')
                            // nav.querySelector('NAV').style.marginRight = '0'
                        }
                    }
                });
            });
        }
    }, {
        key: 'openVideoCardWithSiteHash',
        value: function openVideoCardWithSiteHash() {
            var _this3 = this;

            if (this.siteUrl.indexOf('#vid') != -1) {

                console.log('open magnific popup with hash');

                var ptVidsArr = Array.from(document.querySelectorAll('.ptVid'));

                ptVidsArr.forEach(function (ptVid) {

                    if (ptVid.getAttribute('href') == _this3.siteHash) {

                        var video = '';
                        if (J(ptVid).data('video-url').indexOf('m3u8') > -1) {
                            video = '<video class="video popup-video" data-id="' + ptVid.getAttribute('href') + '" autoplay width="980" height="525"><source src="' + J(ptVid).data('video-url') + '" /></video>';
                        } else {
                            video = '<iframe src="' + J(ptVid).data('video-url') + '?autoplay=true" width="980" height="525" />';
                        }

                        J.magnificPopup.open({
                            items: {
                                src: '<div class="popup popup-open" style="background-color: white;"><h2 class="popup-title">' + J(ptVid).data('title') + '</h2>' + video + '<p class="popup-desc">' + J(ptVid).data('desc') + '</p><div id="share" class="popup-share"></div></div>',
                                type: 'inline'
                            },
                            removalDelay: 350,
                            mainClass: 'mfp-fade',
                            callbacks: {
                                open: function open() {

                                    var thisPopup = J.magnificPopup.instance.contentContainer[0].querySelector('.popup');
                                    var thisPopupH2 = thisPopup.querySelector('H2');

                                    if (/'/.test(thisPopupH2.textContent)) {
                                        var thisPopupH2TextContent = thisPopupH2.textContent.replace(/'/g, "\"");
                                        thisPopupH2.textContent = thisPopupH2TextContent;
                                    }

                                    var magnificPopup = J.magnificPopup.instance;
                                    var $video = magnificPopup.contentContainer.find('.video');

                                    var detaiVideoCard = J($video).data('id');
                                    history.pushState(null, '', '/' + detaiVideoCard);

                                    var currentVideoCardImage = void 0;
                                    var currentVideoCardH2 = magnificPopup.content[0].querySelector('H2').textContent;

                                    currentVideoCardImage = $('[href="' + detaiVideoCard + '"] .c-img img')[0].src;

                                    location.hash = detaiVideoCard;

                                    if ($video.length) {
                                        $video.mediaelementplayer();
                                    }

                                    var share = Ya.share2('share', {
                                        content: {
                                            url: 'https://2019.m24.ru/' + location.hash,
                                            title: '\u0423\u0447\u0430\u0441\u0442\u043D\u0438\u043A \u0441\u043F\u0435\u0446\u043F\u0440\u043E\u0435\u043A\u0442\u0430 \u041C\u043E\u0441\u043A\u0432\u0430 24 "\u0421\u0435\u043C\u0435\u0439\u043D\u044B\u0439 \u043E\u0433\u043E\u043D\u0451\u043A": ' + currentVideoCardH2
                                            // image: currentVideoCardImage
                                        },
                                        theme: {
                                            services: 'vkontakte,facebook,twitter,odnoklassniki'
                                        }
                                    });
                                },
                                close: function close() {
                                    history.pushState(null, '', '/');
                                }
                            }
                        });
                    }
                });
            }
        }
    }]);
    return ShowVideosConstr;
}();

var ShowVideosSection = exports.ShowVideosSection = new ShowVideosConstr(document.querySelector('#videos'));
},{"babel-runtime/helpers/classCallCheck":"..\\node_modules\\babel-runtime\\helpers\\classCallCheck.js","babel-runtime/helpers/createClass":"..\\node_modules\\babel-runtime\\helpers\\createClass.js","magnific-popup":"..\\node_modules\\magnific-popup\\dist\\jquery.magnific-popup.js","../../common_libraries/jquery-global":"common_libraries\\jquery-global.js","mediaelement":"..\\node_modules\\mediaelement\\full.js"}],"..\\node_modules\\dm-file-uploader\\src\\js\\jquery.dm-uploader.js":[function(require,module,exports) {
var define;
/*
 * dmUploader - jQuery Ajax File Uploader Widget
 * https://github.com/danielm/uploader
 *
 * Copyright Daniel Morales <daniel85mg@gmail.com>
 * Released under the MIT license.
 * https://github.com/danielm/uploader/blob/master/LICENSE.txt
 *
 * @preserve
 */

/* global define, define, window, document, FormData */

(function(factory) {
  "use strict";
  if (typeof define === "function" && define.amd) {
    // AMD. Register as an anonymous module.
    define(["jquery"], factory);
  } else if (typeof exports !== "undefined") {
    module.exports = factory(require("jquery"));
  } else {
    // Browser globals
    factory(window.jQuery);
  }
}(function($) {
  "use strict";

  var pluginName = "dmUploader";

  var FileStatus = {
    PENDING: 0,
    UPLOADING: 1,
    COMPLETED: 2,
    FAILED: 3,
    CANCELLED: 4 //(by the user)
  };

  // These are the plugin defaults values
  var defaults = {
    auto: true,
    queue: true,
    dnd: true,
    hookDocument: true,
    multiple: true,
    url: document.URL,
    method: "POST",
    extraData: {},
    headers: {},
    dataType: null,
    fieldName: "file",
    maxFileSize: 0,
    allowedTypes: "*",
    extFilter: null,
    onInit: function(){},
    onComplete: function(){},
    onFallbackMode: function() {},
    onNewFile: function(){},        //params: id, file
    onBeforeUpload: function(){},   //params: id
    onUploadProgress: function(){}, //params: id, percent
    onUploadSuccess: function(){},  //params: id, data
    onUploadCanceled: function(){}, //params: id
    onUploadError: function(){},    //params: id, xhr, status, message
    onUploadComplete: function(){}, //params: id
    onFileTypeError: function(){},  //params: file
    onFileSizeError: function(){},  //params: file
    onFileExtError: function(){},   //params: file
    onDragEnter: function(){},
    onDragLeave: function(){},
    onDocumentDragEnter: function(){},
    onDocumentDragLeave: function(){}
  };
  
  var DmUploaderFile = function(file, widget)
  {
    this.data = file;

    this.widget = widget;

    this.jqXHR = null;

    this.status = FileStatus.PENDING;

    // The file id doesnt have to bo that special.... or not?
    this.id = Math.random().toString(36).substr(2);
  };

  DmUploaderFile.prototype.upload = function()
  {
    var file = this;

    if (!file.canUpload()) {

      if (file.widget.queueRunning && file.status !== FileStatus.UPLOADING) {
        file.widget.processQueue();
      }

      return false;
    }

    // Form Data
    var fd = new FormData();
    fd.append(file.widget.settings.fieldName, file.data);

    // Append extra Form Data
    var customData = file.widget.settings.extraData;
    if (typeof(customData) === "function") {
      customData = customData.call(file.widget.element, file.id);
    }

    $.each(customData, function(exKey, exVal) {
      fd.append(exKey, exVal);
    });

    file.status = FileStatus.UPLOADING;
    file.widget.activeFiles++;

    file.widget.settings.onBeforeUpload.call(file.widget.element, file.id);

    // Ajax Submit
    file.jqXHR = $.ajax({
      url: file.widget.settings.url,
      type: file.widget.settings.method,
      dataType: file.widget.settings.dataType,
      data: fd,
      headers: file.widget.settings.headers,
      cache: false,
      contentType: false,
      processData: false,
      forceSync: false,
      xhr: function() { return file.getXhr(); },
      success: function(data) { file.onSuccess(data); },
      error: function(xhr, status, errMsg) { file.onError(xhr, status, errMsg); },
      complete: function() { file.onComplete(); },
    });

    return true;
  };

  DmUploaderFile.prototype.onSuccess = function(data)
  {
    this.status = FileStatus.COMPLETED;
    this.widget.settings.onUploadSuccess.call(this.widget.element, this.id, data);
  };

  DmUploaderFile.prototype.onError = function(xhr, status, errMsg)
  {
    // If the status is: cancelled (by the user) don't invoke the error callback
    if (this.status !== FileStatus.CANCELLED) {
      this.status = FileStatus.FAILED;
      this.widget.settings.onUploadError.call(this.widget.element, this.id, xhr, status, errMsg);
    }
  };

  DmUploaderFile.prototype.onComplete = function()
  {
    this.widget.activeFiles--;

    if (this.status !== FileStatus.CANCELLED) {
      this.widget.settings.onUploadComplete.call(this.widget.element, this.id);
    }

    if (this.widget.queueRunning) {
      this.widget.processQueue();
    } else if (this.widget.settings.queue && this.widget.activeFiles === 0) {
      this.widget.settings.onComplete.call(this.element);
    }
  };

  DmUploaderFile.prototype.getXhr = function()
  {
    var file = this;
    var xhrobj = $.ajaxSettings.xhr();

    if (xhrobj.upload) {
      xhrobj.upload.addEventListener("progress", function(event) {
        var percent = 0;
        var position = event.loaded || event.position;
        var total = event.total || event.totalSize;

        if (event.lengthComputable) {
          percent = Math.ceil(position / total * 100);
        }
        file.widget.settings.onUploadProgress.call(file.widget.element, file.id, percent);
      }, false);
    }

    return xhrobj;
  };

  DmUploaderFile.prototype.cancel = function(abort)
  {
    // The abort flag is to track if we are calling this function directly (using the cancel Method, by id)
    // or the call comes from the 'gobal' method aka cancelAll.
    // THis mean that we don't want to trigger the cancel event on file that isn't uploading, UNLESS directly doing it
    // ... and yes, it could be done prettier. Review (?)
    abort = (typeof abort === "undefined" ? false : abort);

    var myStatus = this.status;

    if (myStatus === FileStatus.UPLOADING || (abort && myStatus === FileStatus.PENDING)) {
      this.status = FileStatus.CANCELLED;
    } else {
      return false;
    }

    this.widget.settings.onUploadCanceled.call(this.widget.element, this.id);

    if (myStatus === FileStatus.UPLOADING) {
      this.jqXHR.abort();
    }

    return true;
  };

  DmUploaderFile.prototype.canUpload = function()
  {
    return (
      this.status === FileStatus.PENDING ||
      this.status === FileStatus.FAILED
    );
  };

  var DmUploader = function(element, options)
  {
    this.element = $(element);
    this.settings = $.extend({}, defaults, options);

    if (!this.checkSupport()) {
      $.error("Browser not supported by jQuery.dmUploader");

      this.settings.onFallbackMode.call(this.element);

      return false;
    }

    this.init();

    return this;
  };

  DmUploader.prototype.checkSupport = function()
  {
    // This one is mandatory for all modes
    if (typeof window.FormData === "undefined") {
      return false;
    }

    // Test based on: Modernizr/feature-detects/forms/fileinput.js
    var exp = new RegExp(
      "/(Android (1.0|1.1|1.5|1.6|2.0|2.1))|"+
      "(Windows Phone (OS 7|8.0))|(XBLWP)|"+
      "(ZuneWP)|(w(eb)?OSBrowser)|(webOS)|"+
      "(Kindle\/(1.0|2.0|2.5|3.0))/");

    if (exp.test(window.navigator.userAgent)) {
      return false;
    }

    return !$("<input type=\"file\" />").prop("disabled");
  };

  DmUploader.prototype.init = function()
  {
    var widget = this;

    // Queue vars
    this.queue = [];
    this.queuePos = -1;
    this.queueRunning = false;
    this.activeFiles = 0;
    this.draggingOver = 0;
    this.draggingOverDoc = 0;

    var input = widget.element.is("input[type=file]") ?
      widget.element : widget.element.find("input[type=file]");

    //-- Is the input our main element itself??
    if (input.length > 0) {
      input.prop("multiple", this.settings.multiple);

      // Or does it has the input as a child
      input.on("change." + pluginName, function(evt) {
        var files = evt.target && evt.target.files;

        if (!files || !files.length){
          return;
        }

        widget.addFiles(files);

        $(this).val("");
      });
    }

    if (this.settings.dnd) {
      this.initDnD();
    }

    if (input.length === 0 && !this.settings.dnd) {
      // Trigger an error because if this happens the plugin wont do anything.
      $.error("Markup error found by jQuery.dmUploader");

      return null;
    }

    // We good to go, tell them!
    this.settings.onInit.call(this.element);

    return this;
  };

  DmUploader.prototype.initDnD = function()
  {
    var widget = this;

    // -- Now our own Drop
    widget.element.on("drop." + pluginName, function (evt) {
      evt.preventDefault();

      if (widget.draggingOver > 0){
        widget.draggingOver = 0;
        widget.settings.onDragLeave.call(widget.element);
      }

      var dataTransfer = evt.originalEvent && evt.originalEvent.dataTransfer;
      if (!dataTransfer || !dataTransfer.files || !dataTransfer.files.length) {
        return;
      }

      // Take only the first file if not acepting multiple, this is kinda ugly. Needs Review ?
      var files = [];

      if (widget.settings.multiple) {
        files = dataTransfer.files;
      } else {
        files.push(dataTransfer.files[0]);
      }

      widget.addFiles(files);
    });

    //-- These two events/callbacks are onlt to maybe do some fancy visual stuff
    widget.element.on("dragenter." + pluginName, function(evt) {
      evt.preventDefault();

      if (widget.draggingOver === 0){
        widget.settings.onDragEnter.call(widget.element);
      }

      widget.draggingOver++;
    });

    widget.element.on("dragleave." + pluginName, function(evt) {
      evt.preventDefault();

      widget.draggingOver--;

      if (widget.draggingOver === 0){
        widget.settings.onDragLeave.call(widget.element);
      }
    });

    if (!widget.settings.hookDocument) {
      return;
    }

    // Adding some off/namepacing to prevent some weird cases when people use multiple instances
    $(document).off("drop." + pluginName).on("drop." + pluginName, function(evt) {
      evt.preventDefault();

      if (widget.draggingOverDoc > 0){
        widget.draggingOverDoc = 0;
        widget.settings.onDocumentDragLeave.call(widget.element);
      }
    });

    $(document).off("dragenter." + pluginName).on("dragenter." + pluginName, function(evt) {
      evt.preventDefault();

      if (widget.draggingOverDoc === 0){
        widget.settings.onDocumentDragEnter.call(widget.element);
      }

      widget.draggingOverDoc++;
    });

    $(document).off("dragleave." + pluginName).on("dragleave." + pluginName, function(evt) {
      evt.preventDefault();

      widget.draggingOverDoc--;

      if (widget.draggingOverDoc === 0){
        widget.settings.onDocumentDragLeave.call(widget.element);
      }
    });

    $(document).off("dragover." + pluginName).on("dragover." + pluginName, function(evt) {
      evt.preventDefault();
    });
  };

  DmUploader.prototype.releaseEvents = function() {
    // Leave everyone ALONE ;_;

    this.element.off("." + pluginName);
    this.element.find("input[type=file]").off("." + pluginName);

    if (this.settings.hookDocument) {
      $(document).off("." + pluginName);
    }
  };

  DmUploader.prototype.validateFile = function(file)
  {
    // Check file size
    if ((this.settings.maxFileSize > 0) &&
        (file.size > this.settings.maxFileSize)) {

      this.settings.onFileSizeError.call(this.element, file);

      return false;
    }

    // Check file type
    if ((this.settings.allowedTypes !== "*") &&
        !file.type.match(this.settings.allowedTypes)){

      this.settings.onFileTypeError.call(this.element, file);

      return false;
    }

    // Check file extension
    if (this.settings.extFilter !== null) {
      var ext = file.name.toLowerCase().split(".").pop();

      if ($.inArray(ext, this.settings.extFilter) < 0) {
        this.settings.onFileExtError.call(this.element, file);

        return false;
      }
    }

    return new DmUploaderFile(file, this);
  };

  DmUploader.prototype.addFiles = function(files)
  {
    var nFiles = 0;

    for (var i= 0; i < files.length; i++)
    {
      var file = this.validateFile(files[i]);

      if (!file){
        continue;
      }

      // If the callback returns false file will not be processed. This may allow some customization
      var can_continue = this.settings.onNewFile.call(this.element, file.id, file.data);
      if (can_continue === false) {
        continue;
      }

      // If we are using automatic uploading, and not a file queue: go for the upload
      if (this.settings.auto && !this.settings.queue) {
        file.upload();
      }

      this.queue.push(file);
      
      nFiles++;
    }

    // No files were added
    if (nFiles === 0) {
      return this;
    }

    // Are we auto-uploading files?
    if (this.settings.auto && this.settings.queue && !this.queueRunning) {
      this.processQueue();
    }

    return this;
  };

  DmUploader.prototype.processQueue = function()
  {
    this.queuePos++;

    if (this.queuePos >= this.queue.length) {
      if (this.activeFiles === 0) {
        this.settings.onComplete.call(this.element);
      }

      // Wait until new files are droped
      this.queuePos = (this.queue.length - 1);

      this.queueRunning = false;

      return false;
    }

    this.queueRunning = true;

    // Start next file
    return this.queue[this.queuePos].upload();
  };

  DmUploader.prototype.restartQueue = function()
  {
    this.queuePos = -1;
    this.queueRunning = false;

    this.processQueue();
  };

  DmUploader.prototype.findById = function(id)
  {
    var r = false;

    for (var i = 0; i < this.queue.length; i++) {
      if (this.queue[i].id === id) {
        r = this.queue[i];
        break;
      }
    }

    return r;
  };

  DmUploader.prototype.cancelAll =  function()
  {
    var queueWasRunning = this.queueRunning;
    this.queueRunning = false;

    // cancel 'em all
    for (var i = 0; i < this.queue.length; i++) {
      this.queue[i].cancel();
    }

    if (queueWasRunning) {
      this.settings.onComplete.call(this.element);
    }
  };

  DmUploader.prototype.startAll = function()
  {
    if (this.settings.queue) {
      // Resume queue
      this.restartQueue();
    } else {
      // or upload them all
      for (var i = 0; i < this.queue.length; i++) {
        this.queue[i].upload();
      }
    }
  };

  // Public API methods
  DmUploader.prototype.methods = {
    start: function(id) {
      if (this.queueRunning){
        // Do not allow to manually upload Files when a queue is running
        return false;
      }

      var file = false;

      if (typeof id !== "undefined") {
        file = this.findById(id);

        if (!file) {
          // File not found in stack
          $.error("File not found in jQuery.dmUploader");

          return false;
        }
      }
      
      // Trying to Start an upload by ID
      if (file) {
        if (file.status === FileStatus.CANCELLED) {
          file.status = FileStatus.PENDING;
        }
        return file.upload();
      }

      // With no id provided...

      this.startAll();

      return true;
    },
    cancel: function(id) {
      var file = false;
      if (typeof id !== "undefined") {
        file = this.findById(id);

        if (!file) {
          // File not found in stack
          $.error("File not found in jQuery.dmUploader");

          return false;
        }
      }

      if (file) {
        return file.cancel(true);
      }

      // With no id provided...
      
      this.cancelAll();

      return true;
    },
    reset: function() {

      this.cancelAll();

      this.queue = [];
      this.queuePos = -1;
      this.activeFiles = 0;

      return true;
    },
    destroy: function() {
      this.cancelAll();

      this.releaseEvents();

      this.element.removeData(pluginName);
    }
  };

  $.fn.dmUploader = function(options) {
    var args = arguments;

    if (typeof options === "string") {
      this.each(function() {
        var plugin = $.data(this, pluginName);

        if (plugin instanceof DmUploader) {
          if (typeof plugin.methods[options] === "function") {
            plugin.methods[options].apply(plugin, Array.prototype.slice.call(args, 1));
          } else {
            $.error("Method " +  options + " does not exist in jQuery.dmUploader");
          }
        } else {
          $.error("Unknown plugin data found by jQuery.dmUploader");
        }
      });
    } else {
      return this.each(function () {
        if (!$.data(this, pluginName)) {
          $.data(this, pluginName, new DmUploader(this, options));
        }
      });
    }
  };
}));
},{"jquery":"..\\node_modules\\jquery\\dist\\jquery.js"}],"scripts\\main.js":[function(require,module,exports) {
'use strict';

var _formConstr = require('./constructors/formConstr');

var _showVideosConstr = require('./constructors/showVideosConstr');

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _dmFileUploader = require('dm-file-uploader');

var _dmFileUploader2 = _interopRequireDefault(_dmFileUploader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Libraries
// Custom modules
function contentReady() {

    if (document.getElementById('form')) {

        _formConstr.VideoUploadingForm.initForm();
        _formConstr.VideoUploadingForm.submitForm();
    }

    if (document.getElementById('videos')) {

        _showVideosConstr.ShowVideosSection.initShowVideosSection();
    }
}

document.addEventListener('DOMContentLoaded', contentReady);

// Adds an entry to our debug area
function ui_add_log(message, color) {
    var d = new Date();
    var dateString = ('0' + d.getHours()).slice(-2) + ':' + ('0' + d.getMinutes()).slice(-2) + ':' + ('0' + d.getSeconds()).slice(-2);
    color = typeof color === 'undefined' ? 'muted' : color;
    var template = (0, _jquery2.default)('#debug-template').text();
    template = template.replace('%%date%%', dateString);
    template = template.replace('%%message%%', message);
    template = template.replace('%%color%%', color);
    (0, _jquery2.default)('#debug').find('li.empty').fadeOut(); // remove the 'no messages yet'
    (0, _jquery2.default)('#debug').prepend(template);
}
// Creates a new file and add it to our list
function ui_multi_add_file(id, file) {
    var template = (0, _jquery2.default)('#files-template').text();
    template = template.replace('%%filename%%', file.name);
    template = (0, _jquery2.default)(template);
    template.prop('id', 'uploaderFile' + id);
    template.data('file-id', id);
    (0, _jquery2.default)('#files').find('li.empty').fadeOut(); // remove the 'no files yet'
    (0, _jquery2.default)('#files').html(template);
}
// Changes the status messages on our list
function ui_multi_update_file_status(id, status, message) {
    (0, _jquery2.default)('#uploaderFile' + id).find('span').html(message).prop('class', 'status text-' + status);
}
// Updates a file progress, depending on the parameters it may animate it or change the color.
function ui_multi_update_file_progress(id, percent, color, active) {
    color = typeof color === 'undefined' ? false : color;
    active = typeof active === 'undefined' ? true : active;
    var bar = (0, _jquery2.default)('#uploaderFile' + id).find('div.progress-bar');
    bar.width(percent + '%').attr('aria-valuenow', percent);
    bar.toggleClass('progress-bar-striped progress-bar-animated', active);
    if (percent === 0) {
        bar.html('');
    } else {
        bar.html(percent + '%');
    }
    if (color !== false) {
        bar.removeClass('bg-success bg-info bg-warning bg-danger');
        bar.addClass('bg-' + color);
    }
}
(0, _jquery2.default)(function () {
    (0, _jquery2.default)('#drag-and-drop-zone').dmUploader({ //
        url: 'backend/upload.php',
        extFilter: ["mov", "flv", "mpeg", "mpg", "mkv", "m4v", "mp4", "avi", "wmv", "mpegps", "3gp", "ogv", "ogg"],
        maxFileSize: 1000000000, // 3 Megs'
        multiple: false,
        onDragEnter: function onDragEnter() {
            // Happens when dragging something over the DnD area
            document.querySelector('#drag-and-drop-zone').style.border = '5px solid purple';
            this.addClass('active');
        },
        onDragLeave: function onDragLeave() {
            // Happens when dragging something OUT of the DnD area
            document.querySelector('#drag-and-drop-zone').style.border = '5px dashed pink';
            this.removeClass('active');
            document.querySelector('.video-uploading-form__submit').setAttribute('disabled', true);
        },
        onInit: function onInit() {
            // Plugin is ready to use
            ui_add_log('Penguin initialized :)', 'info');
        },
        onComplete: function onComplete() {
            // All files in the queue are processed (success or error)
            // ui_add_log('All pending tranfers finished');
        },
        onNewFile: function onNewFile(id, file) {
            // When a new file is added using the file selector or the DnD area
            ui_add_log('New file added #' + id);
            ui_multi_add_file(id, file);
        },
        onBeforeUpload: function onBeforeUpload(id) {
            // about tho start uploading a file
            ui_add_log('Starting the upload of #' + id);
            ui_multi_update_file_status(id, 'uploading', 'Uploading...');
            ui_multi_update_file_progress(id, 0, '', true);
        },
        onUploadCanceled: function onUploadCanceled(id) {
            // Happens when a file is directly canceled by the user.
            ui_multi_update_file_status(id, 'warning', 'Canceled by User');
            ui_multi_update_file_progress(id, 0, 'warning', false);
        },
        onUploadProgress: function onUploadProgress(id, percent) {
            // Updating file progress
            ui_multi_update_file_progress(id, percent);
        },
        onUploadSuccess: function onUploadSuccess(id, data) {
            document.querySelector('.video-uploading-form__input-box--linkUploaded').style.visibility = 'hidden';
            // A file was successfully uploaded
            document.querySelector('.video-uploading-form__submit').removeAttribute('disabled');
            var inputFileUploade = document.querySelector('.inputFileAnchor');
            inputFileUploade.value = data.file_name;

            ui_add_log('Server Response for file #' + id + ': ' + JSON.stringify(data));
            ui_add_log('Upload of file #' + id + ' COMPLETED', 'success');
            ui_multi_update_file_status(id, 'success', 'Файл загружен успешно');
            ui_multi_update_file_progress(id, 100, 'success', false);
        },
        onUploadError: function onUploadError(id, xhr, status, message) {
            document.querySelector('.video-uploading-form__input-box--linkUploaded').style.visibility = 'visible';
            ui_multi_update_file_status(id, 'danger', message);
            ui_multi_update_file_progress(id, 0, 'danger', false);
        },
        onFallbackMode: function onFallbackMode() {
            // When the browser doesn't support this plugin :(
            ui_add_log('Plugin cant be used here, running Fallback callback', 'danger');
        },
        onFileSizeError: function onFileSizeError(file) {
            document.querySelector('.video-uploading-form__input-box--linkUploaded').style.visibility = 'visible';
            // ui_add_log('Файл \'' + file.name + '\' не может быть добавлен. Слишком большой размер', 'danger');
            modalSizeError(file.name);
        },
        onFileExtError: function onFileExtError(file) {
            modalExtError(file.name);
        }
    });

    function modalExtError(file) {
        (0, _jquery2.default)(".modalResolveReject").html('<div class="m-overlay" id="bg-close"><h5>\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430 \u0444\u0430\u0439\u043B\u0430 \u043E\u0441\u0442\u0430\u043D\u043E\u0432\u043B\u0435\u043D\u0430.<br/> \u0412\u0430\u0448 \u0444\u0430\u0439\u043B ' + file + ' \u043D\u0435 \u0443\u0434\u043E\u0432\u043B\u0435\u0442\u0432\u043E\u0440\u044F\u0435\u0442 \u0442\u0440\u0435\u0431\u043E\u0432\u0430\u043D\u0438\u0435 \u043F\u043E \u0432\u0438\u0434\u0435\u043E \u0444\u043E\u0440\u043C\u0430\u0442\u0430\u043C:<br/> mov, flv, mpeg, mpg, mkv, m4v, mp4, avi, wmv, mpegps, 3gp, ogv, ogg<i id="close-m"></i></h5></div>').show("slow");
        (0, _jquery2.default)("#close-m").click(function () {
            (0, _jquery2.default)(".modalResolveReject").hide("slow");
        });
        (0, _jquery2.default)("#bg-close").click(function (event) {
            event.preventDefault();
            (0, _jquery2.default)(".modalResolveReject").hide();
        });
    }
    function modalSizeError(file) {
        (0, _jquery2.default)(".modalResolveReject").html('<div class="m-overlay" id="bg-close"><h5>\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430 \u0444\u0430\u0439\u043B\u0430 \u043E\u0441\u0442\u0430\u043D\u043E\u0432\u043B\u0435\u043D\u0430.<br/> \u0412\u0430\u0448 \u0444\u0430\u0439\u043B ' + file + ' \u043F\u0440\u0435\u0432\u044B\u0448\u0430\u0435\u0442 \u0434\u043E\u043F\u0443\u0441\u0442\u0438\u043C\u0443\u044E \u0432\u0435\u043B\u0438\u0447\u0438\u043D\u0443 \u0432 1\u0413\u0411<i id="close-m"></i></h5></div>').show("slow");
        (0, _jquery2.default)("#close-m").click(function () {
            (0, _jquery2.default)(".modalResolveReject").hide("slow");
        });
        (0, _jquery2.default)("#bg-close").click(function (event) {
            event.preventDefault();
            (0, _jquery2.default)(".modalResolveReject").hide();
        });
    }
});
},{"./constructors/formConstr":"scripts\\constructors\\formConstr.js","./constructors/showVideosConstr":"scripts\\constructors\\showVideosConstr.js","jquery":"..\\node_modules\\jquery\\dist\\jquery.js","dm-file-uploader":"..\\node_modules\\dm-file-uploader\\src\\js\\jquery.dm-uploader.js"}],"C:\\Users\\Алексей\\AppData\\Roaming\\npm\\node_modules\\parcel-bundler\\src\\builtins\\hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';

var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };

  module.bundle.hotData = null;
}

module.bundle.Module = Module;

var parent = module.bundle.parent;
if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = '' || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + '13291' + '/');
  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();

      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });

      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();
      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');

      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);

      removeErrorOverlay();

      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);
  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID;

  // html encode message and stack trace
  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;

  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';

  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;
  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];
      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;
  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;
  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};
  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);

  cached = bundle.cache[id];
  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["C:\\Users\\Алексей\\AppData\\Roaming\\npm\\node_modules\\parcel-bundler\\src\\builtins\\hmr-runtime.js","scripts\\main.js"], null)
//# sourceMappingURL=/main.dd725806.map