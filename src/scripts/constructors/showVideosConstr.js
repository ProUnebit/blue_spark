import magnificPopup from 'magnific-popup'
import '../../common_libraries/jquery-global'
import mediaelementplayer from 'mediaelement'

var J = jQuery.noConflict();

class ShowVideosConstr {
    constructor(videosSection) {
        this.videosSection = videosSection;
        this.videoCardsContainer;
        this.siteUrl;
        this.siteHash;
    }

    initShowVideosSection() {
        this.siteUrl = window.location.href,
        this.siteHash = window.location.hash,
        this.fetchGetVideosData()
    }

    fetchGetVideosData() {
        fetch('https://2019.m24.ru/blue/frontend/web/get')
            .then(res => res.json())
            .then(data => this.showVideoCardsFromData(data))
            .catch(err => console.log(err))
    }

    showVideoCardsFromData(videoCards) {

        this.videoCardsContainer = document.getElementById('participants')

        if (!videoCards.length) {
            this.videoCardsContainer.visibility = 'hidden'
        }

        videoCards.forEach(videoCard => {

            let videoCardCorrectTitr = videoCard.titr;

            if (/"/.test(videoCard.titr)) {
                let videoCardTitr = videoCard.titr.replace(/"/g, "\'")
                videoCard.titr = videoCardTitr
                videoCardCorrectTitr = videoCardTitr.replace(/'/g, "\"")
            }

            let divCol = document.createElement('div')
            divCol.className = 'col-md-4 col-sm-12'
            divCol.innerHTML = `
               <div class="entry">
                  <a class="ptVid" href="#vid-${videoCard.id}" data-name="${videoCard.name}" data-title="${videoCard.titr}" data-desc="${videoCard.comment}" data-video-url="${videoCard.film}">
                  <span class="c-img">
                      <img alt="" src="${videoCard.film_img}" />
                  </span>
                  <span class="c-info">
                      <span class="c-titr">${videoCardCorrectTitr}</span>
                      <span class="c-comment">
                          ${videoCard.comment}
                      </span>
                  </span>
                  </a>
               </div>`

            this.videoCardsContainer.appendChild(divCol)

            this.detailVideoCardPopup()
        })

        this.openVideoCardWithSiteHash()
    }

    detailVideoCardPopup() {

        J('.ptVid').each(function() {

            var video = '';
            if (J(this).data('video-url').indexOf('m3u8') > -1) {
                video = '<video class="video popup-video" data-id="' + this.getAttribute('href') + '" autoplay width="980" height="525"><source src="' + J(this).data('video-url') + '" /></video>';
            } else {
                video = '<iframe src="' + J(this).data('video-url') + '?autoplay=true" width="980" height="525" />';
            }

            J(this).magnificPopup({
                items: {
                    src: '<div class="popup popup-open" style="background-color: white;"><h2 class="popup-title">' + J(this).data('title') + '</h2>' + video + '<p class="popup-desc">' + J(this).data('name') + '</p><div id="share" class="popup-share"></div></div>',
                    type: 'inline'
                },
                removalDelay: 350,
                mainClass: 'mfp-fade',
                callbacks: {
                    open: function() {

                        let thisPopup = J.magnificPopup.instance.contentContainer[0].querySelector('.popup')
                        let thisPopupH2 = thisPopup.querySelector('H2')

                        if (/'/.test(thisPopupH2.textContent)) {
                            let thisPopupH2TextContent = thisPopupH2.textContent.replace(/'/g, "\"")
                            thisPopupH2.textContent = thisPopupH2TextContent
                        }

                        // let nav = document.querySelector('.thenav')
                        // nav.querySelector('NAV').style.marginRight = '17px'

                        var magnificPopup = J.magnificPopup.instance;
                        var $video = magnificPopup.contentContainer.find('.video');

                        let currentVideoCardImage;
                        let currentVideoCardH2 = magnificPopup.content[0].querySelector('H2').textContent

                        var detaiVideoCard = J($video).data('id')
                        history.pushState(null, '', `/${detaiVideoCard}`)

                        location.hash = detaiVideoCard

                        currentVideoCardImage = $(`[href="${detaiVideoCard}"] .c-img img`)[0].src

                        if ($video.length) {
                            $video.mediaelementplayer();
                        }

                        var share = Ya.share2('share', {
                            content: {
                                url: 'https://2019.m24.ru/' + location.hash,
                                title: `Участник спецпроекта Москва 24 "Семейный огонёк": ${currentVideoCardH2}`,
                                // image: currentVideoCardImage
                            },
                            theme: {
                                services: 'vkontakte,facebook,twitter,odnoklassniki',
                            }
                        });
                    },
                    close: function() {

                        let thisPopup = J.magnificPopup.instance.contentContainer[0].querySelector('.popup')
                        history.pushState(null, '', '/')
                        // let nav = document.querySelector('.thenav')
                        // nav.querySelector('NAV').style.marginRight = '0'
                    }
                }
            });
        })
    }

    openVideoCardWithSiteHash() {
        if (this.siteUrl.indexOf('#vid') != -1) {

            console.log('open magnific popup with hash');

            let ptVidsArr = Array.from(document.querySelectorAll('.ptVid'))

            ptVidsArr.forEach(ptVid => {

                if (ptVid.getAttribute('href') == this.siteHash) {

                    var video = '';
                    if (J(ptVid).data('video-url').indexOf('m3u8') > -1) {
                        video = '<video class="video popup-video" data-id="' + ptVid.getAttribute('href') + '" autoplay width="980" height="525"><source src="' + J(ptVid).data('video-url') + '" /></video>';
                    } else {
                        video = '<iframe src="' + J(ptVid).data('video-url') + '?autoplay=true" width="980" height="525" />';
                    }

                    J.magnificPopup.open({
                      items: {
                        src: '<div class="popup popup-open" style="background-color: white;"><h2 class="popup-title">' + J(ptVid).data('title') + '</h2>' + video +  '<p class="popup-desc">' + J(ptVid).data('desc') + '</p><div id="share" class="popup-share"></div></div>',
                        type: 'inline'
                      },
                      removalDelay: 350,
                      mainClass: 'mfp-fade',
                      callbacks: {
                          open: function() {

                              let thisPopup = J.magnificPopup.instance.contentContainer[0].querySelector('.popup')
                              let thisPopupH2 = thisPopup.querySelector('H2')

                              if (/'/.test(thisPopupH2.textContent)) {
                                  let thisPopupH2TextContent = thisPopupH2.textContent.replace(/'/g, "\"")
                                  thisPopupH2.textContent = thisPopupH2TextContent
                              }

                              var magnificPopup = J.magnificPopup.instance;
                              var $video = magnificPopup.contentContainer.find('.video');

                              var detaiVideoCard = J($video).data('id')
                              history.pushState(null, '', `/${detaiVideoCard}`)

                              let currentVideoCardImage;
                              let currentVideoCardH2 = magnificPopup.content[0].querySelector('H2').textContent

                              currentVideoCardImage = $(`[href="${detaiVideoCard}"] .c-img img`)[0].src

                              location.hash = detaiVideoCard

                              if ($video.length) {
                                  $video.mediaelementplayer();
                              }

                              var share = Ya.share2('share', {
                                  content: {
                                      url: 'https://2019.m24.ru/' + location.hash,
                                      title: `Участник спецпроекта Москва 24 "Семейный огонёк": ${currentVideoCardH2}`,
                                      // image: currentVideoCardImage
                                  },
                                  theme: {
                                      services: 'vkontakte,facebook,twitter,odnoklassniki',
                                  }
                              });
                          },
                          close: function() {
                              history.pushState(null, '', '/')
                          }
                      }
                    });
                }
            })
        }
    }
}

export let ShowVideosSection = new ShowVideosConstr(document.querySelector('#videos'))
