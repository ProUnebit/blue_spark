import serialize from 'form-serialize'
import IMask from 'imask'
import $ from 'jquery'

class FormConstr {
    constructor(form) {
        this.form = form;
        this.name;
        this.email;
        this.phone;
        this.linkUploaded;
        this.uploaded;
        this.description;
        this.checkToS;
        this.inputsValues;
        this.formValuesAreValid = false;
    }

    initForm() {

        let phoneMask = new IMask(
            this.form.phone, {
            mask: '+{7}(000)000-00-00'
        });
    }

    initInputsNames() {

        const name = this.form.name
        name.removeAttribute('name')
        name.setAttribute('name', 'ListInfo[name]')
        const nameValue = name.value.trim()
        this.name = name

        const email = this.form.email
        email.removeAttribute('name')
        email.setAttribute('name', 'ListInfo[email]')
        const emailValue = email.value.trim()
        this.email = email

        const phone = this.form.phone
        phone.removeAttribute('name')
        phone.setAttribute('name', 'ListInfo[phone]')
        const phoneValue = phone.value.trim()
        this.phone = phone

        const linkUploaded = this.form.linkUploaded
        linkUploaded.removeAttribute('name')
        linkUploaded.setAttribute('name', 'ListInfo[link_uploaded]')
        const linkUploadedValue = linkUploaded.value.trim()
        this.linkUploaded = linkUploaded

        const uploaded = this.form.uploaded
        uploaded.removeAttribute('name')
        uploaded.setAttribute('name', 'ListInfo[uploaded]')
        const uploadedValue = uploaded.value.trim()
        this.uploaded = uploaded

        const description = this.form.description
        description.removeAttribute('name')
        description.setAttribute('name', 'ListInfo[description]')
        const descriptionValue = description.value
        this.description = description

        const checkToS = this.form.ToS
        const checkToSValue = checkToS.value
        this.checkToS = checkToS

        return {
            name: [name, nameValue],
            email: [email, emailValue],
            phone: [phone, phoneValue],
            linkUploaded: [linkUploaded, linkUploadedValue],
            uploaded: [uploaded, uploadedValue],
            description: [description, descriptionValue],
            checkToS: [checkToS, checkToSValue]
        }
    }

    showAlert(message, className, form, inputBox, input) {
        const div = document.createElement('div')

        div.className = `video-uploading-form__helper alert alert-${className}`
        div.appendChild(document.createTextNode(message))

        inputBox.appendChild(div, input)

        document.querySelectorAll('.alert').forEach(alert => {
          alert.style.cursor = 'pointer'
          alert.addEventListener('click', function() {
              this.remove()
          })
        })
        setTimeout(() => document.querySelector('.alert').remove(), 5000);
    }

    returnInputBox(type) {
        return document.querySelector(`.video-uploading-form__input-box--${type}`)
    }

    validateInput(input) {

        let regExp;

        switch (input[0]) {
            case this.name:
                if (input[1].length == '') {
                    return false
                }
                regExp = /^[А-ЯЁа-яёA-Za-z\s]+$/;
                return regExp.test(input[1]);
                break;
            case this.email:
                regExp = /\S+@\S+\.\S+/;
                if (regExp.test(input[1])) {
                    return true;
                }
                break;
            case this.phone:
                if (input[1].length == 16) {
                    return true
                }
                break;
            case this.linkUploaded:
                regExp = /(?:http|https):\/\/((?:[\w-]+)(?:\.[\w-]+)+)(?:[\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
                let booVar = regExp.test(input[1])
                if (booVar) {
                    return true;
                }
                if (!booVar) {
                    return false
                }
                break;
            default:
                return true;
        }
    }

    failValidateScrollUpToFormTop() {
        let destination = $(this.form).offset().top - 80
        $('html').animate({ scrollTop: destination }, 400)
        return false;
    }

    validateForm(...inputs) {

        inputs.map(input => {
            if (!this.validateInput(input)) {

                switch (input[0]) {
                    case this.name:
                        this.showAlert(
                            'Введите валидное имя (только буквы)',
                            'warning',
                            this.form,
                            this.returnInputBox('name'),
                            input[0]
                        );
                        this.formValuesAreValid = false
                        this.failValidateScrollUpToFormTop()
                        break;
                    case this.email:
                        this.showAlert(
                            'Введите валидный адрес эл. почты',
                            'warning',
                            this.form,
                            this.returnInputBox('email'),
                            input[0]
                        );
                        this.formValuesAreValid = false
                        this.failValidateScrollUpToFormTop()
                        break;
                    case this.phone:
                        this.showAlert(
                            'Введите свой телефон',
                            'warning',
                            this.form,
                            this.returnInputBox('phone'),
                            input[0]
                        );
                        this.formValuesAreValid = false
                        this.failValidateScrollUpToFormTop()
                        break;
                    case this.linkUploaded:
                        this.showAlert(
                            'Укажите ссылку или загрузите видео ниже',
                            'warning',
                            this.form,
                            this.returnInputBox('linkUploaded'),
                            input[0]
                        );
                        this.failValidateScrollUpToFormTop()
                        break;
                    case this.uploaded:
                        this.showAlert(
                            'Загрузите ваше видео',
                            'warning',
                            this.form,
                            this.returnInputBox('uploaded'),
                            input[0]
                        );
                        break;
                    default:
                        break;
                }
            } else if (this.validateInput(input)) {

                switch (input[0]) {
                    case this.name:
                        this.formValuesAreValid = true
                        break;
                    case this.email:
                        this.formValuesAreValid = true
                        break;
                    case this.phone:
                        this.formValuesAreValid = true
                        break;
                    case this.linkUploaded:
                        break;
                    case this.uploaded:
                        break;
                    default:
                        break;
            }}

            if (input[1].trim() == '') {
                switch (input[0]) {
                    case this.name:
                        this.showAlert(
                            'Необходимо ввести имя',
                            'danger',
                            this.form,
                            this.returnInputBox('name'),
                            input[0]
                        );
                        this.formValuesAreValid = false
                        this.failValidateScrollUpToFormTop()
                        break;
                    case this.email:
                        this.showAlert(
                            'Необходимо ввести адрес эл. почты',
                            'danger',
                            this.form,
                            this.returnInputBox('email'),
                            input[0]
                        );
                        this.formValuesAreValid = false
                        this.failValidateScrollUpToFormTop()
                        break;
                    case this.phone:
                        this.showAlert(
                            'Необходимо ввести свой телефон',
                            'danger',
                            this.form,
                            this.returnInputBox('phone'),
                            input[0]
                        );
                        this.formValuesAreValid = false
                        this.failValidateScrollUpToFormTop()
                        break;
                    case this.linkUploaded:
                        this.showAlert(
                            'Укажите ссылку на загруженное видео',
                            'danger',
                            this.form,
                            this.returnInputBox('linkUploaded'),
                            input[0]
                            );
                        break;
                    case this.uploaded:
                        this.showAlert(
                            'Необходимо загрузить видео',
                            'danger',
                            this.form,
                            this.returnInputBox('uploaded'),
                            input[0]
                            );
                        break;
                    default:
                        break;
                }
            }
        })

        if (this.inputsValues.name[1] == '') {
            return;
        }
        let anyNumRegExp = /-?\d+\.?\d*/;
        if (anyNumRegExp.test(this.inputsValues.name[1])) {
            return;
        }


        if (this.inputsValues.email[1] == '') {
            return;
        }

        if (this.inputsValues.linkUploaded[1] == '' && this.inputsValues.uploaded[1] == '') {
            // this.modalNoFileError()
            return;
        }

        if (this.checkToS.checked && this.formValuesAreValid) {
            this.fetchPostForm()
        }
        if (!this.checkToS.checked) {
            this.modalToSError()
        }
    }

    fetchPostForm() {

        let submitButton = this.form.querySelector('.video-uploading-form__submit')
        submitButton.setAttribute('disabled', true)
        submitButton.style.cursor = 'not-allowed'

        fetch(`blue/frontend/web/send`, {
            method: 'POST',
            body: JSON.stringify({
                ListInfo: {
                    name: this.inputsValues.name[1],
                    email: this.inputsValues.email[1],
                    phone: this.inputsValues.phone[1],
                    link_uploaded: this.inputsValues.linkUploaded[1],
                    uploaded: this.inputsValues.uploaded[1],
                    description: this.inputsValues.description[1]
                }
            }),
            headers: {
              "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(res => res.json())
            .then(data => {
                this.modalResolveReject(data.success)
                submitButton.removeAttribute('disabled')
                submitButton.style.cursor = ''
            })
            .catch(err => console.log(err))

    }

    modalNoFileError() {
        $(".modalResolveReject").html('<div class="m-overlay" id="bg-close"><h5>Необходимо загрузить видеофайл или указать ссылку!<i id="close-m"></i></h5></div>').show("slow" );
        $( "#close-m" ).click(function() {
          $( ".modalResolveReject" ).hide( "slow" );
        });
        $( "#bg-close" ).click(function( event ) {
          event.preventDefault();
          $(".modalResolveReject").hide();
        });
    }

    modalToSError() {
        $(".modalResolveReject").html('<div class="m-overlay" id="bg-close"><h5>Нужно принять пользовательское соглашение!<i id="close-m"></i></h5></div>').show("slow" );
        $( "#close-m" ).click(function() {
          $( ".modalResolveReject" ).hide( "slow" );
        });
        $( "#bg-close" ).click(function( event ) {
          event.preventDefault();
          $(".modalResolveReject").hide();
        });
    }

    modalResolveReject(data) {
        if (data) {
            $(".modalResolveReject").html('<div class="m-overlay" id="bg-close"><h5>Спасибо!<br/> Видео успешно загружено! <i id="close-m"></i></h5></div>').show("slow" );
            $( "#close-m" ).click(function() {
              $( ".modalResolveReject" ).hide( "slow" );
              location.reload()
            });
            $( "#bg-close" ).click(function( event ) {
              event.preventDefault();
              $(".modalResolveReject").hide();
              location.reload()
            });
        }
    }

    submitForm() {

        this.form.addEventListener('submit', e => {

            e.preventDefault();

            this.inputsValues = this.initInputsNames()

            this.validateForm(
                this.inputsValues.name,
                this.inputsValues.email,
                this.inputsValues.phone,
                this.inputsValues.linkUploaded,
                this.inputsValues.uploaded,
                this.inputsValues.description,
            )

        })
    }
}

export let VideoUploadingForm = new FormConstr(document.querySelector('.video-uploading-form'))
