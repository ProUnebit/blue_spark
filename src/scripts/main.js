// Custom modules
import { VideoUploadingForm } from './constructors/formConstr'
import { ShowVideosSection } from './constructors/showVideosConstr'
// Libraries
import $ from 'jquery'
import dmUploader from 'dm-file-uploader'

function contentReady() {

    if (document.getElementById('form')) {

        VideoUploadingForm.initForm()
        VideoUploadingForm.submitForm()
    }

    if (document.getElementById('videos')) {

        ShowVideosSection.initShowVideosSection()
    }

}

document.addEventListener('DOMContentLoaded', contentReady);


// Adds an entry to our debug area
function ui_add_log(message, color) {
    var d = new Date();
    var dateString = (('0' + d.getHours())).slice(-2) + ':' +
        (('0' + d.getMinutes())).slice(-2) + ':' +
        (('0' + d.getSeconds())).slice(-2);
    color = (typeof color === 'undefined' ? 'muted' : color);
    var template = $('#debug-template').text();
    template = template.replace('%%date%%', dateString);
    template = template.replace('%%message%%', message);
    template = template.replace('%%color%%', color);
    $('#debug').find('li.empty').fadeOut(); // remove the 'no messages yet'
    $('#debug').prepend(template);
}
// Creates a new file and add it to our list
function ui_multi_add_file(id, file) {
    var template = $('#files-template').text();
    template = template.replace('%%filename%%', file.name);
    template = $(template);
    template.prop('id', 'uploaderFile' + id);
    template.data('file-id', id);
    $('#files').find('li.empty').fadeOut(); // remove the 'no files yet'
    $('#files').html(template);
}
// Changes the status messages on our list
function ui_multi_update_file_status(id, status, message) {
    $('#uploaderFile' + id).find('span').html(message).prop('class', 'status text-' + status);
}
// Updates a file progress, depending on the parameters it may animate it or change the color.
function ui_multi_update_file_progress(id, percent, color, active) {
    color = (typeof color === 'undefined' ? false : color);
    active = (typeof active === 'undefined' ? true : active);
    var bar = $('#uploaderFile' + id).find('div.progress-bar');
    bar.width(percent + '%').attr('aria-valuenow', percent);
    bar.toggleClass('progress-bar-striped progress-bar-animated', active);
    if (percent === 0) {
        bar.html('');
    } else {
        bar.html(percent + '%');
    }
    if (color !== false) {
        bar.removeClass('bg-success bg-info bg-warning bg-danger');
        bar.addClass('bg-' + color);
    }
}
$(function() {
    $('#drag-and-drop-zone').dmUploader({ //
        url: 'backend/upload.php',
        extFilter: ["mov", "flv", "mpeg", "mpg", "mkv", "m4v", "mp4", "avi", "wmv", "mpegps", "3gp", "ogv", "ogg"],
        maxFileSize: 1000000000, // 3 Megs'
        multiple: false,
        onDragEnter: function() {
            // Happens when dragging something over the DnD area
            document.querySelector('#drag-and-drop-zone').style.border = '5px solid purple'
            this.addClass('active');
        },
        onDragLeave: function() {
            // Happens when dragging something OUT of the DnD area
            document.querySelector('#drag-and-drop-zone').style.border = '5px dashed pink'
            this.removeClass('active');
            document.querySelector('.video-uploading-form__submit').setAttribute('disabled', true)
        },
        onInit: function() {
            // Plugin is ready to use
            ui_add_log('Penguin initialized :)', 'info');
        },
        onComplete: function() {
            // All files in the queue are processed (success or error)
            // ui_add_log('All pending tranfers finished');
        },
        onNewFile: function(id, file) {
            // When a new file is added using the file selector or the DnD area
            ui_add_log('New file added #' + id);
            ui_multi_add_file(id, file);
        },
        onBeforeUpload: function(id) {
            // about tho start uploading a file
            ui_add_log('Starting the upload of #' + id);
            ui_multi_update_file_status(id, 'uploading', 'Uploading...');
            ui_multi_update_file_progress(id, 0, '', true);
        },
        onUploadCanceled: function(id) {
            // Happens when a file is directly canceled by the user.
            ui_multi_update_file_status(id, 'warning', 'Canceled by User');
            ui_multi_update_file_progress(id, 0, 'warning', false);
        },
        onUploadProgress: function(id, percent) {
            // Updating file progress
            ui_multi_update_file_progress(id, percent);
        },
        onUploadSuccess: function(id, data) {
            document.querySelector('.video-uploading-form__input-box--linkUploaded').style.visibility = 'hidden'
            // A file was successfully uploaded
            document.querySelector('.video-uploading-form__submit').removeAttribute('disabled')
            let inputFileUploade = document.querySelector('.inputFileAnchor')
            inputFileUploade.value = data.file_name

            ui_add_log('Server Response for file #' + id + ': ' + JSON.stringify(data));
            ui_add_log('Upload of file #' + id + ' COMPLETED', 'success');
            ui_multi_update_file_status(id, 'success', 'Файл загружен успешно');
            ui_multi_update_file_progress(id, 100, 'success', false);
        },
        onUploadError: function(id, xhr, status, message) {
            document.querySelector('.video-uploading-form__input-box--linkUploaded').style.visibility = 'visible'
            ui_multi_update_file_status(id, 'danger', message);
            ui_multi_update_file_progress(id, 0, 'danger', false);
        },
        onFallbackMode: function() {
            // When the browser doesn't support this plugin :(
            ui_add_log('Plugin cant be used here, running Fallback callback', 'danger');
        },
        onFileSizeError: function(file) {
            document.querySelector('.video-uploading-form__input-box--linkUploaded').style.visibility = 'visible'
            // ui_add_log('Файл \'' + file.name + '\' не может быть добавлен. Слишком большой размер', 'danger');
            modalSizeError(file.name)
        },
        onFileExtError: function (file) {
            modalExtError(file.name)
        }
    });

    function modalExtError(file) {
        $(".modalResolveReject").html(`<div class="m-overlay" id="bg-close"><h5>Загрузка файла остановлена.<br/> Ваш файл ${file} не удовлетворяет требование по видео форматам:<br/> mov, flv, mpeg, mpg, mkv, m4v, mp4, avi, wmv, mpegps, 3gp, ogv, ogg<i id="close-m"></i></h5></div>`).show("slow" );
        $( "#close-m" ).click(function() {
          $( ".modalResolveReject" ).hide( "slow" );
        });
        $( "#bg-close" ).click(function( event ) {
          event.preventDefault();
          $(".modalResolveReject").hide();
        });
    }
    function modalSizeError(file) {
        $(".modalResolveReject").html(`<div class="m-overlay" id="bg-close"><h5>Загрузка файла остановлена.<br/> Ваш файл ${file} превышает допустимую величину в 1ГБ<i id="close-m"></i></h5></div>`).show("slow" );
        $( "#close-m" ).click(function() {
          $( ".modalResolveReject" ).hide( "slow" );
        });
        $( "#bg-close" ).click(function( event ) {
          event.preventDefault();
          $(".modalResolveReject").hide();
        });
    }
});
