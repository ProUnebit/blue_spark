var $ = jQuery.noConflict();
var siteUrl = window.location.href,
    siteHash = window.location.hash,
    siteWidth = window.innerWidth;
$(document).ready(function() {
    //detect viewport
    var viewport = $(window),
        setVisible = function(e) {
            var viewportTop = viewport.scrollTop(),
                viewportBottom = viewport.scrollTop() + viewport.height();
            $('.go').each(function() {
                var self = $(this),
                    top = self.offset().top,
                    bottom = top + self.height(),
                    topOnScreen = top >= viewportTop && top <= viewportBottom,
                    bottomOnScreen = bottom >= viewportTop && bottom <= viewportBottom,
                    elemVisible = topOnScreen || bottomOnScreen;
                self.toggleClass('live', elemVisible);
                console.log(JSON.stringify({
                    ".go": self[0].classList[0],
                    "viewportTop": viewportTop,
                    "top": top,
                    "topOnScreen": topOnScreen,
                    "viewportBottom": viewportBottom,
                    "bottom": bottom,
                    "bottomOnScreen": bottomOnScreen,
                    "elemVisible": elemVisible
                }));
            });
        };
    viewport.scroll(setVisible);
    setVisible();
    $('a.scroll').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - $('#topnav').outerHeight()
                }, 1000);
                return false;
            }
        }
    });
    $('anims').append('<i class="circle greenCircle"></i><i class="circle purpleCircle"></i><i class="circle blueCircle"></i>');
    //scrollr
    // initialize skrollr if the window width is large enough
    if ($(window).width() > 980) {
        skrollr.init();
    }
    // disable skrollr if the window is resized below 980px wide
    $(window).on('resize', function() {
        if ($(window).width() <= 980) {
            skrollr.init().destroy(); // skrollr.init() returns the singleton created above
        }
    });
    function preload(arrayOfImages) {
        $(arrayOfImages).each(function() {
            $('<img/>')[0].src = this;
        });
    }
    preload(['/vote.svg']);
    //mfp
    $.ajax({
            url: 'https://crossx.m24.ru/guide/frontend/web/site/index',
            dataType: 'json',
            cache: false,
            xhrFields: {
                withCredentials: true
            }
        })
        .done(function(data) {
            console.log('Данные получены');
            var voters = [];
            var guides = [];
            var $voters = $('#ptForm .cards');
            var $guides = $('#participants .cards');
            var $desc = $('.programm-desc');
            $.each(data.voters, function(index, value) {
                var input = '';
                var votes_show = '<b class="votes-show">' + value.votes + '</b>';
                if (value.status == true) {
                    input = '<input type="checkbox" name="name[]" value="" disabled checked />';
                } else {
                    input = '<input data-id=' + value.id + ' type="checkbox" name="name[]" value="" />';
                }
                voters.push('<div class="col-md-4"><div class="entry"><a class="ptVid" href="#guide-' + value.id + '" data-title="' + value.fio + '" data-short-desc="' + value.preview + '" data-full-desc="' + value.description + '" data-video-url="' + value.video + '"><img alt="" src="' + value.img + '" /><span class="ptInfo"><span class="ptName">' + value.fio + '</span><span class="ptDesc">' + value.preview + '</span></span></a>' + input + votes_show + '</div></div>');
            });
            if (!data.guides.length) {
                $('#participants').hide();
            }
            $.each(data.guides, function(index, value) {
                guides.push('<div class="col-md-4" id="contestant-' + value.id + '"><div class="entry"><a class="ptVid" href="#guide-' + value.id + '" data-title="' + value.fio + '" data-short-desc="' + value.preview + '" data-full-desc="' + value.description + '" data-video-url="' + value.video + '"><img alt="" src="' + value.img + '" /><span class="ptInfo"><span class="ptName">' + value.fio + '</span><span class="ptDesc">' + value.preview + '</span></span></a><span class="votesCount">' + value.votes + '</span></div></div>');
            });
            voters.join('');
            guides.join('');
            $voters.append(voters);
            $guides.append(guides);
            $desc.text(data.release);
            //formStyler
            $('input, select').styler();
            $('.ptVid').each(function() {
                var video = '';
                if ($(this).data('video-url').indexOf('m3u8') > -1) {
                    video = '<video class="video" autoplay width="980" height="525"><source src="' + $(this).data('video-url') + '" /></video>';
                } else {
                    video = '<iframe src="' + $(this).data('video-url') + '?autoplay=true" width="980" height="525" />';
                }
                $(this).magnificPopup({
                    items: {
                        src: '<div class="popup"><h2>' + $(this).data('title') + '</h2><p>' + $(this).data('short-desc') + '</p>' + video + '<p>' + $(this).data('full-desc') + '</p><div id="share"></div></div>',
                        type: 'inline'
                    },
                    callbacks: {
                        open: function() {
                            var magnificPopup = $.magnificPopup.instance;
                            var $video = magnificPopup.contentContainer.find('.video');
                            if ($video.length) {
                                $video.mediaelementplayer();
                            }
                            var share = Ya.share2('share', {
                                content: {
                                    url: 'https://gid.m24.ru/' + magnificPopup.st.el.attr('href')
                                },
                                theme: {
                                    services: 'vkontakte,facebook,gplus,twitter,odnoklassniki,whatsapp,telegram'
                                }
                            });
                        }
                    }
                });
            })
            $('body').on('click', '#ptForm .jq-checkbox', function(e) {
                var $input = $(this).find('input');
                var id = $input.data('id');
                if (!$input.prop('disabled')) {
                    $.ajax({
                            type: 'GET',
                            url: 'https://crossx.m24.ru/guide/frontend/web/set-guide/' + id,
                            cache: false,
                            xhrFields: {
                                withCredentials: true
                            }
                        })
                        .done(function(data) {
                            if (data.success == true) {
                                $input.parent().parent().find('.votes-show').text(data.votes);
                            }
                        })
                        .fail(function() {
                            console.log('Ошибка получения данных');
                        });
                }
                $input.prop('disabled', true).trigger('refresh');
            });
            if (siteUrl.indexOf('#guide') != -1) {
                console.log('popup');
                $('[href*="' + siteHash + '"]').trigger('click');
            }
        })
        .fail(function() {
            console.log('Ошибка получения данных');
        });
    $('.getVid').each(function() {
        var shareUrl = $(this).data('share-url');
        var shareTitle = $(this).data('share-title');
        $(this).magnificPopup({
            type: 'inline',
            callbacks: {
                beforeOpen: function() {
                    var magnificPopup = $.magnificPopup.instance,
                        shareBlock = '<div class="callToAction"><a href="/story/">Рассказать историю</a></div><div class="share-b"><div class="share" data-url="' + shareUrl + '" data-title="Истории Героев | ' + shareTitle + '"><div class="pdlc">Поделиться:&nbsp;</div><div class="odnoklassniki"></div><div class="vkontakte"></div><div class="facebook"></div><div class="twitter"></div><div class="plusone"></div></div></div>';
                    magnificPopup.contentContainer.append(shareBlock);
                },
                elementParse: function(item) {
                    // Function will fire for each target element
                    // "item.el" is a target DOM element (if present)
                    // "item.src" is a source that you may modify
                    window.location.hash = item.src;
                },
                open: function() {
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.contentContainer.find('.share').socialLikes({
                        counters: false
                    });
                    video = magnificPopup.content.find('video');
                    setTimeout(function() {
                        video[0].play();
                    }, 1000);
                    $.magnificPopup.instance.close = function() {
                        video[0].pause();
                        // "proto" variable holds MagnificPopup class prototype
                        // The above change that we did to instance is not applied to the prototype,
                        // which allows us to call parent method:
                        $.magnificPopup.proto.close.call(this);
                    };
                }
            }
        });
    });
    //ready
});
